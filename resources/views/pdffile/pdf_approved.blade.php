<?php
Fpdf::AddPage();


Fpdf::SetXY(35,25);
Fpdf::SetFont('Arial','B',12);
Fpdf::cell(138,8,"TOOLS & CONSUMABLES CHECK IN REQUEST",0,"","C");

Fpdf::SetFont('Arial','',8);
Fpdf::SetTitle('Tools & Consumables Check In Request');
Fpdf::SetMargins('10','10');
//Fpdf::Image('imagepath',H,V,SIZE);
Fpdf::Image('img/APO_PLAIN.png',93,8,20);



Fpdf::SetXY(10,35);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(0,8,"Request No:",0,"","L");

Fpdf::SetXY(45,35);
Fpdf::SetFont('Arial','',10);
Fpdf::Cell(0,8, $head->RequestNo,0,"","L");

Fpdf::SetXY(10,40);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(0,8,"Business Partner:",0,"","L");

Fpdf::SetXY(45,40);
Fpdf::SetFont('Arial','',10);
Fpdf::Cell(0,8, $head->BusinessPartnerName,0,"","L");
//Fpdf::Cell(100,1, $head->iheadCreatedby_to_user->FirstName . " " . $head->iheadCreatedby_to_user->LastName,0,"","L");

Fpdf::SetXY(10,45);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(0,8,"Name of Contact:",0,"","L");

Fpdf::SetXY(45,45);
Fpdf::SetFont('Arial','',10);
Fpdf::Cell(0,8, $head->Contact,0,"","L");

Fpdf::SetXY(10,50);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(0,8,"Contact Email:",0,"","L");

Fpdf::SetXY(45,50);
Fpdf::SetFont('Arial','',10);
if(!isset($head->ContactEmail) || $head->ContactEmail=="")
    Fpdf::Cell(0,8, "N/A",0,"","L");
else
    Fpdf::Cell(0,8, $head->ContactEmail,0,"","L");



Fpdf::SetXY(10,55);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(0,8,"Division:",0,"","L");

Fpdf::SetXY(45,55);
Fpdf::SetFont('Arial','',10);
Fpdf::Cell(0,8, $dept,0,"",'L');

Fpdf::SetXY(10,60);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(0,8,"Request Date:",0,"","L");

Fpdf::SetXY(45,60);
Fpdf::SetFont('Arial','',10);
Fpdf::Cell(0,8, date('F d, Y',strtotime($head->Date)),0,"",'L');

Fpdf::SetXY(10,70);
Fpdf::Cell("","", '',1,"",'L');

Fpdf::SetXY(10,70);
Fpdf::Cell("","", '',1,"",'L');

//Fpdf::Line(X, Y, lapad, Haba);
//Fpdf::Line(10, 50, 10, 280);
//Fpdf::Line(200, 50, 200, 280);
//Fpdf::Line(10, 280, 200, 280);

Fpdf::SetXY(10,70);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(7,10,"#",1,"","C");

Fpdf::SetXY(17,70);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(65,10,"Description",1,"","C");

Fpdf::SetXY(82,70);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(25,10,"Control No.",1,"","C");

Fpdf::SetXY(107,70);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(20,10,"Quantity",1,"","C");

Fpdf::SetXY(127,70);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(21,10,"UOM",1,"","C");

Fpdf::SetXY(148,70);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(25,10,"Item Type",1,"","C");

Fpdf::SetXY(173,70);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(27,10,"Remarks",1,"","C");

$y = 80;
$ctr = 1;

foreach($line as $l)
{
    Fpdf::SetXY(10,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(7,15,$ctr,1,"","C");

    Fpdf::SetXY(17,$y);
    Fpdf::SetFont('Arial','',8);

    //MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
    if(strlen($l->Description) <= 55)
        Fpdf::cell(65,15,$l->Description,1,"","L");
    elseif(strlen($l->Description) > 40 && strlen($l->Description) <= 80)
        Fpdf::MultiCell(65,5, $l->Description."                    ","BT","L");
    else
        Fpdf::MultiCell(65,5, $l->Description,"BT","L");
    // Fpdf::cell(65,10,$l->Description,1,"","C");

    Fpdf::SetXY(82,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(25,15,$l->ControlNum,1,"","C");

    Fpdf::SetXY(107,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(20,15,$l->Quantity,1,"","C");



    Fpdf::SetXY(127,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(21,15,$l->oUOM->Name,1,"","C");

    if($l->Returnable == 1 && $l->ItemType==0)
    {
        $return = "Tools";
    }
    elseif($l->Returnable == 0 && $l->ItemType==0)
    {
        $return = "Consumables";
    }
    elseif($l->Returnable == 1 && $l->ItemType==1)
    {
        $return = "Equipment";
    }

    Fpdf::SetXY(148,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(25,15,$return,1,"","C");

    Fpdf::SetXY(173,$y);
    Fpdf::SetFont('Arial','',10);
    Fpdf::cell(27,15,"",1,"","C");

    $ctr = $ctr + 1;
    $y = $y + 15;
    if($ctr == 13)
    {
        Fpdf::AddPage();
        $ctr = 1;
        $y = 20;
    }
}


//Footer

if($head->ApprovedBy != "")
    {
        Fpdf::SetXY(15,$y+13);
        Fpdf::SetFont('Arial','',10);
        Fpdf::cell(40,8,$head->iheadCreatedby_to_user->FirstName . " " . $head->iheadCreatedby_to_user->LastName,"B","","C");
    }
else
    {
        Fpdf::SetXY(10,$y+5);
        Fpdf::SetFont('Arial','',10);
        Fpdf::cell(40,30,"______________",0,"","C");
    }
Fpdf::SetXY(15,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"Requestor",0,"","C");


if($head->ApprovedBy != "")
{
    Fpdf::SetXY(85,$y+13);
    Fpdf::SetFont('Arial','',10);
    Fpdf::cell(40,8,$head->iheadApprovedBy_to_user->FirstName . " " . $head->iheadApprovedBy_to_user->LastName,"B","","C");
}
else
{
    Fpdf::SetXY(85,$y+5);
    Fpdf::SetFont('Arial','',10);
    Fpdf::cell(40,30,"______________",0,"","C");
}

Fpdf::SetXY(85,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"Approver",0,"","C");

/*Fpdf::SetXY(110,$y+5);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"______________",0,"","C");

Fpdf::SetXY(110,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"Prepared By",0,"","C");*/

Fpdf::SetXY(155,$y+13);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,8, "","B","","C");

Fpdf::SetXY(155,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30, "Verified By",0,"","C");


Fpdf::SetXY(10,$y);
Fpdf::SetFont('Arial','B',15);
Fpdf::cell(190,30,"",1,"","C");

//Saving as Temporary File for attaching to Email.
/*$filename="test.pdf";
$dir = "../storage/";
Fpdf::Output($dir.$filename,'F');*/

Fpdf::Output();
exit;
?>