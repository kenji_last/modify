<?php
Fpdf::AddPage();


Fpdf::SetXY(35,35);
Fpdf::SetFont('Arial','B',13);
Fpdf::cell(138,8,"PROPERTY GATE PASS",0,"","C");

Fpdf::SetFont('Arial','',8);
Fpdf::SetTitle('Gate Pass');
Fpdf::SetMargins('10','20');
//Fpdf::Image('imagepath',H,V,SIZE);
Fpdf::Image('img/APO_PLAIN.png',93,20,20);

//Fpdf::Line(X, Y, lapad, Haba);
//Fpdf::Line(10, 15, 200, 15);
//Fpdf::Line(10, 15, 10, 270);
//Fpdf::Line(200, 15, 200, 270);
//Fpdf::Line(10, 270, 200, 270);

Fpdf::SetXY(10,50);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(125,8,"Control No.:",0,"","R");

Fpdf::SetXY(10,50);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(165,8,"___________________",0,"","R");

Fpdf::SetXY(10,60);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(125,8,"Date:",0,"","R");

Fpdf::SetXY(10,60);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(165,8,"___________________",0,"","R");

Fpdf::SetXY(15,70);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(165,8,"THIS IS TO AUTHORIZE",0,"","L");

Fpdf::SetXY(15,80);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(165,8,"Name",0,"","L");

Fpdf::SetXY(45,80);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(165,8,"___________________________________________",0,"","L");

Fpdf::SetXY(15,88);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(165,8,"Company/Division",0,"","L");

Fpdf::SetXY(45,88);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(165,8,"___________________________________________",0,"","L");

Fpdf::SetXY(15,100);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(165,8,"to bring out of the company premises the following articles:",0,"","L");

Fpdf::SetXY(15,110);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(30,8,"QTY / UNIT",1,"","C");

Fpdf::SetXY(15,118);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(30,8,"1",1,"","C");

Fpdf::SetXY(45,110);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(65,8,"ITEM DESCRIPTION",1,"","C");

Fpdf::SetXY(45,118);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(65,8,"Ruler",1,"","C");

Fpdf::SetXY(110,110);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(45,8,"Purpose",1,"","C");

Fpdf::SetXY(110,118);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(45,8,"Used for technical drawing",1,"","C");

Fpdf::SetXY(155,110);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(20,8,"Returnable",1,"","C");

Fpdf::SetXY(155,118);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(20,8,"YES",1,"","C");

Fpdf::SetXY(175,110);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(20,8,"When",1,"","C");

Fpdf::SetXY(175,118);
Fpdf::SetFont('Arial','',8);
Fpdf::cell(20,8,"10-10-17",1,"","C");

Fpdf::SetXY(40,135);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"____________________",0,"","L");

Fpdf::SetXY(50,140);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"REQUESTOR",0,"","L");

Fpdf::SetXY(130,135);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"____________________",0,"","L");

Fpdf::SetXY(143,140);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"CARRIER",0,"","L");

Fpdf::SetXY(40,150);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"____________________",0,"","L");

Fpdf::SetXY(51,155);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"APPROVAL",0,"","L");

Fpdf::SetXY(130,150);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"____________________",0,"","L");

Fpdf::SetXY(135,155);
Fpdf::SetFont('Arial','',10);
Fpdf::cell(20,8,"GUARD ON DUTY",0,"","L");

Fpdf::SetXY(15,175);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"AU-FRM-F001 R0",0,"","L");

Fpdf::SetXY(50,175);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Copy 1 - Accounting",0,"","L");

Fpdf::SetXY(80,175);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Copy 2 - Requesting Party",0,"","L");

Fpdf::SetXY(120,175);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Copy 3 - Security Office",0,"","L");
Fpdf::Output();
exit;
?>