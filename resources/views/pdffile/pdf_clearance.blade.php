<?php
Fpdf::AddPage();
Fpdf::SetXY(40,10);
Fpdf::SetFont('Arial','B',15);
Fpdf::cell(138,8,"TOOLS & CONSUMABLES CHECK IN REQUEST",0,"","C");

Fpdf::SetFont('Arial','',8);
Fpdf::SetTitle('Tools & Consumables Check In Request');
Fpdf::SetMargins('10','10');
//Fpdf::Image('imagepath',H,V,SIZE);
Fpdf::Image('img/APO_LOGO.png',15,20,55);



Fpdf::SetXY(40,23);
Fpdf::SetFont('Arial','B',15);
Fpdf::cell(100,8,"Request No:",0,"","R");

Fpdf::SetXY(140,27);
Fpdf::SetFont('Arial','',13);
Fpdf::Cell(100,0, $head->RequestNo,0,"","L");

Fpdf::SetXY(40,30);
Fpdf::SetFont('Arial','B',15);
Fpdf::cell(100,8,"Name of Contractor:",0,"","R");

Fpdf::SetXY(140,34);
Fpdf::SetFont('Arial','',13);
Fpdf::Cell(100,1, $head->iheadCreatedby_to_user->FirstName . " " . $head->iheadCreatedby_to_user->LastName,0,"","L");

Fpdf::SetXY(40,37);
Fpdf::SetFont('Arial','B',15);
Fpdf::cell(100,8,"Division:",0,"","R");

Fpdf::SetXY(140,41);
Fpdf::SetFont('Arial','',13);
Fpdf::Cell("","", 'IT',0,"",'L');

Fpdf::SetXY(40,44);
Fpdf::SetFont('Arial','B',15);
Fpdf::cell(100,8,"Request Date:",0,"","R");

Fpdf::SetXY(140,47);
Fpdf::SetFont('Arial','',13);
Fpdf::Cell("","", 'Oct 06, 2017',0,"",'L');

Fpdf::SetXY(10,70);
Fpdf::Cell("","", '',1,"",'L');

Fpdf::SetXY(10,70);
Fpdf::Cell("","", '',1,"",'L');

//Fpdf::Line(X, Y, lapad, Haba);
//Fpdf::Line(10, 50, 10, 280);
//Fpdf::Line(200, 50, 200, 280);
//Fpdf::Line(10, 280, 200, 280);

Fpdf::SetXY(10,70);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(7,10,"#",1,"","C");

Fpdf::SetXY(17,70);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(65,10,"Description",1,"","C");

Fpdf::SetXY(82,70);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(25,10,"Control No.",1,"","C");

Fpdf::SetXY(107,70);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(20,10,"Quantity",1,"","C");

Fpdf::SetXY(127,70);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(21,10,"UOM",1,"","C");

Fpdf::SetXY(148,70);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(25,10,"Returnable",1,"","C");

Fpdf::SetXY(173,70);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(27,10,"Remarks",1,"","C");

$y = 80;
$ctr = 1;

foreach($line as $l)
{
    Fpdf::SetXY(10,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(7,10,$ctr,1,"","C");

    Fpdf::SetXY(17,$y);
    Fpdf::SetFont('Arial','',8);
    if(strlen($l->Description) <= 40)
        {
            Fpdf::cell(65,10,$l->Description,1,"","C");
        }
    else
        {
            //MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
            Fpdf::MultiCell(65, 3, $l->Description,"C");
        }

    // Fpdf::cell(65,10,$l->Description,1,"","C");

    Fpdf::SetXY(82,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(25,10,$l->ControlNum,1,"","C");

    Fpdf::SetXY(107,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(20,10,$l->Quantity,1,"","C");



    Fpdf::SetXY(127,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(21,10,$l->oUOM->Name,1,"","C");

    if($l->Returnable == 1)
    {
        $return = "YES";
    }
    else
    {
        $return = "NO";
    }

    Fpdf::SetXY(148,$y);
    Fpdf::SetFont('Arial','',8);
    Fpdf::cell(25,10,$return,1,"","C");

    Fpdf::SetXY(173,$y);
    Fpdf::SetFont('Arial','',10);
    Fpdf::cell(27,10,"",1,"","C");

    $ctr = $ctr + 1;
    $y = $y + 10;
    if($ctr == 19)
        {
            Fpdf::AddPage();
            $ctr = 1;
            $y = 20;
        }
}


//Footer

if($head->ApprovedBy != "")
    {
        Fpdf::SetXY(10,$y+5);
        Fpdf::SetFont('Arial','',10);
        Fpdf::cell(40,30,$head->iheadApprovedBy_to_user->FirstName . " " . $head->iheadApprovedBy_to_user->LastName,0,"","C");
    }
else
    {
        Fpdf::SetXY(10,$y+5);
        Fpdf::SetFont('Arial','',10);
        Fpdf::cell(40,30,"______________",0,"","C");
    }
Fpdf::SetXY(10,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"Requestor",0,"","C");

Fpdf::SetXY(57,$y+5);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"______________",0,"","C");

Fpdf::SetXY(57,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"Approver",0,"","C");

Fpdf::SetXY(110,$y+5);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"______________",0,"","C");

Fpdf::SetXY(110,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30,"Prepared By",0,"","C");

Fpdf::SetXY(155,$y+5);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30, "______________",0,"","C");

Fpdf::SetXY(155,$y+10);
Fpdf::SetFont('Arial','B',10);
Fpdf::cell(40,30, "Verified By",0,"","C");


Fpdf::SetXY(10,$y);
Fpdf::SetFont('Arial','B',15);
Fpdf::cell(190,30,"",1,"","C");

Fpdf::Output();
exit;
?>