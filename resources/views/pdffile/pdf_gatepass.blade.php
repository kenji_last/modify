<?php
Fpdf::AddPage();
Fpdf::SetAutoPageBreak(false);

Fpdf::SetXY(35,35);
Fpdf::SetFont('Arial','B',13);
Fpdf::cell(138,8,"PROPERTY GATE PASS",0,"","C");

Fpdf::SetFont('Arial','',8);
Fpdf::SetTitle('Gate Pass');
Fpdf::SetMargins('10','20');
//Fpdf::Image('imagepath',H,V,SIZE);
Fpdf::Image('img/APO_PLAIN.png',93,20,20);

//Fpdf::Line(X, Y, lapad, Haba);
//Fpdf::Line(10, 15, 200, 15);
//Fpdf::Line(10, 15, 10, 270);
//Fpdf::Line(200, 15, 200, 270);
//Fpdf::Line(10, 270, 200, 270);

Fpdf::SetXY(15,50);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(125,8,"Request No:",0,"","L");

Fpdf::SetXY(40,50);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,$ohead->RequestNo,0,"","L");

Fpdf::SetXY(15,55);
Fpdf::SetFont('Arial','B',9);
Fpdf::cell(125,8,"Request Date:",0,"","L");

Fpdf::SetXY(40,55);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,date('F d, Y',strtotime($ohead->Date)),0,"","L");

Fpdf::SetXY(15,65);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,"This is to authorize",0,"","L");

Fpdf::SetXY(15,70);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,"Name",0,"","L");

Fpdf::SetXY(50,70);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,$ohead->Contact,0,"","L");

Fpdf::SetXY(15,75);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,"Company/Division",0,"","L");

Fpdf::SetXY(50,75);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,$ohead->BusinessPartnerName,0,"","L");

Fpdf::SetXY(15,80);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,"Reason",0,"","L");

Fpdf::SetXY(50,80);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,$ohead->Reason,0,"","L");

Fpdf::SetXY(15,90);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(165,8,"to bring out of the company premises the following articles:",0,"","L");

Fpdf::SetXY(15,100);
Fpdf::SetFont('Arial','',7);
Fpdf::cell(10,8,"#",1,"","C");
Fpdf::SetXY(25,100);
Fpdf::SetFont('Arial','',7);
Fpdf::cell(18,8,"Qty./Unit",1,"","C");
Fpdf::SetXY(43,100);
Fpdf::SetFont('Arial','',7);
Fpdf::cell(67,8,"Item Description",1,"","C");
Fpdf::SetXY(110,100);
Fpdf::SetFont('Arial','',7);
Fpdf::cell(45,8,"Purpose",1,"","C");
Fpdf::SetXY(155,100);
Fpdf::SetFont('Arial','',7);
Fpdf::cell(20,8,"Returnable",1,"","C");
Fpdf::SetXY(175,100);
Fpdf::SetFont('Arial','',7);
Fpdf::cell(20,8,"Return Date",1,"","C");

$y = 108;
$ctr = 1;

foreach($oline as $l)
{
    Fpdf::SetXY(15,$y);
    Fpdf::SetFont('Arial','',7);
    Fpdf::cell(10,8,$l->LineNum,1,"","C");
    Fpdf::SetXY(15,$y);
    Fpdf::SetFont('Arial','',7);
    Fpdf::cell(28,8,$l->Quantity ." ". $l->oUom->Name,1,"","C");
    Fpdf::SetXY(43,$y);
    Fpdf::SetFont('Arial','',7);

    //MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
    if(strlen($l->Description) <= 40)
        Fpdf::cell(67,8,$l->Description,1,"","L");
    elseif(strlen($l->Description) > 40 && strlen($l->Description) <= 80)
        Fpdf::MultiCell(67,4, $l->Description."                    ","BT","L");
    else
        Fpdf::MultiCell(67,4, $l->Description,"BT","L");

    //Fpdf::cell(67,8,$l->Description,1,"","C");

    Fpdf::SetXY(110,$y);
    Fpdf::SetFont('Arial','',7);

    if(strlen($l->Purpose) <= 40)
        Fpdf::cell(45,8,$l->Purpose,1,"","L");
    elseif(strlen($l->Purpose) > 40 && strlen($l->Purpose) <= 50)
        Fpdf::MultiCell(45,4, $l->Purpose."                    ","LBT","L");
    /*else
        Fpdf::MultiCell(67,4, $l->Description,"BT","L");*/
    //Fpdf::cell(45,8,$l->Purpose,1,"","C");
    Fpdf::SetXY(155,$y);
    Fpdf::SetFont('Arial','',7);

    if($l->Returnable == 1)
        $return = "Yes";
    else
        $return = "No";

    Fpdf::cell(20,8,$return,1,"","C");
    Fpdf::SetXY(175,$y);
    Fpdf::SetFont('Arial','',7);
    Fpdf::cell(20,8,date('m/d/Y',strtotime($l->ExpReturn)),1,"","C");

    $ctr = $ctr + 1;
    $y = $y + 8;

    if($ctr == 19)
    {
        Fpdf::SetXY(10,280);
        Fpdf::SetFont('Arial','',9);
        Fpdf::cell(20,8,"AU-FRM-F001 R0",0,"","L");

        Fpdf::SetXY(50,280);
        Fpdf::SetFont('Arial','',9);
        Fpdf::cell(20,8,"Copy 1 - Accounting",0,"","L");

        Fpdf::SetXY(90,280);
        Fpdf::SetFont('Arial','',9);
        Fpdf::cell(20,8,"Copy 2 - Requesting Party",0,"","L");

        Fpdf::SetXY(139,280);
        Fpdf::SetFont('Arial','',9);
        Fpdf::cell(20,8,"Copy 3 - Security Office",0,"","L");
        Fpdf::AddPage();
        $y = 20;
    }
}

$y = $y + 16;

Fpdf::SetXY(18,$y);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(25,8,$ohead->oUser->FirstName." ".$ohead->oUser->LastName,0,"","C");

Fpdf::SetXY(18,$y+8);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(25,8,"Requestor",0,"","C");

Fpdf::SetXY(69,$y);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,$ohead->Transporter,0,"","C");

Fpdf::SetXY(69,$y+8);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Transporter",0,"","C");

Fpdf::SetXY(115,$y);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,$ohead->oheadCOApprovedBy_to_user->FirstName." ".$ohead->oheadCOApprovedBy_to_user->LastName,0,"","C");

Fpdf::SetXY(115,$y+8);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Approver",0,"","C");

Fpdf::SetXY(161,$y);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"_____________________",0,"","C");

Fpdf::SetXY(161,$y+8);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Security Personnel",0,"","C");

Fpdf::SetXY(10,280);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"AU-FRM-F001 R0",0,"","L");

Fpdf::SetXY(50,280);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Copy 1 - Accounting",0,"","L");

Fpdf::SetXY(90,280);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Copy 2 - Requesting Party",0,"","L");

Fpdf::SetXY(139,280);
Fpdf::SetFont('Arial','',9);
Fpdf::cell(20,8,"Copy 3 - Security Office",0,"","L");

Fpdf::Output();
exit;
?>