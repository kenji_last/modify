<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vendor Asset Management | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../vendor/fortawesome/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{{ asset('../vendor/driftyco/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--Vue js-->
    <script src="{{ asset('/js/vendor.js') }}"></script>
    <!--End Vuejs-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!-- <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <style>
        labels{
            font-family: 'Arial','Helvetica Neue',Helvetica,Arial,sans-serif !important;
            font-size: 18pt;
        }
        .logo{
            font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
            letter-spacing: -1px;
        }
    </style>
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;margin:10% 25% 0 25%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="fa fa-square"></i>&nbsp;&nbsp;Reject request - {{$title}}
                        </h4>
                    </div>
                        <div class="panel-body">
                            <div style="padding:0 15% 0 15%;text-align:center;">
                                <h5>
                                    <div style="text-align:left !important;">
                                    <form method="post" action="{{url('emailReject')}}">
                                        <input type="hidden" name="requestID" value={{$iHeaderUpdate->id}}>
                                        <input type="hidden" name="approverID" value="{{$approver}}">
                                        <label>Please specify the reason for rejecting this request.</label>
                                        <textarea style="resize: none;" maxlength="100" onkeyup="rejectReasonChecker();" id="RejectReason" name="RejectReason" class="form-control" rows="4"></textarea>
                                        <br>
                                        <input type="submit" disabled id="submit" class="btn btn-sm btn-default" value="Reject Request">
                                        &nbsp;<a href="javascript:window.open('','_self').close();" class="btn btn-sm btn-default">Close this page</a>
                                    </form>
                                    </div>
                                </h5>
                                <br>
                                <br>
                                {{--<a href="{{url('in_requestor')}}" class="btn btn-sm btn-default">Create New</a>
                                <a href="{{ URL::previous() }}" class="btn btn-sm btn-default">Go back</a>--}}
                                <script>
                                    function rejectReasonChecker(){
                                        if(document.getElementById('RejectReason').value!="")
                                            document.getElementById('submit').disabled=false;
                                        else
                                            document.getElementById('submit').disabled=true
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </body>

    @push('scripts')

    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endpush