@extends('layouts.admin')
@section('title', 'Incoming')
@section('guard_oview', 'active')
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4><i class="fa fa-user-secret"></i>&nbsp;Incoming Requests</h4></div>
                        <div class="panel-body">

    <form role="form" method="POST" action="{{ url('guard_in_view') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group col-md-6">
                                <label>Asset Number:</label>
                                {{$singleRequest->RequestNo}}
                            </div>
                            <div class="form-group col-md-6">
                                <label>Date:</label>
                                {{date('F d, Y', strtotime($singleRequest->Date))}}
                            </div>

                            <div class="form-group col-md-6">
                                <label>Business Partner:</label>
                                {{$singleRequest->oBusinessPartner->Name}}
                            </div>
                            <div class="form-group col-md-6">
                                <label>Contact:</label>
                                {{$singleRequest->oContact->FirstName}} {{$singleRequest->oContact->LastName}}
                            </div>

                                <div class="form-group col-md-6">
                                    <label>Status:</label>
                                    {{$singleRequest->oStatus->Name}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Created By:</label>
                                    {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                                </div>

                            <div class="col-xs-12">
                                    <label>Assets</label>
                            </div>
                            <div class="col-xs-12">
                                <input type="hidden" id="tablecount" name="assetCount" value="{{$lineItemsCount}}"/>
                                <input type="hidden" id="realTableCount" name="realAssetCount" value="{{$lineItemsCount}}"/>
                                <button type="button" id="addingAssets" onclick="addAssets('aBody');" class='btn btn-sm btn-success'><i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Assets</button>
                            </div>
                            <div class="form-group col-xs-12">
                                    <div id="aHeaders" class="box-primary" style="align-content: center;">
                                        <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:13px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                            {{--<div class="col-md-1">Line #</div>--}}
                                            <div class="col-xs-2" style="padding:0;">Description</div>
                                            <div class="col-xs-2" style="padding:0;">Serial Number</div>
                                            <div class="col-xs-1" style="padding:0;">Line Status</div>
                                            <div class="col-xs-1" style="padding:0;">UOM</div>
                                            <div class="col-xs-1" style="padding:0;">In Date</div>
                                            <div class="col-xs-1" style="padding:0;">Out Date</div>
                                            <div class="col-xs-1" style="padding:0;">Returnable?</div>
                                            <div class="col-xs-1" style="padding:0;"></div>
                                        </div>
                                    </div>
                                    <div id="aBody" class="box-primary" style="align-content: center;height:240px;max-height:240px;overflow-y:scroll;font-size:13px;">
                                    @foreach($lineItems as $li)

                                            <div class="row" style="width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                                <div class="col-xs-2" style='padding:9px 14px;'>{{$li->Description}}</div>
                                                <div class="col-xs-2" style='padding:9px 14px;'>{{$li->SerialNum}}</div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>{{$li->oStatus->Name}}</div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>{{$li->oUOM->Name}}</div>
                                                <div class="col-xs-1" style='padding:9px 0px;'>
                                                    <button type='button' class='btn btn-xs btn-link' onclick='inDateModal(<?php echo $li->LineNum;?>);'><i class='fa fa-pencil'></i></button>
                                                    <span id="vInDate[{{$li->LineNum}}]">
                                                        @if($li->InDate=="")
                                                            N/A
                                                        @else
                                                            {{$li->InDate}}
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="col-xs-1" style='padding:9px 0px;'>
                                                    <button type='button' class='btn btn-xs btn-link' onclick='outDateModal(<?php echo $li->LineNum;?>);'><i class='fa fa-pencil'></i></button>
                                                    <span id="vOutDate[{{$li->LineNum}}]">
                                                        @if($li->OutDate=="")
                                                            N/A
                                                        @else
                                                            {{$li->OutDate}}
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="col-xs-1" style='padding:auto 0; text-align: center;'>
                                                    <label style="margin:13% auto;">
                                                        <input type="checkbox" name="Returnable[{{$li->LineNum}}]" id="Returnable[{{$li->LineNum}}]" checked>
                                                    </label>
                                                </div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>
                                                    AA
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;border-bottom: 1px solid #e9e9e9;">
                                                <input type="text" name="LineID[{{$li->LineNum}}]" id="LineID[{{$li->LineNum}}]" value="{{$li->id}}"/>
                                                <input type="text" name="Description[{{$li->LineNum}}]" id="Description[{{$li->LineNum}}]" value="{{$li->Description}}"/>
                                                <input type="text" name="SerialNum[{{$li->LineNum}}]" id="SerialNum[{{$li->LineNum}}]" value="{{$li->SerialNum}}"/>
                                                <input type="text" name="InDate[{{$li->LineNum}}]" id="InDate[{{$li->LineNum}}]" value="{{$li->InDate}}"/>
                                                <input type="text" name="InRemarks[{{$li->LineNum}}]" id="InRemarks[{{$li->LineNum}}]" value="{{$li->InRemarks}}"/>
                                                <input type="text" name="OutDate[{{$li->LineNum}}]" id="OutDate[{{$li->LineNum}}]" value="{{$li->OutDate}}"/>
                                                <input type="text" name="OutRemarks[{{$li->LineNum}}]" id="OutRemarks[{{$li->LineNum}}]" value="{{$li->OutRemarks}}"/>
                                            </div>


                                    @endforeach
                                    </div>
                                </div>
                            <div class="form-group col-xs-12">
                                <button type="submit" id="sendrequest" class="btn btn-primary">Save Request</button>
                                <button type="button" id="" class="btn btn-primary" onclick="location.reload();">Cancel</button>
                            </div>
    </form>
                        </div>
                    </div>
            </div>
        </div>

        <div id="inDateModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">In Date</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="mLineID">
                        <div class="row" style="padding:0 auto 0 0;margin:0 3% 0 3%;">
                            <label>Date:</label>
                            <div class="input-group col-md-12">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="inDate" onchange="inDateChecker();" type="text" class="form-control pull-right" id="inDateAll" required="required" value="">
                            </div>
                            <label>Remarks:</label>
                            <div class="input-group col-md-12">
                                <textarea id="inDateRemarks" class="form-control pull-right" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="inDateModalOK();" id="inDateModalOK">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <div id="outDateModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Out Date</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="mLineID">
                        <div class="row" style="padding:0 auto 0 0;margin:0 3% 0 3%;">
                            <label>Date:</label>
                            <div class="input-group col-md-12">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="outDate" type="text" class="form-control pull-right" id="outDateAll" required="required" value="">
                            </div>
                            <label>Remarks:</label>
                            <div class="input-group col-md-12">
                                <textarea id="outDateRemarks" class="form-control pull-right" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>



        <table id="lineStatRef" style="display:none;">
        @foreach($linestat as $lst)
            <tr>
            <td>{{$lst->id}}</td>
            <td>{{$lst->Name}}</td>
            </tr>
        @endforeach

        <table id="uomRef" style="display:none;">
        @foreach($uoms as $u)
                    <tr>
                        <td>{{$u->id}}</td>
                        <td>{{$u->Name}}</td>
                    </tr>
        @endforeach

    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#inDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
            $('#outDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });

        function addAssets(divName){
            $('#sendrequest').attr('disabled', 'disabled');
            var theTableCount = document.getElementById('tablecount').value;
            var newdiv = document.createElement('div');

            //Setting Line Status in Drop Down
            /*var ddLineStatus = "<select class='form-control' style='width:100%;' name='LineStatus["+theTableCount+"]' id='eLineStatus["+theTableCount+"]'>";
            ddLineStatus = ddLineStatus + "<option value=\'NULL\' disabled selected>Please Select...</option>";
            var oTable = document.getElementById('lineStatRef');
            var rowLength = oTable.rows.length;
            for (i = 0; i < rowLength; i++){
                var oCells = oTable.rows.item(i).cells;
                ddLineStatus = ddLineStatus + "<option value='"+oCells.item(0).innerHTML+"'>"+oCells.item(1).innerHTML+"</option>";
            }
            ddLineStatus = ddLineStatus + "</select>";*/

            //Setting UOMs in Drop Down
            var ddUOMs = "<select onchange='requestorChecks("+theTableCount+");' class='form-control' style='width:100%;' name='UOM["+theTableCount+"]' id='eUOM["+theTableCount+"]'>";
            ddUOMs = ddUOMs + "<option value=\'NULL\' disabled selected>Please Select...</option>";
            var uomTable = document.getElementById('uomRef');
            var uomRowLength = uomTable.rows.length;
            for (i = 0; i < uomRowLength; i++){
                var uomTableCells = uomTable.rows.item(i).cells;
                ddUOMs = ddUOMs + "<option value='"+uomTableCells.item(0).innerHTML+"'>"+uomTableCells.item(1).innerHTML+"</option>";
            }
            ddUOMs = ddUOMs + "</select>";

            newdiv.innerHTML = "<div class=\"row\" id='divEdit"+theTableCount+"' style=\"width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:4px;\">" +
            //"<div class=\"col-md-1\" style='padding:3px;'><input type='text' class='form-control' style=\"width:100%;\" name='LineNum["+theTableCount+"]' id='eLineNum["+theTableCount+"]'/></div>"+
            "<div class=\"col-md-4\" style='padding:3px;'>" +
            "<input placeholder='Description' onkeyup='requestorChecks("+theTableCount+");' type='text' class='form-control' style=\"width:100%;\" name='Description["+theTableCount+"]' id='eDescription["+theTableCount+"]'/></div>"+
            "<div class=\"col-md-4\" style='padding:3px;'>" +
            "<input placeholder='Serial Number' onkeyup='requestorChecks("+theTableCount+");'type='text' class='form-control' style=\"width:100%;\" name='SerialNum["+theTableCount+"]' id='eSerialNum["+theTableCount+"]'/></div>"+
            //"<div class=\"col-md-2\" style='padding:3px;'>" +
            //ddLineStatus +
            //"</div>"+
            "<div class=\"col-md-2\" style='padding:3px;'>" +
            ddUOMs+
            "</div>"+
                    //DatePicker for InDate and OutDate
            /*"<div class=\"col-md-2\" style='padding:3px;'>"+
            "<div class=\"input-group\" style='width:100%;'>"+
            "<div class=\"input-group-addon\">"+
            "<i class=\"fa fa-calendar\"></i>"+
            "</div>"+
            "<input type=\"text\" class=\"form-control pull-right recurring_dates\" name='InDate["+theTableCount+"]' id='eInDate["+theTableCount+"]'>"+
            "</div></div>"+
            "<div class=\"col-md-2\" style='padding:3px;'>"+
            "<div class=\"input-group\" style='width:100%;'>"+
            "<div class=\"input-group-addon\">"+
            "<i class=\"fa fa-calendar\"></i>"+
            "</div>"+
            "<input type=\"text\" class=\"form-control pull-right recurring_dates\" name='OutDate["+theTableCount+"]' id='eOutDate["+theTableCount+"]'>"+
            "</div></div>"+*/
                //"<div class=\"col-md-1\" style='padding:3px;'><input type='text' class='form-control' style=\"width:100%;\" name='OutDate["+theTableCount+"]' id='eOutDate["+theTableCount+"]'/></div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' class='btn' id='confirmAsset"+theTableCount+"' onclick='confirmAsset("+theTableCount+");' disabled><i class='fa fa-check'></i></button>&nbsp;"+
            "<button type='button' class='btn cancelButton' onclick='cancelAsset("+theTableCount+");'><i class='fa fa-close'></i></button></div>"+
            "</div>"+
            "<div class=\"row\" id='divView"+theTableCount+"' style=\"width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:5px;display:none;border-bottom: 1px solid #e9e9e9;\">" +
            //"<div class=\"col-md-1\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vLineNum["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vDescription["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vSerialNum["+theTableCount+"]'/></span></div>"+
            //"<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vLineStatus["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vUOM["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vInDate["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vOutDate["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' class='btn editButton' onclick='editAsset("+theTableCount+");'><i class='fa fa-edit'></i></button>&nbsp;" +
            "<button type='button' class='btn deleteButton' onclick='removeAsset("+theTableCount+");'><i class='fa fa-close'></i></button></div>"+
            "</div>";
            //var counters = document.getElementById("tablecount").value;

            document.getElementById(divName).appendChild(newdiv);
            document.getElementById('tablecount').value = +theTableCount + 1;
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value + 1;
            document.getElementById('addingAssets').disabled=true;
            document.getElementById("eDescription["+theTableCount+"]").focus();
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');

        }

        function confirmAsset(lineNumber){
            document.getElementById('divEdit'+lineNumber).style.display = "none";
            document.getElementById('divView'+lineNumber).style.display = "block";
            //document.getElementById('vLineNum['+lineNumber+']').innerHTML = document.getElementById('eLineNum['+lineNumber+']').value;
            ////document.getElementById('vLineStatus['+lineNumber+']').innerHTML = document.getElementById('eLineStatus['+lineNumber+']').value;
            document.getElementById('vDescription['+lineNumber+']').innerHTML = document.getElementById('eDescription['+lineNumber+']').value;
            document.getElementById('vSerialNum['+lineNumber+']').innerHTML = document.getElementById('eSerialNum['+lineNumber+']').value;


            //var LineStatusView = document.getElementById("eLineStatus["+lineNumber+"]");
            //var strLineStatus = LineStatusView.options[LineStatusView.selectedIndex].text;
            //document.getElementById('vLineStatus['+lineNumber+']').innerHTML = streLineStatus;

            var UOMView = document.getElementById("eUOM["+lineNumber+"]");
            var strUOM = UOMView.options[UOMView.selectedIndex].text;
            document.getElementById('vUOM['+lineNumber+']').innerHTML = strUOM;

            //document.getElementById('vInDate['+lineNumber+']').innerHTML = document.getElementById('eInDate['+lineNumber+']').value;
            //document.getElementById('vOutDate['+lineNumber+']').innerHTML = document.getElementById('eOutDate['+lineNumber+']').value;

            document.getElementById('addingAssets').disabled=false;
            //headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }
        function editAsset(lineNumber){
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');
            $('#sendrequest').attr('disabled', 'disabled');
            document.getElementById('divView'+lineNumber).style.display = "none";
            document.getElementById('divEdit'+lineNumber).style.display = "block";
            document.getElementById('addingAssets').disabled=true;
        }
        function removeAsset(lineNumber){
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            headerChecker();
        }
        function cancelAsset(lineNumber){
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            document.getElementById('addingAssets').disabled=false;
            headerChecker();
        }
        function requestorChecks(lineNumber){
            var errCount = 0;

            if(document.getElementById("eDescription["+lineNumber+"]").value=="" || document.getElementById("eDescription["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eSerialNum["+lineNumber+"]").value=="" || document.getElementById("eSerialNum["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eUOM["+lineNumber+"]").value=="NULL") {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(errCount!=0)
                $('#confirmAsset'+lineNumber).attr('disabled', 'disabled');
            else
                $('#confirmAsset'+lineNumber).attr('disabled', false);
        }

        function inDateModal(id){
            document.getElementById('mLineID').value = id;
            document.getElementById('inDateAll').value = document.getElementById('InDate['+id+']').value;
            document.getElementById('inDateRemarks').value = document.getElementById('InRemarks['+id+']').value;
            $('#inDateModal').modal('show');
        }
        function inDateModalOK(){
            var id = document.getElementById('mLineID').value;
            document.getElementById('InDate['+id+']').value = document.getElementById('inDateAll').value;
            document.getElementById('vInDate['+id+']').innerHTML = document.getElementById('inDateAll').value;
            document.getElementById('InRemarks['+id+']').value = document.getElementById('inDateRemarks').value;
            $('#inDateModal').modal('hide');
            document.getElementById('inDateAll').value = "";
            document.getElementById('inDateRemarks').value = "";
        }

        function outDateModal(id){
            document.getElementById('mLineID').value = id;
            document.getElementById('outDateAll').value = document.getElementById('OutDate['+id+']').value;
            document.getElementById('outDateRemarks').value = document.getElementById('OutRemarks['+id+']').value;
            $('#outDateModal').modal('show');
        }
        function outDateModalOK(){
            var id = document.getElementById('mLineID').value;
            document.getElementById('OutDate['+id+']').value = document.getElementById('outDateAll').value;
            document.getElementById('vOutDate['+id+']').innerHTML = document.getElementById('outDateAll').value;
            document.getElementById('OutRemarks['+id+']').value = document.getElementById('outDateRemarks').value;
            $('#outDateModal').modal('hide');
            document.getElementById('outDateAll').value = "";
            document.getElementById('outDateRemarks').value = "";
        }
    </script>
@endpush
@endsection