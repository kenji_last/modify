@extends('layouts.admin')
@section('title', 'Message')
@section('header_title', 'Message')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="fa fa-home"></i>&nbsp;&nbsp;Hello, {{Auth::user()->FirstName}}!
                        </h4>
                    </div>
                    <div class="panel-body">

                    <!--Admin-->
                        @if(\App\Http\Controllers\TransactionController::knowYourRole(1)==true)
                            <div class="col-md-12">
                                <div class="box box-default collapsed-box">
                                    <div class="box-header box-warning with-border">
                                        <h3 class="box-title">Administrator</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="small-box bg-aqua">
                                                    <div class="inner">
                                                        <h3>{{$userCount}}</h3>
                                                        <p>Active Users</p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-people"></i>
                                                    </div>
                                                    <a href="{{ url('users') }}" class="small-box-footer">User Management <i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    <!--Admin-->
                    <!--Requestor-->
                        @if(\App\Http\Controllers\TransactionController::knowYourRole(2)==true)
                            <div class="col-md-12">
                                <div class="box box-success collapsed-box">
                                    <div class="box-header box-warning with-border">
                                        <h3 class="box-title">Requestor</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="box box-warning box-solid">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">My Requests</h3>
                                                    </div>
                                                    <div class="box-body" style="display: block;">
                                                        @foreach($rStatus as $rS)
                                                            <div class="row" style="padding-left:5%;padding-right:5%;font-size:12px;">
                                                                <div class="col-md-9">
                                                                     <a href="{{url('in_myrequests/'.$rS->id)}}">
                                                                        {{$rS->Name}}
                                                                    </a>
                                                                </div>
                                                                <div class="col-md-3" style="text-align:right;">
                                                                    <a href="{{url('in_myrequests/'.$rS->id)}}">
                                                                        {{$rS->my_iHeaders(Auth::user()->id)->count()}}
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="box">
                                                    <div class="box-header with-border bg-blue-gradient">
                                                        <h3 class="box-title">
                                                            <span>Items Currently Checked-In:
                                                            &nbsp;
                                                                {{$myCILineItems->count()}}
                                                        </h3>
                                                        {{--<div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>

                                                        </div>--}}
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <div class="table-responsive">
                                                            <table class="table no-margin table-condensed" style="width:100%;">
                                                                <thead style="width: calc( 100% - 1em );display:table;table-layout:fixed;font-size: 12px;">
                                                                <tr>
                                                                    <th>Request No.</th>
                                                                    <th>Control No.</th>
                                                                    <th width="30%">Description</th>
                                                                    <th width="30%">Check-In</th>
                                                                    <th>Age</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody style="display:block;height:100px;overflow:auto;font-size: 12px;">
                                                                @foreach($myCILineItems as $ci)
                                                                    <tr style="display:table;width:100%;table-layout:fixed;">
                                                                        <td>{{$ci->iline_to_ihead->RequestNo}}</td>
                                                                        <td>{{$ci->ControlNum}}</td>
                                                                        <td width="30%">{{$ci->Description}}</td>
                                                                        <td width="30%">{{date('M d, Y h:i A',strtotime($ci->InDate))}}</td>
                                                                        <td>
                                                                            <?php

                                                                            $datetime1 = date_create(date('Y-m-d',strtotime($ci->InDate)));
                                                                            $datetime2 = date_create(date('Y-m-d'));
                                                                            $interval = date_diff($datetime1, $datetime2);
                                                                            if ($interval->format('%a')=='0')
                                                                            $age = "Today";
                                                                            elseif ($interval->format('%a')=='1')
                                                                            $age =  $interval->format('%a day ago');
                                                                            else
                                                                            $age =  $interval->format('%a days ago');
                                                                            echo $age;
                                                                            ?>

                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                    </div>
                                                    <!-- /.box-body -->
                                                    <div class="box-footer clearfix">
                                                        @if($myCILineItems->count()==0)

                                                        @else
                                                            <a href="{{url('excel_req_checkin')}}" class="btn btn-sm btn-info btn-flat pull-left">Save as Excel File</a>
                                                        @endif
                                                    </div>
                                                    <!-- /.box-footer -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                &nbsp;
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="box">
                                                    <div class="box-header with-border bg-red-gradient">
                                                        <h3 class="box-title">
                                                            Items Checked-Out:
                                                            &nbsp;
                                                            {{$myCOLineItems->count()}}
                                                        </h3>
                                                        {{--<div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>

                                                        </div>--}}
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <div class="table-responsive">
                                                            <table class="table no-margin table-condensed" style="width:100%;">
                                                                <thead style="width: calc( 100% - 1em );display:table;table-layout:fixed;font-size: 12px;">
                                                                <tr>
                                                                    <th>Request No.</th>
                                                                    <th>Control No.</th>
                                                                    <th width="35%">Description</th>
                                                                    <th>Check-In</th>
                                                                    <th>Check-Out</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody style="display:block;height:100px;overflow:auto;font-size: 12px;">
                                                                @foreach($myCOLineItems as $ci)
                                                                    <tr style="display:table;width:100%;table-layout:fixed;">
                                                                        <td>{{$ci->iline_to_ihead->RequestNo}}</td>
                                                                        <td>{{$ci->ControlNum}}</td>
                                                                        <td width="35%">{{$ci->Description}}</td>
                                                                        <td>{{date('M d, Y h:i A',strtotime($ci->InDate))}}</td>
                                                                        <td>{{date('M d, Y h:i A',strtotime($ci->OutDate))}}</td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                    </div>
                                                    <!-- /.box-body -->
                                                    <div class="box-footer clearfix">
                                                        @if($myCOLineItems->count()==0)

                                                        @else
                                                            <a href="{{url('excel_req_checkout')}}" class="btn btn-sm btn-info btn-flat pull-left">Save as Excel File</a>
                                                        @endif
                                                    </div>
                                                    <!-- /.box-footer -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    <!--/Requestor-->
                    <!--Approver-->
                        @if(\App\Http\Controllers\TransactionController::knowYourRole(3)==true)
                    <div class="col-md-12">
                        <div class="box box-warning collapsed-box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Approver</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body" style="">
                            <div class="row">
                                <div class="col-xs-3">
                                   <div class="box box-warning box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Incoming Requests</h3>
                                        </div>
                                        <div class="box-body" style="display: block;">
                                            <div class="row" style="padding-left:5%;padding-right:5%;font-size:12px;">
                                                <div class="col-md-9">
                                                    <a href="{{url('in_approver/ci_for_approval')}}">
                                                        Check-In Requests
                                                    </a>
                                                </div>
                                                <div class="col-md-3" style="text-align:right;">
                                                    {{$ICIReqsForApproval}}
                                                </div>
                                            </div>
                                            <div class="row" style="padding-left:5%;padding-right:5%;font-size:12px;">
                                                <div class="col-md-9">
                                                    <a href="{{url('in_approver/ci_approved')}}">
                                                        Check-In Requests Approved
                                                    </a>
                                                </div>
                                                <div class="col-md-3" style="text-align:right;">
                                                    {{$ICIReqsApproved}}
                                                </div>
                                            </div>
                                            <div class="row" style="padding-left:5%;padding-right:5%;font-size:12px;">
                                                <div class="col-md-9">
                                                    <a href="{{url('in_approver/ci_rejected')}}">
                                                        Check-In Request Rejected
                                                    </a>
                                                </div>
                                                <div class="col-md-3" style="text-align:right;">
                                                    {{$ICIReqsRejected}}
                                                </div>
                                            </div>
                                            <div class="row" style="padding-left:5%;padding-right:5%;font-size:12px;">
                                                <div class="col-md-9">
                                                    <a href="{{url('in_approver/co_for_approval')}}">
                                                        Check-Out Requests
                                                    </a>
                                                </div>
                                                <div class="col-md-3" style="text-align:right;">
                                                    {{$ICOReqsForApproval}}
                                                </div>
                                            </div>
                                            <div class="row" style="padding-left:5%;padding-right:5%;font-size:12px;">
                                                <div class="col-md-9">
                                                    <a href="{{url('in_approver/co_approved')}}">
                                                        Check-Out Requests Approved
                                                    </a>
                                                </div>
                                                <div class="col-md-3" style="text-align:right;">
                                                    {{$ICOReqsApproved}}
                                                </div>
                                            </div>
                                        </div>
                                   </div>
                                </div>
                                <div class="col-xs-9">
                                    <div class="box">
                                        <div class="box-header with-border bg-blue-gradient">
                                            <h3 class="box-title">
                                                    <span>Items Currently Checked-In:
                                                    &nbsp;
                                                        {{$myCILineItems->count()}}
                                            </h3>
                                            {{--<div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                    <i class="fa fa-minus"></i>
                                                </button>

                                            </div>--}}
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table no-margin table-condensed" style="width:100%;">
                                                    <thead style="width: calc( 100% - 1em );display:table;table-layout:fixed;font-size: 12px;">
                                                    <tr>
                                                        <th>Request No.</th>
                                                        <th>Control No.</th>
                                                        <th width="30%">Description</th>
                                                        <th width="30%">Check-In</th>
                                                        <th>Age</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody style="display:block;height:100px;overflow:auto;font-size: 12px;">
                                                    @foreach($myCILineItems as $ci)
                                                        <tr style="display:table;width:100%;table-layout:fixed;">
                                                            <td>{{$ci->iline_to_ihead->RequestNo}}</td>
                                                            <td>{{$ci->ControlNum}}</td>
                                                            <td width="30%">{{$ci->Description}}</td>
                                                            <td width="30%">{{date('M d, Y h:i A',strtotime($ci->InDate))}}</td>
                                                            <td>
                                                                <?php

                                                                $datetime1 = date_create(date('Y-m-d',strtotime($ci->InDate)));
                                                                $datetime2 = date_create(date('Y-m-d'));
                                                                $interval = date_diff($datetime1, $datetime2);
                                                                if ($interval->format('%a')=='0')
                                                                    $age = "Today";
                                                                elseif ($interval->format('%a')=='1')
                                                                    $age =  $interval->format('%a day ago');
                                                                else
                                                                    $age =  $interval->format('%a days ago');
                                                                echo $age;
                                                                ?>

                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer clearfix">
                                            @if($myCILineItems->count()==0)

                                            @else
                                                <a href="{{url('excel_appr_checkin')}}" class="btn btn-sm btn-info btn-flat pull-left">Save as Excel File</a>
                                            @endif
                                        </div>
                                        <!-- /.box-footer -->
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    &nbsp;
                                </div>
                                <div class="col-xs-9">
                                    <div class="box">
                                        <div class="box-header with-border bg-red-gradient">
                                            <h3 class="box-title">
                                                Items Checked-Out:
                                                &nbsp;
                                                {{$myCOLineItems->count()}}
                                            </h3>
                                            {{--<div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                    <i class="fa fa-minus"></i>
                                                </button>

                                            </div>--}}
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table no-margin table-condensed" style="width:100%;">
                                                    <thead style="width: calc( 100% - 1em );display:table;table-layout:fixed;font-size: 12px;">
                                                    <tr>
                                                        <th>Request No.</th>
                                                        <th>Control No.</th>
                                                        <th width="35%">Description</th>
                                                        <th>Check-In</th>
                                                        <th>Check-Out</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody style="display:block;height:100px;overflow:auto;font-size: 12px;">
                                                    @foreach($myCOLineItems as $ci)
                                                        <tr style="display:table;width:100%;table-layout:fixed;">
                                                            <td>{{$ci->iline_to_ihead->RequestNo}}</td>
                                                            <td>{{$ci->ControlNum}}</td>
                                                            <td width="35%">{{$ci->Description}}</td>
                                                            <td>{{date('M d, Y h:i A',strtotime($ci->InDate))}}</td>
                                                            <td>{{date('M d, Y h:i A',strtotime($ci->OutDate))}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer clearfix">
                                            @if($myCOLineItems->count()==0)

                                            @else
                                                <a href="{{url('excel_appr_checkout')}}" class="btn btn-sm btn-info btn-flat pull-left">Save as Excel File</a>
                                            @endif
                                        </div>
                                        <!-- /.box-footer -->
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                        @endif
                    <!--Approver-->

                    <!--Gate Control-->
                    @if(\App\Http\Controllers\TransactionController::knowYourRole(5)==true)
                <div class="col-md-12">
                    <div class="box box-primary collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Gate</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body" style="">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="box box-warning box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Incoming Requests</h3>
                                    </div>
                                    <div class="box-body" style="display: block;">
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('in_gateview')}}">
                                                    Check-In Requests
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$ICIReqsForApproval}}
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('in_gate_co_view')}}">
                                                    Check-Out Requests
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$ICOReqsForApproval}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-xs-4">
                                <div class="box box-danger box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Outgoing Requests</h3>
                                    </div>
                                    <div class="box-body" style="display: block;">
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('out_gateview')}}">
                                                    Check-Out Requests
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$OCOReqsForApproval}}
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('out_gate_ci_view')}}">
                                                    Check-In Requests
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$OCIReqsForApproval}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                        </div>
                    </div>
                </div>
                    @endif
                    <!--Gate Control-->

                    <!--Accounting-->
                    @if(\App\Http\Controllers\TransactionController::knowYourRole(7)==true)
                        {{--<div class="row">
                            <div class="col-xs-4">
                                <div class="box box-danger box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Outgoing Requests</h3>
                                    </div>
                                    <div class="box-body" style="display: block;">
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('out_accounting/co_for_approval')}}">
                                                    Check-Out Requests
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$OCOReqsForApproval}}
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('out_accounting/co_approved')}}">
                                                    Check-Out Requests Approved
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$OCOReqsApproved}}
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('out_accounting/co_rejected')}}">
                                                    Check-Out Requests Rejected
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$OCOReqsRejected}}
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('out_accounting/ci_for_approval')}}">
                                                    Check-In Requests
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$OCIReqsForApproval}}
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left:5%;padding-right:5%;font-size:14px;">
                                            <div class="col-md-9">
                                                <a href="{{url('out_accounting/ci_approved')}}">
                                                    Check-In Requests Approved
                                                </a>
                                            </div>
                                            <div class="col-md-3" style="text-align:right;">
                                                {{$OCIReqsApproved}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                <span>Items Currently Checked-In:
                                &nbsp;
                                    {{$myCILineItems->count()}}
                                        </h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table no-margin" style="width:100%;">
                                                <thead style="width: calc( 100% - 1em );display:table;table-layout:fixed;font-size: 12px;">
                                                <tr>
                                                    <th>Request No.</th>
                                                    <th>Control No.</th>
                                                    <th width="30%">Description</th>
                                                    <th width="30%">Check-In</th>
                                                </tr>
                                                </thead>
                                                <tbody style="display:block;height:100px;overflow:auto;font-size: 12px;">
                                                @foreach($myCILineItems as $ci)
                                                    <tr style="display:table;width:100%;table-layout:fixed;">
                                                        <td>{{$ci->iline_to_ihead->RequestNo}}</td>
                                                        <td>{{$ci->ControlNum}}</td>
                                                        <td width="30%">{{$ci->Description}}</td>
                                                        <td width="30%">{{date('M d, Y h:i A',strtotime($ci->InDate))}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer clearfix">
                                        @if($myCILineItems->count()==0)

                                        @else
                                            <a href="{{url('excel_req_checkin')}}" class="btn btn-sm btn-info btn-flat pull-left">Save as Excel File</a>
                                        @endif
                                    </div>
                                    <!-- /.box-footer -->
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="box box-warning">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">
                                            Items Checked-Out:
                                            &nbsp;
                                            {{$myCOLineItems->count()}}
                                        </h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table no-margin" style="width:100%;">
                                                <thead style="width: calc( 100% - 1em );display:table;table-layout:fixed;font-size: 12px;">
                                                <tr>
                                                    <th>Request No.</th>
                                                    <th>Control No.</th>
                                                    <th width="35%">Description</th>
                                                    <th>Check-In</th>
                                                    <th>Check-Out</th>
                                                </tr>
                                                </thead>
                                                <tbody style="display:block;height:100px;overflow:auto;font-size: 12px;">
                                                @foreach($myCOLineItems as $ci)
                                                    <tr style="display:table;width:100%;table-layout:fixed;">
                                                        <td>{{$ci->iline_to_ihead->RequestNo}}</td>
                                                        <td>{{$ci->ControlNum}}</td>
                                                        <td width="35%">{{$ci->Description}}</td>
                                                        <td>{{date('M d, Y h:i A',strtotime($ci->InDate))}}</td>
                                                        <td>{{date('M d, Y h:i A',strtotime($ci->OutDate))}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer clearfix">
                                        @if($myCOLineItems->count()==0)

                                        @else
                                            <a href="{{url('excel_req_checkout')}}" class="btn btn-sm btn-info btn-flat pull-left">Save as Excel File</a>
                                        @endif
                                    </div>
                                    <!-- /.box-footer -->
                                </div>
                            </div>
                        </div>--}}
                    @endif
                        <!--Accounting-->

                    </div>

                </div>
            </div>
        </div>
        {!! Form::open(array('url' => 'firstTimePassword')) !!}
        {!! Form::token() !!}
        <div id="changePass" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:480px;margin-left:35%;margin-right:35%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="changePassTitle">Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;">
                            <input type="hidden" name="changePassID" id="changePassID" value="{{Auth::user()->id}}">
                            <div class="col-xs-12">
                                <span id="changePassMsg" style="font-size:12px;">
                                    You logged in using a temporary password.
                                </span>
                            </div>
                            <div class="col-xs-12">
                                <blockquote style="font-size:13px;color:#ff0000;margin-top:10px;" id="changePassBlock">
                                    Password must contain <b>at least</b> one uppercase character, one lowercase character, one special character and one numeric character.<br>
                                    Password cannot contain spaces and must be at least 8 characters long.
                                </blockquote>
                                <span id="changePassErrMsg" style="font-size:14px;color:#ff0000;">

                                </span>

                            </div>
                            <div class="col-xs-12">
                                <label>Password</label>
                                <input type="password" class="form-control" id="changePassNew1" onchange="checkPass();" onkeyup="checkPass();" name="changePassNew1">
                            </div>
                            <div class="col-xs-12">
                                <label>Confirm New Password</label>
                                <input type="password" class="form-control" id="changePassNew2" onchange="checkPass();" onkeyup="checkPass();" name="changePassNew2">
                            </div>
                            <div style="display:none;">
                                <?php $plist = \App\PList::all();?>
                                <span id="plcount">{{$plist->count()}}</span>
                                <table id="pls">
                                    @foreach($plist as $pl)
                                        <tr>
                                            <td>
                                                {{$pl->Name}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="col-xs-12">
                                <br>
                                <button type="button" id="changePassView" onclick="viewPassword();" class="btn btn-default"><i class="fa fa-eye-slash"></i></button>
                                <button type="button" id="changePassView" onclick="generatePassword();" class="btn btn-default" style="display:none;">Generate Password</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="rejectSets">
                        {!! Form::submit('Continue', array('class'=>'btn btn-primary','id'=>'changePassSubmit','name'=>'changePassSubmit','disabled'), array('data-dismiss'=>'modal')) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

    </body>


    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script>
        var date1 = new Date("{{Auth::user()->PasswordUpdated}}");
        var date2 = new Date(Date.now());
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        //alert(date2);
        var first = {{Auth::user()->FirstLogin}};
        if(first == 1 || diffDays>=90) {
            $('#changePass').modal({
             backdrop: 'static',
             keyboard: false
             })
            $('#changePass').modal('show');
        }

        //CHECK NEW PASSWORDS
        function checkChars(password){
            return !!password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%* #+-=\.\-\(\)\^?&])[A-Za-z\d$@$!%* #+=\.\-\(\)\^?&]{8,}$/);
        }

        function checkPass(){
            var errCount = 0;
            var errMsg = "";
            document.getElementById("changePassErrMsg").innerHTML = errMsg;
            if(document.getElementById("changePassNew1").value != document.getElementById("changePassNew2").value) {
                errCount = errCount + 1;
                errMsg = errMsg + "The Passwords do not match.<br>";
            }
            var password = document.getElementById("changePassNew1").value;
            if(checkChars(password)==false) {
                errCount = errCount + 1;
                document.getElementById("changePassBlock").style.borderLeft = "5px solid #ff0000";
                //errMsg = errMsg + " Password must:<br>&nbsp;be at least 8 characters long<br>&nbsp;contain at least 1 letter<br>&nbsp;contain at least one Number<br>&nbsp;contain at least one special character(!.-)";
            }

            if(errCount>0) {
                document.getElementById("changePassSubmit").disabled = true;
                document.getElementById("changePassErrMsg").innerHTML = errMsg;
            }
            else{
                document.getElementById("changePassBlock").style.borderLeft = "5px solid #eee";
                document.getElementById("changePassSubmit").disabled = false;
            }

        }
        //SHOW PASSWORD
        var viewPass = 0;
        function viewPassword(){
            var tag1 = document.getElementById("changePassNew1");
            var tag2 = document.getElementById("changePassNew2");
            if(viewPass == 0) {
                tag1.setAttribute('type', 'text');
                tag2.setAttribute('type', 'text');
                document.getElementById("changePassView").innerHTML = "<i class='fa fa-eye'></i>";
                viewPass = 1;
            }
            else {
                tag1.setAttribute('type', 'password');
                tag2.setAttribute('type', 'password');
                document.getElementById("changePassView").innerHTML = "<i class='fa fa-eye-slash'></i>";
                viewPass = 0;
            }
        }
        //SHOW PASSWORD - END

        //GENERATING PASSWORD
        function generatePassword(){
            if(viewPass == 0)
                viewPassword();

            var pass = makePassword();
            document.getElementById("changePassNew1").value = pass;
            document.getElementById("changePassNew2").value = pass;
            checkPass();
        }

        function selectSigns(){
            var signs = "!-.";
            var signs_len = 1;
            var randomstring2 = '';
            for (var i=0; i<signs_len; i++) {
                var rnum = Math.floor(Math.random() * signs.length);
                randomstring2 += signs.substring(rnum,rnum+1);
            }
            return randomstring2;
        }
        function selectNumbers(){
            var signs = "1234567890";
            var signs_len = 1;
            var randomstring2 = '';
            for (var i=0; i<signs_len; i++) {
                var rnum = Math.floor(Math.random() * signs.length);
                randomstring2 += signs.substring(rnum,rnum+1);
            }
            return randomstring2;
        }
        function makePassword() {
            if(document.getElementById("plcount").innerHTML>5){
                var plc = document.getElementById("plcount").innerHTML;
                var randomnumber = Math.floor(Math.random() * (plc - 1 + 1)) + 1;

                var oTable = document.getElementById('pls');
                var oCells = oTable.rows.item(randomnumber).cells;
                var cellVal = oCells.item(0).innerHTML;
                cellVal = cellVal.replace(/\s+/g, '');
                //alert(cellVal);
                randomstring = selectSigns() + cellVal + selectNumbers();
            }
            else{
                //var text = Math.random().toString(36).slice(-8);
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
                var string_length = 8;
                var randomstring = '';
                for (var i=0; i<string_length; i++) {
                    var rnum = Math.floor(Math.random() * chars.length);
                    randomstring += chars.substring(rnum,rnum+1);
                }
                randomstring = selectSigns() + randomstring + selectNumbers();
            }
            return randomstring;
        }
        //GENERATING PASSWORD

    </script>
@endpush
@endsection