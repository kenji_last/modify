@extends('layouts.admin')
@section('title', 'View Outgoing')
    {{--@if($singleRequest->Status==2)
        @section('in_approver_view', 'active')
    @elseif($singleRequest->Status==6)
        @section('in_checkout_view', 'active')
    @if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_approverview')))
        @section('in_approver_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_checkoutview')))
        @section('in_checkout_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_approvals')))
        @section('in_my_approvals', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_rejections')))
        @section('in_my_rejections', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_co_approvals')))
        @section('in_my_co_approvals', 'active')
    @endif--}}
@section('approver', 'active')
    @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_approver')))
        @section('in_approver_'.$code, 'active')
    @endif
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4><i class="fa fa-user"></i>&nbsp;
                                {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_approverview')))
                                    <a href="{{url('in_approverview')}}">Incoming Request</a> - {{$singleRequest->RequestNo}}
                                @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_approvals')))
                                    <a href="{{url('in_my_approvals')}}">Incoming Request</a> - {{$singleRequest->RequestNo}}
                                @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_rejections')))
                                    <a href="{{url('in_my_rejections')}}">Incoming Request</a> - {{$singleRequest->RequestNo}}
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_checkoutview')))
                                    <a href="{{url('in_checkoutview')}}">Incoming Request</a> - {{$singleRequest->RequestNo}}
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_co_approvals')))
                                    <a href="{{url('in_my_co_approvals')}}">Incoming Request</a> - {{$singleRequest->RequestNo}}
                                @endif--}}
                                @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_approver')))
                                        <a href="{{url('in_approver/'.$code)}}">Incoming Request</a> - {{$singleRequest->RequestNo}}
                                @endif
                            </h4>
                        @if($singleRequest->Status==8)
                            <small>
                                        <span>
                                            {{--&nbsp;&nbsp;&nbsp;&nbsp;
                                            Check-Out History&nbsp;
                                            @foreach($coVersion as $ver)
                                                @if($ver->checkout_ID!=0)
                                                    <a href="{{url('in_co_view/'.$singleRequest->id."/".$ver->checkout_ID)}}">{{$ver->checkout_ID}}</a>&nbsp;
                                                @endif
                                            @endforeach--}}
                                        </span>
                            </small>
                        @endif
                    </div>
                    <div class="panel-body" style="padding-left:10%;padding-right:10%;padding-top:2%;padding-bottom:1%;">
                        <div class="form-group col-md-12">
                            @if($singleRequest->Status==2)
                                <button type="button" id="approve" onclick="approveModal()" class="btn btn-sm btn-primary">Approve</button>
                                <button type="button" id="reject" onclick="$('#rejectModal').modal('show');" class="btn btn-sm btn-primary">Reject</button>
                            @elseif($singleRequest->Status==6)
                                <button type="button" id="checkOut" onclick="checkoutModal()" class="btn btn-sm btn-primary">Approve Check-Out</button>
                                <button type="button" id="checkOut" onclick="$('#rejectModal').modal('show');" class="btn btn-sm btn-primary">Reject Check-Out</button>
                            @endif

                        </div>
                        <div class="col-md-6">
                            <label>Request Number:</label>
                            {{$singleRequest->RequestNo}}
                        </div>
                        <div class="col-md-6">
                            <label>Business Partner Type:</label>
                            {{$singleRequest->oBusinessPartner->Name}}
                        </div>
                        <div class="col-md-6">
                            <label>Status:</label>
                            {{$singleRequest->oStatus->Name}}
                        </div>
                        <div class="col-md-6">
                            <label>Business Partner:</label>
                            {{$singleRequest->BusinessPartnerName}}
                        </div>
                        <div class="col-md-6">
                            <label>Date:</label>
                            {{date('F d, Y', strtotime($singleRequest->Date))}}
                        </div>
                        <div class="col-md-6">
                            <label>Contact:</label>
                            {{$singleRequest->Contact}}
                        </div>
                        <div class="col-md-6">
                            <label>Host:</label>
                            {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                        </div>
                        <div class="col-md-6">
                            <label>Contact Email:</label>
                            {{$singleRequest->ContactEmail}}
                        </div>
                        <div class="col-md-6">
                            <label>Project/Purpose:</label>
                            {{$singleRequest->Project}}
                        </div>
                        @if($singleRequest->Status == 3 || $singleRequest->Status == 4 || $singleRequest->Status == 11)
                            <div class="col-md-6">
                                <label>Approved By:</label>
                                {{$singleRequest->iheadApprovedBy_to_user->FirstName}} {{$singleRequest->iheadApprovedBy_to_user->LastName}}
                            </div>
                        @elseif($singleRequest->Status == 10)
                            <div class="col-md-6">
                                <label>Rejected By:</label>
                                {{$singleRequest->iheadRejectedBy_to_user->FirstName}} {{$singleRequest->iheadRejectedBy_to_user->LastName}}
                            </div>
                            <div class="col-md-6">
                                <label>Reject Reason:</label><br>
                                {{$singleRequest->RejectReason}}
                            </div>
                        @endif

                        <div class="col-xs-12" style="padding-top:20px;">

                            <div class="nav-tabs-custom" style="align-content: center;">
                                <ul class="nav nav-tabs" style="background-color: #eee;">
                                    <li class="active">
                                        <a href="#tab_1" data-toggle="tab" aria-expanded="true">Tools
                                            &nbsp;&nbsp;
                                            <span style="background:#ff0000;color:#ffffff;padding:0 3px 0 3px;border-radius:30px;" id="RetCount">{{$retCount}}</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_2" data-toggle="tab" aria-expanded="false">Consumables
                                            &nbsp;&nbsp;
                                            <span style="background:#ff0000;color:#ffffff;padding:0 3px 0 3px;border-radius:30px;" id="NonRetCount">{{$nonRetCount}}</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_3" data-toggle="tab" aria-expanded="false">Equipment
                                            &nbsp;&nbsp;
                                            <span style="background:#ff0000;color:#ffffff;padding:0 3px 0 3px;border-radius:30px;" id="EquipCount">{{$equipCount}}</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content" style="padding:0px;">
                                    <div class="tab-pane active" id="tab_1">
                                        <div id="aHeaders" class="box-primary" style="align-content: center;">
                                            <div class="row box-header" style="
                                                width:98%;
                                                margin-right:0;
                                                margin-left:0;
                                                margin-top: 25px;
                                                margin-bottom: 0px;
                                                font-weight: bold;
                                                padding:0px;
                                                font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                                font-size:12px;
                                                text-align: left;
                                                border-bottom: 1px solid #e9e9e9;">
                                                <div class="col-xs-4">Description</div>
                                                <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                                <div class="col-xs-1" style="text-align: center;">UOM</div>
                                                <div class="col-xs-2" style="text-align: center;">Location</div>
                                                <div class="col-xs-2" style="text-align: center;">Check-In</div>
                                                <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                            </div>
                                        </div>
                                        <div id="aBodyR" class="box-primary" style="align-content: center;height:260px;max-height:240px;overflow-y:scroll;">
                                            @foreach($lineItems as $li)
                                                @if($li->Returnable==1 && $li->ItemType==0)
                                                    <div class="row" id='divViewtheTableCount+"' style="
                                                    width:100%;
                                                    margin-left:0px;
                                                    margin-bottom:0px;
                                                    margin-top:0px;
                                                    padding:5px;
                                                    font-size:12px;
                                                    border-bottom: 1px solid #e9e9e9;">
                                                        <div class="col-xs-4" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                                        <div class="col-xs-1" style='padding:9px 14px; text-align: center;'>{{$li->Quantity}}</div>
                                                        <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:left;'>
                                                            <span><b>Destination</b>: {{$li->iline_to_destination->Name}}</span><br>
                                                            <span><b>Storage</b>: {{$li->iline_to_storage->storage_to_destination->Name}}</span>
                                                        </div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                            @if($li->InDate!="")
                                                                {{date('M d, Y h:i A',strtotime($li->InDate))}}
                                                            @else
                                                                -
                                                            @endif
                                                        </div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                            @if($li->Returnable==1)
                                                                @if($li->OutDate!="")
                                                                    {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                                                @else
                                                                    -
                                                                @endif
                                                            @else
                                                                N/A
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <?php $retCount = $retCount + 1;?>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div id="aHeaders" class="box-primary" style="align-content: center;">
                                            <div class="row box-header" style="
                                                width:98%;
                                                margin-right:0;
                                                margin-left:0;
                                                margin-top: 25px;
                                                margin-bottom: 0px;
                                                font-weight: bold;
                                                padding:0px;
                                                font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                                font-size:12px;
                                                text-align: left;
                                                border-bottom: 1px solid #e9e9e9;">
                                                <div class="col-xs-4">Description</div>
                                                <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                                <div class="col-xs-1" style="text-align: center;">UOM</div>
                                                <div class="col-xs-2" style="text-align: center;">Location</div>
                                                <div class="col-xs-2" style="text-align: center;">Check-In</div>
                                                <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                            </div>
                                        </div>
                                        <div id="aBodyN" class="box-primary" style="align-content: center;height:260px;max-height:240px;overflow-y:scroll;">
                                            @foreach($lineItems as $li)
                                                @if($li->Returnable==0)
                                                    <div class="row" id='divViewtheTableCount+"' style="
                                                    width:100%;
                                                    margin-left:0px;
                                                    margin-bottom:0px;
                                                    margin-top:0px;
                                                    padding:5px;
                                                    font-size:12px;
                                                    border-bottom: 1px solid #e9e9e9;">
                                                        <div class="col-xs-4" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                                        <div class="col-xs-1" style='padding:9px 14px; text-align: center;'>{{$li->Quantity}}</div>
                                                        <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:left;'>
                                                            <span><b>Destination</b>: {{$li->iline_to_destination->Name}}</span><br>
                                                            <span><b>Storage</b>: {{$li->iline_to_storage->storage_to_destination->Name}}</span>
                                                        </div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                            @if($li->InDate!="")
                                                                {{date('M d, Y h:i A',strtotime($li->InDate))}}
                                                            @else
                                                                -
                                                            @endif
                                                        </div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                            @if($li->Returnable==1)
                                                                @if($li->OutDate!="")
                                                                    {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                                                @else
                                                                    -
                                                                @endif
                                                            @else
                                                                N/A
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3">
                                        <div id="aHeaders" class="box-primary" style="align-content: center;">
                                            <div class="row box-header" style="
                                                width:98%;
                                                margin-right:0;
                                                margin-left:0;
                                                margin-top: 25px;
                                                margin-bottom: 0px;
                                                font-weight: bold;
                                                padding:0px;
                                                font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                                font-size:12px;
                                                text-align: left;
                                                border-bottom: 1px solid #e9e9e9;">
                                                <div class="col-xs-4">Description</div>
                                                <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                                <div class="col-xs-1" style="text-align: center;">UOM</div>
                                                <div class="col-xs-2" style="text-align: center;">Location</div>
                                                <div class="col-xs-2" style="text-align: center;">Check-In</div>
                                                <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                            </div>
                                        </div>
                                        <div id="aBodyS" class="box-primary" style="align-content: center;height:260px;max-height:240px;overflow-y:scroll;">
                                            @foreach($lineItems as $li)
                                                    @if($li->Returnable==1 && $li->ItemType==1)
                                                    <div class="row" id='divViewtheTableCount+"' style="
                                                    width:100%;
                                                    margin-left:0px;
                                                    margin-bottom:0px;
                                                    margin-top:0px;
                                                    padding:5px;
                                                    font-size:12px;
                                                    border-bottom: 1px solid #e9e9e9;">
                                                        <div class="col-xs-4" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                                        <div class="col-xs-1" style='padding:9px 14px; text-align: center;'>{{$li->Quantity}}</div>
                                                        <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:left;'>
                                                            <span><b>Destination</b>: {{$li->iline_to_destination->Name}}</span><br>
                                                            <span><b>Storage</b>: {{$li->iline_to_storage->storage_to_destination->Name}}</span>
                                                        </div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                            @if($li->InDate!="")
                                                                {{date('M d, Y h:i A',strtotime($li->InDate))}}
                                                            @else
                                                                -
                                                            @endif
                                                        </div>
                                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                            @if($li->Returnable==1)
                                                                @if($li->OutDate!="")
                                                                    {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                                                @else
                                                                    -
                                                                @endif
                                                            @else
                                                                N/A
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        {{--<div class="form-group col-xs-12">
                            <div id="aHeaders" class="box-primary" style="align-content: center;">
                                <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                    <div class="col-xs-4">Description</div>
                                    --}}{{--<div class="col-xs-4">Serial Number</div>--}}{{--
                                    <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                    <div class="col-xs-1" style="text-align: center;">UOM</div>
                                    <div class="col-xs-2" style="text-align: center;">Returnable?</div>
                                    <div class="col-xs-2" style="text-align: center;">Check-In</div>
                                    <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                </div>
                            </div>
                            <div id="aBody" class="box-primary" style="
                                    align-content: center;
                                    height:260px;
                                    max-height:260px;
                                    overflow-y:scroll;
                                    padding-bottom:0px;">
                                @foreach($lineItems as $li)
                                    <div class="row" id='divViewtheTableCount+"' style="
                                        width:100%;
                                        margin-left:0px;
                                        margin-bottom:0px;
                                        margin-top:0px;
                                        padding:5px;
                                        font-size:12px;
                                        border-bottom: 1px solid #e9e9e9;">
                                        <div class="col-xs-4" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                        <div class="col-xs-1" style='padding:9px 14px; text-align: center;'>{{$li->Quantity}}</div>
                                        <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->Returnable==0)
                                                No
                                            @else
                                                Yes
                                            @endif
                                        </div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->InDate!="")
                                                {{date('M d, Y h:i A',strtotime($li->InDate))}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->Returnable==1)
                                                @if($li->OutDate!="")
                                                    {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                                @else
                                                    -
                                                @endif
                                            @else
                                                N/A
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>--}}
                        </div>
                </div>
            </div>
            </div>
        </div>

        <div id="updateModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;" id="modalMessage">

                        </div>
                    </div>
                    <div class="modal-footer" id="approveSet">
                        {!! Form::open(array('url' => 'in_approve')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer" id="approveCOSet">
                        {!! Form::open(array('url' => 'in_co_approve')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                        {{--{!! Form::close() !!}--}}
                    </div>
                    <div class="modal-footer" id="rejectSet">

                        @if($singleRequest->Status==2)
                            {!! Form::open(array('url' => 'in_reject')) !!}
                        @elseif($singleRequest->Status==6)
                            {!! Form::open(array('url' => 'in_co_reject')) !!}
                        @endif
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    </div>


                    {{--<div class="modal-footer" id="cancelSet">
                        {!! Form::open(array('url' => 'in_cancel')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        --}}{{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}{{--
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}

                    </div>--}}
                </div>

            </div>
        </div>

        <div id="rejectModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;">
                            Please specify the reason for rejecting this request.
                            <textarea style="resize: none;" onkeyup="rejectReasonChecker();" id="RejectReason" name="RejectReason" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer" id="rejectSets">
                        <button type="button" id="continue" disabled onclick="rejectModal()" class="btn btn-primary">Continue</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>


    </body>

    @push('scripts')
    <script>
        function rejectReasonChecker(){
            if(document.getElementById('RejectReason').value!="")
                document.getElementById('continue').disabled=false;
            else
                document.getElementById('continue').disabled=true;
        }
        function approveModal(){
            document.getElementById('modalMessage').innerHTML = "Approve this request?";
            $('#approveSet').show();
            $('#rejectSet').hide();
            $('#approveCOSet').hide();
            $('#rejectCOSet').hide();
            //$('#cancelSet').hide();
            $('#updateModal').modal('show');
        }
        function rejectModal(){
            document.getElementById('modalMessage').innerHTML = "Reject this request?";
            $('#approveSet').hide();
            $('#rejectSet').show();
            $('#approveCOSet').hide();
            $('#rejectCOSet').hide();
            //$('#cancelSet').hide();
            $('#rejectModal').modal('hide');
            $('#updateModal').modal('show');
        }
        function cancelModal(){
            document.getElementById('modalMessage').innerHTML = "Cancel this request?";
            $('#approveSet').hide();
            $('#rejectSet').hide();
            $('#cancelSet').show();
            $('#updateModal').modal('show');
        }
        function checkoutModal(){
            document.getElementById('modalMessage').innerHTML = "Approve request for check-out?";
            $('#approveCOSet').show();
            $('#approveSet').hide();
            $('#rejectSet').hide();
            $('#rejectCOSet').hide();
            //$('#cancelSet').hide();
            $('#updateModal').modal('show');
        }
    </script>

    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endpush
@endsection