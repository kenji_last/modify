@extends('layouts.admin')
@section('title', 'View Outgoing')
    {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_approverview')))
        @section('in_approver_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_checkoutview')))
        @section('in_checkout_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_approvals')))
        @section('in_my_approvals', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_rejections')))
        @section('in_my_rejections', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_co_approvals')))
        @section('in_my_co_approvals', 'active')
    @endif--}}
@section('approver', 'active')
    @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_approver')))
        @if($code=="ci_for_approval")
            @section('in_approver_ci_for_approval', 'active')
        @elseif($code=="ci_approved")
            @section('in_approver_ci_approved', 'active')
        @elseif($code=="ci_rejected")
            @section('in_approver_ci_rejected', 'active')
        @elseif($code=="co_for_approval")
            @section('in_approver_co_for_approval', 'active')
        @elseif($code=="co_approved")
            @section('in_approver_co_approved', 'active')
        @elseif($code=="co_rejected")
            @section('in_approver_co_rejected', 'active')
        @endif
    @endif
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">

    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default" style="max-height:100%;height:90% !important;">
                    <div class="panel-heading">
                        @if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_approver')))
                            @if($code=="ci_for_approval")
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - For Check-In</h4>
                            @elseif($code=="ci_approved")
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - Approved Requests</h4>
                            @elseif($code=="ci_rejected")
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - Rejected Requests</h4>
                            @elseif($code=="co_for_approval")
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - For Check-Out</h4>
                            @elseif($code=="co_approved")
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - Approved Check-Out Requests</h4>
                            @elseif($code=="co_rejected")
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - Rejected Check-Out Requests</h4>
                            @endif
                        @endif

                            {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_approverview')))
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - For Check-In</h4>
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_approvals')))
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - Approved Requests</h4>
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_rejections')))
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - Rejected Requests</h4>
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_checkoutview')))
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - For Check-Out</h4>
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_co_approvals')))
                                <h4><i class="fa fa-user"></i>&nbsp;Incoming Requests - Approved Check-Out Requests</h4>
                            @endif--}}

                    </div>
                    <div class="panel-body">
                            <table class="table table-bordered table-striped" id="example1" style="font-size:12px !important;">
                                <thead>
                                <th>&nbsp;</th>
                                <th>Request No.</th>
                                <th>Status</th>
                                <th>B. Partner Name</th>
                                <th>B. Partner Type</th>
                                <th>Contact</th>
                                <th>Host</th>
                                <th>Date</th>
                                </thead>
                                <tbody>
                                @foreach($myRequests as $mR)
                                    <tr>
                                            {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_approverview')))
                                                <td><a href="{{url('in_approverview/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_approvals')))
                                                <td><a href="{{url('in_my_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_rejections')))
                                                <td><a href="{{url('in_my_rejections/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_checkoutview')))
                                                <td><a href="{{url('in_checkoutview/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_my_co_approvals')))
                                                <td><a href="{{url('in_my_co_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @endif--}}

                                                {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_approver')))
                                                    @if($code=="ci_for_approval")
                                                        <td><a href="{{url('in_approverview/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="ci_approved")
                                                        <td><a href="{{url('in_my_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="ci_rejected")
                                                        <td><a href="{{url('in_my_rejections/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="co_for_approval")
                                                        <td><a href="{{url('in_checkoutview/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="co_approved")
                                                        <td><a href="{{url('in_my_co_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="co_rejected")
                                                        <td><a href="{{url('in_my_co_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @endif
                                                @endif--}}
                                        <td><a href="{{url('in_approver/'.$code.'/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                        </td>
                                        <td>{{$mR->RequestNo}}</td>
                                        <td>{{$mR->oStatus->Name}}</td>
                                        <td>{{$mR->BusinessPartnerName}}</td>
                                        <td>{{$mR->oBusinessPartner->Name}}</td>
                                        <td>{{$mR->Contact}}</td>
                                        <td>{{$mR->oUser->FirstName}} {{$mR->oUser->LastName}}</td>
                                        <td>{{date('F d, Y', strtotime($mR->Date))}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endpush
@endsection