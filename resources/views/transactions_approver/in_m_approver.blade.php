@extends('layouts.admin')
@section('title', 'Message')
@section('header_title', 'Message')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;margin:0 25% 0 25%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="fa fa-square"></i>&nbsp;&nbsp;{{$title}}
                        </h4>
                    </div>
                        <div class="panel-body">
                            <div style="padding:0 15% 0 15%;text-align:center;">
                                <h5>
                                    {{$message}}
                                </h5>
                                <br>
                                <br>
                                @if(isset($restriction) && $restriction==1)
                                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-default">Go back</a>
                                @else
                                    <a href="{{url('in_approver/ci_for_approval')}}" class="btn btn-sm btn-default">Incoming Requests</a>
                                @endif
                                {{--<a href="{{ URL::previous() }}" class="btn btn-sm btn-default">Go back</a>--}}
                                <a href="{{url('dBoard')}}" class="btn btn-sm btn-default">Home</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endpush
@endsection