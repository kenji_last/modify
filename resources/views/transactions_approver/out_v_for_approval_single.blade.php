@extends('layouts.admin')
@section('title', 'View Outgoing')
@section('approver', 'active')
    {{--@if($singleRequest->Status==2)
        @section('in_approver_view', 'active')
    @elseif($singleRequest->Status==6)
        @section('in_checkout_view', 'active')--}}
    {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'out_approverview')))
        @section('out_approver_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_checkoutview')))
        @section('out_checkout_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_my_approvals')))
        @section('out_my_approvals', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_my_rejections')))
        @section('out_my_rejections', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_my_co_approvals')))
        @section('out_my_co_approvals', 'active')
    @endif--}}
    @if((starts_with(Route::getCurrentRoute()->getPath(), 'out_approver')))
        @section('out_approver_'.$code, 'active')
    @endif
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4><i class="fa fa-user"></i>&nbsp;
                                {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'out_approverview')))
                                    <a href="{{url('out_approverview')}}">Outgoing Request</a> - {{$singleRequest->RequestNo}}
                                @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'out_my_approvals')))
                                    <a href="{{url('out_my_approvals')}}">Outgoing Request</a> - {{$singleRequest->RequestNo}}
                                @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'out_my_rejections')))
                                    <a href="{{url('out_my_rejections')}}">Outgoing Request</a> - {{$singleRequest->RequestNo}}
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_checkinview')))
                                    <a href="{{url('out_checkinview')}}">Outgoing Request</a> - {{$singleRequest->RequestNo}}
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_my_ci_approvals')))
                                    <a href="{{url('out_my_ci_approvals')}}">Outgoing Request</a> - {{$singleRequest->RequestNo}}
                                @endif--}}
                                @if((starts_with(Route::getCurrentRoute()->getPath(), 'out_approver')))
                                    <a href="{{url('out_approver/'.$code)}}">Outgoing Request</a> - {{$singleRequest->RequestNo}}
                                @endif
                            </h4>
                        @if($singleRequest->Status==8)
                            <small>
                                        <span>
                                            {{--&nbsp;&nbsp;&nbsp;&nbsp;
                                            Check-Out History&nbsp;
                                            @foreach($coVersion as $ver)
                                                @if($ver->checkin_ID!=0)
                                                    <a href="{{url('in_co_view/'.$singleRequest->id."/".$ver->checkin_ID)}}">{{$ver->checkin_ID}}</a>&nbsp;
                                                @endif
                                            @endforeach--}}
                                        </span>
                            </small>
                        @endif
                    </div>
                    <div class="panel-body" style="padding-left:10%;padding-right:10%;padding-top:2%;padding-bottom:1%;">
                        <div class="form-group col-md-12">
                            @if($singleRequest->Status==2)
                                <button type="button" id="approve" onclick="approveModal()" class="btn btn-sm btn-primary">Approve</button>
                                <button type="button" id="reject" onclick="$('#rejectModal').modal('show');" class="btn btn-sm btn-primary">Reject</button>
                            @elseif($singleRequest->Status==6)
                                <button type="button" id="checkOut" onclick="checkoutModal()" class="btn btn-sm btn-primary">Approve Check-In</button>
                                <button type="button" id="checkOut" onclick="$('#rejectModal').modal('show');" class="btn btn-sm btn-primary">Reject Check-In</button>
                            @endif

                        </div>
                        <div class=" col-md-6">
                            <label>Request Number:</label>
                            {{$singleRequest->RequestNo}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>B. Partner Type:</label>
                            {{$singleRequest->oBusinessPartner->Name}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Status:</label>
                            {{$singleRequest->oStatus->Name}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Business Partner:</label>
                            {{$singleRequest->BusinessPartnerName}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date:</label>
                            {{date('F d, Y', strtotime($singleRequest->Date))}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact:</label>
                            {{$singleRequest->Contact}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Host:</label>
                            {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Email:</label>
                            {{$singleRequest->ContactEmail}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Request Type:</label>
                            {{$singleRequest->oRequest->Name}}
                        </div>
                        @if($singleRequest->Status == 3 || $singleRequest->Status == 4 || $singleRequest->Status == 11)
                            <div class="form-group col-md-6">
                                <label>Approved By:</label>
                                {{$singleRequest->oheadCOApprovedBy_to_user->FirstName}} {{$singleRequest->oheadCOApprovedBy_to_user->LastName}}
                            </div>
                            @if(isset($singleRequest->COAccountingApprovedBy))
                                <div class="form-group col-md-6">
                                    <label>Accounting Approver:</label>
                                    {{$singleRequest->oheadCOAccounting_to_user->FirstName}} {{$singleRequest->oheadCOAccounting_to_user->LastName}}
                                </div>
                            @endif
                        @elseif($singleRequest->Status == 10)
                            <div class="form-group col-md-6">
                                <label>Rejected By:</label>
                                {{$singleRequest->oheadRejectedBy_to_user->FirstName}} {{$singleRequest->oheadRejectedBy_to_user->LastName}}
                            </div>
                            <div class="form-group col-md-6">
                                <label>Reject Reason:</label><br>
                                {{$singleRequest->CORejectReason}}
                            </div>
                        @endif


                        <div class="form-group col-xs-12">
                            <div id="aHeaders" class="box-primary" style="align-content: center;">
                                <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                    <div class="col-xs-2">Description</div>
                                    <div class="col-xs-2">Purpose</div>
                                    {{--<div class="col-xs-4">Serial Number</div>--}}
                                    <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                    <div class="col-xs-1" style="text-align: center;">UOM</div>
                                    <div class="col-xs-2" style="text-align: center;">Returnable?</div>
                                    <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                    <div class="col-xs-2" style="text-align: center;">Check-In</div>

                                </div>
                            </div>
                            <div id="aBody" class="box-primary" style="
                                    align-content: center;
                                    height:260px;
                                    max-height:260px;
                                    overflow-y:scroll;
                                    padding-bottom:0px;">
                                @foreach($lineItems as $li)
                                    <div class="row" id='divViewtheTableCount+"' style="
                                        width:100%;
                                        margin-left:0px;
                                        margin-bottom:0px;
                                        margin-top:0px;
                                        padding:5px;
                                        font-size:12px;
                                        border-bottom: 1px solid #e9e9e9;">
                                        <div class="col-xs-2" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                        <div class="col-xs-2" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Purpose}}</div>
                                        <div class="col-xs-1" style='padding:9px 14px; text-align: center;'>{{$li->Quantity}}</div>
                                        <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->Returnable==0)
                                                No
                                            @else
                                                Yes
                                            @endif
                                        </div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->OutDate!="")
                                                {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->Returnable==1)
                                                @if($li->InDate!="")
                                                    {{date('M d, Y h:i A',strtotime($li->InDate))}}

                                                @else
                                                    -
                                                @endif
                                            @else
                                                N/A
                                            @endif
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                            {{--<div class="form-group col-xs-12">
                                <button type="button" id="approve" onclick="approveModal()" class="btn btn-primary">Approve</button>
                                <button type="button" id="reject" onclick="$('#rejectModal').modal('show');" class="btn btn-primary">Reject</button>
                                <button type="button" id="cancel" onclick="cancelModal()" class="btn btn-primary">Cancel</button>
                            </div>--}}
                        </div>
                </div>
            </div>
            </div>
        </div>

        <div id="updateModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;" id="modalMessage">

                        </div>
                    </div>
                    <div class="modal-footer" id="approveSet">
                        {!! Form::open(array('url' => 'out_approve')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer" id="approveCOSet">
                        {!! Form::open(array('url' => 'out_ci_approve')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                        {{--{!! Form::close() !!}--}}
                    </div>
                    <div class="modal-footer" id="rejectSet">

                        @if($singleRequest->Status==2)
                            {!! Form::open(array('url' => 'out_reject')) !!}
                        @elseif($singleRequest->Status==6)
                            {!! Form::open(array('url' => 'out_ci_reject')) !!}
                        @endif
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    </div>


                    {{--<div class="modal-footer" id="cancelSet">
                        {!! Form::open(array('url' => 'in_cancel')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        --}}{{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}{{--
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}

                    </div>--}}
                </div>

            </div>
        </div>

        <div id="rejectModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;">
                            Please specify the reason for rejecting this request.
                            <textarea style="resize: none;" onkeyup="rejectReasonChecker();" id="RejectReason" name="RejectReason" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer" id="rejectSets">
                        <button type="button" id="continue" disabled onclick="rejectModal()" class="btn btn-primary">Continue</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>


    </body>

    @push('scripts')
    <script>
        function rejectReasonChecker(){
            if(document.getElementById('RejectReason').value!="")
                document.getElementById('continue').disabled=false;
            else
                document.getElementById('continue').disabled=true;
        }
        function approveModal(){
            document.getElementById('modalMessage').innerHTML = "Approve this request?";
            $('#approveSet').show();
            $('#rejectSet').hide();
            $('#approveCOSet').hide();
            $('#rejectCOSet').hide();
            //$('#cancelSet').hide();
            $('#updateModal').modal('show');
        }
        function rejectModal(){
            document.getElementById('modalMessage').innerHTML = "Reject this request?";
            $('#approveSet').hide();
            $('#rejectSet').show();
            $('#approveCOSet').hide();
            $('#rejectCOSet').hide();
            //$('#cancelSet').hide();
            $('#rejectModal').modal('hide');
            $('#updateModal').modal('show');
        }
        function cancelModal(){
            document.getElementById('modalMessage').innerHTML = "Cancel this request?";
            $('#approveSet').hide();
            $('#rejectSet').hide();
            $('#cancelSet').show();
            $('#updateModal').modal('show');
        }
        function checkoutModal(){
            document.getElementById('modalMessage').innerHTML = "Approve request for check-out?";
            $('#approveCOSet').show();
            $('#approveSet').hide();
            $('#rejectSet').hide();
            $('#rejectCOSet').hide();
            //$('#cancelSet').hide();
            $('#updateModal').modal('show');
        }
    </script>

    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endpush
@endsection