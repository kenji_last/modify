@extends('layouts.admin')
@section('title', 'View Outgoing')
    @if($singleRequest->Status==2)
        @section('in_approver_view', 'active')
    @elseif($singleRequest->Status==6)
        @section('in_checkout_view', 'active')
    @endif
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_securityview')))
                            <h4><i class="fa fa-user-secret"></i>&nbsp;<a href="{{url('in_securityview')}}">Incoming Requests</a> - {{$singleRequest->RequestNo}}</h4>
                        @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_security_co_view')))
                            <h4><i class="fa fa-user-secret"></i>&nbsp;<a href="{{url('in_security_co_view')}}">Incoming Requests</a> - {{$singleRequest->RequestNo}}</h4>
                        @endif



                    </div>
                    <div class="panel-body" style="padding-left:10%;padding-right:10%;padding-top:2%;padding-bottom:1%;">
                        <div class="form-group col-md-12">
                            @if($singleRequest->Status==3)
                                <button type="button" id="approve" onclick="approveModal()" class="btn btn-sm btn-primary">Verify</button>
                                {{--<button type="button" id="reject" --}}{{--onclick="$('#rejectModal').modal('show');"--}}{{-- class="btn btn-sm btn-primary">Print</button>--}}
                            @elseif($singleRequest->Status==7)
                                    <button type="button" id="approve" onclick="checkoutModal()" class="btn btn-sm btn-primary">Verify</button>
                            @endif

                        </div>
                        <div class=" col-md-6">
                            <label>Request Number:</label>
                            {{$singleRequest->RequestNo}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>B. Partner Type:</label>
                            {{$singleRequest->oBusinessPartner->Name}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Status:</label>
                            {{$singleRequest->oStatus->Name}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Business Partner:</label>
                            {{$singleRequest->BusinessPartnerName}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date:</label>
                            {{date('F d, Y', strtotime($singleRequest->Date))}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact:</label>
                            {{$singleRequest->Contact}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Host:</label>
                            {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Email:</label>
                            {{$singleRequest->ContactEmail}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Approved By:</label>
                            {{$singleRequest->iheadApprovedBy_to_user->FirstName}} {{$singleRequest->iheadApprovedBy_to_user->LastName}}
                        </div>
                        <div class="form-group col-xs-12">
                            <div id="aHeaders" class="box-primary" style="align-content: center;">
                                <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                    <div class="col-xs-4">Description</div>
                                    {{--<div class="col-xs-4">Serial Number</div>--}}
                                    <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                    <div class="col-xs-1" style="text-align: center;">UOM</div>
                                    <div class="col-xs-2" style="text-align: center;">Returnable?</div>
                                    <div class="col-xs-2" style="text-align: center;">Check-In</div>
                                    <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                </div>
                            </div>
                            <div id="aBody" class="box-primary" style="
                                    align-content: center;
                                    height:260px;
                                    max-height:260px;
                                    overflow-y:scroll;
                                    padding-bottom:0px;">
                                @foreach($lineItems as $li)
                                    <div class="row" id='divViewtheTableCount+"' style="
                                        width:100%;
                                        margin-left:0px;
                                        margin-bottom:0px;
                                        margin-top:0px;
                                        padding:5px;
                                        font-size:12px;
                                        border-bottom: 1px solid #e9e9e9;">
                                        <div class="col-xs-4" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                        <div class="col-xs-1" style='padding:9px 14px; text-align: center;'>{{$li->Quantity}}</div>
                                        <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->Returnable==0)
                                                No
                                            @else
                                                Yes
                                            @endif
                                        </div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->InDate!="")
                                                {{date('M d, Y h:i A',strtotime($li->InDate))}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                        <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                            @if($li->Returnable==1)
                                                @if($li->OutDate!="")
                                                    {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                                @else
                                                    -
                                                @endif
                                            @else
                                                N/A
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                            {{--<div class="form-group col-xs-12">
                                <button type="button" id="approve" onclick="approveModal()" class="btn btn-primary">Approve</button>
                                <button type="button" id="reject" onclick="$('#rejectModal').modal('show');" class="btn btn-primary">Reject</button>
                                --}}{{--<button type="button" id="cancel" onclick="cancelModal()" class="btn btn-primary">Cancel</button>--}}{{--
                            </div>--}}
                        </div>
                </div>
            </div>
            </div>
        </div>

        <div id="updateModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;" id="modalMessage">

                        </div>
                    </div>
                    <div class="modal-footer" id="approveSet">
                        {!! Form::open(array('url' => 'in_securityview')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer" id="rejectSet">
                        {!! Form::open(array('url' => 'in_reject')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                        {{--{!! Form::close() !!}--}}
                    </div>

                    <div class="modal-footer" id="approveCOSet">
                        {!! Form::open(array('url' => 'in_security_co_view')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                        {{--{!! Form::close() !!}--}}
                    </div>
                    {{--<div class="modal-footer" id="cancelSet">
                        {!! Form::open(array('url' => 'in_cancel')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        --}}{{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();">Ok</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}{{--
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}

                    </div>--}}
                </div>

            </div>
        </div>

        <div id="rejectModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;">
                            Please specify the reason for rejecting this request.
                            <textarea style="resize: none;" onkeyup="rejectReasonChecker();" id="RejectReason" name="RejectReason" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer" id="rejectSets">
                        <button type="button" id="continue" disabled onclick="rejectModal()" class="btn btn-primary">Continue</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>


    </body>

    @push('scripts')
    <script>
        function rejectReasonChecker(){
            if(document.getElementById('RejectReason').value!="")
                document.getElementById('continue').disabled=false;
            else
                document.getElementById('continue').disabled=true;
        }
        function approveModal(){
            document.getElementById('modalMessage').innerHTML = "Please ensure that you have printed the Barcode labels before verifying this for check-in.<br><br> Do you wish to continue?";
            $('#approveSet').show();
            $('#rejectSet').hide();
            $('#approveCOSet').hide();
            //$('#cancelSet').hide();
            $('#updateModal').modal('show');
        }
        function rejectModal(){
            /*document.getElementById('modalMessage').innerHTML = "Reject this request?";
            $('#approveSet').hide();
            $('#rejectSet').show();
            $('#approveCOSet').hide();
            //$('#cancelSet').hide();
            $('#rejectModal').modal('hide');
            $('#updateModal').modal('show');*/
        }
        function cancelModal(){
            document.getElementById('modalMessage').innerHTML = "Cancel this request?";
            $('#approveSet').hide();
            $('#rejectSet').hide();
            $('#cancelSet').show();
            $('#updateModal').modal('show');
        }
        function checkoutModal(){
            document.getElementById('modalMessage').innerHTML = "Verify this request for check-out?";
            $('#approveCOSet').show();
            $('#approveSet').hide();
            $('#rejectSet').hide();
            //$('#cancelSet').hide();
            $('#updateModal').modal('show');
        }
    </script>

    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endpush
@endsection