@extends('layouts.admin')
@section('title', 'View Incoming')
@section('audit', 'active')

    @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_a_all')))
        @section('in_a_all', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_a_all')))
        @section('out_a_all', 'active')
    @endif

@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">

    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default" style="max-height:100%;height:90% !important;">
                    @if(isset($statusName))
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-9">
                                    <h4><i class="fa fa-user"></i>&nbsp;{{$statusName->Name}} Incoming Requests</h4>
                                </div>
                                <div class="col-xs-3">
                                    <select class="form-control" onchange="sortRequests(value);">
                                        <option value="">All</option>
                                        @foreach($status as $st)
                                            @if($st->Name == $statusName->Name)
                                                <option value="/{{$st->id}}" selected>{{$st->Name}}</option>
                                            @else
                                                <option value="/{{$st->id}}">{{$st->Name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-9">
                                    <h4><i class="fa fa-user"></i>&nbsp;My Incoming Requests</h4>
                                </div>
                                <div class="col-xs-3">
                                    {{--<select class="form-control" onchange="sortRequests(value);">
                                        <option value="" selected>All</option>
                                        @foreach($status as $st)
                                                <option value="/{{$st->id}}">{{$st->Name}}</option>
                                        @endforeach
                                    </select>--}}
                                </div>
                            </div>
                        </div>
                    @endif
                        <div class="panel-body">
                            @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_a_all')))
                                <a href="{{url('excel_a_checkin')}}" class="btn btn-sm btn-primary">Export to Excel</a>
                                <table class="table table-bordered table-striped" id="example1" style="font-size:12px !important;">
                                    <thead>
                                    <th>Request No.</th>
                                    <th>Control No.</th>
                                    <th>Description</th>
                                    <th>Check-In Date</th>
                                    <th>Checked-In</th>
                                    </thead>
                                    <tbody>
                                    @foreach($myCILineItems as $ci)
                                        <tr>
                                            <td>{{$ci->iline_to_ihead->RequestNo}}</td>
                                            <td>{{$ci->ControlNum}}</td>
                                            <td>{{$ci->Description}}</td>
                                            <td>{{date('M d, Y h:i A',strtotime($ci->InDate))}}</td>
                                            <td>
                                                <?php
                                                $datetime1 = date_create(date('Y-m-d',strtotime($ci->InDate)));
                                                $datetime2 = date_create(date('Y-m-d'));
                                                $interval = date_diff($datetime1, $datetime2);
                                                if ($interval->format('%a')=='0')
                                                    echo "Today";
                                                else
                                                    echo $interval->format('%a days ago');
                                                ?>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_a_all')))
                                <a href="{{url('excel_a_checkout')}}" class="btn btn-sm btn-primary">Export to Excel</a>
                                <table class="table table-bordered table-striped" id="example1" style="font-size:12px !important;">
                                    <thead>
                                    <th>Request No.</th>
                                    <th>Control No.</th>
                                    <th>Description</th>
                                    <th>Check-Out Date</th>
                                    <th>Checked-Out</th>
                                    </thead>
                                    <tbody>
                                    @foreach($myCILineItems as $ci)
                                        <tr>
                                            <td>{{$ci->oline_to_ihead->RequestNo}}</td>
                                            <td>{{$ci->ControlNum}}</td>
                                            <td>{{$ci->Description}}</td>
                                            <td>{{date('M d, Y h:i A',strtotime($ci->OutDate))}}</td>
                                            <td>
                                                <?php
                                                $datetime1 = date_create(date('Y-m-d',strtotime($ci->OutDate)));
                                                $datetime2 = date_create(date('Y-m-d'));
                                                $interval = date_diff($datetime1, $datetime2);
                                                if ($interval->format('%a')=='0')
                                                    echo "Today";
                                                else
                                                    echo $interval->format('%a days ago');
                                                ?>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif



                        </div>
                    </div>
            </div>
        </div>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
    <script>
        function sortRequests(id){
            if(id=="")
                window.location.href = "{{ url('in_myrequests') }}";
            else
               window.location.href = "{{ url('in_myrequests') }}"+id;
        }
    </script>
@endpush
@endsection