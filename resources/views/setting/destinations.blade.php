@extends('layouts.admin')
@section('title', 'Destinations')
@section('page_title', 'Manage Destinations')
@section('current', 'Users')
@section('destinations', 'active')
@section('side_othersetting_treeview', 'active')
@section('content')
    <section class="content">
        <div id="elemuom">
            @if (Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}<br>
                </div>
            @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-default">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Manage Destinations</h3>
                                    </div>
                                    <div class="box-body" style="padding-left:5%;">
                                        @if(starts_with(Route::getCurrentRoute()->getPath(), 'destinations'))
                                            {!! Form::open(array('url' => 'save_destination')) !!}
                                        @else
                                            {!! Form::open(array('url' => 'update_destination/' . $select_destination->id)) !!}
                                        @endif

                                        <div class="form-group">
                                            <label>Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Administration Area</em></small>
                                            @if(starts_with(Route::getCurrentRoute()->getPath(), 'destinations'))
                                                <input name="dName" class="form-control" type="text" v-model="dName" required> </input>
                                            @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_destinations/'))
                                                <input name="dName" value="{{ $select_destination->Name}}" class="form-control" type="text" v-model="uom"> </input>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label>Sub-Location</label>&nbsp;&nbsp;
                                            @if(starts_with(Route::getCurrentRoute()->getPath(), 'destinations'))
                                                <input type="checkbox" name="SubLocation" id="SubLocation" onclick="subLoc();">
                                            @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_destinations/'))
                                                @if($select_destination->SubLocation==TRUE)
                                                    <input type="checkbox" name="SubLocation" checked id="SubLocation" onclick="subLoc();">
                                                @else
                                                    <input type="checkbox" name="SubLocation" id="SubLocation" onclick="subLoc();">
                                                @endif
                                            @endif
                                        </div>

                                        @if(starts_with(Route::getCurrentRoute()->getPath(), 'destinations'))
                                            <div id="parentLocationBox" style="display:none;" class="form-group">
                                        @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_destinations/'))
                                            @if($select_destination->SubLocation==TRUE)
                                                <div id="parentLocationBox" style="display:block;" class="form-group">
                                            @else
                                                <div id="parentLocationBox" style="display:none;" class="form-group">
                                            @endif
                                        @endif

                                            <label>Parent Location</label>
                                            <select class="form-control" name="parentLocation" id="parentLocation" onchange="parentLoc();">
                                                @if(starts_with(Route::getCurrentRoute()->getPath(), 'destinations'))
                                                    <option value="NULL" disabled selected hidden>Please Choose...</option>
                                                    @foreach($parentDestinations as $pd)
                                                        <option value="{{ $pd->id }}">{{ $pd->Name }}</option>
                                                    @endforeach
                                                @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_destinations/'))
                                                    @if($select_destination->Parent!=NULL)
                                                        @foreach($parentDestinations as $pd)
                                                            @if($pd->id == $select_destination->Parent)
                                                                <option value="{{ $pd->id }}" selected>{{ $pd->Name }}</option>
                                                            @else
                                                                <option value="{{ $pd->id }}">{{ $pd->Name }}</option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value="NULL" disabled selected hidden>Please Choose...</option>
                                                        @foreach($parentDestinations as $pd)
                                                            <option value="{{ $pd->id }}">{{ $pd->Name }}</option>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </select>
                                            @if ($errors->has('department')) <p class="help-block">{{ $errors->first('department') }}</p> @endif
                                        </div>
                                        @if(starts_with(Route::getCurrentRoute()->getPath(), 'destinations'))

                                        @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_destinations/'))
                                            <div class="form-group">
                                                <label>Active</label>&nbsp;&nbsp;
                                                {!! Form::checkbox('Active', $select_destination->Active, ($select_destination->Active) ? true: false,['data-tooltip'=>'tooltip','title'=>'Super Approver role']) !!}
                                            </div>
                                        @endif

                                        <div class="box-footer">
                                            {!! Form::submit(
                                            'Submit',
                                            array(
                                            'class'=>'btn btn-primary',
                                            'id'=>'submit_destination',
                                            'data-tooltip'=>'tooltip',
                                            'title'=>'Add'
                                            ))
                                            !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">&nbsp;</h3>
                                    </div>
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Sub</th>
                                            <th>Parent</th>
                                            <th>Active</th>
                                            <th>Action</th>
                                            </thead>
                                            <tbody>
                                            <?php $no=1; ?>
                                            @foreach($destinations as $destinations)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $destinations->Name }}</td>
                                                    <td>{!! ($destinations->SubLocation == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                                                    <td>{{ ($destinations->Parent != NULL)? $destinations->sub_to_parent->Name : '&nbsp;' }}</td>
                                                    <td>{!! ($destinations->Active == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                                                    <td><a href="{{ route('edit_destinations', $destinations->id) }}">Edit</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
@endsection
@push('scripts')
<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="{{ asset('/js/uom.js') }}"></script>

<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
    $('#example1').on('click', 'tbody tr', function(event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
        var trid = $(this).attr('id');
        console.log(trid);
        // $('#myModal').modal('show');
    });
    /*$(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

    });*/
    function subLoc(){
        if(document.getElementById("SubLocation").checked)
        {
            parentLoc();
            document.getElementById("parentLocationBox").style.display = "block";
        }
        else
        {
            document.getElementById("parentLocationBox").style.display = "none";
            document.getElementById("submit_destination").disabled = false;
        }
    }
    function parentLoc(){
        var parentLoc = document.getElementById("parentLocation");
        var division = parentLoc.options[parentLoc.selectedIndex].value;
        if(division=="NULL"){
            document.getElementById("submit_destination").disabled = true;
        }
        else{
            document.getElementById("submit_destination").disabled = false;
        }
    }

</script>
@endpush