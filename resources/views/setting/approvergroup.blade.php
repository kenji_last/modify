@extends('layouts.admin')
@section('title', 'Approver Groups')
@section('page_title', 'Approver Groups Management')
@section('approvergroup', 'active')
@section('content')
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/all.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap-duallistbox.css') }}" rel="stylesheet">

    <style>
        #example1 tr {
            cursor: pointer;
        }
        #example1 tbody tr.highlight td {
            background-color: #9DD5F5;
        }
        #example1 tbody tr:hover{
            background-color: #9DD5F5 !important;
        }
    </style>

    {!! Form::open(array('url' => 'addapproverstages')) !!}
    {!! Form::token() !!}
    <div class="box-header" style="padding-left:25px;">
        <h3 class="box-title">Add New Approver Group</h3>
    </div>
    <div class="row" style="padding-left:45px;">

        <div class="col-md-4" >
            <label>Description</label>
            <div class="form-group has-feedback {{ $errors->has('purpose') ? 'has-error' : '' }}">
                {!! Form::text('description', old('description'), ['id'=>'newDescription','onkeyup'=>'addApprovalGroupChecker();','class'=>'form-control', 'placeholder'=>'Description','maxlength' => 50]) !!}
                <span class="glyphicon glyphicon-list-alt form-control-feedback"></span>
                @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
            </div>
        </div>
        <div class="col-md-4" >
            <label>Division</label>
            <select class="form-control" id="newDivision" name="division" onchange="addApprovalGroupChecker();">
                <option value="">---</option>
                @foreach($div as $division)
                    <option value="{{$division->id}}">{{ $division->Name }}</option>
                @endforeach
            </select>
            @if ($errors->has('division')) <p class="text-danger">Please select a Department.</p> @endif
        </div>
    </div>
    <div class="row" style="padding-left:45px;">
        <div class="col-md-4" >
            <label>&nbsp;</label>
            <button type="submit" disabled id="submitNew" class="btn btn-primary btn-block btn-flat">Add</button>
        </div>
    </div>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List Approver Groups</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                        <table id="example2" class="table table-bordered table-striped">
                            <thead class="alert-info">
                            <th></th>
                            <th>Description</th>
                            <th>Division</th>
                            <th style="width:25px;">Active</th>
                            <th>&nbsp;</th>

                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($supp as $super)
                                <tr>
                                    <td style="width:5%;">{{$no++}}</td>
                                    <td>{{ $super->Name}}</td>
                                    <td>{{ $super->toDivision->Name}}</td>
                                    <td align="center">{!! ($super->Active == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                                    <td style="width:18%;">
                                        <?php $hasPending=0 ?>
                                        @foreach($test2 as $themPending)
                                            @if($themPending->as_id == $super->as_id)
                                                <?php $hasPending=1 ?>
                                            @endif
                                        @endforeach
                                        <button type="button" class="btn btn-primary btn-sm" onclick="modalSelect({{ $super->id }},'{{$super->Name}}',{{$super->Division_ID}},{{$super->Active}});" id="{{ $super->id }}" data-target="#suppModal_{{ $super->id }}" data-toggle="modal" data_value="{{ $super->as_id }}">
                                            <i class="fa fa-btn fa-pencil"></i>&nbsp;&nbsp;Edit</button>
                                            <a href="{{url('approvergroup_approvers/6e6f/'.$super->id)}}" class="btn btn-info btn-sm">
                                            <i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Approvers</a>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::close() !!}
    <div id="updateModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('url' => 'edit_approvergroup')) !!}
                    {!! Form::token() !!}
                    <input type="hidden" id="editID" name="editID">
                    <div class="row">
                        <div class="col-md-12" >
                            <label>Description</label>
                            <div class="form-group has-feedback {{ $errors->has('purpose') ? 'has-error' : '' }}">
                                {!! Form::text('editDescription', old('editDescription'), ['id'=>'editDescription','onkeyup'=>'editApprovalGroupChecker();','class'=>'form-control', 'placeholder'=>'Description','maxlength' => 50]) !!}
                                <span class="glyphicon glyphicon-list-alt form-control-feedback"></span>
                                @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                            </div>
                        </div>
                        <div class="col-md-12" >
                            <label>Division</label>
                            <select class="form-control" id="editDivision" name="editDivision" onchange="editApprovalGroupChecker();">
                                @foreach($div as $division)
                                    <option value="{{$division->id}}">{{ $division->Name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('division')) <p class="text-danger">Please select a Department.</p> @endif
                        </div>
                        <div class="col-md-12" >
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="editActive" id="editActive"> Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Save Changes', array('id'=>'editSave','class'=>'btn btn-primary')) !!}
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>






@endsection
@push('scripts')
<!-- DataTables -->

<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>
<script src="{{ asset('/js/jquery.bootstrap-duallistbox.js') }}"></script>

<script>
    $(function () {
        /*$("#example1").DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });*/
        $('#example2').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": false,
            "scrollY":"300px",
            "autoWidth": false
        });
    });
    function addApprovalGroupChecker(){
        var errCount = 0;

        if(document.getElementById('newDescription').value=='')
            errCount = errCount + 1;
        if(document.getElementById('newDivision').value=='')
            errCount = errCount + 1;

        if (errCount==0)
            document.getElementById('submitNew').disabled=false;
        else
            document.getElementById('submitNew').disabled=true;
    }
    function editApprovalGroupChecker(){
        var editCount = 0;

        if(document.getElementById('editDescription').value=='')
            editCount = editCount + 1;
        if(document.getElementById('editDivision').value=='')
            editCount = editCount + 1;

        if (editCount==0)
            document.getElementById('editSave').disabled=false;
        else
            document.getElementById('editSave').disabled=true;
    }
    function modalSelect(id,desc,divi,act){
        document.getElementById('editID').value=id;
        document.getElementById('editDescription').value=desc;
        //document.getElementById('editDivision').value=id;
        var element = document.getElementById('editDivision');
        element.value = divi;
        if(act==1)
        document.getElementById("editActive").checked = true;
        $('#updateModal').modal('show');
    }
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

    });

</script>
@endpush