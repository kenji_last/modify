@extends('layouts.admin')
@section('title', 'Departments/Sections')
@section('page_title', 'Manage Departments/Sections')
@section('current', 'Users')
@section('department', 'active')
@section('side_othersetting_treeview', 'active')
@section('content')
    <section class="content">
        <div id="elemdepartment">
            @if (Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}<br>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Manage Departments/Sections</h3>
                                </div>
                                <div class="box-body" style="padding-left:5%;">
                                    @if(starts_with(Route::getCurrentRoute()->getPath(), 'department'))
                                        {!! Form::open(array('url' => 'save_department')) !!}
                                    @else
                                        {!! Form::open(array('url' => 'update_department/' . $select_department->id)) !!}
                                    @endif
                                    <div class="form-group">
                                        <label>Department/Section Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Recruitment Team</em></small>
                                        @if(starts_with(Route::getCurrentRoute()->getPath(), 'department'))
                                            <input name="department" class="form-control" type="text" v-model="department" required> </input>
                                        @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_department/'))
                                            <input name="department" value="{{ $select_department->Name }}" class="form-control" type="text" v-model="department"> </input>
                                        @endif
                                    </div>

                                    <div class="form-group has-feedback {{ $errors->has('department') ? 'has-error' : '' }}">
                                        <label>Division Name</label>
                                        <select class="form-control" name="department_series" required>
                                            @if(starts_with(Route::getCurrentRoute()->getPath(), 'department'))
                                                <option value="" disabled selected hidden>Please Choose...</option>
                                            @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_department/'))
                                                <option value="{{ $select_department->id }}" selected>{{ $select_department->department_to_division->Name }}</option>
                                            @endif
                                            @foreach($division as $div)
                                                <option value="{{ $div->id }}">{{ $div->Name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('department')) <p class="help-block">{{ $errors->first('department') }}</p> @endif
                                    </div>


                                    <div class="form-group">
                                        <label>Active</label>&nbsp;&nbsp;
                                        @if(starts_with(Route::getCurrentRoute()->getPath(), 'department'))
                                            {!!  Form::checkbox('active', 'value','checked') !!}
                                        @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_department/'))
                                            {!! Form::checkbox('Active', $select_department->Active, ($select_department->Active) ? true: false,['data-tooltip'=>'tooltip','title'=>'Super Approver role']) !!}
                                        @endif
                                    </div>
                                    <div class="box-footer">
                                        {!! Form::submit(
                                        'Submit',
                                        array(
                                        'class'=>'btn btn-primary',
                                        'id'=>'submit_itinerary',
                                        'data-tooltip'=>'tooltip',
                                        'title'=>'Add'
                                        ))
                                        !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-header with-border">
                                    <h3 class="box-title">&nbsp;</h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Name</th>
                                                <th>Division</th>
                                                <th>Active</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($department as $dept)
                                            <tr>
                                                <td>{{ $dept->id }}</td>
                                                <td>{{$dept->Name}}</td>
                                                <td>{{$dept->department_to_division->Name}}</td>
                                                <td>{!! ($dept->Active == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                                                <td><a href="{{ route('edit_department', $dept->id) }}">Edit</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="{{ asset('/js/department.js') }}"></script>

<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>

<script>
    $(function () {
        $("#example1").DataTable({
            "columnDefs": [
                {
                    "visible": false,
                    "searchable": true
                }
            ],
            "paging": false,
            "scrollY":        "420px",
            "scrollCollapse": true,
            "autoWidth": false
        });

    /*$('#example1').on('click', 'tbody tr', function(event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
        var trid = $(this).attr('id');
        console.log(trid);
        // $('#myModal').modal('show');
    });*/

        //Initialize Select2 Elements
        $(".select2").select2();

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

    });

</script>
@endpush