<!-- Modal -->
<div class="modal fade" id="modalRequestor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="height:600px;width:700px;margin-left:-70px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Approval Template</h4>
      </div>
        <div class="modal-body" style="padding:25px;">
            <!-- Custom Tabs -->

            @if (Session::has('fail_apps'))
                <div><span class="text-danger"><b>Not Saved. </b>
                    {{ Session::get('fail_apps') }}<br><br></span></div>
            @endif
            <div class="nav-tabs-custom" style="height:95%;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">
                                <span id="tab-1-err" style="display:none;">
                                    <i class="fa fa-exclamation-circle" style="color:#ff0000;"></i>&nbsp;&nbsp;
                                </span>
                                Approval Template Details</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">
                            <span id="tab-2-err" style="display:none;">
                                <i class="fa fa-exclamation-circle" style="color:#ff0000;"></i>&nbsp;&nbsp;
                            </span>
                            Employees</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">
                            <span id="tab-3-err" style="display:none;">
                                <i class="fa fa-exclamation-circle" style="color:#ff0000;"></i>&nbsp;&nbsp;
                            </span>
                            Approver Group</a></li>
                    <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">
                            <span id="tab-4-err" style="display:none;">
                                <i class="fa fa-exclamation-circle" style="color:#ff0000;"></i>&nbsp;&nbsp;
                                </span>
                            Approvers</a></li>
                    {{--<li class="pull-right"><a href="{{ url('approvaltemplates') }}" class="btn btn-info btn-sm">
                            <span class="fa-stack fa-lg">
                              <i class="fa fa-refresh fa-stack-1x"></i>
                            </span>
                        </a></li>--}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <label>Description</label>
                        <div class="form-group has-feedback {{ $errors->has('purpose') ? 'has-error' : '' }}">
                            @if (Session::has('fail_apps'))
                                {!! Form::text('description', Session::get('theDesc'), ['id'=>'description','class'=>'form-control', 'placeholder'=>'Description', 'required' => 'required','maxlength' => 50]) !!}
                            @else
                                {!! Form::text('description', old('description'), ['id'=>'description','class'=>'form-control', 'placeholder'=>'Description', 'required' => 'required','maxlength' => 50]) !!}
                            @endif
                            <span class="glyphicon glyphicon-list-alt form-control-feedback"></span>
                                <p class="text-danger" id="err-description" style="display:none;">Please enter a description.</p>
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <label>Division</label>
                        <div class="form-group has-feedback {{ $errors->has('purpose') ? 'has-error' : '' }}">
                            <select class="form-control" name="department" id="department" onchange="changeFunc();">
                                <option value="">---</option>
                                @foreach($users as $user)
                                    @if (Session::has('fail_apps'))
                                        @if(Session::get('theDept')==$user->id)
                                            <option value="{{$user->id}}" selected>{{ $user->Name }}</option>
                                        @else
                                            <option value="{{$user->id}}">{{ $user->Name }}</option>
                                        @endif
                                    @else
                                        @if (Input::old('department') == $user->id)
                                            <option value="{{$user->id}}" selected>{{ $user->Name }}</option>
                                        @else
                                            <option value="{{$user->id}}">{{ $user->Name }}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            <p class="text-danger" id="err-department" style="display:none;">Please select a Division.</p>
                            <span id="loading" style="display:none;"><i class="fa fa-cog fa-spin"></i>&nbsp; Loading Employee list...</span>

                        </div>
                        <!-- /.tab-pane -->
                    </div>

                    <div class="tab-pane" id="tab_2">
                        <p class="text-danger" id="err-emp-1" style="display:none;">A requestor is not allowed to be an approver in the same Approval template</p>
                        <p class="text-danger" id="err-emp-0" style="display:none;">Please select a requestor.</p>
                            <table id="example1" class="table table-bordered table-striped" width="100%">
                                <thead>
                                    <th>Name</th>
                                    <th>Department</th>
                                </thead>
                            <tbody>
                            @foreach($theUsers as $usee)
                                <tr>
                                    @if (Session::has('fail_apps'))
                                        <?php $match=0 ?>
                                        @foreach(Session::get('theEmp') as $lol)
                                            @if($lol==$usee->id)
                                                    <?php $match=1 ?>
                                            @endif
                                        @endforeach

                                            @if($match!=0)
                                                <td><input type="checkbox" checked value="{{$usee->id}}" name="emp[]">&nbsp;&nbsp;&nbsp;{{$usee->LastName}}, {{$usee->FirstName}}</td>
                                                <td>{{$usee->Division}}</td>
                                            @else
                                                <td><input type="checkbox" value="{{$usee->id}}" name="emp[]">&nbsp;&nbsp;&nbsp;{{$usee->LastName}}, {{$usee->FirstName}}</td>
                                                <td>{{$usee->Division}}</td>
                                            @endif

                                    @else
                                        <td><input type="checkbox" value="{{$usee->id}}" name="emp[]">&nbsp;&nbsp;&nbsp;{{$usee->LastName}}, {{$usee->FirstName}}</td>
                                        <td>{{$usee->Division}}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                        <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <div class="row">
                            <label>Approver Group</label>
                            <select class="form-control" name="approvalstages" id="approvalstages">
                                <option value=null>---</option>
                                @foreach($t3st as $userx)
                                    @if (Session::has('fail_apps'))
                                        @if(Session::get('theAppz')==$userx->as_id)
                                            <option value="{{$userx->id}}" selected>{{ $userx->Name }}</option>
                                        @else
                                            <option value="{{$userx->id}}">{{ $userx->Name }}</option>
                                        @endif
                                    @else

                                        @if (Input::old('approvalstages') == $userx->as_id)
                                            <option value="{{$userx->id}}" selected>{{ $userx->Name }}</option>
                                        @else
                                            <option value="{{$userx->id}}">{{ $userx->Name }}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            <p class="text-danger" id="err-approvalstages" style="display:none;">Please select an Approver Group.</p>


                        </div>
                        <div id="oldapp" class="row">
                            <br>
                            <p class="btn btn-link pull-right" onclick="
                            $('#newapproval').show();
                            $('#oldapp').hide();
                            //$('#approvalstages').val('a');
                            $('#approvalstages').prop('disabled', true);
                            "><i class="fa fa-plus"></i>&nbsp;New Approver Group</p>
                        </div>
                        <div id="newapproval" style="display:none;" class="row">
                            <br>
                            <p class="btn btn-link pull-right" onclick="$('#newapproval').hide();$('#oldapp').show();$('#approvalstages').val('');
                            $('#approvalstages').prop('disabled', false);">
                                <i class="fa fa-minus"></i>&nbsp;Existing Approver Group
                            </p><br>
                            <br>
                                <label>New Approver Group</label>
                                <label>Description</label>
                                <div class="form-group has-feedback {{ $errors->has('purpose') ? 'has-error' : '' }}">
                                    {!! Form::text('description3', old('description3'), ['class'=>'form-control', 'id'=>'description3','placeholder'=>'Description','maxlength' => 50]) !!}
                                    <span class="glyphicon glyphicon-list-alt form-control-feedback"></span>
                                    <p class="text-danger" id="err-description3" style="display:none;">The New Approver Group Description is required.</p>
                                    @if ($errors->has('description3')) <p class="help-block">{{ $errors->first('description3') }}</p> @endif
                                </div>
                                <label>Division</label>
                                <select class="form-control" name="department3" id="department3" onchange="changeFuncDep3();">
                                    <option value="">---</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{ $user->Name }}</option>
                                    @endforeach
                                </select>
                            <p class="text-danger" id="err-department3" style="display:none;">Please select a division for the New Approver Group.</p>
                                @if ($errors->has('department3')) <p class="text-danger">Please select a Division.</p> @endif
                            <span id="loading3" style="display:none;"><i class="fa fa-cog fa-spin"></i>&nbsp; Loading Approvers list...</span>
                        </div>

                    </div>
                    <div class="tab-pane" id="tab_4">
                        <div class="row">
                            <p class="text-danger" id="err-appr-1" style="display:none;">An approver is not allowed to be a requestor in the same Approval template</p>
                            <p class="text-danger" id="err-appr-0" style="display:none;">Please select an approver.</p>
                            <table id="example3" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <th>Name</th>
                                <th>Department</th>
                                </thead>
                                <tbody>
                                @foreach($theApprovers as $usee)
                                    <tr>
                                        @if (Session::has('fail_apps'))
                                            <?php $match=0 ?>
                                            @foreach(Session::get('theEmp') as $lol)
                                                @if($lol==$usee->id)
                                                    <?php $match=1 ?>
                                                @endif
                                            @endforeach

                                            @if($match!=0)
                                                <td><input type="checkbox" checked value="{{$usee->id}}" name="appr[]">&nbsp;&nbsp;&nbsp;{{$usee->LastName}}, {{$usee->FirstName}}</td>
                                                <td>{{$usee->Division}}</td>
                                            @else
                                                <td><input type="checkbox" value="{{$usee->id}}" name="appr[]">&nbsp;&nbsp;&nbsp;{{$usee->LastName}}, {{$usee->FirstName}}</td>
                                                <td>{{$usee->Division}}</td>
                                            @endif

                                        @else
                                            <td><input type="checkbox" value="{{$usee->id}}" name="appr[]">&nbsp;&nbsp;&nbsp;{{$usee->LastName}}, {{$usee->FirstName}}</td>
                                            <td>{{$usee->Division}}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p class="text-danger">
                                @if ($errors->has('approvalstages')) Please select an Approver Group.<br>@endif
                                @if ($errors->has('description3')) The New Approver Group Description is required.<br>@endif
                                @if ($errors->has('department3')) Please select a Division for the new Approver Group.<br>@endif
                            </p>
                        </div>

                    </div>
                        <!-- /.tab-pane -->
                </div>
                    <!-- /.tab-content -->
            </div>
                <!-- nav-tabs-custom -->
            <div class="pull-bottom">
                <button type="submit" id="submitNew" class="btn btn-primary btn-block btn-flat"><i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add</button>

            </div>
        </div>

    </div>
  </div>
</div>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>