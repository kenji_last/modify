@extends('layouts.admin')
@section('title', 'Approval Templates')
@section('page_title', 'Approval Templates Management')
@section('page_subtitle', '')
@section('prev_link', 'dashboard')
@section('previous', 'Dashboard')
@section('current', 'Users')
@section('templates', 'active')
@section('user_roles_permission', 'active')
@section('content')
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/all.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">

    <style>
        #example1 tr {
            cursor: pointer;
        }
        #example1 tbody tr.highlight td {
            background-color: #9DD5F5;
        }
        #example1 tbody tr:hover{
            background-color: #9DD5F5 !important;
        }
    </style>
    @if (Session::has('flash_message'))
        <div class="alert alert-danger">
            <b>Not Saved. </b>
            {{ Session::get('flash_message') }}<br>
            {{Session::get('flash_message1')}}</div>
    @endif
    @if (Session::has('succ_message'))
        <div class="alert alert-success">
            {{ Session::get('succ_message') }}<br>
        </div>
    @endif

    {!! Form::open(array('url' => 'addApprovalTemplates')) !!}
    {!! Form::token() !!}

    @include('setting.modalRequestor')
    <div class="row" style="padding-left:45px;">
        <div class="box-header">
            <button type="button" class="btn btn-danger btn" id="raeht" data-target="#modalRequestor" data-toggle="modal">
                <i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add New Approval Template</button>
        </div><!-- /.box-header -->

    </div>
    <section class="content">
        @if (count($errors) > 0 || Session::has('fail_apps'))
            @push('scripts')
            <script>
                $('#modalRequestor').modal('show');
            </script>
            @endpush
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Approval Templates</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                        <table id="example2" class="table table-bordered table-striped">
                            <thead class="alert-info">
                            <th></th>
                            <th>Description</th>
                            <th>Division</th>
                            <th>Approver Group</th>
                            <th style="width:25px;">Active</th>
                            <th></th>
                            </thead>
                            <tbody>
                            <?php $no=1; ?>
                            @foreach($t4st as $super)
                                <tr>
                                    <td style="width:5%;">{{$no++}}</td>
                                    <td>{{ $super->atname}}</td>
                                    <td>{{ $super->diviname}}</td>
                                    <td>
                                        @foreach($t3st as $stages)
                                            @if ($super->as_id == $stages->id)
                                                <?php $theApproverz= App\ApprovalStageApprovers::where('AS_id',$super->AS_id)->where('active',1)->get();?>
                                                <a id="popoverOption{{$super->id}}"
                                                   data-content="
                                                            @foreach($theApproverz as $apps)
                                                                    {{$apps->toUsers->last_name}}, {{$apps->toUsers->first_name}}&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                            @endforeach
                                                           " rel="popover" data-placement="top" data-original-title="<b>Approvers</b>">
                                                    {{$stages->Name}}
                                                </a>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td align="center">
                                        {!! ($super->Active == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                                    <td style="width:18%;">

                                        <?php $hasPending=0 ?>
                                        @foreach($test2 as $themPending)
                                            @if($themPending->AT_id == $super->AT_id)
                                                <?php $hasPending=1 ?>
                                            @endif
                                        @endforeach
                                        @if($hasPending==1)
                                            <button type="button" disabled class="btn btn-danger btn-sm disabled" id="{{ $super->AT_id }}" onclick="modalSelect({{$super->id }},'{{$super->atname}}',{{$super->divid }},{{$super->as_id}},{{$super->Active}});"  data-toggle="modal" data_value="{{ $super->AT_id }}">
                                                <i class="fa fa-btn fa-pencil"></i>&nbsp;&nbsp;Edit</button>
                                            <a href="{{url('approvaltemplate_requestors/796573/'.$super->id)}}" class="btn btn-warning btn-sm">
                                                <i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Requestors</a>
                                        @else
                                            <button type="button" class="btn btn-primary btn-sm" id="{{ $super->AT_id }}"  onclick="modalSelect({{$super->id}},'{{$super->atname}}',{{$super->divid }},{{$super->as_id}},{{$super->Active}});"  data-toggle="modal" data_value="{{ $super->AT_id }}">
                                                <i class="fa fa-btn fa-pencil"></i>&nbsp;&nbsp;Edit</button>
                                            <a href="{{url('approvaltemplate_requestors/6e6f/'.$super->id)}}" class="btn btn-info btn-sm">
                                                <i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Requestors</a>
                                        @endif

                                    </td>
                                </tr>
                                {!! Form::close() !!}
                            @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->

            <div class="container">
                <div class="modal fade" id="updateModal" role="dialog">
                    <div class="modal-dialog modal-sm" style="width:400px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Approval Template</h4>
                            </div>
                            {!! Form::open(array('url' => 'update_approvaltemplate')) !!}
                            {!! Form::token() !!}
                            <div class="modal-body">
                                <div class="form-group has-feedback {{ $errors->has('purpose') ? 'has-error' : '' }}">
                                    <input type="hidden" name="editID" id="editID">
                                    <label>Description</label>
                                    {!! Form::text('editDescription', old('editDescription'), ['id'=>'editDescription','onkeyup'=>'editApprovalGroupChecker();','class'=>'form-control', 'placeholder'=>'Description','maxlength' => 50]) !!}
                                    <span class="glyphicon glyphicon-list-alt form-control-feedback"></span>
                                    @if ($errors->has('editDescription')) <p class="help-block">{{ $errors->first('description1') }}</p> @endif
                                </div>
                                <div class="form-group" >
                                    <label>Division</label>
                                    <select class="form-control" name="editDivision" id="editDivision">
                                        @foreach($users as $user)
                                                <option value="{{$user->id}}">{{ $user->Name }}</option>
                                        @endforeach
                                    </select>
                                    <!--<button type="submit" class="btn btn-primary btn-block btn-flat">Add</button>-->
                                </div>
                                <div class="form-group" >
                                    <label>Approver Group</label>
                                    {{--<input type="hidden" name="role" value="{{$super->id}}">--}}
                                    <select class="form-control" name="editApprovalStage" id="editApprovalStages">
                                        @foreach($t3st as $userx)
                                            <option value="{{$userx->id}}">{{ $userx->Name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('approvalstages')) <p class="text-danger">Please select an Approver Group.</p> @endif
                                </div>
                                <div class="col-md-12" >
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="editActive" id="editActive"> Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                {!! Form::submit('OK', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section><!-- /.content -->

@endsection
@push('scripts')
<!-- DataTables -->

<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>

<script>
    function changeFunc() {
        $("#loading").show();
        var selectBox = document.getElementById("department");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        //alert(selectedValue);
        var table = $('#example1').DataTable();
        table.search(selectedValue).draw();
        setTimeout(function() {
            $("#loading").hide();
        }, 1000);
    }
    function changeFuncDep3() {
        $("#loading3").show();
        var selectBox2 = document.getElementById("department3");
        var selectedValue2 = selectBox2.options[selectBox2.selectedIndex].value;
        var table2 = $('#example3').DataTable();
        table2.search(selectedValue2).draw();
        setTimeout(function() {
            $("#loading3").hide();
        }, 1000);
    }
    @foreach($t4st as $stages)
    $('#popoverOption'+'{{$stages->id}}').popover({
        trigger: "hover",
        html : true,
        content: function() {
            return $('#popoverOption'+'{{$stages->AS_id}}').html();
        },
        title: function() {
            return $('#popoverOption'+'{{$stages->AS_id}}').html();
        }
    });
    @endforeach




    $(function () {
                $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
                    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                } );

                $("#example1").DataTable({
                    "sDom": '<"top"i>rt<"bottom"lp><"clear">',
                    "paging": false,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": false,
                    "autoWidth": false,
                    "scrollY": 200,
                    "scrollCollapse": true,
                    "columnDefs": [
                        {"targets": [ 1 ], "visible": false, "searchable": true},
                        {"targets": [ 0 ], "visible": true, "searchable": false}
                    ]
                });
                $('#example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": false,
                    "scrollY":"300px",
                    "autoWidth": false
                });
                $("#example3").DataTable({
                    "sDom": '<"top"i>rt<"bottom"lp><"clear">',
                    "paging": false,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": false,
                    "autoWidth": false,
                    "scrollY": 200,
                    "scrollCollapse": true,
                    "columnDefs": [
                        {"targets": [ 1 ], "visible": false, "searchable": true},
                        {"targets": [ 0 ], "visible": true, "searchable": false}
                    ]
                });

                var selectBox = document.getElementById("department");
                var selectedValue = selectBox.options[selectBox.selectedIndex].value;
                //alert(selectedValue);
                var table = $('#example1').DataTable();
                var table2 = $('#example3').DataTable();

                if (selectedValue=="") {
                    table.search("x").draw();
                    table2.search("x").draw();
                }
                else {
                    table.search(selectedValue).draw();
                    table2.search(selectedValue).draw();
                }
            });

    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });
    });
    function modalSelect(id,desc,divi,appr,act){
        document.getElementById('editID').value=id;
        document.getElementById('editDescription').value=desc;

        var element = document.getElementById('editDivision');
        element.value = divi;

        var element2 = document.getElementById('editApprovalStages');
        element2.value = appr;

        if(act==1)
            document.getElementById("editActive").checked = true;
        else
            document.getElementById("editActive").checked = false;
        /*document.getElementById('editID').value=id;

        //document.getElementById('editDivision').value=id;
        var element = document.getElementById('editDivision');
        element.value = divi;
        */
        $('#updateModal').modal('show');
    }
    $("#submitNew").click(function(e) {
        //INITIAL CHECKS BEFORE SAVING.
        if(document.getElementById('description').value==""){
            $("#tab-1-err").show();
            $("#err-description").show();
            e.preventDefault();
        }
        if(document.getElementById('department').value==""){
            $("#tab-1-err").show();
            $("#err-department").show();
            e.preventDefault();
        }

        if(document.getElementById('approvalstages').disabled==true){

            if(document.getElementById('description3').value==""){
                $("#tab-3-err").show();
                $("#err-description3").show();
                e.preventDefault();
            }
            if(document.getElementById('department3').value==""){
                $("#tab-3-err").show();
                $("#err-department3").show();
                e.preventDefault();
            }

            var reqs = [].filter.call(document.getElementsByName('emp[]'), function(c) {
                return c.checked;
            }).map(function(c) {
                return c.value;
            });

            var apprs = [].filter.call(document.getElementsByName('appr[]'), function(c) {
                return c.checked;
            }).map(function(c) {
                return c.value;
            });
            if (reqs.length>0 && apprs.length>0) {
                var match = 0;
                for (i = 0, len = reqs.length; i < len; ++i) {
                    //alert(reqs[i]);
                    for (j = 0, len = apprs.length; j < len; ++j) {
                        if (reqs[i] == apprs[j])
                            match = match + 1;
                    }
                }
                if (match != 0) {
                    $("#tab-2-err").show();
                    $("#err-emp-1").show();
                    $("#tab-4-err").show();
                    $("#err-appr-1").show();
                    e.preventDefault();
                }
                else {
                    //WHERE THE SAVING PROCEEDS
                    //e.preventDefault();
                }
            }
            else{
                if (reqs.length==0 && apprs.length>0) {
                    $("#tab-2-err").show();
                    $("#err-emp-0").show();
                    e.preventDefault();
                }
                else if (reqs.length>0 && apprs.length==0) {
                    $("#tab-4-err").show();
                    $("#err-appr-0").show();
                    e.preventDefault();
                }
                else if (reqs.length==0 && apprs.length==0) {
                    $("#tab-2-err").show();
                    $("#err-emp-0").show();
                    $("#tab-4-err").show();
                    $("#err-appr-0").show();
                    e.preventDefault();
                }

            }
        }
        else {

            var reqstrs = [].filter.call(document.getElementsByName('emp[]'), function(c) {
                return c.checked;
            }).map(function(c) {
                return c.value;
            });

            if (reqstrs.length>0) {
                if(document.getElementById('approvalstages').value==" "){
                    $("#tab-3-err").show();
                    $("#err-approvalstages").show();
                    e.preventDefault();
                }
                else{
                    //WHERE THE SAVING PROCEEDS
                    //e.preventDefault();
                }
            }
            else{
                $("#tab-2-err").show();
                $("#err-emp-0").show();
                e.preventDefault();
            }

        }
    });

</script>
@endpush