@extends('layouts.admin')
@section('title', 'Approver Groups')
@section('page_title', 'Approver Groups Management - Add Approvers')
@section('page_subtitle', '')
@section('prev_link', 'dashboard')
@section('previous', 'Dashboard')
@section('current', 'Users')
@section('stages', 'active')
@section('user_roles_permission', 'active')
@section('content')
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/all.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap-duallistbox.css') }}" rel="stylesheet">

    <style>
        #example1 tr {
            cursor: pointer;
        }
        #example1 tbody tr.highlight td {
            background-color: #9DD5F5;
        }
        #example1 tbody tr:hover{
            background-color: #9DD5F5 !important;
        }
    </style>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <!--<h3 class="box-title">Add Approvers</h3>-->
                        <a href="{{ url('approvergroup') }}" class="btn btn-default">
                            <i class="fa fa-btn fa-backward"></i>&nbsp;&nbsp;Back</a>
                        @if ($hasPending!=796573)
                            @if ($all===0)

                                <a href="{{ url('approvergroup_approvers_all/6e6f/'.$id) }}" class="btn btn-primary">
                                    <i class="fa fa-btn fa-list"></i>&nbsp;&nbsp;Show All Employees</a>
                            @else
                                <a href="{{ url('approvergroup_approvers/6e6f/'.$id) }}" class="btn btn-primary">
                                    <i class="fa fa-btn fa-list"></i>&nbsp;&nbsp;Division Only</a>
                            @endif
                        @endif
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-6">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead class="alert-info">
                                    <th></th>
                                    <th>Name</th>
                                    <th>&nbsp;</th>

                                    </thead>
                                    <tbody>
                                    <?php $no=1; ?>
                                    @foreach($theUsers as $super)
                                        <?php $isApprover=0; ?>
                                        @foreach($currentRequestors as $currApp)
                                            @if ($currApp->User_id==$super->id)
                                                <?php $isApprover=1; ?>
                                            @endif
                                        @endforeach
                                        @if($isApprover!=1)
                                            <tr>
                                                <td style="width:10%;">{{$no++}}</td>
                                                <td>{{$super->LastName}}, {{$super->FirstName}}</td>
                                                <td style="width:15%;">
                                                    @if($hasPending!=796573)
                                                        <a href="{{url('addapprover/'.$id.'/'.$super->id)}}" class="btn btn-primary btn-sm">
                                                            <i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-6">
                                <table id="example3" class="table table-bordered table-striped">
                                    <thead class="alert-danger">
                                    <th></th>
                                    <th>Name</th>
                                    <th><abbr title="This is for enabling emails for this approver.">Email</abbr></th>
                                    <th>&nbsp;</th>

                                    </thead>
                                    <tbody>
                                    <?php $no=1; ?>
                                    @foreach($apprvrs as $super)
                                        <tr>
                                            <td style="width:10%;">{{$no++}}</td>
                                            <td>{{$super->toUsers->LastName}}, {{$super->toUsers->FirstName}}</td>

                                            <td style="width:15%;">
                                                @if ($super->Email==1)
                                                    <a href="{{url('approverEmailToggle/'.$hasPending.'/'.$id.'/'.$super->id)}}" class="btn btn-warning btn-sm">
                                                        <i class="fa fa-btn fa-check"></i></a>
                                                @else
                                                    <a href="{{url('approverEmailToggle/'.$hasPending.'/'.$id.'/'.$super->id)}}" class="btn btn-danger btn-sm">
                                                        <i class="fa fa-btn fa-close"></i></a>
                                                @endif
                                            </td>

                                            <td style="width:15%;">
                                                @if ($hasPending!=796573)
                                                    <a href="{{url('removeapprover/'.$id.'/'.$super->id)}}" class="btn btn-danger btn-sm">
                                                        <i class="fa fa-btn fa-trash"></i>&nbsp;&nbsp;Remove</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->






@endsection
@push('scripts')
<!-- DataTables -->

<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>
<script src="{{ asset('/js/jquery.bootstrap-duallistbox.js') }}"></script>

<script>
    $(function () {
        $("#example1").DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
        $('#example2').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
        $('#example3').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
    });
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

    });

</script>
@endpush