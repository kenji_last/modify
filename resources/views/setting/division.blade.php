@extends('layouts.admin')
@section('title', 'Divisions')
@section('page_title', 'Manage Divisions')
@section('current', 'Users')
@section('division', 'active')
@section('side_othersetting_treeview', 'active')
@section('content')
    <section class="content">
        <div id="elemdivision">
            @if (Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}<br>
                </div>
            @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-default">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Manage Divisions</h3>
                                    </div>
                                    <div class="box-body" style="padding-left:5%;">
                                        @if(starts_with(Route::getCurrentRoute()->getPath(), 'division'))
                                            {!! Form::open(array('url' => 'save_division')) !!}
                                        @else
                                            {!! Form::open(array('url' => 'update_division/' . $select_division->id)) !!}
                                        @endif
                                        {{--<div class="form-group">
                                            <label>Prefix</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Recommended Examples: OB-HRD, OB-MIS</em></small>
                                            @if(starts_with(Route::getCurrentRoute()->getPath(), 'division'))
                                                <input name="prefix" class="form-control" type="text" v-model="prefix" required> </input>
                                            @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_division/'))
                                                <input name="prefix" value="{{ $select_division->prefix }}" class="form-control" type="text" v-model="prefix"> </input>
                                            @endif
                                        </div>--}}
                                        <div class="form-group">
                                            <label>Division Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Human Resources</em></small>
                                            @if(starts_with(Route::getCurrentRoute()->getPath(), 'division'))
                                                <input name="division" class="form-control" type="text" v-model="division" required> </input>
                                            @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_division/'))
                                                <input name="division" value="{{ $select_division->Name }}" class="form-control" type="text" v-model="division"> </input>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label>Active</label>&nbsp;&nbsp;
                                            @if(starts_with(Route::getCurrentRoute()->getPath(), 'division'))
                                                {!!  Form::checkbox('Active', 'value','checked') !!}
                                            @elseif (starts_with(Route::getCurrentRoute()->getPath(), 'edit_division/'))
                                                {!! Form::checkbox('Active', $select_division->Active, ($select_division->Active) ? true: false,['data-tooltip'=>'tooltip']) !!}
                                            @endif
                                        </div>

                                        <div class="box-footer">
                                            {!! Form::submit(
                                            'Submit',
                                            array(
                                            'class'=>'btn btn-primary',
                                            'id'=>'submit_itinerary',
                                            'data-tooltip'=>'tooltip',
                                            'title'=>'Add'
                                            ))
                                            !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">&nbsp;</h3>
                                    </div>
                                    <div class="box-body">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Active</th>
                                            <th>Action</th>
                                            </thead>
                                            <tbody>
                                            <?php $no=1; ?>
                                            @foreach($division as $dept)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $dept->Name }}</td>
                                                    <td>{!! ($dept->Active == 1)? '<span class="bg-green btn-xs">Yes</span>' : '<span class="bg-yellow btn-xs">No</span>' !!}</td>
                                                    <td><a href="{{ route('edit_division', $dept->id) }}">Edit</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
@endsection
@push('scripts')
<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="{{ asset('/js/division.js') }}"></script>

<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
    $('#example1').on('click', 'tbody tr', function(event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
        var trid = $(this).attr('id');
        console.log(trid);
        // $('#myModal').modal('show');
    });
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

    });

</script>
@endpush