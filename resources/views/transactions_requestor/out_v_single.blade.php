@extends('layouts.admin')
@section('title', 'View Outgoing')
@section('requestor', 'active')
@section('out_view', 'active')
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <h4><i class="fa fa-user"></i>&nbsp;<a href="{{url('out_myrequests')}}">My Outgoing Requests</a> | {{$singleRequest->RequestNo}}
                                @if($singleRequest->Status==8)
                                    <small>
                                        <span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            Check-In History:&nbsp;
                                            {{--@foreach($coVersion as $ver)
                                                @if($ver->checkin_ID!=0)
                                                <a href="{{url('in_co_view/'.$singleRequest->id."/".$ver->checkin_ID)}}">{{$ver->checkin_ID}}</a>&nbsp;
                                                @endif
                                            @endforeach--}}
                                        </span>
                                    </small>
                                @endif
                            </h4>
                    </div>
                        <div class="panel-body" style="padding-left:10%;padding-right:10%;padding-top:2%;padding-bottom:1%;">
                            <div class="form-group col-md-12">
                                @if($singleRequest->Status==1)
                                    <button type="button" class="btn btn-sm btn-primary" onclick="editModal();">Edit Request</button>
                                @elseif($singleRequest->Status==4)
                                    <button type="button" class="btn btn-sm btn-primary" onclick="cancelModal();">Cancel Request</button>
                                    {{--<button type="button" class="btn btn-sm btn-primary" onclick="printModal();">Print</button>--}}
                                    <a href="{{url('pdf_gatepass/'.$singleRequest->id)}}" class="btn btn-sm btn-primary" target="_new">Print</a>
                                @elseif($singleRequest->Status==5)
                                    <a href="{{url('out_checkin/'.$singleRequest->id)}}" class="btn btn-sm btn-primary">Check-In</a>
                                @elseif($singleRequest->Status==8)
                                    @if($alreadyCheckedOut==0)
                                        <button type="button" class="btn btn-sm btn-primary" onclick="cancelModal();">Cancel Check-In Request</button>
                                        <a href="{{url('pdf_gatepass/'.$singleRequest->id)}}" class="btn btn-sm btn-primary" target="_new">Print</a>
                                    @endif
                                @endif
                            </div>
                            <div class="col-xs-6">
                                <label>Request Number:</label>
                                {{$singleRequest->RequestNo}}
                            </div>
                            <div class="col-xs-6">
                                <label>B. Partner Type:</label>
                                {{$singleRequest->oBusinessPartner->Name}}
                            </div>
                            <div class="col-xs-6">
                                <label>Status:</label>
                                {{$singleRequest->oStatus->Name}}
                            </div>
                            <div class="col-xs-6">
                                <label>Business Partner:</label>
                                {{$singleRequest->BusinessPartnerName}}
                            </div>
                            <div class="col-xs-6">
                                <label>Date:</label>
                                {{date('F d, Y', strtotime($singleRequest->Date))}}
                            </div>
                            <div class="col-xs-6">
                                <label>Contact:</label>
                                {{$singleRequest->Contact}}
                            </div>
                            <div class="col-xs-6">
                                <label>Host:</label>
                                {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                            </div>
                            <div class="col-xs-6">
                                <label>Contact Email:</label>
                                @if($singleRequest->ContactEmail!=NULL)
                                    {{$singleRequest->ContactEmail}}
                                @else
                                    N/A
                                @endif
                            </div>
                            <div class="col-xs-6">
                                <label>Request Type:</label>
                                {{$singleRequest->oRequest->Name}}
                            </div>
                            @if($singleRequest->Status == 3 || $singleRequest->Status == 4)
                                <div class="col-xs-6">
                                    <label>Approved By:</label>
                                    {{$singleRequest->oheadCOApprovedBy_to_user->FirstName}} {{$singleRequest->oheadCOApprovedBy_to_user->LastName}}
                                </div>
                                @if(isset($singleRequest->COAccountingApprovedBy))
                                    <div class="col-xs-6">
                                        <label>Accounting Approver:</label>
                                        {{$singleRequest->oheadCOAccounting_to_user->FirstName}} {{$singleRequest->oheadCOAccounting_to_user->LastName}}
                                    </div>
                                @endif
                            @elseif($singleRequest->Status == 10)
                                @if(isset($singleRequest->COApprovedBy))
                                    <div class="col-xs-6">
                                        <label>Approver:</label>
                                        {{$singleRequest->oheadCOApprovedBy_to_user->FirstName}} {{$singleRequest->oheadCOApprovedBy_to_user->LastName}}
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Accounting Rejected By:</label>
                                        {{$singleRequest->oheadRejectedBy_to_user->FirstName}} {{$singleRequest->oheadRejectedBy_to_user->LastName}}
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Reject Reason:</label><br>
                                        {{$singleRequest->CORejectReason}}
                                    </div>
                                @else
                                    <div class="col-xs-6">
                                        <label>Rejected By:</label>
                                        {{$singleRequest->oheadRejectedBy_to_user->FirstName}} {{$singleRequest->oheadRejectedBy_to_user->LastName}}
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Reject Reason:</label><br>
                                        {{$singleRequest->CORejectReason}}
                                    </div>
                                @endif

                            @endif

                            <div class="form-group col-xs-12">
                                <div id="aHeaders" class="box-primary" style="align-content: center;">
                                    <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                        <div class="col-xs-2">Description</div>
                                        <div class="col-xs-2">Purpose</div>
                                        {{--<div class="col-xs-4">Serial Number</div>--}}
                                        <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                        <div class="col-xs-1" style="text-align: center;">UOM</div>
                                        <div class="col-xs-2" style="text-align: center;">Returnable?</div>
                                        <div class="col-xs-2" style="text-align: center;">Check-In</div>
                                        <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                    </div>
                                </div>
                                <div id="aBody" class="box-primary" style="
                                    align-content: center;
                                    height:260px;
                                    max-height:260px;
                                    overflow-y:scroll;
                                    padding-bottom:0px;">
                                    @foreach($lineItems as $li)
                                        <div class="row" id='divViewtheTableCount+"' style="
                                        width:100%;
                                        margin-left:0px;
                                        margin-bottom:0px;
                                        margin-top:0px;
                                        padding:5px;
                                        font-size:12px;
                                        border-bottom: 1px solid #e9e9e9;">
                                            <div class="col-xs-2" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                            <div class="col-xs-2" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Purpose}}</div>
                                            <div class="col-xs-1" style='padding:9px 14px; text-align: center;'>{{$li->Quantity}}</div>
                                            <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                            <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                @if($li->Returnable==0)
                                                    No
                                                @else
                                                    Yes
                                                @endif
                                            </div>
                                            <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                @if($li->InDate!="")
                                                    {{date('M d, Y h:i A',strtotime($li->InDate))}}
                                                @else
                                                    -
                                                @endif
                                            </div>
                                            <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                @if($li->Returnable==1)
                                                    @if($li->OutDate!="")
                                                        {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                                    @else
                                                        -
                                                    @endif
                                                @else
                                                    N/A
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <div id="updateModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;" id="modalMessage">
                            Cancel this request?
                        </div>
                    </div>
                    <div class="modal-footer" id="editSet">
                        <a href="{{url('out_requestor/'.$singleRequest->id)}}" class="btn btn-primary">Yes</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    </div>
                    <div class="modal-footer" id="cancelSet">
                        {!! Form::open(array('url' => 'out_cancel')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer" id="printSet">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                    </div>
                    <div class="modal-footer" id="checkOutSet">
                        {!! Form::open(array('url' => 'out_checkin')) !!}
                        {!! Form::token() !!}
                        <input type="hidden" id="mLineID" name="id" value="{{$singleRequest->id}}">
                        {!! Form::submit('Yes', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>


    </body>

    @push('scripts')
    <script>
        function editModal(){
            document.getElementById('modalMessage').innerHTML = "Edit this request?";
            $('#editSet').show();
            $('#cancelSet').hide();
            $('#printSet').hide();
            $('#checkOutSet').hide();
            $('#updateModal').modal('show');
        }
        function cancelModal(){
            document.getElementById('modalMessage').innerHTML = "Cancel this request?";
            $('#editSet').hide();
            $('#cancelSet').show();
            $('#printSet').hide();
            $('#checkOutSet').hide();
            $('#updateModal').modal('show');
        }
        function printModal(){
            document.getElementById('modalMessage').innerHTML = "Print this request?";
            $('#editSet').hide();
            $('#cancelSet').hide();
            $('#printSet').show();
            $('#checkOutSet').hide();
            $('#updateModal').modal('show');
        }
        function checkOutModal(){
            document.getElementById('modalMessage').innerHTML = "Submit a request for check-out?";
            $('#editSet').hide();
            $('#cancelSet').hide();
            $('#printSet').hide();
            $('#checkOutSet').show();
            $('#updateModal').modal('show');
        }
    </script>
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
@endpush
@endsection