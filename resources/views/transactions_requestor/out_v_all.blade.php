@extends('layouts.admin')
@section('title', 'View Incoming')
@section('requestor', 'active')
@section('out_view', 'active')
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">

    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default" style="max-height:100%;height:90% !important;">
                    @if(isset($statusName))
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-9">
                                    <h4><i class="fa fa-user"></i>&nbsp;{{$statusName->Name}} Outgoing Requests</h4>
                                </div>
                                <div class="col-xs-3">
                                    <select class="form-control" onchange="sortRequests(value);">
                                        <option value="">All</option>
                                        @foreach($status as $st)
                                            @if($st->Name == $statusName->Name)
                                                <option value="/{{$st->id}}" selected>{{$st->Name}}</option>
                                            @else
                                                <option value="/{{$st->id}}">{{$st->Name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-9">
                                    <h4><i class="fa fa-user"></i>&nbsp;My Outgoing Requests</h4>
                                </div>
                                <div class="col-xs-3">
                                    <select class="form-control" onchange="sortRequests(value);">
                                        <option value="" selected>All</option>
                                        @foreach($status as $st)
                                                <option value="/{{$st->id}}">{{$st->Name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif
                        <div class="panel-body">
                            <table class="table table-bordered table-striped" id="example1" style="font-size:12px !important;">
                                <thead>
                                    <th>&nbsp;</th>
                                    <th>Request No.</th>
                                    <th>Status</th>
                                    <th>B. Partner Name</th>
                                    <th>B. Partner Type</th>
                                    <th>Request Type</th>
                                    <th>Contact</th>
                                    <th>Host</th>
                                    <th>Date</th>
                                </thead>
                                <tbody>
                                @foreach($myRequests as $mR)
                                    <tr>
                                        <td><a href="{{url('out_view/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                        </td>
                                        <td>{{$mR->RequestNo}}</td>
                                        <td>{{$mR->oStatus->Name}}</td>
                                        <td>{{$mR->BusinessPartnerName}}</td>
                                        <td>{{$mR->oBusinessPartner->Name}}</td>
                                        <td>{{$mR->oRequest->Name}}</td>
                                        <td>{{$mR->Contact}}</td>
                                        <td>{{$mR->oUser->FirstName}} {{$mR->oUser->LastName}}</td>
                                        <td>{{date('F d, Y', strtotime($mR->Date))}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
    <script>
        function sortRequests(id){
            if(id=="")
                window.location.href = "{{ url('out_myrequests') }}";
            else
               window.location.href = "{{ url('out_myrequests') }}"+id;
        }
    </script>
@endpush
@endsection