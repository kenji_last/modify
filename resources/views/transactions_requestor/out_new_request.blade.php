@extends('layouts.admin')
@section('title', 'Incoming Request')
@section('requestor', 'active')
    @if(isset($singleRequest) && isset($lineItems))
        @section('out_view', 'active')
    @else
        @section('out_requestor', 'active')
    @endif

@section('header_title', 'New Incoming Request')
@section('header_desc', 'New Incoming Request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if(isset($singleRequest) && isset($lineItems))
                            <h4><i class="fa fa-user"></i>&nbsp;<a href="{{url('in_myrequests')}}">Outgoing Request</a> - Check-Out - {{$singleRequest->RequestNo}}</h4>
                        @else
                            <h4><i class="fa fa-user"></i>&nbsp;Outgoing Request - Check-Out - New Request</h4>
                        @endif
                    </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="{{ url('out_requestor') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @if(isset($singleRequest) && isset($lineItems))
                                    <input type="hidden" id="editMode" name="editMode" value="1">
                                @else
                                    <input type="hidden" id="editMode" name="editMode" value="0">
                                @endif

                                <div class="col-md-6">
                                    <label>Business Partner:</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <select id="BusinessPartner" onchange="businessPartnerChecker();headerChecker();" name="BusinessPartner" class="form-control input-sm pull-right">
                                                @foreach($bus_part as $bp)
                                                    @if($bp->id == $singleRequest->BusinessPartner)
                                                        <option value="{{$bp->id}}" selected>{{$bp->Name}}</option>
                                                    @else
                                                        <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @else
                                            <select id="BusinessPartner" onchange="businessPartnerChecker();headerChecker();" name="BusinessPartner" class="form-control input-sm pull-right">
                                                <option value="NULL" disabled selected hidden>[Choose one]</option>
                                                @foreach($bus_part as $bp)
                                                    <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Date:</label>
                                    <div class="input-group col-md-12">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <input name="Date" onchange="dateChecker();" type="text" class="form-control input-sm pull-right" id="Date" required="required" value="<?php echo date('m/j/Y g:i a',strtotime($singleRequest->Date));?>">
                                        @else
                                            <input name="Date" onchange="dateChecker();" type="text" class="form-control input-sm pull-right" id="Date" required="required" value="">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Business Partner Name:</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            @if($singleRequest->BusinessPartner == 3 || $singleRequest->BusinessPartner == 4)
                                                <input name="BusinessPartnerName" disabled onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->BusinessPartnerName}}" id="BusinessPartnerName" required="required">
                                            @else
                                                <input name="BusinessPartnerName" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->BusinessPartnerName}}" id="BusinessPartnerName" required="required">
                                            @endif
                                        @else
                                            <input name="BusinessPartnerName" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" id="BusinessPartnerName" required="required">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Contact:</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <input type="text" id="Contact" name="Contact" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right" value="{{$singleRequest->Contact}}">
                                        @else
                                            <input type="text" id="Contact" name="Contact" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Contact Email:</label>
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <input type="text" id="ContactEmail" name="ContactEmail" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right" value="{{$singleRequest->ContactEmail}}">
                                    @else
                                        <input type="text" id="ContactEmail" name="ContactEmail" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right">
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Request Type</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <select id="RequestType" onchange="headerChecker();" name="RequestType" class="form-control input-sm pull-right">
                                                @foreach($reqtype as $bp)
                                                    @if($bp->id == $singleRequest->RequestType)
                                                        <option value="{{$bp->id}}" selected>{{$bp->Name}}</option>
                                                    @else
                                                        <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @else
                                            <select id="RequestType" onchange="headerChecker();" name="RequestType" class="form-control input-sm pull-right">
                                                {{--<option value="NULL" disabled selected hidden>[Choose one]</option>--}}
                                                @foreach($reqtype as $bp)
                                                    <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Destination:</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <input name="Destination" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->Destination}}" id="Destination" required="required">
                                        @else
                                            <input name="Destination" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" id="Destination" required="required">
                                        @endif
                                    </div>

                                </div>


                                <div class="col-md-6">
                                    <div class="input-group col-md-12" style="display:none;">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <input name="id" type="hidden" class="form-control input-sm pull-right" value="{{$singleRequest->id}}">
                                            <input name="AssetNumber" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->RequestNo}}" id="AssetNumber" required="required">
                                        @else
                                            <input name="AssetNumber" type="text" class="form-control input-sm pull-right" value="000000" id="AssetNumber" required="required">
                                        @endif
                                    </div>
                                    <label>Reason:</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <input name="Reason" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->Reason}}" id="Reason" required="required">
                                        @else
                                            <input name="Reason" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" id="Reason" required="required">
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Transporter:</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <input name="Transporter" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->Transporter}}" id="Transporter" required="required">
                                        @else
                                            <input name="Transporter" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" id="Transporter" required="required">
                                        @endif
                                    </div>
                                </div>




                                <div class="col-xs-12" style="padding-top:3%;">
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <input type="hidden" id="tablecount" name="assetCount" value="0"/>
                                        <input type="hidden" id="realTableCount" name="realAssetCount" value="{{$lineItemsCount}}"/>
                                        <input type="hidden" id="savedTableCount" name="savedAssetCount" value="{{$lineItemsCount}}"/>
                                    @else
                                        <input type="hidden" id="tablecount" name="assetCount" value="0"/>
                                        <input type="hidden" id="realTableCount" name="realAssetCount" value="0"/>
                                    @endif
                                    &nbsp;&nbsp;&nbsp;
                                    <button type="button" id="addingAssets" onclick="addAssets('aBody');" class='btn btn-xs btn-success'><i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Line Items</button>
                                </div>
                                <div class="form-group col-xs-12">
                                    <div id="aHeaders" class="box-primary" style="align-content: center;">
                                        <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:13px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                            <div class="col-xs-3">Description</div>
                                            {{--<div class="col-xs-3">Serial Number</div>--}}
                                            <div class="col-xs-2">Purpose</div>
                                            <div class="col-xs-1">Qty</div>
                                            <div class="col-xs-1">UOM</div>
                                            <div class="col-xs-2">Returnable?</div>
                                            <div class="col-xs-2">Expected Return</div>
                                            <div class="col-xs-1"></div>
                                        </div>
                                    </div>
                                    <div id="aBody" class="box-primary" style="font-size:12px;align-content: center;height:240px;max-height:240px;overflow-y:scroll;">
                                        <!--Saved Line Items-->
                                        @if(isset($singleRequest) && isset($lineItems))
                                            @foreach($lineItems as $li)
                                                <div class="row" id='divSavedEdit{{$li->LineNum}}' style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:4px;">
                                                    <div class="col-md-2" style='padding:3px;'>
                                                        <input name="savedID[{{$li->LineNum}}]" type="hidden" value="{{$li->id}}">
                                                        <input value="{{$li->Description}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDescription[{{$li->LineNum}}]' id='eSavedDescription[{{$li->LineNum}}]'/>
                                                    </div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <button type='button' onclick='showSavedAuto({{$li->LineNum}});' class='btn btn-sm btn-primary btn-block'>Select</button>
                                                    </div>
                                                    <div class="col-md-2" style='padding:3px;'>
                                                        <input value="{{$li->Purpose}}" maxlength="50" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedPurpose[{{$li->LineNum}}]' id='eSavedPurpose[{{$li->LineNum}}]'/>
                                                    </div>
                                                    {{--<div class="col-md-3" style='padding:3px;'>
                                                        <input value="{{$li->SerialNum}}" type='text' class='form-control input-sm' style="width:100%;" name='savedSerialNum[{{$li->LineNum}}]' id='eSavedSerialNum[{{$li->LineNum}}]'/>
                                                    </div>--}}
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <input value="{{$li->Quantity}}" type='number' class='form-control input-sm' style="width:100%;" name='savedQuantity[{{$li->LineNum}}]' id='eSavedQuantity[{{$li->LineNum}}]' onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});"/>
                                                    </div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <select class='form-control input-sm' style='width:100%;' name='savedUOM[{{$li->LineNum}}]' id='eSavedUOM[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                        @foreach($uoms as $u)
                                                            @if($li->UOM == $u->id)
                                                                <option value='{{$u->id}}' selected>{{$u->Name}}</option>
                                                            @else
                                                                <option value='{{$u->id}}'>{{$u->Name}}</option>
                                                            @endif
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2" style='align-content: center;text-align: center;vertical-align: middle;'>
                                                        @if($li->Returnable==0)
                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' onclick="savedReturnable({{$li->LineNum}});" id='eSavedReturnable[{{$li->LineNum}}]'>
                                                        @elseif($li->Returnable==1)
                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' onclick="savedReturnable({{$li->LineNum}});" id='eSavedReturnable[{{$li->LineNum}}]' checked>
                                                        @endif
                                                        <input type="hidden" name="savedDeleted[{{$li->LineNum}}]" id='eSavedDeleted[{{$li->LineNum}}]' value="0">
                                                    </div>
                                                    <div class="col-md-2" style='padding:3px;align-content: center;text-align: center;vertical-align: middle;'>
                                                        <input type='hidden' value="{{date("m/d/Y", strtotime($li->ExpReturn))}}" class='form-control input-sm' style="width:100%;" name='savedExpectedReturn[{{$li->LineNum}}]' id='savedExpectedReturn[{{$li->LineNum}}]'/>
                                                        <div class='col-xs-6' id='vSavedExpectedReturnDisp[{{$li->LineNum}}]' style='font-size:12px;padding:3px;align-content: center;text-align: center;vertical-align: middle;'>
                                                            @if($li->ExpReturn==NULL)
                                                                -
                                                            @else
                                                                {{date("m/d/Y", strtotime($li->ExpReturn))}}
                                                            @endif
                                                        </div>
                                                        @if($li->Returnable==0)
                                                            <button class='btn btn-sm' disabled id='savedExpectedReturnBtn[{{$li->LineNum}}]' type='button' onclick='savedExpectedReturn({{$li->LineNum}});'><i class="fa fa-calendar"></i></button>
                                                        @else
                                                            <button class='btn btn-sm' id='savedExpectedReturnBtn[{{$li->LineNum}}]' type='button' onclick='savedExpectedReturn({{$li->LineNum}});'><i class="fa fa-calendar"></i></button>
                                                        @endif

                                                    </div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <button type='button' class='btn btn-sm' id='confirmSavedAsset{{$li->LineNum}}' onclick='confirmSavedAsset("{{$li->LineNum}}");'><i class='fa fa-check'></i></button>
                                                        <button type='button' class='btn btn-sm cancelButton' onclick='cancelSavedAsset("{{$li->LineNum}}");'><i class='fa fa-close'></i></button>
                                                    </div>
                                                </div>
                                                <div class="row" id='divSavedView{{$li->LineNum}}' style="width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:5px;border-bottom: 1px solid #e9e9e9;">
                                                    <div class="col-md-3" style='padding:7px 14px;'><span style="width:100%;word-wrap:break-word;" id='vSavedDescription[{{$li->LineNum}}]'>{{$li->Description}}&nbsp;</span></div>
                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;word-wrap:break-word;" id='vSavedPurpose[{{$li->LineNum}}]'>{{$li->Purpose}}&nbsp;</span></div>
                                                    {{--<div class="col-md-3" style='padding:9px 14px;'><span style="width:100%;" id='vSavedSerialNum[{{$li->LineNum}}]'>{{$li->SerialNum}}&nbsp;</span></div>--}}
                                                    <div class="col-md-1" style='padding:7px 14px;'><span style="width:100%;" id='vSavedQuantity[{{$li->LineNum}}]'>{{$li->Quantity}}&nbsp;</span></div>
                                                    <div class="col-md-1" style='padding:7px 14px;'><span style="width:100%;" id='vSavedUOM[{{$li->LineNum}}]'>{{$li->oUOM->Name}}&nbsp;</span></div>
                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;" id='vSavedReturnable[{{$li->LineNum}}]'>
                                                            @if($li->Returnable==0)
                                                                No
                                                            @else
                                                                Yes
                                                            @endif
                                                            &nbsp;</span></div>
                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;" id='vSavedExpReturn[{{$li->LineNum}}]'>
                                                            @if($li->ExpReturn==NULL)
                                                                N/A
                                                            @else
                                                                {{date("m/d/Y", strtotime($li->ExpReturn))}}
                                                            @endif
                                                            &nbsp;</span></div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <button type='button' class='btn btn-sm editButton' onclick='editSavedAsset("{{$li->LineNum}}");'><i class='fa fa-edit'></i></button>
                                                        <button type='button' class='btn btn-sm deleteButton' onclick='removeSavedAsset("{{$li->LineNum}}");'><i class='fa fa-trash-o'></i></button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <button type="submit" id="sendrequest" name="submitRequest" class="btn btn-primary" data-toggle="modal">Submit Request</button>
                                        <button type="submit" id="saverequest" name="saveRequest" class="btn btn-primary" data-toggle="modal">Save Request</button>
                                    @else
                                        <button type="submit" id="sendrequest" name="submitRequest" class="btn btn-primary" data-toggle="modal" disabled>Submit Request</button>
                                        <button type="submit" id="saverequest" name="saveRequest" class="btn btn-primary" data-toggle="modal" disabled>Save Request</button>
                                    @endif
                                    <button type="button" id="" class="btn btn-primary" onclick="location.reload();">Cancel</button>
                                </div>
                    </div>
            </div>
        </div>
    </form>

            <div id="autofill" class="modal fade" role="dialog">
                <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="modalTitle">Line Item Picker</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="theLineID">
                            <input type="hidden" id="theSavedLineID">
                            <table class="table table-condensed" id="AutoDesc">
                                <thead>
                                <th width="95%">Description</th>
                                <th> </th>
                                </thead>
                                @foreach($descAutoComp as $dA)
                                    <tr>
                                        <td style="width:100%;word-wrap:break-word;">
                                            {{$dA->Description}}
                                        </td>
                                        <td>
                                            <button type="button" id="" class="btn btn-primary btn-sm" onclick="getSetDescription('{{$dA->Description}}');">Select</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="modal-footer" id="approveSet">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>

                    </div>

                </div>
            </div>


        <div id="DateInput" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;" id="modalMessage">
                            <input type="hidden" id="itemDate">
                            <input type="hidden" id="savedDate">
                            <label>Date:</label>
                            <div class="input-group col-md-12">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                @if(isset($singleRequest) && isset($lineItems))
                                    <input name="Date" type="text" onchange="expDateChecker();" class="form-control input-sm pull-right" id="XDate" value="<?php echo date('m/j/Y g:i a',strtotime($singleRequest->Date));?>">
                                @else
                                    <input name="Date" type="text" onchange="expDateChecker();" class="form-control input-sm pull-right" id="XDate" value="" onchange="checkXDate();">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="editSet">
                        <button type="button" id="okDate" onclick="okDate();" class="btn btn-primary">OK</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>

        <table id="lineStatRef" style="display:none;">
        @foreach($linestat as $lst)
            <tr>
            <td>{{$lst->id}}</td>
            <td>{{$lst->Name}}</td>
            </tr>
        @endforeach

        <table id="uomRef" style="display:none;">
        @foreach($uoms as $u)
                    <tr>
                        <td>{{$u->id}}</td>
                        <td>{{$u->Name}}</td>
                    </tr>
        @endforeach
    <?php
        $derContact = "";
        foreach($contacts as $c){
            $derContact = $derContact."'".$c->Contact."',";
        }
        $derContact = substr(trim($derContact), 0, -1);
        //echo $derContact;
    ?>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('/js/autocomplete.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        $( document ).ready(function() {
            $('#XDate').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });

        $('body').on('focus',".recurring_dates", function(){
            $(this).daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        var editMode = 0;
        var states = [
            <?php echo $derContact;?>
        ];
        $(function() {
            $(".tags").autocomplete({
                source:[states]
            });
        });

        function addAssets(divName){
            editMode = 1;
            $('#saverequest').attr('disabled', 'disabled');
            $('#sendrequest').attr('disabled', 'disabled');
            var theTableCount = document.getElementById('tablecount').value;
            var newdiv = document.createElement('div');

            //Setting UOMs in Drop Down
            var ddUOMs = "<select onchange='requestorChecks("+theTableCount+");' class='form-control input-sm' style='width:100%;' name='UOM["+theTableCount+"]' id='eUOM["+theTableCount+"]'>";
            ddUOMs = ddUOMs + "<option value=\'NULL\' disabled selected>Please Select...</option>";
            var uomTable = document.getElementById('uomRef');
            var uomRowLength = uomTable.rows.length;
            for (i = 0; i < uomRowLength; i++){
                var uomTableCells = uomTable.rows.item(i).cells;
                ddUOMs = ddUOMs + "<option value='"+uomTableCells.item(0).innerHTML+"'>"+uomTableCells.item(1).innerHTML+"</option>";
            }
            ddUOMs = ddUOMs + "</select>";

            newdiv.innerHTML = "<div class=\"row\" id='divEdit"+theTableCount+"' style=\"width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0px;\">" +
            "<div class=\"col-md-2\" style='padding:3px;'>" +
            "<input placeholder='Description' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' maxlength='100' type='text' class='form-control input-sm' style=\"width:100%;\" name='Description["+theTableCount+"]' id='eDescription["+theTableCount+"]'/></div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' onclick='showAuto("+theTableCount+");' class='btn btn-sm btn-primary btn-block'>Select</button>" +
            "</div>"+
            "<div class=\"col-md-2\" style='padding:3px;'>" +
            "<input placeholder='Purpose' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' maxlength='50' type='text' class='form-control input-sm' style=\"width:100%;\" name='Purpose["+theTableCount+"]' id='ePurpose["+theTableCount+"]'/></div>"+
            /*"<div class=\"col-md-3\" style='padding:3px;'>" +
            "<input placeholder='Serial Number' onkeyup='requestorChecks("+theTableCount+");' type='text' class='form-control input-sm' style=\"width:100%;\" name='SerialNum["+theTableCount+"]' id='eSerialNum["+theTableCount+"]'/></div>"+*/
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<input placeholder='Qty' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' type='number' class='form-control input-sm' style=\"width:100%;\" name='Quantity["+theTableCount+"]' id='eQuantity["+theTableCount+"]'/></div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            ddUOMs+
            "</div>"+
            "<div class=\"col-md-2\" style='padding-top:8px;align-content: center;text-align: center;vertical-align: middle;'>" +
            "<input type='checkbox' name='Returnable["+theTableCount+"]' id='eReturnable["+theTableCount+"]' onclick='returnable("+theTableCount+");' checked>" +
            "</div>"+
            "<div class=\"col-md-2\" style='padding:3px;align-content: center;text-align: center;vertical-align: middle;'>" +
            "<input type='hidden' class='form-control input-sm' style=\"width:100%;\" name='ExpectedReturn["+theTableCount+"]' id='eExpectedReturn["+theTableCount+"]'/>"+
            "<div class='col-xs-6' id='expectedReturnDisp["+theTableCount+"]' style='font-size:12px;padding:3px;align-content: center;text-align: center;vertical-align: middle;'>-</div>"+
            "<button class='btn btn-sm' id='expectedReturnBtn["+theTableCount+"]' type='button' onclick='expectedReturn("+theTableCount+");'><i class=\"fa fa-calendar\"></i></button>"+
            "</div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' class='btn btn-sm' id='confirmAsset"+theTableCount+"' onclick='confirmAsset("+theTableCount+");' disabled><i class='fa fa-check'></i></button>&nbsp;" +
            "<button type='button' class='btn btn-sm cancelButton' onclick='cancelAsset("+theTableCount+");'><i class='fa fa-close'></i></button>"+
            "</div>"+
            "</div>"+
            "<div class=\"row\" id='divView"+theTableCount+"' style=\"font-size:12px;width:100%;margin-left:0px;margin-bottom:0px;margin-top:1px;padding:0px;display:none;border-bottom: 1px solid #e9e9e9;\">" +
            "<div class=\"col-md-3\" style='padding:7px 14px;'><span style=\"width:100%;word-wrap:break-word;\" id='vDescription["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:7px 14px;'><span style=\"width:100%;word-wrap:break-word;\" id='vPurpose["+theTableCount+"]'/></span></div>"+
            //"<div class=\"col-md-3\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vSerialNum["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-1\" style='padding:7px 14px;'><span style=\"width:100%;word-wrap:break-word;\" id='vQuantity["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-1\" style='padding:7px 14px;'><span style=\"width:100%;\" id='vUOM["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:7px 14px;'><span style=\"width:100%;\" id='vReturnable["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:7px 14px;'><span style=\"width:100%;\" id='vExpReturn["+theTableCount+"]'/>Return Date</span></div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' class='btn btn-sm editButton' onclick='editAsset("+theTableCount+");'><i class='fa fa-edit'></i></button>&nbsp;" +
            "<button type='button' class='btn btn-sm deleteButton' onclick='removeAsset("+theTableCount+");'><i class='fa fa-trash-o'></i></button></div>"+
            "</div>";

            document.getElementById(divName).appendChild(newdiv);
            document.getElementById('tablecount').value = +theTableCount + 1;
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value + 1;
            document.getElementById('addingAssets').disabled=true;
            document.getElementById("eDescription["+theTableCount+"]").focus();
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');

        }

        function confirmAsset(lineNumber){
            editMode = 0;
            document.getElementById('divEdit'+lineNumber).style.display = "none";
            document.getElementById('divView'+lineNumber).style.display = "block";
            document.getElementById('vDescription['+lineNumber+']').innerHTML = document.getElementById('eDescription['+lineNumber+']').value;
            document.getElementById('vPurpose['+lineNumber+']').innerHTML = document.getElementById('ePurpose['+lineNumber+']').value;
            //document.getElementById('vSerialNum['+lineNumber+']').innerHTML = document.getElementById('eSerialNum['+lineNumber+']').value;
            document.getElementById('vQuantity['+lineNumber+']').innerHTML = document.getElementById('eQuantity['+lineNumber+']').value;
            if(document.getElementById('eReturnable['+lineNumber+']').checked)
                document.getElementById('vReturnable['+lineNumber+']').innerHTML = "Yes";
            else
                document.getElementById('vReturnable['+lineNumber+']').innerHTML = "No";
            //var LineStatusView = document.getElementById("eLineStatus["+lineNumber+"]");
            //var strLineStatus = LineStatusView.options[LineStatusView.selectedIndex].text;
            //document.getElementById('vLineStatus['+lineNumber+']').innerHTML = streLineStatus;

            var UOMView = document.getElementById("eUOM["+lineNumber+"]");
            var strUOM = UOMView.options[UOMView.selectedIndex].text;
            document.getElementById('vUOM['+lineNumber+']').innerHTML = strUOM;


            //document.getElementById('vInDate['+lineNumber+']').innerHTML = document.getElementById('eInDate['+lineNumber+']').value;
            //document.getElementById('vOutDate['+lineNumber+']').innerHTML = document.getElementById('eOutDate['+lineNumber+']').value;

            document.getElementById('addingAssets').disabled=false;
            headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }
        function editAsset(lineNumber){
            editMode = 1;
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');
            $('#saverequest').attr('disabled', 'disabled');
            $('#sendrequest').attr('disabled', 'disabled');
            document.getElementById('divView'+lineNumber).style.display = "none";
            document.getElementById('divEdit'+lineNumber).style.display = "block";
            document.getElementById('addingAssets').disabled=true;
        }
        function removeAsset(lineNumber){
            editMode = 0;
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            headerChecker();
        }
        function cancelAsset(lineNumber){
            editMode = 0;
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            document.getElementById('addingAssets').disabled=false;
            headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }

        function confirmSavedAsset(lineNumber){
            editMode = 0;
            document.getElementById('divSavedEdit'+lineNumber).style.display = "none";
            document.getElementById('divSavedView'+lineNumber).style.display = "block";
            document.getElementById('vSavedDescription['+lineNumber+']').innerHTML = document.getElementById('eSavedDescription['+lineNumber+']').value;
            document.getElementById('vSavedPurpose['+lineNumber+']').innerHTML = document.getElementById('eSavedPurpose['+lineNumber+']').value;
            //document.getElementById('vSavedSerialNum['+lineNumber+']').innerHTML = document.getElementById('eSavedSerialNum['+lineNumber+']').value;
            document.getElementById('vSavedQuantity['+lineNumber+']').innerHTML = document.getElementById('eSavedQuantity['+lineNumber+']').value;
            var UOMView = document.getElementById("eSavedUOM["+lineNumber+"]");
            var strUOM = UOMView.options[UOMView.selectedIndex].text;
            document.getElementById('vSavedUOM['+lineNumber+']').innerHTML = strUOM;
            //alert(document.getElementById('eSavedReturnable['+lineNumber+']').checked);
            if(document.getElementById('eSavedReturnable['+lineNumber+']').checked)
                document.getElementById('vSavedReturnable['+lineNumber+']').innerHTML = "Yes";
            else
                document.getElementById('vSavedReturnable['+lineNumber+']').innerHTML = "No";
            document.getElementById('addingAssets').disabled=false;
            headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }
        function editSavedAsset(lineNumber){
            editMode = 1;
            headerChecker();
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');
            //$('#saverequest').attr('disabled', 'disabled');
            //$('#sendrequest').attr('disabled', 'disabled');
            document.getElementById('divSavedView'+lineNumber).style.display = "none";
            document.getElementById('divSavedEdit'+lineNumber).style.display = "block";
            document.getElementById('addingAssets').disabled=true;
        }
        function removeSavedAsset(lineNumber){
            editMode = 0;
            document.getElementById("divSavedEdit"+lineNumber).style.display = "none";
            document.getElementById("divSavedView"+lineNumber).style.display = "none";
            document.getElementById("eSavedDeleted["+lineNumber+"]").value="1";
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            headerChecker();
        }
        function cancelSavedAsset(lineNumber){
            editMode = 0;
            headerChecker();
            document.getElementById('divSavedEdit'+lineNumber).style.display = "none";
            document.getElementById('divSavedView'+lineNumber).style.display = "block";
            document.getElementById('addingAssets').disabled=false;
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
            //headerChecker();
        }
        function savedChecks(lineNumber){
            var errCount = 0;

            if(document.getElementById("eSavedDescription["+lineNumber+"]").value=="" || document.getElementById("eSavedDescription["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedDescription[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedDescription[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eSavedPurpose["+lineNumber+"]").value=="" || document.getElementById("eSavedPurpose["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedPurpose[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedPurpose[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            /*if(document.getElementById("eSavedSerialNum["+lineNumber+"]").value=="" || document.getElementById("eSavedSerialNum["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedSerialNum[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedSerialNum[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }*/

            if(document.getElementById("eSavedQuantity["+lineNumber+"]").value=="" || document.getElementById("eSavedQuantity["+lineNumber+"]").value<=0 || document.getElementById("eSavedQuantity["+lineNumber+"]").value.indexOf("e")>0 || document.getElementById("eSavedQuantity["+lineNumber+"]").value.indexOf("E")>0){
                document.getElementById("eSavedQuantity[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedQuantity[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eSavedUOM["+lineNumber+"]").value=="NULL") {
                document.getElementById("eSavedUOM[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedUOM[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eSavedReturnable[" + lineNumber + "]").checked){
                if(document.getElementById("savedExpectedReturn[" + lineNumber + "]").value==""){
                    document.getElementById("vSavedExpectedReturnDisp[" + lineNumber + "]").style.border = "2px solid #FF0000";
                    errCount = errCount + 1;
                }
                else {
                    document.getElementById("vSavedExpectedReturnDisp[" + lineNumber + "]").style.border = "0px solid #FF0000";
                }
            }
            else {
                document.getElementById("vSavedExpectedReturnDisp[" + lineNumber + "]").style.border = "0px solid #000000";
            }

            //alert(errCount);
            if(errCount!=0)
                $('#confirmSavedAsset'+lineNumber).attr('disabled', 'disabled');
            else
                $('#confirmSavedAsset'+lineNumber).attr('disabled', false);

        }
        function requestorChecks(lineNumber){
            var errCount = 0;

            if(document.getElementById("eDescription["+lineNumber+"]").value=="" || document.getElementById("eDescription["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("ePurpose["+lineNumber+"]").value=="" || document.getElementById("ePurpose["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("ePurpose[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("ePurpose[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            /*if(document.getElementById("eSerialNum["+lineNumber+"]").value=="" || document.getElementById("eSerialNum["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            */

            if(document.getElementById("eQuantity["+lineNumber+"]").value=="" || document.getElementById("eQuantity["+lineNumber+"]").value<=0 || document.getElementById("eQuantity["+lineNumber+"]").value.trim().length == 0  || document.getElementById("eQuantity["+lineNumber+"]").value.indexOf("e")>0 || document.getElementById("eQuantity["+lineNumber+"]").value.indexOf("E")>0){
                    document.getElementById("eQuantity[" + lineNumber + "]").style.border = "2px solid #FF0000";
                    errCount = errCount + 1;
            }
            else {
                    document.getElementById("eQuantity[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }


            if(document.getElementById("eUOM["+lineNumber+"]").value=="NULL") {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eReturnable[" + lineNumber + "]").checked){
                if(document.getElementById("eExpectedReturn[" + lineNumber + "]").value==""){
                    document.getElementById("expectedReturnDisp[" + lineNumber + "]").style.border = "2px solid #FF0000";
                    errCount = errCount + 1;
                }
                else {
                    document.getElementById("expectedReturnDisp[" + lineNumber + "]").style.border = "0px solid #FF0000";
                }
            }
            else {
                document.getElementById("expectedReturnDisp[" + lineNumber + "]").style.border = "0px solid #000000";
            }

            if(errCount!=0)
                $('#confirmAsset'+lineNumber).attr('disabled', 'disabled');
            else
                $('#confirmAsset'+lineNumber).attr('disabled', false);


        }
        function validateEmail(sEmail) {
            var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
            if(!sEmail.match(reEmail))
                return false;
            else
                return true;
        }

        function headerChecker(){
            var hErrCount = 0;

            if(document.getElementById("BusinessPartner").value=="NULL") {
                document.getElementById("BusinessPartner").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("BusinessPartner").style.border = "1px solid #d2d6de";
            }
            if(document.getElementById("BusinessPartnerName").disabled == false && document.getElementById("BusinessPartnerName").value=="") {
                document.getElementById("BusinessPartnerName").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("BusinessPartnerName").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("Contact").value=="") {
                document.getElementById("Contact").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("Contact").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("Destination").value=="") {
                document.getElementById("Destination").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("Destination").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("Reason").value=="") {
                document.getElementById("Reason").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("Reason").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("Transporter").value=="") {
                document.getElementById("Transporter").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("Transporter").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("ContactEmail").value!="" && validateEmail(document.getElementById("ContactEmail").value)==false) {
                document.getElementById("ContactEmail").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("ContactEmail").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("tablecount").value==0 && document.getElementById("realTableCount").value==0) {
                hErrCount = hErrCount + 1;
            }
            if (editMode == 1)
                hErrCount = hErrCount + 1;

            if(hErrCount!=0 || document.getElementById('realTableCount').value == 0) {
                $('#saverequest').attr('disabled', 'disabled');
                $('#sendrequest').attr('disabled', 'disabled');
            }
            else {
                $('#saverequest').attr('disabled', false);
                $('#sendrequest').attr('disabled', false);
            }

        }
        function returnable(item){
            if(document.getElementById("eReturnable[" + item + "]").checked){
                document.getElementById("expectedReturnBtn[" + item + "]").disabled = false;
            }
            else{
                document.getElementById("expectedReturnBtn[" + item + "]").disabled = true;
                document.getElementById("vExpReturn[" + item + "]").innerHTML = "N/A";
                document.getElementById("eExpectedReturn["+item+"]").value = "";
                //document.getElementById("expectedReturnDisp[" + item + "]").style.border = "0px solid #000000";
            }
            requestorChecks(item);
        }
        function savedReturnable(item){
            if(document.getElementById("eSavedReturnable[" + item + "]").checked){
                document.getElementById("savedExpectedReturnBtn[" + item + "]").disabled = false;
            }
            else{
                document.getElementById("savedExpectedReturnBtn[" + item + "]").disabled = true;
                document.getElementById("vSavedExpectedReturnDisp[" + item + "]").innerHTML = "N/A";
                document.getElementById("savedExpectedReturn["+item+"]").value = "";
                //document.getElementById("expectedReturnDisp[" + item + "]").style.border = "0px solid #000000";
            }
            savedChecks(item);
        }

        function expectedReturn(item){
            document.getElementById("itemDate").value = item;
            document.getElementById("savedDate").value = 0;
            if(document.getElementById("eExpectedReturn["+item+"]").value!="")
                document.getElementById("XDate").value = document.getElementById("eExpectedReturn["+item+"]").value;
            if(document.getElementById("XDate").value=="")
                document.getElementById("okDate").disabled = true;
            $('#DateInput').modal('show');
        }
        function savedExpectedReturn(item){
            document.getElementById("itemDate").value = item;
            document.getElementById("savedDate").value = 1;
            if(document.getElementById("savedExpectedReturn["+item+"]").value!="")
                document.getElementById("XDate").value = document.getElementById("savedExpectedReturn["+item+"]").value;
            if(document.getElementById("XDate").value=="")
                document.getElementById("okDate").disabled = true;
            $('#DateInput').modal('show');
        }

        function okDate(){
            var item = document.getElementById("itemDate").value;
            if(document.getElementById("savedDate").value==0){
                document.getElementById("eExpectedReturn["+item+"]").value = document.getElementById("XDate").value;
                document.getElementById("expectedReturnDisp["+item+"]").innerHTML = document.getElementById("XDate").value;
                document.getElementById("vExpReturn["+item+"]").innerHTML = document.getElementById("XDate").value;
                requestorChecks(item);
            }
            else if(document.getElementById("savedDate").value==1){
                document.getElementById("savedExpectedReturn["+item+"]").value = document.getElementById("XDate").value;
                document.getElementById("vSavedExpectedReturnDisp["+item+"]").innerHTML = document.getElementById("XDate").value;
                document.getElementById("vSavedExpReturn["+item+"]").innerHTML = document.getElementById("XDate").value;
                savedChecks(item);
            }

            document.getElementById("XDate").value = "";

            headerChecker();
            $('#DateInput').modal('hide');
        }
        function dateChecker(){
            var x =document.getElementById('tablecount').value;
            for (var i=0;i<x;i++){
                var element =  document.getElementById('eExpectedReturn['+i+']');
                if (typeof(element) != 'undefined' && element != null){
                    if(document.getElementById("eReturnable[" + i + "]").checked ==true ){
                        //alert(document.getElementById('eExpectedReturn['+i+']').value);
                        var xx = new Date(document.getElementById('eExpectedReturn['+i+']').value);
                        var yy = new Date(document.getElementById("Date").value);
                        var zz = +xx>+yy;
                        var errCount = 0;
                        if(zz==false) {
                            errCount = errCount + 1;
                            document.getElementById("expectedReturnDisp[" + i + "]").style.border = "2px solid #FF0000";
                            document.getElementById("vExpReturn[" + i + "]").style.border = "2px solid #FF0000";
                        }

                        if(errCount>=1){
                            $('#saverequest').attr('disabled', 'disabled');
                            $('#sendrequest').attr('disabled', 'disabled');
                        }
                        else{
                            $('#saverequest').attr('disabled', false);
                            $('#sendrequest').attr('disabled', false);
                        }
                    }
                    //else Non Returnable
                }
                //else it is Deleted, Skip
            }
        }
        function expDateChecker(){
            var x = new Date(document.getElementById("XDate").value);
            var y = new Date(document.getElementById("Date").value);
            var z = +x>+y;
            var errCount = 0;

            if(z==false)
                errCount = errCount + 1;

            if(document.getElementById("XDate").value == "")
                errCount = errCount + 1;

            if(errCount>=1)
                document.getElementById("okDate").disabled = true;
            else
                document.getElementById("okDate").disabled = false;

        }
        function checkXDate(){
           // alert("S");
        }
        function businessPartnerChecker(){
            var xx = document.getElementById('BusinessPartner');
            if(xx.options[xx.selectedIndex].value==3 || xx.options[xx.selectedIndex].value==4){
                document.getElementById('BusinessPartnerName').disabled = true;
            }
            else{
                document.getElementById('BusinessPartnerName').disabled = false;
            }
        }
        function showAuto(id){
            document.getElementById("theLineID").value = id;
            document.getElementById("theSavedLineID").value = -1;
            $('#autofill').modal('show');
        }
        function getSetDescription(description){
            if(document.getElementById("theSavedLineID").value == -1){
                var id = document.getElementById("theLineID").value;
                document.getElementById("eDescription[" + id + "]").value = description;
                requestorChecks(id);
                document.getElementById("theLineID").value = "";
                $('#autofill').modal('hide');
            }
            else if(document.getElementById("theLineID").value == -1){
                var id = document.getElementById("theSavedLineID").value;
                document.getElementById("eSavedDescription[" + id + "]").value = description;
                savedChecks(id);
                document.getElementById("theSavedLineID").value = "";
                $('#autofill').modal('hide');
            }

        }
        function showSavedAuto(id){
            document.getElementById("theSavedLineID").value = id;
            document.getElementById("theLineID").value = -1;
            $('#autofill').modal('show');
        }
    </script>
@endpush
@endsection