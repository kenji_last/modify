@extends('layouts.admin')
@section('requestor', 'active')
@section('title', 'Incoming Request')
    @if(isset($singleRequest) && isset($lineItems))
        @section('in_view', 'active')
    @else
        @section('in_requestor', 'active')
    @endif

@section('header_title', 'New Incoming Request')
@section('header_desc', 'New Incoming Request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if(isset($singleRequest) && isset($lineItems))
                            <h4><i class="fa fa-user"></i>&nbsp;<a href="{{url('in_myrequests')}}">Incoming Request</a> - Check-In - {{$singleRequest->RequestNo}}</h4>
                        @else
                            <h4><i class="fa fa-user"></i>&nbsp;Incoming Request - Check-In - New Request</h4>
                        @endif
                    </div>
                        <div class="panel-body">

                            <form role="form" method="POST" action="{{ url('in_requestor') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @if(isset($singleRequest) && isset($lineItems))
                                    <input type="hidden" id="editMode" name="editMode" value="1">
                                @else
                                    <input type="hidden" id="editMode" name="editMode" value="0">
                                @endif

                            <div class="col-md-6">
                                <label>Business Partner:</label>
                                <div class="input-group col-md-12">
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <select id="BusinessPartner" onchange="businessPartnerChecker();headerChecker();" name="BusinessPartner" class="form-control input-sm pull-right">
                                            @foreach($bus_part as $bp)
                                                @if($bp->id == $singleRequest->BusinessPartner)
                                                    <option value="{{$bp->id}}" selected>{{$bp->Name}}</option>
                                                @else
                                                    <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select id="BusinessPartner" onchange="businessPartnerChecker();headerChecker();" name="BusinessPartner" class="form-control input-sm pull-right">
                                            <option value="NULL" disabled selected hidden>[Choose one]</option>
                                            @foreach($bus_part as $bp)
                                                <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Date:</label>
                                <div class="input-group col-md-12">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <input name="Date" type="text" class="form-control input-sm pull-right" id="Date" required="required" value="<?php echo date('m/j/Y g:i a',strtotime($singleRequest->Date));?>">
                                    @else
                                        <input name="Date" type="text" class="form-control input-sm pull-right" id="Date" required="required" value="">
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group col-md-12" style="display:none;">
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <input name="id" type="hidden" class="form-control input-sm pull-right" value="{{$singleRequest->id}}">
                                        <input name="AssetNumber" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->RequestNo}}" id="AssetNumber" required="required">
                                    @else
                                        <input name="AssetNumber" type="text" class="form-control input-sm pull-right" value="000000" id="AssetNumber" required="required">
                                    @endif
                                </div>

                                <label>Business Partner Name:</label>
                                <div class="input-group col-md-12">
                                    @if(isset($singleRequest) && isset($lineItems))
                                        @if($singleRequest->BusinessPartner == 3 || $singleRequest->BusinessPartner == 4)
                                            <input name="BusinessPartnerName" disabled onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->BusinessPartnerName}}" id="BusinessPartnerName" required="required">
                                        @else
                                            <input name="BusinessPartnerName" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->BusinessPartnerName}}" id="BusinessPartnerName" required="required">
                                        @endif
                                    @else
                                        <input name="BusinessPartnerName" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" id="BusinessPartnerName" required="required">
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-6">
                                <label>Contact:</label>
                                <div class="input-group col-md-12">
                                @if(isset($singleRequest) && isset($lineItems))
                                    <input type="text" id="Contact" name="Contact" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right" value="{{$singleRequest->Contact}}">
                                @else
                                    <input type="text" id="Contact" name="Contact" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right">
                                @endif
                                    </div>
                            </div>
                                <div class="col-md-6">
                                    <label>Contact Email:</label>
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <input type="text" id="ContactEmail" name="ContactEmail" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right" value="{{$singleRequest->ContactEmail}}">
                                    @else
                                        <input type="text" id="ContactEmail" name="ContactEmail" onkeyup="headerChecker();" onchange="headerChecker();" class="form-control input-sm pull-right">
                                    @endif
                                </div>

                                <div class="col-md-6" style="display:none;">
                                    <label>Request Type</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <select id="RequestType" onchange="headerChecker();" name="RequestType" class="form-control input-sm pull-right">
                                                @foreach($reqtype as $bp)
                                                    @if($bp->id == $singleRequest->RequestType)
                                                        <option value="{{$bp->id}}" selected>{{$bp->Name}}</option>
                                                    @else
                                                        <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @else
                                            <select id="RequestType" onchange="headerChecker();" name="RequestType" class="form-control input-sm pull-right">
                                                {{--<option value="NULL" disabled selected hidden>[Choose one]</option>--}}
                                                @foreach($reqtype as $bp)
                                                    <option value="{{$bp->id}}" >{{$bp->Name}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Project/Purpose:</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <input name="Project" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" value="{{$singleRequest->Project}}" id="Project" required="required">
                                        @else
                                            <input name="Project" onkeyup="headerChecker();" onchange="headerChecker();" type="text" class="form-control input-sm pull-right" id="Project" required="required">
                                        @endif
                                    </div>

                                </div>

                                <div class="col-xs-12" style="padding-top:3%;">
                                    {{--<label>Line Items</label>--}}
                                    @if(isset($singleRequest) && isset($lineItems))
                                        <input type="hidden" id="tablecount" name="assetCount" value="0"/>
                                        <input type="hidden" id="realTableCount" name="realAssetCount" value="{{$lineItemsCount}}"/>
                                        <input type="hidden" id="savedTableCount" name="savedAssetCount" value="{{$lineItemsCount}}"/>
                                    @else
                                        <input type="hidden" id="tablecount" name="assetCount" value="0"/>
                                        <input type="hidden" id="realTableCount" name="realAssetCount" value="0"/>
                                    @endif
                                    &nbsp;&nbsp;&nbsp;
                                    {{--<button type="button" id="addingAssets" onclick="addAssets('aBody');" class='btn btn-xs btn-success'><i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Line Item</button>--}}
                                </div>

                                <div class="col-md-12" style="padding-top:5px;">
                                    <div class="nav-tabs-custom" style="background-color:#eee;margin:0px;">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" style="padding-bottom:15px;">Tools</a></li>
                                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" style="padding-bottom:15px;">Consumables</a></li>
                                            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" style="padding-bottom:15px;">Equipment</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <button type="button" id="addingAssetsR" onclick="addAssets('aBodyR');" class='btn btn-sm btn-success'><i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Line Item</button>
                                                <div id="aHeaders" class="box-primary" style="align-content: center;">
                                                    <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:13px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                                        <div class="col-xs-6">Description</div>
                                                        <div class="col-xs-1">Qty</div>
                                                        <div class="col-xs-2">UOM</div>
                                                        <div class="col-xs-2">Location</div>
                                                        <div class="col-xs-1"></div>
                                                    </div>
                                                </div>
                                                <div id="aBodyR" class="box-primary" style="align-content: center;height:240px;max-height:240px;overflow-y:scroll;">
                                                    @if(isset($singleRequest) && isset($lineItems))
                                                        @foreach($lineItems as $li)
                                                            @if($li->Returnable == 1 && $li->ItemType == 0)
                                                            <div class="row" id='divSavedEdit{{$li->LineNum}}' style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0px;">
                                                                <div class="col-md-5" style='padding:3px;'>
                                                                    <input name="savedID[{{$li->LineNum}}]" type="hidden" value="{{$li->id}}">
                                                                    <input value="{{$li->Description}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDescription[{{$li->LineNum}}]' id='eSavedDescription[{{$li->LineNum}}]'/>
                                                                </div>
                                                                <div class="col-md-1" style='padding:3px;'>
                                                                    <button type='button' onclick='showSavedAuto({{$li->LineNum}});' class='btn btn-sm btn-primary btn-block'>Select</button>
                                                                </div>
                                                                <div class="col-md-1" style='padding:3px;'>
                                                                    <input value="{{$li->Quantity}}" type='number' class='form-control input-sm' style="width:100%;" name='savedQuantity[{{$li->LineNum}}]' id='eSavedQuantity[{{$li->LineNum}}]' onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});"/>
                                                                </div>
                                                                <div class="col-md-2" style='padding:3px;'>
                                                                    <select class='form-control input-sm' style='width:100%;' name='savedUOM[{{$li->LineNum}}]' id='eSavedUOM[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                        <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                        @foreach($uoms as $u)
                                                                            @if($li->UOM == $u->id)
                                                                                <option value='{{$u->id}}' selected>{{$u->Name}}</option>
                                                                            @else
                                                                                <option value='{{$u->id}}'>{{$u->Name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2" style='padding:3px;'>
                                                                    {{--<input value="{{$li->Destination}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDestination[{{$li->LineNum}}]' id='eSavedDestination[{{$li->LineNum}}]'/>
                                                                    --}}

                                                                    <select class='form-control input-sm' style='width:100%;' name='savedDestination[{{$li->LineNum}}]' id='eSavedDestination[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                        <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                        @foreach($generaldestinations as $u)
                                                                            @if($li->Destination == $u->id)
                                                                                <option value='{{$u->id}}' style='font-weight: bolder;' selected>{{$u->Name}}</option>
                                                                                @foreach($specificdestinations as $spc)
                                                                                    @if($spc->Parent == $u->id)
                                                                                        <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                <option value='{{$u->id}}' style='font-weight: bolder;'>{{$u->Name}}</option>
                                                                                @foreach($specificdestinations as $spc)
                                                                                    @if($spc->Parent == $u->id)
                                                                                        @if($li->Destination == $spc->id)
                                                                                            <option value='{{$spc->id}}' selected>{{$spc->Name}}</option>
                                                                                        @else
                                                                                            <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                        @endif
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                    <select class='form-control input-sm' style='width:100%;' name='savedStorage[{{$li->LineNum}}]' id='eSavedStorage[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                        <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                        @foreach($generaldestinations as $u)
                                                                            @if($li->iline_to_storage->Storage == $u->id)
                                                                                <option value='{{$u->id}}' style='font-weight: bolder;' selected>{{$u->Name}}</option>
                                                                                @foreach($specificdestinations as $spc)
                                                                                    @if($spc->Parent == $u->id)
                                                                                        <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                    @endif
                                                                                @endforeach
                                                                            @else
                                                                                <option value='{{$u->id}}' style='font-weight: bolder;'>{{$u->Name}}</option>
                                                                                @foreach($specificdestinations as $spc)
                                                                                    @if($spc->Parent == $u->id)
                                                                                        @if($li->iline_to_storage->Storage == $spc->id)
                                                                                            <option value='{{$spc->id}}' selected>{{$spc->Name}}</option>
                                                                                        @else
                                                                                            <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                        @endif
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    </select>

                                                                    @if($li->Returnable==0)
                                                                        <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]' style="display:none;">
                                                                    @elseif($li->Returnable==1)
                                                                        <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]' checked style="display:none;">
                                                                    @endif
                                                                    <input type="hidden" name="savedDeleted[{{$li->LineNum}}]" id='eSavedDeleted[{{$li->LineNum}}]' value="0">
                                                                </div>
                                                                <div class="col-md-1" style='padding:3px;'>
                                                                    <button type='button' class='btn btn-sm' id='confirmSavedAsset{{$li->LineNum}}' onclick='confirmSavedAsset("{{$li->LineNum}}");'><i class='fa fa-check'></i></button>
                                                                    <button type='button' class='btn btn-sm cancelButton' onclick='cancelSavedAsset("{{$li->LineNum}}");'><i class='fa fa-close'></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="row" id='divSavedView{{$li->LineNum}}' style="font-size:12px;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:1px;border-bottom: 1px solid #e9e9e9;">
                                                                <div class="col-md-6" style='padding:7px 14px;'><span style="width:100%;word-wrap:break-word;" id='vSavedDescription[{{$li->LineNum}}]'>{{$li->Description}}&nbsp;</span></div>

                                                    <div class="col-md-1" style='padding:7px 14px;'><span style="width:100%;" id='vSavedQuantity[{{$li->LineNum}}]'>{{$li->Quantity}}&nbsp;</span></div>
                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;" id='vSavedUOM[{{$li->LineNum}}]'>{{$li->oUOM->Name}}&nbsp;</span></div>
                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;">
                                                        <span id='vSavedDestination[{{$li->LineNum}}]'><b>Destination</b>: {{$li->iline_to_destination->Name}}</span><br>
                                                        <span id='vSavedStorage[{{$li->LineNum}}]'><b>Storage</b>: {{$li->iline_to_storage->storage_to_destination->Name}}</span>
                                                    </div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <button type='button' class='btn btn-sm editButton' onclick='editSavedAsset("{{$li->LineNum}}");'><i class='fa fa-edit'></i></button>
                                                        <button type='button' class='btn btn-sm deleteButton' onclick='removeSavedAsset("{{$li->LineNum}}");'><i class='fa fa-trash-o'></i></button>
                                                    </div>
                                                </div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_2">
                                                <div id="aHeaders" class="box-primary" style="align-content: center;">
                                                    <button type="button" id="addingAssetsN" onclick="addAssets('aBodyN');" class='btn btn-sm btn-success'><i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Line Item</button>
                                                    <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:13px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                                        <div class="col-xs-6">Description</div>
                                                        <div class="col-xs-1">Qty</div>
                                                        <div class="col-xs-2">UOM</div>
                                                        <div class="col-xs-2">Location</div>
                                                        <div class="col-xs-1"></div>
                                                    </div>
                                                </div>
                                                <div id="aBodyN" class="box-primary" style="align-content: center;height:240px;max-height:240px;overflow-y:scroll;">
                                                    @if(isset($singleRequest) && isset($lineItems))
                                                        @foreach($lineItems as $li)
                                                            @if($li->Returnable == 0)
                                                                <div class="row" id='divSavedEdit{{$li->LineNum}}' style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0px;">
                                                                    <div class="col-md-5" style='padding:3px;'>
                                                                        <input name="savedID[{{$li->LineNum}}]" type="hidden" value="{{$li->id}}">
                                                                        <input value="{{$li->Description}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDescription[{{$li->LineNum}}]' id='eSavedDescription[{{$li->LineNum}}]'/>
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <button type='button' onclick='showSavedAuto({{$li->LineNum}});' class='btn btn-sm btn-primary btn-block'>Select</button>
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <input value="{{$li->Quantity}}" type='number' class='form-control input-sm' style="width:100%;" name='savedQuantity[{{$li->LineNum}}]' id='eSavedQuantity[{{$li->LineNum}}]' onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});"/>
                                                                    </div>
                                                                    <div class="col-md-2" style='padding:3px;'>
                                                                        <select class='form-control input-sm' style='width:100%;' name='savedUOM[{{$li->LineNum}}]' id='eSavedUOM[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                            @foreach($uoms as $u)
                                                                                @if($li->UOM == $u->id)
                                                                                    <option value='{{$u->id}}' selected>{{$u->Name}}</option>
                                                                                @else
                                                                                    <option value='{{$u->id}}'>{{$u->Name}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-2" style='padding:3px;'>
                                                                        {{--<input value="{{$li->Destination}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDestination[{{$li->LineNum}}]' id='eSavedDestination[{{$li->LineNum}}]'/>
                                                                        --}}

                                                                        <select class='form-control input-sm' style='width:100%;' name='savedDestination[{{$li->LineNum}}]' id='eSavedDestination[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                            @foreach($generaldestinations as $u)
                                                                                @if($li->Destination == $u->id)
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;' selected>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                        @endif
                                                                                    @endforeach
                                                                                @else
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;'>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            @if($li->Destination == $spc->id)
                                                                                                <option value='{{$spc->id}}' selected>{{$spc->Name}}</option>
                                                                                            @else
                                                                                                <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                        <select class='form-control input-sm' style='width:100%;' name='savedStorage[{{$li->LineNum}}]' id='eSavedStorage[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                            @foreach($generaldestinations as $u)
                                                                                @if($li->iline_to_storage->Storage == $u->id)
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;' selected>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                        @endif
                                                                                    @endforeach
                                                                                @else
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;'>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            @if($li->iline_to_storage->Storage == $spc->id)
                                                                                                <option value='{{$spc->id}}' selected>{{$spc->Name}}</option>
                                                                                            @else
                                                                                                <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            @endforeach
                                                                        </select>

                                                                        @if($li->Returnable==0)
                                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]' style="display:none;">
                                                                        @elseif($li->Returnable==1)
                                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]' checked style="display:none;">
                                                                        @endif
                                                                        <input type="hidden" name="savedDeleted[{{$li->LineNum}}]" id='eSavedDeleted[{{$li->LineNum}}]' value="0">
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <button type='button' class='btn btn-sm' id='confirmSavedAsset{{$li->LineNum}}' onclick='confirmSavedAsset("{{$li->LineNum}}");'><i class='fa fa-check'></i></button>
                                                                        <button type='button' class='btn btn-sm cancelButton' onclick='cancelSavedAsset("{{$li->LineNum}}");'><i class='fa fa-close'></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id='divSavedView{{$li->LineNum}}' style="font-size:12px;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:1px;border-bottom: 1px solid #e9e9e9;">
                                                                    <div class="col-md-6" style='padding:7px 14px;'><span style="width:100%;word-wrap:break-word;" id='vSavedDescription[{{$li->LineNum}}]'>{{$li->Description}}&nbsp;</span></div>

                                                                    <div class="col-md-1" style='padding:7px 14px;'><span style="width:100%;" id='vSavedQuantity[{{$li->LineNum}}]'>{{$li->Quantity}}&nbsp;</span></div>
                                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;" id='vSavedUOM[{{$li->LineNum}}]'>{{$li->oUOM->Name}}&nbsp;</span></div>
                                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;">
                                                                        <span id='vSavedDestination[{{$li->LineNum}}]'><b>Destination</b>: {{$li->iline_to_destination->Name}}</span><br>
                                                                        <span id='vSavedStorage[{{$li->LineNum}}]'><b>Storage</b>: {{$li->iline_to_storage->storage_to_destination->Name}}</span>
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <button type='button' class='btn btn-sm editButton' onclick='editSavedAsset("{{$li->LineNum}}");'><i class='fa fa-edit'></i></button>
                                                                        <button type='button' class='btn btn-sm deleteButton' onclick='removeSavedAsset("{{$li->LineNum}}");'><i class='fa fa-trash-o'></i></button>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_3">
                                                <div id="aHeaders" class="box-primary" style="align-content: center;">
                                                    <button type="button" id="addingAssetsE" onclick="addAssets('aBodyE');" class='btn btn-sm btn-success'><i class="fa fa-btn fa-plus"></i>&nbsp;&nbsp;Add Line Item</button>
                                                    <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:13px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                                        <div class="col-xs-6">Description</div>
                                                        <div class="col-xs-1">Qty</div>
                                                        <div class="col-xs-2">UOM</div>
                                                        <div class="col-xs-2">Location</div>
                                                        <div class="col-xs-1"></div>
                                                    </div>
                                                </div>
                                                <div id="aBodyE" class="box-primary" style="align-content: center;height:240px;max-height:240px;overflow-y:scroll;">
                                                    @if(isset($singleRequest) && isset($lineItems))
                                                        @foreach($lineItems as $li)
                                                            @if($li->Returnable == 1 && $li->ItemType == 1)
                                                                <div class="row" id='divSavedEdit{{$li->LineNum}}' style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0px;">
                                                                    <div class="col-md-5" style='padding:3px;'>
                                                                        <input name="savedID[{{$li->LineNum}}]" type="hidden" value="{{$li->id}}">
                                                                        <input value="{{$li->Description}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDescription[{{$li->LineNum}}]' id='eSavedDescription[{{$li->LineNum}}]'/>
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <button type='button' onclick='showSavedAuto({{$li->LineNum}});' class='btn btn-sm btn-primary btn-block'>Select</button>
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <input value="{{$li->Quantity}}" type='number' class='form-control input-sm' style="width:100%;" name='savedQuantity[{{$li->LineNum}}]' id='eSavedQuantity[{{$li->LineNum}}]' onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});"/>
                                                                    </div>
                                                                    <div class="col-md-2" style='padding:3px;'>
                                                                        <select class='form-control input-sm' style='width:100%;' name='savedUOM[{{$li->LineNum}}]' id='eSavedUOM[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                            @foreach($uoms as $u)
                                                                                @if($li->UOM == $u->id)
                                                                                    <option value='{{$u->id}}' selected>{{$u->Name}}</option>
                                                                                @else
                                                                                    <option value='{{$u->id}}'>{{$u->Name}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-2" style='padding:3px;'>
                                                                        {{--<input value="{{$li->Destination}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDestination[{{$li->LineNum}}]' id='eSavedDestination[{{$li->LineNum}}]'/>
                                                                        --}}

                                                                        <select class='form-control input-sm' style='width:100%;' name='savedDestination[{{$li->LineNum}}]' id='eSavedDestination[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                            @foreach($generaldestinations as $u)
                                                                                @if($li->Destination == $u->id)
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;' selected>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                        @endif
                                                                                    @endforeach
                                                                                @else
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;'>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            @if($li->Destination == $spc->id)
                                                                                                <option value='{{$spc->id}}' selected>{{$spc->Name}}</option>
                                                                                            @else
                                                                                                <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                        <select class='form-control input-sm' style='width:100%;' name='savedStorage[{{$li->LineNum}}]' id='eSavedStorage[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                                            @foreach($generaldestinations as $u)
                                                                                @if($li->iline_to_storage->Storage == $u->id)
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;' selected>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                        @endif
                                                                                    @endforeach
                                                                                @else
                                                                                    <option value='{{$u->id}}' style='font-weight: bolder;'>{{$u->Name}}</option>
                                                                                    @foreach($specificdestinations as $spc)
                                                                                        @if($spc->Parent == $u->id)
                                                                                            @if($li->iline_to_storage->Storage == $spc->id)
                                                                                                <option value='{{$spc->id}}' selected>{{$spc->Name}}</option>
                                                                                            @else
                                                                                                <option value='{{$spc->id}}'>{{$spc->Name}}</option>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            @endforeach
                                                                        </select>

                                                                        @if($li->Returnable==0)
                                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]' style="display:none;">
                                                                        @elseif($li->Returnable==1)
                                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]' checked style="display:none;">
                                                                        @endif
                                                                        <input type="hidden" name="savedDeleted[{{$li->LineNum}}]" id='eSavedDeleted[{{$li->LineNum}}]' value="0">
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <button type='button' class='btn btn-sm' id='confirmSavedAsset{{$li->LineNum}}' onclick='confirmSavedAsset("{{$li->LineNum}}");'><i class='fa fa-check'></i></button>
                                                                        <button type='button' class='btn btn-sm cancelButton' onclick='cancelSavedAsset("{{$li->LineNum}}");'><i class='fa fa-close'></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id='divSavedView{{$li->LineNum}}' style="font-size:12px;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:1px;border-bottom: 1px solid #e9e9e9;">
                                                                    <div class="col-md-6" style='padding:7px 14px;'><span style="width:100%;word-wrap:break-word;" id='vSavedDescription[{{$li->LineNum}}]'>{{$li->Description}}&nbsp;</span></div>

                                                                    <div class="col-md-1" style='padding:7px 14px;'><span style="width:100%;" id='vSavedQuantity[{{$li->LineNum}}]'>{{$li->Quantity}}&nbsp;</span></div>
                                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;" id='vSavedUOM[{{$li->LineNum}}]'>{{$li->oUOM->Name}}&nbsp;</span></div>
                                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;">
                                                                        <span id='vSavedDestination[{{$li->LineNum}}]'><b>Destination</b>: {{$li->iline_to_destination->Name}}</span><br>
                                                                        <span id='vSavedStorage[{{$li->LineNum}}]'><b>Storage</b>: {{$li->iline_to_storage->storage_to_destination->Name}}</span>
                                                                    </div>
                                                                    <div class="col-md-1" style='padding:3px;'>
                                                                        <button type='button' class='btn btn-sm editButton' onclick='editSavedAsset("{{$li->LineNum}}");'><i class='fa fa-edit'></i></button>
                                                                        <button type='button' class='btn btn-sm deleteButton' onclick='removeSavedAsset("{{$li->LineNum}}");'><i class='fa fa-trash-o'></i></button>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            {{--<div class="form-group col-xs-12">
                                    <div id="aHeaders" class="box-primary" style="align-content: center;">
                                        <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:13px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                            <div class="col-xs-6">Description</div>
                                            --}}{{--<div class="col-xs-3">Serial Number</div>--}}{{--
                                            <div class="col-xs-1">Qty</div>
                                            <div class="col-xs-2">UOM</div>
                                            <div class="col-xs-2">Returnable?</div>
                                            <div class="col-xs-1"></div>
                                        </div>
                                    </div>
                                    <div id="aBody" class="box-primary" style="align-content: center;height:240px;max-height:240px;overflow-y:scroll;">
                                        <!--Saved Line Items-->
                                        @if(isset($singleRequest) && isset($lineItems))
                                            @foreach($lineItems as $li)
                                                <div class="row" id='divSavedEdit{{$li->LineNum}}' style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0px;">
                                                    <div class="col-md-5" style='padding:3px;'>
                                                        <input name="savedID[{{$li->LineNum}}]" type="hidden" value="{{$li->id}}">
                                                        <input value="{{$li->Description}}" maxlength="100" onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});" type='text' class='form-control input-sm' style="width:100%;" name='savedDescription[{{$li->LineNum}}]' id='eSavedDescription[{{$li->LineNum}}]'/>
                                                    </div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <button type='button' onclick='showSavedAuto({{$li->LineNum}});' class='btn btn-sm btn-primary btn-block'>Select</button>
                                                    </div>
                                                    --}}{{--<div class="col-md-3" style='padding:3px;'>
                                                        <input value="{{$li->SerialNum}}" type='text' class='form-control input-sm' style="width:100%;" name='savedSerialNum[{{$li->LineNum}}]' id='eSavedSerialNum[{{$li->LineNum}}]'/>
                                                    </div>--}}{{--
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <input value="{{$li->Quantity}}" type='number' class='form-control input-sm' style="width:100%;" name='savedQuantity[{{$li->LineNum}}]' id='eSavedQuantity[{{$li->LineNum}}]' onkeyup="savedChecks({{$li->LineNum}});" onchange="savedChecks({{$li->LineNum}});"/>
                                                    </div>
                                                    <div class="col-md-2" style='padding:3px;'>
                                                        <select class='form-control input-sm' style='width:100%;' name='savedUOM[{{$li->LineNum}}]' id='eSavedUOM[{{$li->LineNum}}]' onchange="savedChecks({{$li->LineNum}});">
                                                            <option value=\'NULL\' disabled selected>Please Select...</option>
                                                        @foreach($uoms as $u)
                                                            @if($li->UOM == $u->id)
                                                                <option value='{{$u->id}}' selected>{{$u->Name}}</option>
                                                            @else
                                                                <option value='{{$u->id}}'>{{$u->Name}}</option>
                                                            @endif
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2" style='align-content: center;text-align: center;vertical-align: middle;'>
                                                        @if($li->Returnable==0)
                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]'>
                                                        @elseif($li->Returnable==1)
                                                            <input type='checkbox' name='savedReturnable[{{$li->LineNum}}]' id='eSavedReturnable[{{$li->LineNum}}]' checked>
                                                        @endif
                                                        <input type="hidden" name="savedDeleted[{{$li->LineNum}}]" id='eSavedDeleted[{{$li->LineNum}}]' value="0">
                                                    </div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <button type='button' class='btn btn-sm' id='confirmSavedAsset{{$li->LineNum}}' onclick='confirmSavedAsset("{{$li->LineNum}}");'><i class='fa fa-check'></i></button>
                                                        <button type='button' class='btn btn-sm cancelButton' onclick='cancelSavedAsset("{{$li->LineNum}}");'><i class='fa fa-close'></i></button>
                                                    </div>
                                                </div>
                                                <div class="row" id='divSavedView{{$li->LineNum}}' style="font-size:12px;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:1px;border-bottom: 1px solid #e9e9e9;">
                                                    <div class="col-md-6" style='padding:7px 14px;'><span style="width:100%;word-wrap:break-word;" id='vSavedDescription[{{$li->LineNum}}]'>{{$li->Description}}&nbsp;</span></div>
                                                    --}}{{--<div class="col-md-3" style='padding:9px 14px;'><span style="width:100%;" id='vSavedSerialNum[{{$li->LineNum}}]'>{{$li->SerialNum}}&nbsp;</span></div>--}}{{--
                                                    <div class="col-md-1" style='padding:7px 14px;'><span style="width:100%;" id='vSavedQuantity[{{$li->LineNum}}]'>{{$li->Quantity}}&nbsp;</span></div>
                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;" id='vSavedUOM[{{$li->LineNum}}]'>{{$li->oUOM->Name}}&nbsp;</span></div>
                                                    <div class="col-md-2" style='padding:7px 14px;'><span style="width:100%;" id='vSavedReturnable[{{$li->LineNum}}]'>
                                                            @if($li->Returnable==0)
                                                                No
                                                            @else
                                                                Yes
                                                            @endif
                                                            &nbsp;</span></div>
                                                    <div class="col-md-1" style='padding:3px;'>
                                                        <button type='button' class='btn btn-sm editButton' onclick='editSavedAsset("{{$li->LineNum}}");'><i class='fa fa-edit'></i></button>
                                                        <button type='button' class='btn btn-sm deleteButton' onclick='removeSavedAsset("{{$li->LineNum}}");'><i class='fa fa-trash-o'></i></button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>--}}
                            <div class="form-group col-xs-12">
                                @if(isset($singleRequest) && isset($lineItems))
                                    <button type="submit" id="sendrequest" name="submitRequest" class="btn btn-primary" data-toggle="modal">Submit Request</button>
                                    <button type="submit" id="saverequest" name="saveRequest" class="btn btn-primary" data-toggle="modal">Save Request</button>
                                @else
                                    <button type="submit" id="sendrequest" name="submitRequest" class="btn btn-primary" data-toggle="modal" disabled>Submit Request</button>
                                    <button type="submit" id="saverequest" name="saveRequest" class="btn btn-primary" data-toggle="modal" disabled>Save Request</button>
                                @endif
                                <button type="button" id="" class="btn btn-primary" onclick="location.reload();">Cancel</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </form>
        <div id="autofill" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left:30%;margin-right:30%;margin-top:10%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="modalTitle">Line Item Picker</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="theLineID">
                        <input type="hidden" id="theSavedLineID">
                        <table class="table table-condensed" id="AutoDesc">
                            <thead>
                                <th width="95%">Description</th>
                                <th> </th>
                            </thead>
                        @foreach($descAutoComp as $dA)
                            <tr>
                                <td style="width:100%;word-wrap:break-word;">
                                    {{$dA->Description}}
                                </td>
                                <td>
                                    <button type="button" id="" class="btn btn-primary btn-sm" onclick="getSetDescription('{{$dA->Description}}');">Select</button>
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    </div>
                    <div class="modal-footer" id="approveSet">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>

                </div>

            </div>
        </div>

        <table id="destinationTable" style="display:none;">
            @foreach($generaldestinations as $lst)
                <tr>
                    <td>{{$lst->id}}</td>
                    <td>{{$lst->Name}}</td>
                    <td>{{$lst->SubLocation}}</td>
                </tr>
                @foreach($specificdestinations as $spc)
                    @if($spc->Parent == $lst->id)
                    <tr>
                        <td>{{$spc->id}}</td>
                        <td>{{$spc->Name}}</td>
                        <td>{{$spc->SubLocation}}</td>
                    </tr>
                    @endif
                @endforeach
            @endforeach
        </table>

        <table id="lineStatRef" style="display:none;">
        @foreach($linestat as $lst)
            <tr>
            <td>{{$lst->id}}</td>
            <td>{{$lst->Name}}</td>
            </tr>
        @endforeach

        <table id="uomRef" style="display:none;">
        @foreach($uoms as $u)
                    <tr>
                        <td>{{$u->id}}</td>
                        <td>{{$u->Name}}</td>
                    </tr>
        @endforeach
    <?php
        $derContact = "";
        foreach($contacts as $c){
            $derContact = $derContact."'".$c->Contact."',";
        }
        $derContact = substr(trim($derContact), 0, -1);
        //echo $derContact;
    ?>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('/js/autocomplete.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        $(function () {
            $('#AutoDesc').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": false,
                "scrollY": "200px",
                "autoWidth": true
            });
        });

        $('body').on('focus',".recurring_dates", function(){
            $(this).daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        var editMode = 0;
        var states = [
            <?php echo $derContact;?>
        ];
        $(function() {
            $(".tags").autocomplete({
                source:[states]
            });
        });

        function addAssets(divName){
            editMode = 1;

            $('#saverequest').attr('disabled', 'disabled');
            $('#sendrequest').attr('disabled', 'disabled');
            var theTableCount = document.getElementById('tablecount').value;
            var newdiv = document.createElement('div');

            //Setting UOMs in Drop Down
            var ddUOMs = "<select onchange='requestorChecks("+theTableCount+");' class='form-control input-sm' style='width:100%;' name='UOM["+theTableCount+"]' id='eUOM["+theTableCount+"]'>";
            ddUOMs = ddUOMs + "<option value=\'NULL\' disabled selected>Please Select...</option>";
            var uomTable = document.getElementById('uomRef');
            var uomRowLength = uomTable.rows.length;
            for (i = 0; i < uomRowLength; i++){
                var uomTableCells = uomTable.rows.item(i).cells;
                ddUOMs = ddUOMs + "<option value='"+uomTableCells.item(0).innerHTML+"'>"+uomTableCells.item(1).innerHTML+"</option>";
            }
            ddUOMs = ddUOMs + "</select>";

            var chkBx = "";
            var itemType = 0;
            if(divName == 'aBodyR'){
                chkBx = "<input type='checkbox' name='Returnable["+theTableCount+"]' id='eReturnable["+theTableCount+"]' checked style='display:none;'>";
            }
            else if(divName == 'aBodyN'){
                chkBx = "<input type='checkbox' name='Returnable["+theTableCount+"]' id='eReturnable["+theTableCount+"]' style='display:none;'>";
            }
            else if(divName == 'aBodyE'){
                chkBx = "<input type='checkbox' name='Returnable["+theTableCount+"]' id='eReturnable["+theTableCount+"]' checked style='display:none;'>";
                itemType = 1;
            }

            //Setting Destinations in Drop-Down
            var ddDestinations = "<select onchange='requestorChecks("+theTableCount+");' class='form-control input-sm' style='width:100%;' name='Destinations["+theTableCount+"]' id='Destinations["+theTableCount+"]'>";
            ddDestinations = ddDestinations + "<option value=\'NULL\' disabled selected>Destination</option>";
            var destTable = document.getElementById('destinationTable');
            var destRowLength = destTable.rows.length;
            for (i = 0; i < destRowLength; i++){
                var destTableCells = destTable.rows.item(i).cells;
                if(destTableCells.item(2).innerHTML == "1")
                    ddDestinations = ddDestinations + "<option value='"+destTableCells.item(0).innerHTML+"'>"+destTableCells.item(1).innerHTML+"</option>";
                else
                    ddDestinations = ddDestinations + "<option value='"+destTableCells.item(0).innerHTML+"'  style='font-weight: bolder;'>"+destTableCells.item(1).innerHTML+"</option>";
            }
            ddDestinations = ddDestinations + "</select>";
            //Setting Storage in Drop-Down
            var ddStorage = "<select onchange='requestorChecks("+theTableCount+");' class='form-control input-sm' style='width:100%;' name='Storage["+theTableCount+"]' id='Storage["+theTableCount+"]'>";
            ddStorage = ddStorage + "<option value=\'NULL\' disabled selected>Storage</option>";
            var storTable = document.getElementById('destinationTable');
            var storRowLength = storTable.rows.length;
            for (i = 0; i < storRowLength; i++){
                var storTableCells = storTable.rows.item(i).cells;
                if(storTableCells.item(2).innerHTML == "1")
                    ddStorage = ddStorage + "<option value='"+storTableCells.item(0).innerHTML+"'>"+storTableCells.item(1).innerHTML+"</option>";
                else
                    ddStorage = ddStorage + "<option value='"+storTableCells.item(0).innerHTML+"'  style='font-weight: bolder;'>"+storTableCells.item(1).innerHTML+"</option>";
            }
            ddStorage = ddStorage + "</select>";


            newdiv.innerHTML = "<div class=\"row\" id='divEdit"+theTableCount+"' style=\"width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0px;\">" +
            "<div class=\"col-md-5\" style='padding:3px;'>" +
            "<input placeholder='Description' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' maxlength='100' type='text' class='form-control input-sm' style=\"width:100%;\" name='Description["+theTableCount+"]' id='eDescription["+theTableCount+"]'/>"+
            "</div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' onclick='showAuto("+theTableCount+");' class='btn btn-sm btn-primary btn-block'>Select</button>" +
            "</div>"+
            /*"<div class=\"col-md-3\" style='padding:3px;'>" +
            "<input placeholder='Serial Number' onkeyup='requestorChecks("+theTableCount+");' type='text' class='form-control input-sm' style=\"width:100%;\" name='SerialNum["+theTableCount+"]' id='eSerialNum["+theTableCount+"]'/></div>"+*/
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<input placeholder='Qty' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' type='number' class='form-control input-sm' style=\"width:100%;\" name='Quantity["+theTableCount+"]' id='eQuantity["+theTableCount+"]'/></div>"+
            "<div class=\"col-md-2\" style='padding:3px;'>" +
            ddUOMs+
            "</div>"+
            "<div class=\"col-md-2\" style='padding:3px;'>" +
            //"<input placeholder='Destination' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' maxlength='100' type='text' class='form-control input-sm' style=\"width:100%;\" name='Destination["+theTableCount+"]' id='eDestination["+theTableCount+"]'/>"+
            ddDestinations+
            ddStorage+
            "</div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            chkBx+
            "<input type='hidden' name='ItemType["+theTableCount+"]' id='eItemType["+theTableCount+"]' value="+itemType+">" +
            "<button type='button' class='btn btn-sm' id='confirmAsset"+theTableCount+"' onclick='confirmAsset("+theTableCount+");' disabled><i class='fa fa-check'></i></button>&nbsp;" +
            "<button type='button' class='btn btn-sm cancelButton' onclick='cancelAsset("+theTableCount+");'><i class='fa fa-close'></i></button>"+
            "</div>"+
            "</div>"+
            "<div class=\"row\" id='divView"+theTableCount+"' style=\"font-size:12px;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:1px;display:none;border-bottom: 1px solid #e9e9e9;\">" +
            "<div class=\"col-md-6\" style='padding:7px 14px;'><span style=\"width:100%;word-wrap:break-word;\" id='vDescription["+theTableCount+"]'/></span></div>"+
            //"<div class=\"col-md-3\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vSerialNum["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-1\" style='padding:7px 14px;'><span style=\"width:100%;word-wrap:break-word;\" id='vQuantity["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:7px 14px;'><span style=\"width:100%;\" id='vUOM["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:7px 14px;'><span style=\"width:100%;\" id='vDestination["+theTableCount+"]'/>Destination</span><br><span style=\"width:100%;\" id='vStorage["+theTableCount+"]'/>Storage</span></div>"+
            "<div class=\"col-md-3\" style='padding:7px 14px;display:none;'><span style=\"width:100%;\" id='vReturnable["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' class='btn btn-sm editButton' onclick='editAsset("+theTableCount+");'><i class='fa fa-edit'></i></button>&nbsp;" +
            "<button type='button' class='btn btn-sm deleteButton' onclick='removeAsset("+theTableCount+");'><i class='fa fa-trash-o'></i></button></div>"+
            "</div>";

            document.getElementById(divName).appendChild(newdiv);
            document.getElementById('tablecount').value = +theTableCount + 1;
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value + 1;
            document.getElementById('addingAssetsR').disabled=true;
            document.getElementById('addingAssetsN').disabled=true;
            document.getElementById('addingAssetsE').disabled=true;
            document.getElementById("eDescription["+theTableCount+"]").focus();
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');

        }

        function confirmAsset(lineNumber){
            editMode = 0;
            document.getElementById('divEdit'+lineNumber).style.display = "none";
            document.getElementById('divView'+lineNumber).style.display = "block";
            document.getElementById('vDescription['+lineNumber+']').innerHTML = document.getElementById('eDescription['+lineNumber+']').value;
            //document.getElementById('vDestination['+lineNumber+']').innerHTML = document.getElementById('eDestination['+lineNumber+']').value;
            //document.getElementById('vSerialNum['+lineNumber+']').innerHTML = document.getElementById('eSerialNum['+lineNumber+']').value;
            document.getElementById('vQuantity['+lineNumber+']').innerHTML = document.getElementById('eQuantity['+lineNumber+']').value;
            if(document.getElementById('eReturnable['+lineNumber+']').checked)
                document.getElementById('vReturnable['+lineNumber+']').innerHTML = "Yes";
            else
                document.getElementById('vReturnable['+lineNumber+']').innerHTML = "No";
            //var LineStatusView = document.getElementById("eLineStatus["+lineNumber+"]");
            //var strLineStatus = LineStatusView.options[LineStatusView.selectedIndex].text;
            //document.getElementById('vLineStatus['+lineNumber+']').innerHTML = streLineStatus;

            var DestinView = document.getElementById("Destinations["+lineNumber+"]");
            var strDest = DestinView.options[DestinView.selectedIndex].text;
            document.getElementById('vDestination['+lineNumber+']').innerHTML = '<b>Destination:</b> ' + strDest;

            var StorageView = document.getElementById("Storage["+lineNumber+"]");
            var strStorage = StorageView.options[StorageView.selectedIndex].text;
            document.getElementById('vStorage['+lineNumber+']').innerHTML = '<b>Storage:</b> ' + strStorage;

            var UOMView = document.getElementById("eUOM["+lineNumber+"]");
            var strUOM = UOMView.options[UOMView.selectedIndex].text;
            document.getElementById('vUOM['+lineNumber+']').innerHTML = strUOM;


            //document.getElementById('vInDate['+lineNumber+']').innerHTML = document.getElementById('eInDate['+lineNumber+']').value;
            //document.getElementById('vOutDate['+lineNumber+']').innerHTML = document.getElementById('eOutDate['+lineNumber+']').value;

            document.getElementById('addingAssetsR').disabled=false;
            document.getElementById('addingAssetsN').disabled=false;
            document.getElementById('addingAssetsE').disabled=false;
            headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }
        function editAsset(lineNumber){
            editMode = 1;
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');
            $('#saverequest').attr('disabled', 'disabled');
            $('#sendrequest').attr('disabled', 'disabled');
            document.getElementById('divView'+lineNumber).style.display = "none";
            document.getElementById('divEdit'+lineNumber).style.display = "block";
            document.getElementById('addingAssetsR').disabled=true;
            document.getElementById('addingAssetsN').disabled=true;
            document.getElementById('addingAssetsE').disabled=true;
        }
        function removeAsset(lineNumber){
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            headerChecker();
        }
        function cancelAsset(lineNumber){
            editMode = 0;
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            document.getElementById('addingAssetsR').disabled=false;
            document.getElementById('addingAssetsN').disabled=false;
            document.getElementById('addingAssetsE').disabled=false;
            headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }

        function confirmSavedAsset(lineNumber){
            editMode = 0;
            document.getElementById('divSavedEdit'+lineNumber).style.display = "none";
            document.getElementById('divSavedView'+lineNumber).style.display = "block";
            document.getElementById('vSavedDescription['+lineNumber+']').innerHTML = document.getElementById('eSavedDescription['+lineNumber+']').value;
            //document.getElementById('vSavedDestination['+lineNumber+']').innerHTML = document.getElementById('eSavedDestination['+lineNumber+']').value;
            var DestView = document.getElementById("eSavedDestination["+lineNumber+"]");
            var strDest = DestView.options[DestView.selectedIndex].text;
            document.getElementById('vSavedDestination['+lineNumber+']').innerHTML = "<b>Location:</b> " + strDest;

            var StorageView = document.getElementById("eSavedStorage["+lineNumber+"]");
            var strStorage = StorageView.options[StorageView.selectedIndex].text;
            document.getElementById('vSavedStorage['+lineNumber+']').innerHTML = '<b>Storage:</b> ' + strStorage;

            //document.getElementById('vSavedSerialNum['+lineNumber+']').innerHTML = document.getElementById('eSavedSerialNum['+lineNumber+']').value;
            document.getElementById('vSavedQuantity['+lineNumber+']').innerHTML = document.getElementById('eSavedQuantity['+lineNumber+']').value;
            var UOMView = document.getElementById("eSavedUOM["+lineNumber+"]");
            var strUOM = UOMView.options[UOMView.selectedIndex].text;
            document.getElementById('vSavedUOM['+lineNumber+']').innerHTML = strUOM;
            //alert(document.getElementById('eSavedReturnable['+lineNumber+']').checked);
            /*if(document.getElementById('eSavedReturnable['+lineNumber+']').checked)
                document.getElementById('vSavedReturnable['+lineNumber+']').innerHTML = "Yes";
            else
                document.getElementById('vSavedReturnable['+lineNumber+']').innerHTML = "No";*/
            document.getElementById('addingAssetsR').disabled=false;
            document.getElementById('addingAssetsN').disabled=false;
            document.getElementById('addingAssetsE').disabled=false;
            //headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }
        function editSavedAsset(lineNumber){
            editMode = 1;
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');
            //$('#saverequest').attr('disabled', 'disabled');
            //$('#sendrequest').attr('disabled', 'disabled');
            document.getElementById('divSavedView'+lineNumber).style.display = "none";
            document.getElementById('divSavedEdit'+lineNumber).style.display = "block";
            document.getElementById('addingAssetsR').disabled=true;
            document.getElementById('addingAssetsN').disabled=true;
            document.getElementById('addingAssetsE').disabled=true;
        }
        function removeSavedAsset(lineNumber){
            editMode = 0;
            document.getElementById("divSavedEdit"+lineNumber).style.display = "none";
            document.getElementById("divSavedView"+lineNumber).style.display = "none";
            document.getElementById("eSavedDeleted["+lineNumber+"]").value="1";
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            headerChecker();
        }
        function cancelSavedAsset(lineNumber){
            editMode = 0;
            document.getElementById('divSavedEdit'+lineNumber).style.display = "none";
            document.getElementById('divSavedView'+lineNumber).style.display = "block";
            document.getElementById('addingAssetsR').disabled=false;
            document.getElementById('addingAssetsN').disabled=false;
            document.getElementById('addingAssetsE').disabled=false;
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
            //headerChecker();
        }

        function savedChecks(lineNumber){
            var errCount = 0;

            if(document.getElementById("eSavedDescription["+lineNumber+"]").value=="" || document.getElementById("eSavedDescription["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedDescription[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedDescription[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eSavedDestination["+lineNumber+"]").value=="" || document.getElementById("eSavedDestination["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedDestination[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedDestination[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            /*if(document.getElementById("eSavedSerialNum["+lineNumber+"]").value=="" || document.getElementById("eSavedSerialNum["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedSerialNum[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedSerialNum[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }*/

            if(document.getElementById("eSavedQuantity["+lineNumber+"]").value=="" || document.getElementById("eSavedQuantity["+lineNumber+"]").value<=0 || document.getElementById("eSavedQuantity["+lineNumber+"]").value.indexOf("e")>0 || document.getElementById("eSavedQuantity["+lineNumber+"]").value.indexOf("E")>0) {
                document.getElementById("eSavedQuantity[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedQuantity[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eSavedUOM["+lineNumber+"]").value=="NULL") {
                document.getElementById("eSavedUOM[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedUOM[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            //alert(errCount);
            if(errCount!=0)
                $('#confirmSavedAsset'+lineNumber).attr('disabled', 'disabled');
            else
                $('#confirmSavedAsset'+lineNumber).attr('disabled', false);

        }

        function requestorChecks(lineNumber){
            var errCount = 0;

            if(document.getElementById("eDescription["+lineNumber+"]").value=="" || document.getElementById("eDescription["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            //OLD DESTINATION FIELD
            /*if(document.getElementById("eDestination["+lineNumber+"]").value=="" || document.getElementById("eDestination["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eDestination[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eDestination[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }*/
            //NEW DESTINATION FIELD
            if(document.getElementById("Destinations["+lineNumber+"]").value=="NULL") {
                document.getElementById("Destinations[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("Destinations[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            //NEW STORAGE FIELD
            if(document.getElementById("Storage["+lineNumber+"]").value=="NULL") {
                document.getElementById("Storage[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("Storage[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            /*if(document.getElementById("eSerialNum["+lineNumber+"]").value=="" || document.getElementById("eSerialNum["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            */

            if(document.getElementById("eQuantity["+lineNumber+"]").value=="" || document.getElementById("eQuantity["+lineNumber+"]").value<=0 || document.getElementById("eQuantity["+lineNumber+"]").value.trim().length == 0 || document.getElementById("eQuantity["+lineNumber+"]").value.indexOf("e")>0 || document.getElementById("eQuantity["+lineNumber+"]").value.indexOf("E")>0) {
                    document.getElementById("eQuantity[" + lineNumber + "]").style.border = "2px solid #FF0000";
                    errCount = errCount + 1;
            }
            else {
                document.getElementById("eQuantity[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }




            if(document.getElementById("eUOM["+lineNumber+"]").value=="NULL") {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            if(errCount!=0)
                $('#confirmAsset'+lineNumber).attr('disabled', 'disabled');
            else
                $('#confirmAsset'+lineNumber).attr('disabled', false);


        }

        function validateEmail(sEmail) {
            var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
            if(!sEmail.match(reEmail))
                return false;
            else
                return true;
        }
        function headerChecker(){
            var hErrCount = 0;

            if(document.getElementById("BusinessPartner").value=="NULL") {
                document.getElementById("BusinessPartner").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("BusinessPartner").style.border = "1px solid #d2d6de";
            }
            if(document.getElementById("BusinessPartnerName").disabled == false && document.getElementById("BusinessPartnerName").value=="") {
                document.getElementById("BusinessPartnerName").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("BusinessPartnerName").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("Contact").value=="") {
                document.getElementById("Contact").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("Contact").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("Project").value=="") {
                document.getElementById("Project").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("Project").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("ContactEmail").value!="" && validateEmail(document.getElementById("ContactEmail").value)==false) {
                document.getElementById("ContactEmail").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("ContactEmail").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("tablecount").value==0 && document.getElementById("realTableCount").value==0) {
                hErrCount = hErrCount + 1;
            }
            if (editMode == 1)
                hErrCount = hErrCount + 1;

            if(hErrCount!=0 || document.getElementById('realTableCount').value == 0) {
                $('#saverequest').attr('disabled', 'disabled');
                $('#sendrequest').attr('disabled', 'disabled');
            }
            else {
                $('#saverequest').attr('disabled', false);
                $('#sendrequest').attr('disabled', false);
            }

        }
        function businessPartnerChecker(){
            var xx = document.getElementById('BusinessPartner');
            if(xx.options[xx.selectedIndex].value==3 || xx.options[xx.selectedIndex].value==4){
                document.getElementById('BusinessPartnerName').disabled = true;
            }
            else{
                document.getElementById('BusinessPartnerName').disabled = false;
            }
        }
        function showAuto(id){
            document.getElementById("theLineID").value = id;
            document.getElementById("theSavedLineID").value = -1;
            $('#autofill').modal('show');
        }
        function getSetDescription(description){
            if(document.getElementById("theSavedLineID").value == -1){
                var id = document.getElementById("theLineID").value;
                document.getElementById("eDescription[" + id + "]").value = description;
                requestorChecks(id);
                document.getElementById("theLineID").value = "";
                $('#autofill').modal('hide');
            }
            else if(document.getElementById("theLineID").value == -1){
                var id = document.getElementById("theSavedLineID").value;
                document.getElementById("eSavedDescription[" + id + "]").value = description;
                savedChecks(id);
                document.getElementById("theSavedLineID").value = "";
                $('#autofill').modal('hide');
            }

        }
        function showSavedAuto(id){
            document.getElementById("theSavedLineID").value = id;
            document.getElementById("theLineID").value = -1;
            $('#autofill').modal('show');
        }
    </script>
@endpush
@endsection