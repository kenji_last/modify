@extends('layouts.admin')
@section('title', 'Incoming Request')
@section('requestor', 'active')
    @if(isset($singleRequest) && isset($lineItems))
        @section('out_view', 'active')
    @else
        @section('out_requestor', 'active')
    @endif

@section('header_title', 'New Incoming Request')
@section('header_desc', 'New Incoming Request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-user"></i>&nbsp;<a href="{{url('out_view/'.$singleRequest->id)}}">Outgoing Request</a> - Check-in - {{$singleRequest->RequestNo}}</h4>
                    </div>
                        <div class="panel-body">

                            <form role="form" method="POST" action="{{ url('out_checkin') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="reqID" value="{{$singleRequest->id}}">
                                <div class=" col-md-6">
                                    <label>Request Number:</label>
                                    {{$singleRequest->RequestNo}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>B. Partner Type:</label>
                                    {{$singleRequest->oBusinessPartner->Name}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status:</label>
                                    {{$singleRequest->oStatus->Name}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Business Partner:</label>
                                    {{$singleRequest->BusinessPartnerName}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Date:</label>
                                    {{date('F d, Y', strtotime($singleRequest->Date))}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Contact:</label>
                                    {{$singleRequest->Contact}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Host:</label>
                                    {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Contact Email:</label>
                                    {{$singleRequest->ContactEmail}}
                                </div>

                            <div class="col-xs-12" style="padding-top:0;">
                                    <input type="hidden" id="savedTableCount" name="savedAssetCount" value="{{$lineItemsCount}}"/>
                            </div>

                            <div class="form-group col-xs-12">
                                    <div id="aHeaders" class="box-primary" style="align-content: center;">
                                        <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: left;
                                        border-bottom: 1px solid #e9e9e9;">
                                            <div class="col-xs-4">Description</div>
                                            <div class="col-xs-1" style="text-align: center;">Quantity</div>
                                            <div class="col-xs-1" style="text-align: center;">UOM</div>
                                            <div class="col-xs-1" style="text-align: center;">Returnable?</div>
                                            <div class="col-xs-2" style="text-align: center;">Check-Out</div>
                                            <div class="col-xs-2" style="text-align: center;">Check-In</div>
                                            <div class="col-xs-1"></div>
                                        </div>
                                    </div>
                                    <div id="aBody" class="box-primary" style="align-content: center;height:240px;max-height:240px;overflow-y:scroll;">
                                        <!--Saved Line Items-->
                                        @if(isset($singleRequest) && isset($lineItems))
                                            <?php $counter=0;?>
                                            @foreach($lineItems as $li)
                                                <div class="row" id='divSavedView{{$li->LineNum}}' style="
                                                width:100%;
                                                margin-left:0px;
                                                margin-bottom:0px;
                                                margin-top:0px;
                                                padding:5px;
                                                font-size:12px;
                                                border-bottom: 1px solid #e9e9e9;
                                                ">
                                                    <div class="col-xs-4" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                                    <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->Quantity}}</div>
                                                    <div class="col-xs-1" style='padding:9px 14px;text-align: center;'>{{$li->oUOM->Name}}</div>
                                                    <div class="col-xs-1" style='padding:9px 14px;text-align:center;'>
                                                        @if($li->Returnable==0)
                                                            No
                                                        @else
                                                            Yes
                                                        @endif
                                                    </div>
                                                    <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                        {{date('M d, Y h:i A',strtotime($li->OutDate))}}
                                                    </div>
                                                    <div class="col-xs-2" style='padding:9px 14px;text-align:center;'>
                                                        @if($li->Returnable==1)
                                                            @if($li->InDate!="")
                                                                {{date('M d, Y h:i A',strtotime($li->InDate))}}
                                                            @else
                                                                -
                                                            @endif
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                    <div class="col-xs-1" style='padding:3px;text-align:center;'>
                                                        @if($li->checkin_ID!=0)
                                                            <input type='checkbox' disabled checked name='Include[{{$li->LineNum}}]' id='Include{{$counter}}'>
                                                        @elseif($li->Returnable==0)
                                                            <input type='checkbox' style="display:none;" name='Include[{{$li->LineNum}}]' id='Include{{$counter}}'>
                                                        @else
                                                            <input type='checkbox' onclick="check();" name='Include[{{$li->LineNum}}]' id='Include{{$counter}}'>
                                                        @endif
                                                    </div>
                                                </div>
                                                <?php $counter=$counter + 1;?>
                                            @endforeach
                                            <input type="hidden" id="checkboxcounter" value="{{$counter}}">
                                        @endif
                                    </div>
                                </div>
                            <div class="form-group col-xs-12">
                                @if(isset($singleRequest) && isset($lineItems))
                                    <button type="submit" id="sendrequest" disabled name="submitRequest" class="btn btn-primary" data-toggle="modal">Submit Request</button>
                                    {{--<button type="submit" id="saverequest" name="saveRequest" class="btn btn-primary" data-toggle="modal">Save Request</button>--}}
                                @else
                                    <button type="submit" id="sendrequest" name="submitRequest" class="btn btn-primary" data-toggle="modal" disabled>Submit Request</button>
                                    {{--<button type="submit" id="saverequest" name="saveRequest" class="btn btn-primary" data-toggle="modal" disabled>Save Request</button>--}}
                                @endif
                                {{--<button type="button" id="" class="btn btn-primary" onclick="location.reload();">Cancel</button>--}}
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </form>
        <table id="lineStatRef" style="display:none;">
        @foreach($linestat as $lst)
            <tr>
            <td>{{$lst->id}}</td>
            <td>{{$lst->Name}}</td>
            </tr>
        @endforeach

        <table id="uomRef" style="display:none;">
        @foreach($uoms as $u)
                    <tr>
                        <td>{{$u->id}}</td>
                        <td>{{$u->Name}}</td>
                    </tr>
        @endforeach

    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });

        $('body').on('focus',".recurring_dates", function(){
            $(this).daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });

        function check() {
            var errCount = 0;
            for(i=0;i<document.getElementById('checkboxcounter').value;i++){
                if(document.getElementById('Include'+i).checked) {
                    errCount = errCount + 1;
                }
            }
            if(errCount==0)
                document.getElementById('sendrequest').disabled = true;
            else
                document.getElementById('sendrequest').disabled = false;
        }


        function addAssets(divName){
            $('#saverequest').attr('disabled', 'disabled');
            $('#sendrequest').attr('disabled', 'disabled');
            var theTableCount = document.getElementById('tablecount').value;
            var newdiv = document.createElement('div');

            //Setting UOMs in Drop Down
            var ddUOMs = "<select onchange='requestorChecks("+theTableCount+");' class='form-control' style='width:100%;' name='UOM["+theTableCount+"]' id='eUOM["+theTableCount+"]'>";
            ddUOMs = ddUOMs + "<option value=\'NULL\' disabled selected>Please Select...</option>";
            var uomTable = document.getElementById('uomRef');
            var uomRowLength = uomTable.rows.length;
            for (i = 0; i < uomRowLength; i++){
                var uomTableCells = uomTable.rows.item(i).cells;
                ddUOMs = ddUOMs + "<option value='"+uomTableCells.item(0).innerHTML+"'>"+uomTableCells.item(1).innerHTML+"</option>";
            }
            ddUOMs = ddUOMs + "</select>";

            newdiv.innerHTML = "<div class=\"row\" id='divEdit"+theTableCount+"' style=\"width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:4px;\">" +
            "<div class=\"col-md-6\" style='padding:3px;'>" +
            "<input placeholder='Description' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' maxlength='100' type='text' class='form-control' style=\"width:100%;\" name='Description["+theTableCount+"]' id='eDescription["+theTableCount+"]'/></div>"+
            /*"<div class=\"col-md-3\" style='padding:3px;'>" +
            "<input placeholder='Serial Number' onkeyup='requestorChecks("+theTableCount+");' type='text' class='form-control' style=\"width:100%;\" name='SerialNum["+theTableCount+"]' id='eSerialNum["+theTableCount+"]'/></div>"+*/
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<input placeholder='Qty' onkeyup='requestorChecks("+theTableCount+");' onchange='requestorChecks("+theTableCount+");' type='number' class='form-control' style=\"width:100%;\" name='Quantity["+theTableCount+"]' id='eQuantity["+theTableCount+"]'/></div>"+
            "<div class=\"col-md-2\" style='padding:3px;'>" +
            ddUOMs+
            "</div>"+
            "<div class=\"col-md-2\" style='align-content: center;text-align: center;vertical-align: middle;'>" +
            "<input type='checkbox' name='Returnable["+theTableCount+"]' id='eReturnable["+theTableCount+"]' checked>" +
            "</div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' class='btn' id='confirmAsset"+theTableCount+"' onclick='confirmAsset("+theTableCount+");' disabled><i class='fa fa-check'></i></button>&nbsp;" +
            "<button type='button' class='btn cancelButton' onclick='cancelAsset("+theTableCount+");'><i class='fa fa-close'></i></button>"+
            "</div>"+
            "</div>"+
            "<div class=\"row\" id='divView"+theTableCount+"' style=\"width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:5px;display:none;border-bottom: 1px solid #e9e9e9;\">" +
            "<div class=\"col-md-6\" style='padding:9px 14px;'><span style=\"width:100%;word-wrap:break-word;\" id='vDescription["+theTableCount+"]'/></span></div>"+
            //"<div class=\"col-md-3\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vSerialNum["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-1\" style='padding:9px 14px;'><span style=\"width:100%;word-wrap:break-word;\" id='vQuantity["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vUOM["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-2\" style='padding:9px 14px;'><span style=\"width:100%;\" id='vReturnable["+theTableCount+"]'/></span></div>"+
            "<div class=\"col-md-1\" style='padding:3px;'>" +
            "<button type='button' class='btn editButton' onclick='editAsset("+theTableCount+");'><i class='fa fa-edit'></i></button>&nbsp;" +
            "<button type='button' class='btn deleteButton' onclick='removeAsset("+theTableCount+");'><i class='fa fa-trash-o'></i></button></div>"+
            "</div>";

            document.getElementById(divName).appendChild(newdiv);
            document.getElementById('tablecount').value = +theTableCount + 1;
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value + 1;
            document.getElementById('addingAssets').disabled=true;
            document.getElementById("eDescription["+theTableCount+"]").focus();
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');

        }

        function confirmAsset(lineNumber){
            document.getElementById('divEdit'+lineNumber).style.display = "none";
            document.getElementById('divView'+lineNumber).style.display = "block";
            document.getElementById('vDescription['+lineNumber+']').innerHTML = document.getElementById('eDescription['+lineNumber+']').value;
            //document.getElementById('vSerialNum['+lineNumber+']').innerHTML = document.getElementById('eSerialNum['+lineNumber+']').value;
            document.getElementById('vQuantity['+lineNumber+']').innerHTML = document.getElementById('eQuantity['+lineNumber+']').value;
            if(document.getElementById('eReturnable['+lineNumber+']').checked)
                document.getElementById('vReturnable['+lineNumber+']').innerHTML = "Yes";
            else
                document.getElementById('vReturnable['+lineNumber+']').innerHTML = "No";
            //var LineStatusView = document.getElementById("eLineStatus["+lineNumber+"]");
            //var strLineStatus = LineStatusView.options[LineStatusView.selectedIndex].text;
            //document.getElementById('vLineStatus['+lineNumber+']').innerHTML = streLineStatus;

            var UOMView = document.getElementById("eUOM["+lineNumber+"]");
            var strUOM = UOMView.options[UOMView.selectedIndex].text;
            document.getElementById('vUOM['+lineNumber+']').innerHTML = strUOM;


            //document.getElementById('vInDate['+lineNumber+']').innerHTML = document.getElementById('eInDate['+lineNumber+']').value;
            //document.getElementById('vOutDate['+lineNumber+']').innerHTML = document.getElementById('eOutDate['+lineNumber+']').value;

            document.getElementById('addingAssets').disabled=false;
            headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }
        function editAsset(lineNumber){
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');
            $('#saverequest').attr('disabled', 'disabled');
            $('#sendrequest').attr('disabled', 'disabled');
            document.getElementById('divView'+lineNumber).style.display = "none";
            document.getElementById('divEdit'+lineNumber).style.display = "block";
            document.getElementById('addingAssets').disabled=true;
        }
        function removeAsset(lineNumber){
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            headerChecker();
        }
        function cancelAsset(lineNumber){
            document.getElementById("divEdit"+lineNumber).remove();
            document.getElementById("divView"+lineNumber).remove();
            document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            document.getElementById('addingAssets').disabled=false;
            headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }

        function confirmSavedAsset(lineNumber){
            document.getElementById('divSavedEdit'+lineNumber).style.display = "none";
            document.getElementById('divSavedView'+lineNumber).style.display = "block";
            document.getElementById('vSavedDescription['+lineNumber+']').innerHTML = document.getElementById('eSavedDescription['+lineNumber+']').value;
            //document.getElementById('vSavedSerialNum['+lineNumber+']').innerHTML = document.getElementById('eSavedSerialNum['+lineNumber+']').value;
            document.getElementById('vSavedQuantity['+lineNumber+']').innerHTML = document.getElementById('eSavedQuantity['+lineNumber+']').value;
            var UOMView = document.getElementById("eSavedUOM["+lineNumber+"]");
            var strUOM = UOMView.options[UOMView.selectedIndex].text;
            document.getElementById('vSavedUOM['+lineNumber+']').innerHTML = strUOM;
            if(document.getElementById('eSavedReturnable['+lineNumber+']').checked)
                document.getElementById('vSavedReturnable['+lineNumber+']').innerHTML = "Yes";
            else
                document.getElementById('vSavedReturnable['+lineNumber+']').innerHTML = "No";
            document.getElementById('addingAssets').disabled=false;
            //headerChecker();
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
        }
        function editSavedAsset(lineNumber){
            $('.editButton').attr('disabled', 'disabled');
            $('.deleteButton').attr('disabled', 'disabled');
            //$('#saverequest').attr('disabled', 'disabled');
            //$('#sendrequest').attr('disabled', 'disabled');
            document.getElementById('divSavedView'+lineNumber).style.display = "none";
            document.getElementById('divSavedEdit'+lineNumber).style.display = "block";
            document.getElementById('addingAssets').disabled=true;
        }
        function removeSavedAsset(lineNumber){
            document.getElementById("divSavedEdit"+lineNumber).style.display = "none";
            document.getElementById("divSavedView"+lineNumber).style.display = "none";
            document.getElementById("eSavedDeleted["+lineNumber+"]").value="1";
            //document.getElementById('realTableCount').value= +document.getElementById('realTableCount').value - 1;
            //headerChecker();
        }
        function cancelSavedAsset(lineNumber){
            document.getElementById('divSavedEdit'+lineNumber).style.display = "none";
            document.getElementById('divSavedView'+lineNumber).style.display = "block";
            document.getElementById('addingAssets').disabled=false;
            $('.editButton').attr('disabled', false);
            $('.deleteButton').attr('disabled', false);
            //headerChecker();
        }
        function savedChecks(lineNumber){
            var errCount = 0;

            if(document.getElementById("eSavedDescription["+lineNumber+"]").value=="" || document.getElementById("eSavedDescription["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedDescription[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedDescription[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            /*if(document.getElementById("eSavedSerialNum["+lineNumber+"]").value=="" || document.getElementById("eSavedSerialNum["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSavedSerialNum[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedSerialNum[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }*/

            if(document.getElementById("eSavedQuantity["+lineNumber+"]").value=="" || document.getElementById("eSavedQuantity["+lineNumber+"]").value<=0) {
                document.getElementById("eSavedQuantity[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedQuantity[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("eSavedUOM["+lineNumber+"]").value=="NULL") {
                document.getElementById("eSavedUOM[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSavedUOM[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            //alert(errCount);
            if(errCount!=0)
                $('#confirmSavedAsset'+lineNumber).attr('disabled', 'disabled');
            else
                $('#confirmSavedAsset'+lineNumber).attr('disabled', false);

        }
        function requestorChecks(lineNumber){
            var errCount = 0;

            if(document.getElementById("eDescription["+lineNumber+"]").value=="" || document.getElementById("eDescription["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eDescription[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }

            /*if(document.getElementById("eSerialNum["+lineNumber+"]").value=="" || document.getElementById("eSerialNum["+lineNumber+"]").value.trim().length == 0) {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eSerialNum[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            */

            if(document.getElementById("eQuantity["+lineNumber+"]").value=="" || document.getElementById("eQuantity["+lineNumber+"]").value<=0 || document.getElementById("eQuantity["+lineNumber+"]").value.trim().length == 0) {
                    document.getElementById("eQuantity[" + lineNumber + "]").style.border = "2px solid #FF0000";
                    errCount = errCount + 1;
            }
            else {
                    document.getElementById("eQuantity[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }


            if(document.getElementById("eUOM["+lineNumber+"]").value=="NULL") {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "2px solid #FF0000";
                errCount = errCount + 1;
            }
            else {
                document.getElementById("eUOM[" + lineNumber + "]").style.border = "1px solid #d2d6de";
            }
            if(errCount!=0)
                $('#confirmAsset'+lineNumber).attr('disabled', 'disabled');
            else
                $('#confirmAsset'+lineNumber).attr('disabled', false);


        }
        function validateEmail(sEmail) {
            var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
            if(!sEmail.match(reEmail))
                return false;
            else
                return true;
        }

        function headerChecker(){
            var hErrCount = 0;

            if(document.getElementById("BusinessPartner").value=="NULL") {
                document.getElementById("BusinessPartner").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("BusinessPartner").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("BusinessPartnerName").value=="") {
                document.getElementById("BusinessPartnerName").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("BusinessPartnerName").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("Contact").value=="") {
                document.getElementById("Contact").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("Contact").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("ContactEmail").value!="" && validateEmail(document.getElementById("ContactEmail").value)==false) {
                document.getElementById("ContactEmail").style.border = "2px solid #FF0000";
                hErrCount = hErrCount + 1;
            }
            else {
                document.getElementById("ContactEmail").style.border = "1px solid #d2d6de";
            }

            if(document.getElementById("tablecount").value==0 && document.getElementById("realTableCount").value==0) {
                hErrCount = hErrCount + 1;
            }

            if(hErrCount!=0 || document.getElementById('realTableCount').value == 0) {
                $('#saverequest').attr('disabled', 'disabled');
                $('#sendrequest').attr('disabled', 'disabled');
            }
            else {
                $('#saverequest').attr('disabled', false);
                $('#sendrequest').attr('disabled', false);
            }

        }
    </script>
@endpush
@endsection