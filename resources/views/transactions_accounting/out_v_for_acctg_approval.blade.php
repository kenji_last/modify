@extends('layouts.admin')
@section('title', 'View Outgoing')
@section('accounting', 'active')

    @if ((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_view')))
        @section('out_accounting_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_my_rejections')))
        @section('out_accounting_my_rejections', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_my_approvals')))
        @section('out_accounting_my_approvals', 'active')
    @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_ci_view')))
        @section('out_accounting_ci_view', 'active')

            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting')))
                @if($code=="co_for_approval")
                    @section('out_accounting_co_for_approval', 'active')
                @elseif($code=="co_approved")
                    @section('out_accounting_co_approved', 'active')
                @elseif($code=="co_rejected")
                    @section('out_accounting_co_rejected', 'active')
                @elseif($code=="ci_for_approval")
                    @section('out_accounting_ci_for_approval', 'active')
                @elseif($code=="ci_approved")
                    @section('out_accounting_ci_approved', 'active')
                @elseif($code=="ci_rejected")
                    @section('out_accounting_ci_rejected', 'active')
                @endif
    @endif
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">

    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default" style="max-height:100%;height:90% !important;">
                    <div class="panel-heading">
                                @if((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_my_approvals')))
                                    <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Approved Check-In Requests</h4>
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_my_rejections')))
                                    <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Rejected Requests</h4>
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_view')))
                                    <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - For Accounting Approval</h4>
                                @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_ci_view')))
                                    <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - For Accounting Approval - Check-In</h4>

                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting')))
                                    @if($code=="co_for_approval")
                                        <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Check-Out - For Accounting Approval</h4>
                                    @elseif($code=="co_approved")
                                        <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Approved Check-Out Requests</h4>
                                    @elseif($code=="co_rejected")
                                        <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Rejected Check-Out Requests</h4>
                                    @elseif($code=="ci_for_approval")
                                        <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Check-In - For Accounting Approval</h4>
                                    @elseif($code=="ci_approved")
                                        <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Approved Check-In Requests</h4>
                                    @elseif($code=="ci_rejected")
                                        <h4><i class="fa fa-user"></i>&nbsp;Outgoing Requests - Rejected Check-In Requests</h4>
                                    @endif

                                @endif
                    </div>
                    <div class="panel-body">
                            <table class="table table-bordered table-striped" id="example1" style="font-size:12px !important;">
                                <thead>
                                <th>&nbsp;</th>
                                <th>Request No.</th>
                                <th>Status</th>
                                <th>B. Partner Name</th>
                                <th>B. Partner Type</th>
                                <th>Request Type</th>
                                <th>Contact</th>
                                <th>Host</th>
                                <th>Date</th>
                                </thead>
                                <tbody>
                                @foreach($myRequests as $mR)
                                    <tr>
                                            {{--@if((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_my_rejections')))
                                                <td><a href="{{url('out_accounting_my_rejections/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_my_approvals')))
                                                <td><a href="{{url('out_accounting_my_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_view')))
                                                <td><a href="{{url('out_accounting_view/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting_ci_view')))
                                                <td><a href="{{url('out_accounting_ci_view/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>--}}

                                            @if((starts_with(Route::getCurrentRoute()->getPath(), 'out_accounting')))
                                                    <td><a href="{{url('out_accounting/'.$code.'/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>

                                                    {{--@if($code=="co_for_approval")
                                                        <td><a href="{{url('out_accounting_view/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="co_approved")
                                                        <td><a href="{{url('out_accounting_my_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="co_rejected")
                                                        <td><a href="{{url('out_accounting_my_rejections/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="ci_for_approval")
                                                        <td><a href="{{url('out_accounting_ci_view/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="ci_approved")
                                                        <td><a href="{{url('out_accounting_ci_my_approvals/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @elseif($code=="ci_rejected")
                                                        <td><a href="{{url('out_accounting_ci_my_rejections/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                                    @endif--}}
                                            @endif
                                        </td>
                                        <td>{{$mR->RequestNo}}</td>
                                        <td>{{$mR->oStatus->Name}}</td>
                                        <td>{{$mR->BusinessPartnerName}}</td>
                                        <td>{{$mR->oBusinessPartner->Name}}</td>
                                        <td>{{$mR->oRequest->Name}}</td>
                                        <td>{{$mR->Contact}}</td>
                                        <td>{{$mR->oUser->FirstName}} {{$mR->oUser->LastName}}</td>
                                        <td>{{date('F d, Y', strtotime($mR->Date))}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endpush
@endsection