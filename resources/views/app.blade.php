<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vendor Asset Management | @yield('title')</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('../public/favicon.ico') }}"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/fortawesome/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/driftyco/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/square/blue.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;letter-spacing: -1px;">

<header class="main-header" style="margin-bottom:5%; background-color:#0cb759;color:#ffffff;">
    <span class="logo" style="background:#0cb759 !important;">
        <span class="logo-lg" style="font-size:14px;font-family:Arial !important;"><img src="{{asset('img/APO_PLAIN.png')}}" height="30px"/> Vendor Asset Management</span>
    </span>
    <nav class="navbar navbar-static-top" role="navigation">
    </nav>
</header>


{{--<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Vendor Asset Management</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Vendor Asset Management</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                --}}{{--<li><a href="{{ url('/') }}">Home</a></li>--}}{{--
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    --}}{{--<li><a href="{{ url('/auth/login') }}">Login</a></li>--}}{{--
                    --}}{{--<li><a href="{{ url('/auth/register') }}">Register</a></li>--}}{{--
                @else
                    <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                    --}}{{--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                        </ul>
                    </li>--}}{{--
                @endif
            </ul>
        </div>
    </div>
</nav>--}}

@yield('content')

<!-- Scripts -->
{{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>--}}
</body>
</html>