@extends('layouts.master.front')
@section('title', 'Registration')
@section('content')
    <div class="row">
        <div class="col-md-5 col-md-offset-4">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h2 style="color:#337ab7;">Registration Form</h2>
                </div>
                <div class="box-body">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{--  <b>Not Saved. </b>--}}
                            {{ Session::get('flash_message') }}<br>
                        </div>
                    @endif
                    {!! Form::open(['url' => 'auth/register']) !!}
                    {!! Form::token() !!}

                    <div class="form-group">
                        <label>Employee Number</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted"> Example: 11969</em></small>
                        <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">
                            {!! Form::text('username', old('username'), ['id'=> 'username', 'class'=>'form-control','required']) !!}
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <label>Given Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Philip</em></small>
                            <div class="form-group has-feedback {{ $errors->has('given_name') ? 'has-error' : '' }}">
                                {!! Form::text('given_name', old('given_name'), ['id'=> 'given_name', 'class'=>'form-control','required']) !!}
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('given_name')) <p class="help-block">{{ $errors->first('given_name') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Middle Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Jack</em></small>
                            <div class="form-group has-feedback {{ $errors->has('middle_name') ? 'has-error' : '' }}">
                                {!! Form::text('middle_name', old('middle_name'), ['id'=> 'middle_name', 'class'=>'form-control']) !!}
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('middle_name')) <p class="help-block">{{ $errors->first('middle_name') }}</p> @endif
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Brooks</em></small>
                                <div class="form-group has-feedback {{ $errors->has('family_name') ? 'has-error' : '' }}">
                                    {!! Form::text('family_name', old('family_name'), ['id'=> 'family_name', 'class'=>'form-control','required']) !!}
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    @if ($errors->has('family_name')) <p class="help-block">{{ $errors->first('family_name') }}</p> @endif
                                </div>
                            </div>
                           {{-- <div class="form-group">
                                <label>Division</label>&nbsp;&nbsp;&nbsp;<span id="loading" style="display:none;"><i class="fa fa-cog fa-spin"></i>&nbsp; Loading Departments...</span>
                                <select class="form-control" name="division" id="division" onchange="changeFunc();" required>
                                    <option value="">-- Division --</option>

                                </select>
                            </div>--}}
                            <div class="form-group">
                                <label>Division</label>
                                <select class="form-control" name="division" id="division" onchange="changeFunc();" required>
                                    <option value="">-- Division --</option>
                                    @foreach(App\Division::where('Active', 1)->get() as $division)
                                        <option value="{{ $division->id }}">{{ $division->Name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Department</label>
                                <select class="form-control" name="department" id="department" onchange="changeFunc();" required>
                                    <option value="">-- Department --</option>
                                    @foreach(App\Department::where('Active', 1)->get() as $department)
                                        <option value="{{ $department->id }}">{{ $department->Name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Password</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Brooks</em></small>
                                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                    {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Enter new password', 'required' => 'required']) !!}
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Brooks</em></small>
                                {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Confirm new password', 'required' => 'required']) !!}
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
                            </div>
                            <br>
                            <div class="container-fluid">
                                <div class="col-xs-6">
                                    <a href="login" class="btn btn-primary" role="button"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary" >Submit</button>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
<script src="{{ asset('/js/vendor.js') }}"></script>
{{--<script src="{{ asset('/js/department.js') }}"></script>--}}
<script>

</script>
@endpush