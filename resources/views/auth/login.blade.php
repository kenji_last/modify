@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                       {{-- @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Username</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="UserName" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="Password">
                                </div>
                            </div>

                            --}}{{--<div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>--}}{{--

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Login</button>

                                    --}}{{--<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>--}}{{--
                                </div>
                            </div>
                        </form>--}}{{-- @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Username</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="UserName" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="Password">
                                </div>
                            </div>

                            --}}{{--<div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>--}}{{--

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Login</button>

                                    --}}{{--<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>--}}{{--
                                </div>
                            </div>
                        </form>--}}
                        <div class="row" style="background-color:#fff;margin:50px auto;width:360px;padding-left:30px;padding-right:30px;">
                            <h5><strong>Login</strong></h5>
                            {!! Form::open(['url' => 'auth/login']) !!}
                            {!! Form::token() !!}
                            @if ($errors->has('cred')) <p class="help-block" style="color:#a94442">{!! $errors->first('cred') !!}</p> @endif
                            <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}" style="width:100%;">
                                <div class="form-group has-feedback {{ $errors->has('cred') ? 'has-error' : '' }}" style="width:100%;">
                                    {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Employee Number', 'required' => 'required']) !!}

                                    <!--  if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> endif-->
                                </div>
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}" style="width:100%;">
                                {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Password', 'required' => 'required']) !!}

                                @if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                            </div>
                            <div class="row" style="width:100%;">
                                {{--<div class="col-xs-7">
                                    <a href="forgotpassword">Forgot Password?</a>
                                    --}}{{-- {!! link_to('password/username', $title = "I forgot my password") !!}
                                     <br>--}}{{--
                                    --}}{{-- {!! link_to('auth/register', $title = "Register a new membership") !!}--}}{{--
                                    <div class="checkbox icheck">

                                        <!--<label>
                                              {!! Form::checkbox('remember', null, true) !!}  Remember Me
                                      </label>-->
                                    </div>
                                </div><!-- /.col -->--}}
                                <div class="col-xs-5 col-xs-offset-4">

                                    {!! Form::submit('Sign In', ["class"=>"btn btn-primary btn-block btn-flat"]) !!}

                                </div>
                                <!-- /.col -->
                            </div>
                           {{-- <div class="row" style="width:100%;">
                                <div class="col-xs-12" style="padding-top:25px;text-align: center;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Best viewed using Google Chrome.
                                </div>
                            </div>--}}

                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection