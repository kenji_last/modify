@extends('layouts.admin')
@section('title', 'Incoming')
@section('gate', 'active')
@section('out_gate_view', 'active')
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-user-secret"></i>&nbsp;<a href="{{url('out_gateview')}}">Outgoing Request</a> - Check-Out - {{$singleRequest->RequestNo}}</h4>
                    </div>
                    <div class="panel-body" style="padding-top:3%;">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}</div>
                        @endif
                        {{--@if (Session::has('succ_message'))
                        @endif--}}
                    <form role="form" method="POST" action="{{ url('out_gateview') }}" enctype="multipart/form-data" accept-charset="UTF-8">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class=" col-md-6">
                            <label>Request Number:</label>
                            {{$singleRequest->RequestNo}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>B. Partner Type:</label>
                            {{$singleRequest->oBusinessPartner->Name}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Status:</label>
                            {{$singleRequest->oStatus->Name}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Business Partner:</label>
                            {{$singleRequest->BusinessPartnerName}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date:</label>
                            {{date('F d, Y', strtotime($singleRequest->Date))}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact:</label>
                            {{$singleRequest->Contact}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Host:</label>
                            {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                        </div>
                        <div class="form-group col-md-6">
                            <label>Contact Email:</label>
                            {{$singleRequest->ContactEmail}}
                        </div>

                        <div class="form-group col-md-6">
                            <label>Guard Name:</label>
                            <input type="text" id="ReceivedByName" name="ReceivedByName" onkeyup="submitDisabler();" onchange="submitDisabler();" class="form-control pull-right">
                        </div>
                        <input type="hidden" id="id" name="id" value="{{$id}}"/>
                        <input type="hidden" id="tablecount" name="assetCount" value="{{$lineItemsCount}}"/>
                        <input type="hidden" id="realTableCount" name="realAssetCount" value="{{$lineItemsCount}}"/>
                        {{--<div class="col-xs-12" style="padding-top:1%;padding-bottom:1px;">
                            <button type="button" id="startBScanner" onclick="startBScanning();" class='btn btn-sm btn-success'><i class="fa fa-btn fa-barcode"></i>&nbsp;&nbsp;Scan Barcode</button>
                        </div>
                        <div class="col-xs-6" id="bScannerContainer" style="display:none;padding-top:1%;padding-bottom:1px;">
                            <label>Barcode</label>
                            &nbsp;&nbsp;&nbsp;
                            <input type="text" id="bScanner" name="bScanner" onchange="bScanning();" class="form-control"/>
                        </div>--}}

                        {{--<div class="row" id="bScannerContainer" style="display:none;padding-left:2%;padding-right:2%;padding-bottom:1px;padding-top:1%;">
                            <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="col-xs-1">
                                            <label for="bScanner">Barcode</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <input type="text" id="bScanner" name="bScanner" onchange="bScanning();" class="form-control"/>
                                        </div>
                                    </div>
                            </div>
                        </div>--}}
                            <div class="form-group col-xs-12">
                                    <div id="aHeaders" class="box-primary" style="align-content: center;">
                                        <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: center;
                                        border-bottom: 1px solid #e9e9e9;">
                                            {{--<div class="col-md-1">Line #</div>--}}
                                            <div class="col-xs-3" style="padding:1%;">Description</div>
                                            {{--<div class="col-xs-2" style="padding:1%;">Serial Number</div>--}}
                                            <div class="col-xs-2" style="padding:1%;">Control Number</div>
                                            <div class="col-xs-1" style="padding:1%;">L. Status</div>
                                            <div class="col-xs-1" style="padding:1%;">Quantity</div>
                                            <div class="col-xs-1" style="padding:1%;">UOM</div>
                                            <div class="col-xs-1" style="padding:1%;">Returnable?</div>
                                            <div class="col-xs-2" style="padding:1%;">Check-In</div>
                                            <div class="col-xs-1" style="padding:1%;">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div id="aBody" class="box-primary" style="align-content: center;height:240px;max-height:240px;overflow-y:scroll;font-size:13px;">
                                    @foreach($lineItems as $li)
                                            @if($li->InDate!="" && $li->InRemarks!="")
                                                 <div class="row" id="{{$li->ControlNum}}" style="width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @else
                                                 <div class="row" id="{{$li->ControlNum}}" style="background:#ff8784;color:#ffffff !important;display:block;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @endif
                                                <div class="col-xs-3" style='padding:9px 14px;word-wrap:break-word;'>{{$li->Description}}</div>
                                                {{--<div class="col-xs-2" style='padding:9px 14px;'>{{$li->SerialNum}}</div>--}}
                                                <div class="col-xs-2" style='padding:9px 14px;'>{{$li->ControlNum}}</div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>{{$li->oStatus->Name}}</div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>{{$li->Quantity}}</div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>{{$li->oUOM->Name}}</div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>
                                                    @if($li->Returnable=="1")
                                                        Yes
                                                    @else
                                                        No
                                                    @endif
                                                </div>
                                                <div class="col-xs-2" style='padding:9px 0px;'>
                                                    <span id="vInDate[{{$li->LineNum}}]">
                                                        @if($li->InDate=="")
                                                            -
                                                        @else
                                                            {{$li->InDate}}
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="col-xs-1" style='padding:9px 14px;'>
                                                    <button type='button' class='btn btn-xs includeButton' id="Include[{{$li->LineNum}}]" onclick='includeThis("{{$li->LineNum}}");' style="color:#000000 !important;"><i class='fa fa-check'></i></button>
                                                    <button type='button' class='btn btn-xs excludeButton' id="Exclude[{{$li->LineNum}}]" onclick='excludeThis("{{$li->LineNum}}");' style="color:#000000 !important;display:none;"><i class='fa fa-close'></i></button>
                                                    <button type='button' class='btn btn-xs photoButton' id="Image[{{$li->LineNum}}]" onclick='showImageUpload("{{$li->LineNum}}");' style="display:none;color:#000000 !important;"><i class='fa fa-image'></i></button>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;border-bottom: 1px solid #e9e9e9;">
                                                <input type="text" name="Included[{{$li->LineNum}}]" id="Included{{$li->LineNum}}" value="0"/>
                                                <input type="text" name="LineID[{{$li->LineNum}}]" id="LineID[{{$li->LineNum}}]" value="{{$li->id}}"/>
                                                {{--<input type="text" name="Description[{{$li->LineNum}}]" id="Description[{{$li->LineNum}}]" value="{{$li->Description}}"/>--}}
                                                <input type="text" name="ControlNum[{{$li->LineNum}}]" id="ControlNum[{{$li->LineNum}}]" value="{{$li->ControlNum}}"/>
                                                <input type="text" name="OutDate[{{$li->LineNum}}]" id="OutDate[{{$li->LineNum}}]" value="{{$li->OutDate}}"/>
                                                {{--<input type="text" name="InRemarks[{{$li->LineNum}}]" id="InRemarks[{{$li->LineNum}}]" value="{{$li->InRemarks}}"/>
                                                <input type="text" name="OutDate[{{$li->LineNum}}]" id="OutDate[{{$li->LineNum}}]" value="{{$li->OutDate}}"/>
                                                <input type="text" name="OutRemarks[{{$li->LineNum}}]" id="OutRemarks[{{$li->LineNum}}]" value="{{$li->OutRemarks}}"/>--}}
                                            </div>
                                            <div class="row" id="Child[{{$li->LineNum}}]" style="display:none;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                                <div class="col-xs-2 col-xs-offset-4" style='padding:9px 14px;word-wrap:break-word;'>
                                                    {{--<input class='btn btn-xs' type="file" name="fileToUpload[{{$li->LineNum}}]" id="fileToUpload">--}}
                                                    <label for="imgInp1{{$li->LineNum}}" class="clone">
                                                        <img src="{{ asset('/default_user_a.png') }}"
                                                             class= "img-rounded imgId"
                                                             id="imgId{{$li->LineNum}}" name ="imgId{{$li->LineNum}}"
                                                             style="width:100px;">
                                                    </label>
                                                </div>
                                                <div class="col-xs-1" style='padding:50px 14px;word-wrap:break-word;'>
                                                    {!! Form::hidden('pathPhoto',null,array('class'=>'pathPhoto','id'=>'pathPhoto')) !!}

                                                    {!!Form::file('image_path['.$li->LineNum.']', array('id'=>'imgInp'.$li->LineNum,'accept'=>'image/x-png, image/jpeg')) !!}
                                                    {{--{!! Form::hidden('_token', csrf_token()) !!}
                                                    {!! Form::submit('Save') !!}
                                                    {!!Form::close() !!}--}}
                                                </div>
                                            </div>

                                    @endforeach
                                    </div>
                                </div>
                            <div class="form-group col-xs-12">
                                <button type="button" disabled id="sendrequest" class="btn btn-primary" onclick="callSubmit();">Update Request</button>
                                <button type="button" id="" class="btn btn-primary" onclick="location.reload();">Cancel</button>
                            </div>
                                <div id="callSubmit" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" id="dateModalTitle">Confirmation</h4>
                                            </div>
                                            <div class="modal-body" style="padding:5% 10% 5% 10%;" id="confirmationMessage">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>
            </div>
        </div>

        <div id="outDateModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="dateModalTitle">Out Date</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="mLineID">
                        <div class="row" style="padding:0 auto 0 0;margin:0 3% 0 3%;">
                            <label>Date:</label>
                            <div class="input-group col-md-12">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="outDate" type="text" class="form-control pull-right" id="outDateAll" required="required" value="">
                            </div>
                            <label>Remarks:</label>
                            <div class="input-group col-md-12">
                                <textarea id="outDateRemarks" class="form-control pull-right" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="inDateModalOK();" id="inDateModalOK">Ok</button>
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();" id="outDateModalOK">Ok</button>--}}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <table id="uomRef" style="display:none;">
            @foreach($uoms as $u)
                    <tr>
                        <td>{{$u->id}}</td>
                        <td>{{$u->Name}}</td>
                    </tr>
            @endforeach
        </table>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            /*$('#inDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });*/
            $('#outDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });

        });


        function includeThis(id){
            /*document.getElementById('dateModalTitle').innerHTML = "In Date";
            document.getElementById('mLineID').value = id;
            document.getElementById('outDateAll').value = document.getElementById('InDate['+id+']').value;
            $('#outDateModal').modal('show');*/
            var controlNum = document.getElementById('ControlNum['+id+']').value;
            document.getElementById('Included'+id).value = 1;
            document.getElementById(controlNum).style.background = "#ffffff";
            document.getElementById(controlNum).style.color = "#000000";
            document.getElementById("OutDate[" + id + "]").value = moment().format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("vInDate[" + id + "]").innerHTML = moment().format('YYYY/MM/DD - hh:mm A');
            document.getElementById("Include[" + id +"]").style.display = "none";
            document.getElementById("Exclude[" + id +"]").style.display = "inline";
            document.getElementById("Image[" + id +"]").style.display = "inline";
            submitDisabler();
        }

        function outDateModal(id){
            document.getElementById('dateModalTitle').innerHTML = "Out Date";
            document.getElementById('mLineID').value = id;
            document.getElementById('outDateAll').value = document.getElementById('OutDate['+id+']').value;
            document.getElementById('outDateRemarks').value = document.getElementById('OutRemarks['+id+']').value;
            $('#outDateModal').modal('show');
        }
        function outDateModalOK(){
            var id = document.getElementById('mLineID').value;
            document.getElementById('OutDate['+id+']').value = document.getElementById('outDateAll').value;
            document.getElementById('vOutDate['+id+']').innerHTML = document.getElementById('outDateAll').value;
            document.getElementById('OutRemarks['+id+']').value = document.getElementById('outDateRemarks').value;
            $('#outDateModal').modal('hide');
            document.getElementById('outDateAll').value = "";
            document.getElementById('outDateRemarks').value = "";
        }
        function startBScanning(){
            var $yourUl = $("#bScannerContainer");
            $yourUl.css("display", $yourUl.css("display") === 'none' ? '' : 'none');
            //document.getElementById("bScannerContainer").style.display = document.getElementById("bScannerContainer").style.display === 'none' ? '' : 'block';
            document.getElementById("bScanner").focus();
        }
        function bScanning(){
            var table = document.getElementById('tablecount').value;
            for(i = 1;i<=table;i++){
                if(document.getElementById('ControlNum['+i+']').value == document.getElementById('bScanner').value) {
                    var controlNum = document.getElementById('ControlNum['+i+']').value;
                    document.getElementById('Included'+i).value = 1;
                    document.getElementById(controlNum).style.background = "#ffffff";
                    document.getElementById(controlNum).style.color = "#000000";
                    document.getElementById("OutDate[" + i + "]").value = moment().format('YYYY-MM-DD HH:mm:ss');
                    document.getElementById("vInDate[" + i + "]").innerHTML = moment().format('YYYY/MM/DD - hh:mm A');
                    document.getElementById('bScanner').value = "";
                    document.getElementById("bScanner").focus();
                }
            }
            document.getElementById('bScanner').value = "";
            document.getElementById("bScanner").focus();
            submitDisabler();
        }
        function excludeThis(id){
            document.getElementById('Included'+id).value = 0;
            var controlNum = document.getElementById('ControlNum['+id+']').value;
            document.getElementById("OutDate[" + id + "]").value = "";
            document.getElementById("vInDate[" + id + "]").innerHTML = "-";
            document.getElementById(controlNum).style.background = "#ff8784";
            document.getElementById(controlNum).style.color = "#ffffff";
            document.getElementById("Exclude[" + id +"]").style.display = "none";
            document.getElementById("Include[" + id +"]").style.display = "inline";
            document.getElementById("Image[" + id +"]").style.display = "none";
            document.getElementById("Child[" + id +"]").style.display = "none";
            submitDisabler();
        }
        function submitDisabler(){
            //alert(document.getElementById('ReceivedByName').value);
            var table = document.getElementById('tablecount').value;
            var errCounter = 0;
            for(i = 1;i<=table;i++){
                if (document.getElementById('Included'+i).value != 0)
                    errCounter = errCounter + 1;
            }
            if(document.getElementById("ReceivedByName").value=="") {
                document.getElementById("ReceivedByName").style.border = "2px solid #FF0000";
                errCounter = 0;
            }else document.getElementById("ReceivedByName").style.border = "1px solid #d2d6de";

            if(errCounter==0)
                document.getElementById('sendrequest').disabled=true;
            else
                document.getElementById('sendrequest').disabled=false;

        }
        function messageSetter(){
            var table = document.getElementById('tablecount').value;
            var errCounter = 0;

            for(i = 1;i<=table;i++){
                if (document.getElementById('Included'+i).value == 0)
                    errCounter = errCounter + 1;
            }
            if(errCounter!=0)
                document.getElementById("confirmationMessage").innerHTML = "There are excluded line items for this request. Please ensure that the labels for these items are destroyed.<br><br> Do you wish to continue?";
            else
                document.getElementById("confirmationMessage").innerHTML = "Update this request?";
        }
        function callSubmit(){
            messageSetter();
            $('#callSubmit').modal('show');
        }
        $('form input').on('keypress', function(e) {
            return e.which !== 13;
        });
        function showImageUpload(item){
            var x = document.getElementById("Child[" + item +"]");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        //Image
        var t=0;
        function readURL(input, img){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(img).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function browseURL(path, path2){
            $(path).change(function(){
                readURL(this, path2);
            });
        }

        var lineItems = <?php echo $lineItems->count();?>;
        for(var i=1;i<=lineItems;i++) {
            browseURL("#imgInp"+i, "#imgId"+i);
        }

    </script>
@endpush
@endsection