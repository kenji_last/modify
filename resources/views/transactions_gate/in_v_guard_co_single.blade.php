@extends('layouts.admin')
@section('title', 'Incoming')
@section('gate', 'active')

    @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
        @section('in_gate_co_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_closed_view')))
        @section('in_gate_closed_view', 'active')
    @endif

@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
                            <h4><i class="fa fa-user-secret"></i>&nbsp;<a href="{{url('in_co_gateview')}}">Incoming Request</a> - Check-Out - {{$singleRequest->RequestNo}}</h4>
                        @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_closed_view')))
                            <h4><i class="fa fa-user-secret"></i>&nbsp;<a href="{{url('in_gate_closed_view')}}">Incoming Request</a> - Closed Request - {{$singleRequest->RequestNo}}</h4>
                        @endif

                    </div>
                    <div class="panel-body" style="padding-top:3%;">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}</div>
                        @endif

                        @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
                            <div class="col-xs-12">
                                <a href="{{url('pdf_checkout_guard/'.$singleRequest->id.'/'.$ids)}}" class="btn btn-sm btn-primary" target="_new">Print</a>
                                <br><br>
                            </div>
                        @endif
                        {{--@if (Session::has('succ_message'))
                        @endif--}}
                    <form role="form" method="POST" action="{{ url('in_gate_co_view') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if(isset($ids))
                            <input type="hidden" name="checkout_ID" value="{{ $ids }}">
                        @endif
                        <div class="col-xs-6">
                            <label>Request Number:</label>
                            {{$singleRequest->RequestNo}}
                        </div>
                        <div class="col-xs-6">
                            <label>B. Partner Type:</label>
                            {{$singleRequest->oBusinessPartner->Name}}
                        </div>
                        <div class="col-xs-6">
                            <label>Status:</label>
                            {{$singleRequest->oStatus->Name}}
                        </div>
                        <div class="col-xs-6">
                            <label>Business Partner:</label>
                            {{$singleRequest->BusinessPartnerName}}
                        </div>
                        <div class="col-xs-6">
                            <label>Date:</label>
                            {{date('F d, Y', strtotime($singleRequest->Date))}}
                        </div>
                        <div class="col-xs-6">
                            <label>Contact:</label>
                            {{$singleRequest->Contact}}
                        </div>
                        <div class="col-xs-6">
                            <label>Host:</label>
                            {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                        </div>
                        <div class="col-xs-6">
                            <label>Contact Email:</label>
                            {{$singleRequest->ContactEmail}}
                        </div>
                        {{--<div class="col-xs-6">
                            <label>Request Type:</label>
                            {{$singleRequest->ihead_request_type->Name}}
                        </div>--}}
                        <div class="col-xs-6">
                            <label>Project/Purpose:</label>
                            {{$singleRequest->Project}}
                        </div>
                    @if(isset($singleRequest->ApprovedBy))
                        <div class="col-xs-6">
                            <label>Approved By:</label>
                            {{$singleRequest->iheadApprovedBy_to_user->FirstName}} {{$singleRequest->iheadApprovedBy_to_user->LastName}}
                        </div>
                    @endif
                    @if(isset($singleRequest->ReceivedByName))
                        <div class="col-md-6">
                            <label>Received By:</label>
                            {{$singleRequest->ReceivedByName}}
                        </div>
                    @endif
                    @if(isset($singleRequest->COApprovedBy))
                        <div class="col-md-6">
                            <label>Check-Out Approved By:</label>
                            {{$singleRequest->iheadCOApprovedBy_to_user->FirstName}} {{$singleRequest->iheadCOApprovedBy_to_user->LastName}}
                        </div>
                    @endif

                        @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_closed_view')))
                            <div class="form-group col-md-6">
                                <label>Check-in Approved By:</label>
                                {{$singleRequest->iheadApprovedBy_to_user->FirstName}} {{$singleRequest->iheadApprovedBy_to_user->LastName}}
                            </div>
                            {{--<div class="form-group col-md-6">
                                <label>Check-out Approved By:</label>
                                {{$singleRequest->iheadCOApprovedBy_to_user->FirstName}} {{$singleRequest->iheadCOApprovedBy_to_user->LastName}}
                            </div>--}}

                            {{--<div class="form-group col-md-6">
                                <label>Verified By:</label>
                                {{$singleRequest->iheadSecurityControl_to_user->FirstName}} {{$singleRequest->iheadSecurityControl_to_user->LastName}}
                            </div>--}}
                            <div class="form-group col-md-6">
                                <label>Check-out Received By:</label>
                                {{$singleRequest->COReceivedByName}}
                                {{--{{$singleRequest->iheadCOReceivedBy_to_user->FirstName}} {{$singleRequest->iheadCOReceivedBy_to_user->LastName}}--}}
                            </div>
                            {{--<div class="form-group col-md-6">
                                <label>Received By:</label>
                                {{$singleRequest->iheadReceivedBy_to_user->FirstName}} {{$singleRequest->iheadReceivedBy_to_user->LastName}}
                            </div>--}}
                        @endif
                        @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
                        <div class="col-md-6">
                            <label>Name:</label>
                            <input type="text" id="COReceivedByName" name="COReceivedByName" onkeyup="submitDisabler();" onchange="submitDisabler();" class="form-control pull-right">
                        </div>
                        @endif

                        <input type="hidden" id="id" name="id" value="{{$id}}"/>
                        <input type="hidden" id="tablecount" name="assetCount" value="{{$lineItemsCount}}"/>
                        <input type="hidden" id="realTableCount" name="realAssetCount" value="{{$lineItemsCount}}"/>
                        {{--@if((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
                            <div class="col-xs-12" style="padding-top:1%;padding-bottom:1px;">
                                <button type="button" id="startBScanner" onclick="startBScanning();" class='btn btn-sm btn-success'><i class="fa fa-btn fa-barcode"></i>&nbsp;&nbsp;Scan Barcode</button>
                            </div>
                            <div class="col-xs-6" id="bScannerContainer" style="display:none;padding-top:1%;padding-bottom:1px;">
                                <label>Barcode</label>
                                &nbsp;&nbsp;&nbsp;
                                <input type="text" id="bScanner" name="bScanner" onchange="bScanning();" class="form-control"/>
                            </div>
                        @endif--}}

                        {{--<div class="row" id="bScannerContainer" style="display:none;padding-left:2%;padding-right:2%;padding-bottom:1px;padding-top:1%;">
                            <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="col-xs-1">
                                            <label for="bScanner">Barcode</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <input type="text" id="bScanner" name="bScanner" onchange="bScanning();" class="form-control"/>
                                        </div>
                                    </div>
                            </div>
                        </div>--}}
                        <div class="form-group col-xs-12" style="padding-top:15px;">
                            <div style="padding:0px; color:#ff0000;" id="ErrMsg">
                                &nbsp;
                            </div>
                                    <div id="aHeaders" class="box-primary" style="align-content: center;">
                                        <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 25px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: center;
                                        border-bottom: 1px solid #e9e9e9;">
                                            {{--<div class="col-md-1">Line #</div>--}}
                                            <div class="col-xs-2" style="padding:1%;">Description</div>
                                            <div class="col-xs-2" style="padding:1%;">Control Number</div>
                                            <div class="col-xs-1" style="padding:1%;">Status</div>
                                            <div class="col-xs-1" style="padding:1%;">Quantity</div>
                                            <div class="col-xs-1" style="padding:1%;">UOM</div>
                                            <div class="col-xs-1" style="padding:1%;">Item Type</div>
                                            <div class="col-xs-1" style="padding:1%;">Serial Number</div>
                                            <div class="col-xs-1" style="padding:1%;">Check-In</div>
                                            <div class="col-xs-1" style="padding:1%;">Check-Out</div>
                                            <div class="col-xs-1" style="padding:1%;">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div id="aBody" class="box-primary" style="align-content: center;height:400px;max-height:400px;overflow-y:scroll;font-size:12px;">
                                    @foreach($lineItems as $li)
                                            @if($li->Deleted == 1)
                                                 <div class="row" id="{{$li->ControlNum}}" style="display:none;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @elseif(isset($ids) && $li->checkout_ID != $ids)
                                                 <div class="row" id="{{$li->ControlNum}}" style="display:none;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @elseif($li->LineStatus == 2)
                                                 <div class="row" id="{{$li->ControlNum}}" style="width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @else
                                                 <div class="row" id="{{$li->ControlNum}}" style="background:#ff8784;color:#ffffff !important;display:block;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @endif
                                                <div class="col-xs-2" style='padding:5px 2px;word-wrap:break-word;'>{{$li->Description}}</div>
                                                {{--<div class="col-xs-2" style='padding:9px 14px;'>{{$li->SerialNum}}</div>--}}
                                                <div class="col-xs-2" style='padding:5px 2px;'>{{$li->ControlNum}}</div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>{{$li->oStatus->Name}}</div>
                                                <div class="col-xs-1" style='padding:5px 2px; text-align: center;'>{{$li->Quantity}}</div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>{{$li->oUOM->Name}}</div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>
                                                    @if($li->Returnable=="1" && $li->ItemType=="0")
                                                        Tools
                                                    @elseif($li->Returnable=="0" && $li->ItemType=="0")
                                                        Consumables
                                                    @elseif($li->Returnable=="1" && $li->ItemType=="1")
                                                        Equipment
                                                    @endif
                                                </div>
                                                     <div class="col-xs-1" style='padding:5px 2px;'>{{$li->SerialNum}}</div>
                                                     <div class="col-xs-1" style='padding:5px 2px;'>
                                                        <span id="vInDate[{{$li->LineNum}}]">
                                                            @if($li->InDate=="")
                                                                -
                                                            @else
                                                                {{date('Y/m/d - h:i A',strtotime($li->InDate))}}
                                                            @endif
                                                        </span>
                                                     </div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>
                                                    <span id="vOutDate[{{$li->LineNum}}]">
                                                        @if($li->Returnable!="1")
                                                            -
                                                        @else
                                                            @if($li->OutDate=="")
                                                                -
                                                            @else
                                                                {{date('Y/m/d - h:i A',strtotime($li->OutDate))}}
                                                            @endif
                                                        @endif
                                                    </span>
                                                </div>
                                                     <div class="col-xs-1" style='padding:5px 2px;'>
                                                         @if($li->LineStatus == 1)
                                                            {{--<button type='button' class='btn btn-xs includeButton' title="Include" onclick='includeThis("{{$li->LineNum}}");' style="color:#000000 !important;"><i class='fa fa-check'></i></button>
                                                            <button type='button' class='btn btn-xs excludeButton' title="Exclude" onclick='excludeThis("{{$li->LineNum}}");' style="color:#000000 !important;"><i class='fa fa-close'></i></button>--}}

                                                             <button type='button' class='btn btn-xs includeButton' id="Include[{{$li->LineNum}}]" onclick='includeThis("{{$li->LineNum}}");' style="color:#000000 !important;"><i class='fa fa-check'></i></button>
                                                             <button type='button' class='btn btn-xs excludeButton' id="Exclude[{{$li->LineNum}}]" onclick='excludeThis("{{$li->LineNum}}");' style="color:#000000 !important;display:none;"><i class='fa fa-close'></i></button>
                                                             @if($li->ImageFormat!=NULL)
                                                                <button type='button' class='btn btn-xs photoButton' id="Image[{{$li->LineNum}}]" onclick='showUploads({{$li->LineNum}});' style="color:#000000 !important;"><i class='fa fa-image'></i></button>
                                                             @else
                                                                 <button type='button' disabled class='btn btn-xs photoButton' id="Image[{{$li->LineNum}}]" onclick='showImageUpload("{{$li->LineNum}}");' style="display:none;color:#000000 !important;display:none;"><i class='fa fa-image'></i></button>
                                                             @endif

                                                         @endif

                                                     </div>
                                            </div>

                                            <div class="row" id="Child[{{$li->LineNum}}]" style="display:none;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0px;border-bottom: 1px solid #e9e9e9;">
                                                <?php
                                                    $aa = \App\iLineItemImages::where('l_ID','=',$li->id)->get();
                                                ?>
                                                @foreach($aa as $a)
                                                {{--@for ($i = 0; $i <=$li->ImageCount; $i++)--}}
                                                    <div class="col-xs-1" style='min-height:80px;word-wrap:break-word;border-style: dashed;border-width: 1px;padding:5px;margin:5px;border-color:#a6a6a6;'>
                                                        <a onclick='showImageUpload("{{$li->Description}}","{{$li->ControlNum}}","{{ asset('../storage/uploads/'.$a->ControlNum.'-'.$a->ImageNum.'.'.$a->ImageFormat) }}",{{$a->ImageNum}}+1);'>
                                                            <img src="{{ asset('../storage/uploads/'.$a->ControlNum.'-'.$a->ImageNum.'.'.$a->ImageFormat) }}"
                                                             class= "img-rounded img-responsive imgId"
                                                             style="margin-bottom:0;"
                                                             onerror="this.src='{{ asset('/image_unavailable.png') }}';">
                                                        </a>
                                                    </div>
                                                @endforeach
                                                {{--@endfor--}}
                                            </div>

                                            <div class="row" style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;border-bottom: 1px solid #e9e9e9;">
                                                <input type="text" name="Included[{{$li->LineNum}}]" id="Included{{$li->LineNum}}" value="0"/>
                                                <input type="text" name="LineID[{{$li->LineNum}}]" id="LineID[{{$li->LineNum}}]" value="{{$li->id}}"/>
                                                {{--<input type="text" name="Description[{{$li->LineNum}}]" id="Description[{{$li->LineNum}}]" value="{{$li->Description}}"/>--}}
                                                <input type="text" name="ControlNum[{{$li->LineNum}}]" id="ControlNum[{{$li->LineNum}}]" value="{{$li->ControlNum}}"/>
                                                <input type="text" name="OutDate[{{$li->LineNum}}]" id="OutDate[{{$li->LineNum}}]" value="{{$li->OutDate}}"/>
                                                {{--<input type="text" name="InRemarks[{{$li->LineNum}}]" id="InRemarks[{{$li->LineNum}}]" value="{{$li->InRemarks}}"/>
                                                <input type="text" name="OutDate[{{$li->LineNum}}]" id="OutDate[{{$li->LineNum}}]" value="{{$li->OutDate}}"/>
                                                <input type="text" name="OutRemarks[{{$li->LineNum}}]" id="OutRemarks[{{$li->LineNum}}]" value="{{$li->OutRemarks}}"/>--}}
                                            </div>

                                    @endforeach
                                    </div>
                                </div>
                            <div class="form-group col-xs-12">
                                @if((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
                                    <button type="button" disabled id="sendrequest" class="btn btn-primary" onclick="callSubmit();">Update Request</button>
                                    <button type="button" id="" class="btn btn-primary" onclick="location.reload();">Cancel</button>
                                @endif
                            </div>
                                <div id="callSubmit" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" id="dateModalTitle">Confirmation</h4>
                                            </div>
                                            <div class="modal-body" style="padding:5% 10% 5% 10%;" id="confirmationMessage">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>
            </div>
        </div>

        <div id="outDateModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="imageModalTitle">{{$singleRequest->RequestNo}}</h4>
                    </div>
                    <div class="modal-body">
                        <p align="center">
                            <img src="" id="itemImageView" width="50%">
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <table id="uomRef" style="display:none;">
            @foreach($uoms as $u)
                    <tr>
                        <td>{{$u->id}}</td>
                        <td>{{$u->Name}}</td>
                    </tr>
            @endforeach
        </table>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            /*$('#inDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });*/
            $('#outDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });

        });


        function includeThis(id){
            /*document.getElementById('dateModalTitle').innerHTML = "In Date";
            document.getElementById('mLineID').value = id;
            document.getElementById('outDateAll').value = document.getElementById('InDate['+id+']').value;
            $('#outDateModal').modal('show');*/
            document.getElementById("Child["+id+"]").style.display = "inline";
            var controlNum = document.getElementById('ControlNum['+id+']').value;
            document.getElementById('Included'+id).value = 1;
            document.getElementById(controlNum).style.background = "#ffffff";
            document.getElementById(controlNum).style.color = "#000000";
            document.getElementById("OutDate[" + id + "]").value = moment().format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("vOutDate[" + id + "]").innerHTML = moment().format('YYYY/MM/DD - hh:mm A');
            document.getElementById("Include[" + id +"]").style.display = "none";
            document.getElementById("Exclude[" + id +"]").style.display = "inline";
            document.getElementById("Image[" + id +"]").style.display = "inline";
            submitDisabler();
        }

        function outDateModal(id){
            document.getElementById('dateModalTitle').innerHTML = "Out Date";
            document.getElementById('mLineID').value = id;
            document.getElementById('outDateAll').value = document.getElementById('OutDate['+id+']').value;
            document.getElementById('outDateRemarks').value = document.getElementById('OutRemarks['+id+']').value;
            $('#outDateModal').modal('show');
        }
        function outDateModalOK(){
            var id = document.getElementById('mLineID').value;
            document.getElementById('OutDate['+id+']').value = document.getElementById('outDateAll').value;
            document.getElementById('vOutDate['+id+']').innerHTML = document.getElementById('outDateAll').value;
            document.getElementById('OutRemarks['+id+']').value = document.getElementById('outDateRemarks').value;
            $('#outDateModal').modal('hide');
            document.getElementById('outDateAll').value = "";
            document.getElementById('outDateRemarks').value = "";
        }
        function startBScanning(){
            var $yourUl = $("#bScannerContainer");
            $yourUl.css("display", $yourUl.css("display") === 'none' ? '' : 'none');
            //document.getElementById("bScannerContainer").style.display = document.getElementById("bScannerContainer").style.display === 'none' ? '' : 'block';
            document.getElementById("bScanner").focus();
        }
        function bScanning(){
            var table = document.getElementById('tablecount').value;
            for(i = 1;i<=table;i++){
                if(document.getElementById('ControlNum['+i+']').value == document.getElementById('bScanner').value) {
                    var controlNum = document.getElementById('ControlNum['+i+']').value;
                    document.getElementById('Included'+i).value = 1;
                    document.getElementById(controlNum).style.background = "#ffffff";
                    document.getElementById(controlNum).style.color = "#000000";
                    document.getElementById("OutDate[" + i + "]").value = moment().format('YYYY-MM-DD HH:mm:ss');
                    document.getElementById("vOutDate[" + i + "]").innerHTML = moment().format('YYYY/MM/DD - hh:mm A');
                    document.getElementById('bScanner').value = "";
                    document.getElementById("bScanner").focus();
                }
            }
            document.getElementById('bScanner').value = "";
            document.getElementById("bScanner").focus();
            submitDisabler();
        }
        function excludeThis(id){
            document.getElementById('Included'+id).value = 0;
            var controlNum = document.getElementById('ControlNum['+id+']').value;
            document.getElementById("Child["+id+"]").style.display = "none";
            document.getElementById("OutDate[" + id + "]").value = "";
            document.getElementById("vOutDate[" + id + "]").innerHTML = "-";
            document.getElementById(controlNum).style.background = "#ff8784";
            document.getElementById(controlNum).style.color = "#ffffff";
            document.getElementById("Exclude[" + id +"]").style.display = "none";
            document.getElementById("Include[" + id +"]").style.display = "inline";
            document.getElementById("Image[" + id +"]").style.display = "inline";
            //document.getElementById("Child[" + id +"]").style.display = "none";
            submitDisabler();
        }
        function submitDisabler(){

            var table = document.getElementById('tablecount').value;

            var errCounter = 0;

            for(i = 1;i<=table;i++){
                if (document.getElementById('Included'+i).value != 0)
                    errCounter = errCounter + 1;
            }
            if(document.getElementById("COReceivedByName").value=="") {
                document.getElementById("COReceivedByName").style.border = "2px solid #FF0000";
                errCounter = 0;
            }else document.getElementById("COReceivedByName").style.border = "1px solid #d2d6de";
            if(errCounter==0)
                document.getElementById('sendrequest').disabled=true;
            else
                document.getElementById('sendrequest').disabled=false;
        }

        function showImageUpload(desc,cn,img,no){
            document.getElementById('imageModalTitle').innerHTML = desc + " - " + cn + " - " + no;
            $("#itemImageView").attr('src', img);
            $("#itemImageView").attr('onerror', "this.src='{{ asset('/image_unavailable.png') }}';");
            $('#outDateModal').modal('show');
            /*var x = document.getElementById("Child[" + item +"]");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }*/
        }
        function showUploads(linenum){
            if (document.getElementById("Child["+linenum+"]").style.display === "none") {
                document.getElementById("Child["+linenum+"]").style.display = "inline";
            } else {
                document.getElementById("Child["+linenum+"]").style.display = "none";
            }
        }
        function messageSetter(){
            var table = document.getElementById('tablecount').value;
            var errCounter = 0;

            for(i = 1;i<=table;i++){
                if (document.getElementById('Included'+i).value == 0)
                    errCounter = errCounter + 1;
            }
            if(errCounter!=0)
                document.getElementById("confirmationMessage").innerHTML = "Proceed with Check-out?";
            else
                document.getElementById("confirmationMessage").innerHTML = "Update this request?";
        }
        function callSubmit(){
            messageSetter();
            $('#callSubmit').modal('show');
        }

    </script>
@endpush
@endsection