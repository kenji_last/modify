@extends('layouts.admin')
@section('title', 'Incoming')
@section('gate', 'active')
@section('in_gate_view', 'active')
@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">

            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-user-secret"></i>&nbsp;<a href="{{url('in_gateview')}}">Incoming Request</a> - Check-In - {{$singleRequest->RequestNo}}</h4>
                    </div>
                    <div class="panel-body" style="padding-top:3%;">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}</div>
                        @endif
                        {{--@if (Session::has('succ_message'))
                        @endif--}}
                    <form role="form" method="POST" action="{{ url('in_gateview') }}" enctype="multipart/form-data" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-xs-6">
                            <label>Request Number:</label>
                            {{$singleRequest->RequestNo}}
                        </div>
                        <div class="col-xs-6">
                            <label>B. Partner Type:</label>
                            {{$singleRequest->oBusinessPartner->Name}}
                        </div>
                        <div class="col-xs-6">
                            <label>Status:</label>
                            {{$singleRequest->oStatus->Name}}
                        </div>
                        <div class="col-xs-6">
                            <label>Business Partner:</label>
                            {{$singleRequest->BusinessPartnerName}}
                        </div>
                        <div class="col-xs-6">
                            <label>Date:</label>
                            {{date('F d, Y', strtotime($singleRequest->Date))}}
                        </div>
                        <div class="col-xs-6">
                            <label>Contact:</label>
                            {{$singleRequest->Contact}}
                        </div>
                        <div class="col-xs-6">
                            <label>Host:</label>
                            {{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}
                        </div>
                        <div class="col-xs-6">
                            <label>Contact Email:</label>
                            {{$singleRequest->ContactEmail}}
                        </div>
                        <div class="col-xs-6">
                            <label>Project/Purpose:</label>
                            {{$singleRequest->Project}}
                        </div>
                        {{--<div class="col-xs-6">
                            <label>Request Type:</label>
                            {{$singleRequest->ihead_request_type->Name}}
                        </div>--}}
                        @if(isset($singleRequest->ApprovedBy))
                            <div class="col-xs-6">
                                <label>Approved By:</label>
                                {{$singleRequest->iheadApprovedBy_to_user->FirstName}} {{$singleRequest->iheadApprovedBy_to_user->LastName}}
                            </div>
                        @elseif($singleRequest->Status == 10)
                            <div class="col-xs-6">
                                <label>Rejected By:</label>
                                {{$singleRequest->iheadRejectedBy_to_user->FirstName}} {{$singleRequest->iheadRejectedBy_to_user->LastName}}
                            </div>
                            <div class="col-xs-6">
                                <label>Reject Reason:</label><br>
                                {{$singleRequest->RejectReason}}
                            </div>
                        @endif
                        <div class="col-xs-6">
                            <label>Name:</label>
                            <input type="text" id="ReceivedByName" name="ReceivedByName" onkeyup="submitDisabler();" onchange="submitDisabler();" class="form-control pull-right">
                        </div>

                        <input type="hidden" id="id" name="id" value="{{$id}}"/>
                        <input type="hidden" id="tablecount" name="assetCount" value="{{$lineItemsCount}}"/>
                        <input type="hidden" id="realTableCount" name="realAssetCount" value="{{$lineItemsCount}}"/>
                        {{--<div class="col-xs-12" style="padding-top:1%;padding-bottom:1px;">
                            <button type="button" id="startBScanner" onclick="startBScanning();" class='btn btn-sm btn-success'><i class="fa fa-btn fa-barcode"></i>&nbsp;&nbsp;Scan Barcode</button>
                        </div>
                        <div class="col-xs-6" id="bScannerContainer" style="display:none;padding-top:1%;padding-bottom:1px;">
                            <label>Barcode</label>
                            &nbsp;&nbsp;&nbsp;
                            <input type="text" id="bScanner" name="bScanner" onchange="bScanning();" class="form-control"/>
                        </div>--}}

                        {{--<div class="row" id="bScannerContainer" style="display:none;padding-left:2%;padding-right:2%;padding-bottom:1px;padding-top:1%;">
                            <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="col-xs-1">
                                            <label for="bScanner">Barcode</label>
                                        </div>
                                        <div class="col-xs-5">
                                            <input type="text" id="bScanner" name="bScanner" onchange="bScanning();" class="form-control"/>
                                        </div>
                                    </div>
                            </div>
                        </div>--}}

                        <!--Change to Table-->
                        {{--<table class="table table-condensed">
                            <thead style="font-size: 12px;">
                                <th style='width:25%;'>Description</th>
                                <th>Control No.</th>
                                <th>Status</th>
                                <th>Quantity</th>
                                <th>UOM</th>
                                <th>Returnable</th>
                                <th>Destination</th>
                                <th style='width:11%;'>Check-In</th>
                                <th style='width:10%;'>Serial Number</th>
                                <th style='width:10%;'>&nbsp;</th>
                            </thead>
                            <tbody style="font-size: 12px;">
                            @foreach($lineItems as $li)
                                @if($li->InDate!="")
                                    <tr id="{{$li->ControlNum}}" style="display:table-row;width:100%;text-align:left;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                @else
                                    <tr id="{{$li->ControlNum}}" style="background:#ff8784;color:#ffffff !important;display:table-row;width:100%;text-align:left;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                @endif
                                        <td>{{$li->Description}}</td>
                                        <td>{{$li->ControlNum}}</td>
                                        <td>{{$li->oStatus->Name}}</td>
                                        <td>{{$li->Quantity}}</td>
                                        <td>{{$li->oUOM->Name}}</td>
                                        <td>
                                            @if($li->Returnable=="1")
                                                Yes
                                            @else
                                                No
                                            @endif
                                        </td>
                                        <td>{{$li->iline_to_destination->Name}}</td>
                                        <td>
                                            <span id="vInDate[{{$li->LineNum}}]">
                                                @if($li->InDate=="")
                                                    -
                                                @else
                                                    {{$li->InDate}}
                                                @endif
                                            </span>
                                        </td>
                                        <td>
                                            <input class="form-control input-sm" type="text" name="SerialNum[{{$li->LineNum}}]" id="SerialNum[{{$li->LineNum}}]" value="{{$li->SerialNum}}"/>
                                        </td>
                                        <td>
                                            <button type='button' class='btn btn-xs includeButton' id="Include[{{$li->LineNum}}]" onclick='includeThis("{{$li->LineNum}}");' style="color:#000000 !important;"><i class='fa fa-check'></i></button>
                                            <button type='button' class='btn btn-xs excludeButton' id="Exclude[{{$li->LineNum}}]" onclick='excludeThis("{{$li->LineNum}}");' style="color:#000000 !important;display:none;"><i class='fa fa-close'></i></button>
                                            <button type='button' class='btn btn-xs photoButton' id="Image[{{$li->LineNum}}]" onclick='showImageUpload("{{$li->LineNum}}");' style="display:none;color:#000000 !important;"><i class='fa fa-image'></i></button>
                                        </td>
                                </tr>
                                <tr style="display:none;">
                                    <td>
                                        <input type="text" name="Included[{{$li->LineNum}}]" id="Included{{$li->LineNum}}" value="0"/>
                                        <input type="text" name="LineID[{{$li->LineNum}}]" id="LineID[{{$li->LineNum}}]" value="{{$li->id}}"/>
                                        --}}{{--<input type="text" name="Description[{{$li->LineNum}}]" id="Description[{{$li->LineNum}}]" value="{{$li->Description}}"/>--}}{{--
                                        <input type="text" name="ControlNum[{{$li->LineNum}}]" id="ControlNum[{{$li->LineNum}}]" value="{{$li->ControlNum}}"/>
                                        <input type="text" name="InDate[{{$li->LineNum}}]" id="InDate[{{$li->LineNum}}]" value="{{$li->InDate}}"/>
                                        --}}{{--<input type="text" name="InRemarks[{{$li->LineNum}}]" id="InRemarks[{{$li->LineNum}}]" value="{{$li->InRemarks}}"/>
                                        <input type="text" name="OutDate[{{$li->LineNum}}]" id="OutDate[{{$li->LineNum}}]" value="{{$li->OutDate}}"/>
                                        <input type="text" name="OutRemarks[{{$li->LineNum}}]" id="OutRemarks[{{$li->LineNum}}]" value="{{$li->OutRemarks}}"/>--}}{{--
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                AA
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>--}}

                            <div class="form-group col-xs-12" style="padding-top:15px;">
                                <div style="padding:0px; color:#ff0000;" id="ErrMsg">
                                    &nbsp;
                                </div>
                                    <div id="aHeaders" class="box-primary" style="align-content: center;">
                                        <div class="row box-header" style="
                                        width:98%;
                                        margin-right:0;
                                        margin-left:0;
                                        margin-top: 2px;
                                        margin-bottom: 0px;
                                        font-weight: bold;
                                        padding:0px;
                                        font-family: 'Helvetica Neue Light', 'Open Sans', Helvetica;
                                        font-size:12px;
                                        text-align: center;
                                        border-bottom: 1px solid #e9e9e9;">
                                            {{--<div class="col-xs-1">Line #</div>--}}
                                            <div class="col-xs-2" style="padding:1%;">Description</div>
                                            {{--<div class="col-xs-2" style="padding:1%;">Serial Number</div>--}}
                                            <div class="col-xs-1" style="padding:1%;">Control No.</div>
                                            <div class="col-xs-1" style="padding:1%;">Status</div>
                                            <div class="col-xs-1" style="padding:1%;">Quantity</div>
                                            <div class="col-xs-1" style="padding:1%;">UOM</div>
                                            <div class="col-xs-1" style="padding:1%;">Item Type</div>
                                            <div class="col-xs-2" style="padding:1%;">Location</div>
                                            <div class="col-xs-1" style="padding:1%;">Check-In</div>
                                            <div class="col-xs-1" style="padding:1%;">Serial Number</div>
                                            <div class="col-xs-1" style="padding:1%;">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div id="aBody" class="box-primary" style="align-content: center;height:400px;max-height:400px;overflow-y:scroll;font-size:12px;">
                                    @foreach($lineItems as $li)
                                            @if($li->InDate!="")
                                                 <div class="row" id="{{$li->ControlNum}}" style="width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @else
                                                 <div class="row" id="{{$li->ControlNum}}" style="background:#ff8784;color:#ffffff !important;display:block;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                            @endif
                                                <div class="col-xs-2" style='padding:5px 2px;word-wrap:break-word;'>{{$li->Description}}</div>
                                                {{--<div class="col-xs-2" style='padding:9px 14px;'>{{$li->SerialNum}}</div>--}}
                                                <div class="col-xs-1" style='padding:5px 2px;'>{{$li->ControlNum}}</div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>{{$li->oStatus->Name}}</div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>
                                                    <span id="dQuantity[{{$li->LineNum}}]">{{$li->Quantity}}</span>
                                                    <input style="display:none;margin:0px;" onkeyup="submitDisabler();" class="form-control input-sm" type="number" value="{{$li->Quantity}}" type="text" name="Quantity[{{$li->LineNum}}]" id="Quantity[{{$li->LineNum}}]"/>
                                                </div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>{{$li->oUOM->Name}}</div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>
                                                    @if($li->Returnable=="1" && $li->ItemType=="0")
                                                        Tools
                                                    @elseif($li->Returnable=="0" && $li->ItemType=="0")
                                                        Consumables
                                                    @elseif($li->Returnable=="1" && $li->ItemType=="1")
                                                        Equipment
                                                    @endif
                                                </div>
                                                 <div class="col-xs-2" style='padding:5px 2px;word-wrap:break-word;'>
                                                     <span><b>Destination</b>: {{$li->iline_to_destination->Name}}</span><br>
                                                     <span><b>Storage</b>: {{$li->iline_to_storage->storage_to_destination->Name}}</span>
                                                 </div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>
                                                    <span id="vInDate[{{$li->LineNum}}]">
                                                        @if($li->InDate=="")
                                                            -
                                                        @else
                                                            {{$li->InDate}}
                                                        @endif
                                                    </span>
                                                </div>
                                                 <div class="col-xs-1 form-group" style='padding:5px 2px;'>
                                                     @if($li->Returnable=="1")
                                                         <span id="dSerialNum[{{$li->LineNum}}]">-</span>
                                                         <input style="display:none;margin:0px;" class="form-control input-sm" type="text" name="SerialNum[{{$li->LineNum}}]" id="SerialNum[{{$li->LineNum}}]"/>
                                                     @elseif($li->Returnable=="0")
                                                         <span id="dSerialNum[{{$li->LineNum}}]">N/A</span>
                                                         <input disabled style="display:none;margin:0px;" class="form-control input-sm" type="text" name="SerialNum[{{$li->LineNum}}]" id="SerialNum[{{$li->LineNum}}]" value="N/A"/>
                                                     @endif


                                                 </div>
                                                <div class="col-xs-1" style='padding:5px 2px;'>
                                                    <button type='button' class='btn btn-xs includeButton' id="Include[{{$li->LineNum}}]" onclick='includeThis("{{$li->LineNum}}");' style="color:#000000 !important;"><i class='fa fa-check'></i></button>
                                                    <button type='button' class='btn btn-xs excludeButton' id="Exclude[{{$li->LineNum}}]" onclick='excludeThis("{{$li->LineNum}}");' style="color:#000000 !important;display:none;"><i class='fa fa-close'></i></button>
                                                    <button type='button' class='btn btn-xs photoButton' id="Image[{{$li->LineNum}}]" onclick='showImageUpload("{{$li->LineNum}}");' style="display:none;color:#000000 !important;"><i class='fa fa-image'></i></button>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;width:100%;margin-left:0px;margin-bottom:0px;margin-top:0px;border-bottom: 1px solid #e9e9e9;">
                                                <input type="text" name="Included[{{$li->LineNum}}]" id="Included{{$li->LineNum}}" value="0"/>
                                                <input type="text" name="LineID[{{$li->LineNum}}]" id="LineID[{{$li->LineNum}}]" value="{{$li->id}}"/>
                                                {{--<input type="text" name="Description[{{$li->LineNum}}]" id="Description[{{$li->LineNum}}]" value="{{$li->Description}}"/>--}}
                                                <input type="text" name="ControlNum[{{$li->LineNum}}]" id="ControlNum[{{$li->LineNum}}]" value="{{$li->ControlNum}}"/>
                                                <input type="text" name="InDate[{{$li->LineNum}}]" id="InDate[{{$li->LineNum}}]" value="{{$li->InDate}}"/>
                                                {{--<input type="text" name="InRemarks[{{$li->LineNum}}]" id="InRemarks[{{$li->LineNum}}]" value="{{$li->InRemarks}}"/>
                                                <input type="text" name="OutDate[{{$li->LineNum}}]" id="OutDate[{{$li->LineNum}}]" value="{{$li->OutDate}}"/>
                                                <input type="text" name="OutRemarks[{{$li->LineNum}}]" id="OutRemarks[{{$li->LineNum}}]" value="{{$li->OutRemarks}}"/>--}}
                                            </div>
                                            <div class="row" id="Child[{{$li->LineNum}}]" style="display:none;width:100%;text-align:center;margin-left:0px;margin-bottom:0px;margin-top:0px;padding:0;border-bottom: 1px solid #e9e9e9;">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-2" style="padding-top:5px;">
                                                            <input class="form-control" type="hidden" id="imgCounter{{$li->LineNum}}" name="imgCounter[{{$li->LineNum}}]" value="0" onkeyup="callLineItems();">
                                                            <button type='button' class='btn btn-xs photoButton' id="addImageUp{{$li->LineNum}}" onclick='addImageUp("{{$li->LineNum}}");' style="color:#000000 !important;"><i class='fa fa-plus'></i>&nbsp;Add Image</button>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top:10px;padding-bottom:10px;" id="imgUploadContainer[{{$li->LineNum}}]">
                                                        <div class="col-xs-2" style='min-height:240px;word-wrap:break-word;border-style: dashed;border-width: 1px;padding:5px;border-color:#a6a6a6;'>
                                                            {{--<input class='btn btn-xs' type="file" name="fileToUpload[{{$li->LineNum}}]" id="fileToUpload">--}}
                                                            <input class="form-control" type="hidden" id="imgInclude{{$li->LineNum}}-0" name="imgInclude[{{$li->LineNum}}][0]" value="0">
                                                            {{--<button type='button' class='btn btn-link btn-xs removeImg' id="removeImg[{{$li->LineNum}}]" style="color:#000000 !important;">&nbsp;</button>--}}
                                                            <label for="imgInp{{$li->LineNum}}-0" class="clone">
                                                                <img src="{{ asset('/image_upload.png') }}"
                                                                     class= "img-rounded img-responsive imgId"
                                                                     id="imgId{{$li->LineNum}}-0" name ="imgId{{$li->LineNum}}-0"
                                                                     style="margin-bottom:0;">
                                                            </label>
                                                            <span id="imgName{{$li->LineNum}}-0">Choose a File</span>
                                                            {!! Form::hidden('pathPhoto',null,array('class'=>'pathPhoto','id'=>'pathPhoto')) !!}
                                                            {!!Form::file('image_path['.$li->LineNum.'][0]', array('id'=>'imgInp'.$li->LineNum.'-0','accept'=>'image/x-png, image/jpeg','onchange'=>'submitDisabler();','style'=>'display:none;')) !!}
                                                            {{--{!! Form::hidden('_token', csrf_token()) !!}
                                                            {!! Form::submit('Save') !!}
                                                            {!!Form::close() !!}--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                    </div>
                                </div>
                            <div class="form-group col-xs-12">
                                <button type="button" disabled id="sendrequest" class="btn btn-primary" onclick="callSubmit();">Update Request</button>
                                <button type="button" id="" class="btn btn-primary" onclick="location.reload();">Cancel</button>
                            </div>
                                <div id="callSubmit" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title" id="dateModalTitle">Confirmation</h4>
                                            </div>
                                            <div class="modal-body" style="padding:5% 10% 5% 10%;" id="confirmationMessage">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>
            </div>
        </div>

        <div id="outDateModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="dateModalTitle">Out Date</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="mLineID">
                        <div class="row" style="padding:0 auto 0 0;margin:0 3% 0 3%;">
                            <label>Date:</label>
                            <div class="input-group col-xs-12">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="outDate" type="text" class="form-control pull-right" id="outDateAll" required="required" value="">
                            </div>
                            <label>Remarks:</label>
                            <div class="input-group col-xs-12">
                                <textarea id="outDateRemarks" class="form-control pull-right" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="inDateModalOK();" id="inDateModalOK">Ok</button>
                        {{--<button type="button" class="btn btn-primary" onclick="outDateModalOK();" id="outDateModalOK">Ok</button>--}}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <table id="uomRef" style="display:none;">
            @foreach($uoms as $u)
                    <tr>
                        <td>{{$u->id}}</td>
                        <td>{{$u->Name}}</td>
                    </tr>
            @endforeach
        </table>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            /*$('#inDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });*/
            $('#outDateAll').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });

        });

        function outDateModal(id){
            document.getElementById('dateModalTitle').innerHTML = "Out Date";
            document.getElementById('mLineID').value = id;
            document.getElementById('outDateAll').value = document.getElementById('OutDate['+id+']').value;
            document.getElementById('outDateRemarks').value = document.getElementById('OutRemarks['+id+']').value;
            $('#outDateModal').modal('show');
        }
        function outDateModalOK(){
            var id = document.getElementById('mLineID').value;
            document.getElementById('OutDate['+id+']').value = document.getElementById('outDateAll').value;
            document.getElementById('vOutDate['+id+']').innerHTML = document.getElementById('outDateAll').value;
            document.getElementById('OutRemarks['+id+']').value = document.getElementById('outDateRemarks').value;
            $('#outDateModal').modal('hide');
            document.getElementById('outDateAll').value = "";
            document.getElementById('outDateRemarks').value = "";
        }
        function startBScanning(){
            var $yourUl = $("#bScannerContainer");
            $yourUl.css("display", $yourUl.css("display") === 'none' ? '' : 'none');
            //document.getElementById("bScannerContainer").style.display = document.getElementById("bScannerContainer").style.display === 'none' ? '' : 'block';
            document.getElementById("bScanner").focus();
        }
        function bScanning(){
            var table = document.getElementById('tablecount').value;
            for(i = 1;i<=table;i++){
                if(document.getElementById('ControlNum['+i+']').value == document.getElementById('bScanner').value) {
                    var controlNum = document.getElementById('ControlNum['+i+']').value;
                    document.getElementById('Included'+i).value = 1;
                    document.getElementById(controlNum).style.background = "#ffffff";
                    document.getElementById(controlNum).style.color = "#000000";
                    document.getElementById("InDate[" + i + "]").value = moment().format('YYYY-MM-DD HH:mm:ss');
                    document.getElementById("vInDate[" + i + "]").innerHTML = moment().format('YYYY/MM/DD - hh:mm A');
                    document.getElementById('bScanner').value = "";
                    document.getElementById("bScanner").focus();
                }
            }
            document.getElementById('bScanner').value = "";
            document.getElementById("bScanner").focus();
            submitDisabler();
        }
        function includeThis(id){
            document.getElementById('dSerialNum['+id+']').style.display = "none";
            document.getElementById('SerialNum['+id+']').style.display = "inline";
            document.getElementById('dQuantity['+id+']').style.display = "none";
            document.getElementById('Quantity['+id+']').style.display = "inline";
            /*document.getElementById('dateModalTitle').innerHTML = "In Date";
             document.getElementById('mLineID').value = id;
             document.getElementById('outDateAll').value = document.getElementById('InDate['+id+']').value;
             $('#outDateModal').modal('show');*/
            var controlNum = document.getElementById('ControlNum['+id+']').value;
            document.getElementById('Included'+id).value = 1;
            document.getElementById(controlNum).style.background = "#ffffff";
            document.getElementById(controlNum).style.color = "#000000";
            document.getElementById("InDate[" + id + "]").value = moment().format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("vInDate[" + id + "]").innerHTML = moment().format('YYYY/MM/DD - hh:mm A');
            document.getElementById("Include[" + id +"]").style.display = "none";
            document.getElementById("Exclude[" + id +"]").style.display = "inline";
            document.getElementById("Image[" + id +"]").style.display = "inline";
            submitDisabler();
        }
        function excludeThis(id){

            document.getElementById('dSerialNum['+id+']').style.display = "inline";
            document.getElementById('SerialNum['+id+']').style.display = "none";
            document.getElementById('dQuantity['+id+']').style.display = "inline";
            document.getElementById('Quantity['+id+']').style.display = "none";
            document.getElementById('Included'+id).value = 0;
            var controlNum = document.getElementById('ControlNum['+id+']').value;
            document.getElementById("InDate[" + id + "]").value = "";
            document.getElementById("vInDate[" + id + "]").innerHTML = "-";
            document.getElementById(controlNum).style.background = "#ff8784";
            document.getElementById(controlNum).style.color = "#ffffff";
            document.getElementById("Exclude[" + id +"]").style.display = "none";
            document.getElementById("Include[" + id +"]").style.display = "inline";
            document.getElementById("Image[" + id +"]").style.display = "none";
            document.getElementById("Child[" + id +"]").style.display = "none";
            submitDisabler();
        }
        function submitDisabler(){

            document.getElementById('ErrMsg').innerHTML = "&nbsp;";
            //alert(document.getElementById('ReceivedByName').value);
            var table = document.getElementById('tablecount').value;
            var errCounter = 0;

            for(i = 1;i<=table;i++){
                //alert(document.getElementById("imgCounter"+i).value);

                if (document.getElementById('Included'+i).value != 0) {



                    //12-13-2017 Rework Needed.
                    if (document.getElementById('imgInp' + i + '-' + 0).value != "")
                        errCounter = errCounter + 1;
                    else {
                        var controlNumber = document.getElementById('ControlNum[' + i + ']').value;
                        document.getElementById('ErrMsg').innerHTML = document.getElementById('ErrMsg').innerHTML + controlNumber + " has no image selected. ";
                        $("#imgId" + i + "-" + 0).attr('src', '{{ asset('/image_upload.png') }}');
                        errCounter = 0;
                    }
                    //RESETTING IMAGES IF NO FILE/S SELECTED
                    for(var j=0;j<=document.getElementById("imgCounter"+i).value;j++) {
                        //if(document.getElementById("imgInclude"+i+"-"+j).value == 1) {
                            if (document.getElementById('imgInp' + i + '-' + j).value != "") {

                                var str = document.getElementById('imgInp' + i + '-' + j).value;
                                var res = str.replace("C:\\fakepath\\", "");
                                document.getElementById('imgName' + i + '-' + j).innerHTML = res;

                                document.getElementById("imgInclude" + i + "-" + j).value = 1;
                                errCounter = errCounter + 1;
                            }
                            else {
                                var controlNumber = document.getElementById('ControlNum[' + i + ']').value;
                                document.getElementById("imgInclude"+i+"-"+j).value = 0;
                                $("#imgId" + i + "-" + j).attr('src', '{{ asset('/image_upload.png') }}');
                                document.getElementById('imgName' + i + '-' + j).innerHTML = "Choose a File";
                                //errCounter = 0;
                            }
                        //}
                    }

                    if(document.getElementById("Quantity[" + i + "]").value=="" || document.getElementById("Quantity[" + i + "]").value<=0 || document.getElementById("Quantity["+i+"]").value.indexOf("e")>0 || document.getElementById("Quantity["+i+"]").value.indexOf("E")>0) {
                        document.getElementById("Quantity[" + i + "]").style.border = "2px solid #FF0000";
                        document.getElementById('ErrMsg').innerHTML = document.getElementById('ErrMsg').innerHTML + " Invalid quantity.";
                        errCounter = 0;
                    }
                    else {
                        document.getElementById("Quantity[" + i + "]").style.border = "1px solid #d2d6de";
                    }
                }
            }

            if(errCounter==0 &&  document.getElementById('ErrMsg').innerHTML=="&nbsp;")
                document.getElementById('ErrMsg').innerHTML = "No Line Item Selected.";

            if(document.getElementById("ReceivedByName").value=="") {
                document.getElementById("ReceivedByName").style.border = "2px solid #FF0000";
                errCounter = 0;
                document.getElementById('ErrMsg').innerHTML = document.getElementById('ErrMsg').innerHTML+" Name is not set.";
            }else document.getElementById("ReceivedByName").style.border = "1px solid #d2d6de";


            if(errCounter==0)
                document.getElementById('sendrequest').disabled=true;
            else
                document.getElementById('sendrequest').disabled=false;

        }
        function messageSetter(){
            var table = document.getElementById('tablecount').value;
            var errCounter = 0;

            for(i = 1;i<=table;i++){
                if (document.getElementById('Included'+i).value == 0)
                    errCounter = errCounter + 1;
            }
            if(errCounter!=0)
                document.getElementById("confirmationMessage").innerHTML = "There are excluded line items for this request.<br><br> Do you wish to continue?";
            else
                document.getElementById("confirmationMessage").innerHTML = "Update this request?";
        }
        function callSubmit(){
            messageSetter();
            $('#callSubmit').modal('show');
        }
        $('form input').on('keypress', function(e) {
            return e.which !== 13;
        });
        function showImageUpload(item){
            var x = document.getElementById("Child[" + item +"]");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function imageChecker(id){
            //alert(document.getElementById('imgInp'+id).value);
        }

        //Image
        var t=0;
        function readURL(input, img){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(img).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function browseURL(path, path2){
            $(path).change(function(){
                readURL(this, path2);
            });
        }
        var lineItems = <?php echo $lineItems->count();?>;
        for(var i=1;i<=lineItems;i++) {
            for(var j=0;j<=document.getElementById("imgCounter"+i).value;j++){
                browseURL("#imgInp"+ i +"-"+j, "#imgId"+i+"-"+j);
            }

        }

        function callLineItems(){
            var lineItems = <?php echo $lineItems->count();?>;
            for(var i=1;i<=lineItems;i++) {
                for(var j=0;j<=document.getElementById("imgCounter"+i).value;j++){
                    browseURL("#imgInp"+ i +"-"+j, "#imgId"+i+"-"+j);
                }

            }
        }

        function removeImg(lineitemno,imagearrayno){
            //alert("Remove " + lineitemno+", "+imagearrayno);
            document.getElementById("imgInclude"+ lineitemno +"-"+imagearrayno).value=0;
            submitDisabler();
        }
        function addImageUp(lineitem){
            document.getElementById('imgCounter'+lineitem).value = parseInt(document.getElementById('imgCounter'+lineitem).value) + 1;
            var imgNumber = document.getElementById('imgCounter'+lineitem).value;
            var newdiv = document.createElement('div');

            newdiv.innerHTML = '<div class="col-xs-2" style="min-height:240px;word-wrap:break-word;border-style: dashed;border-width: 1px;padding:5px;border-color:#a6a6a6;">' +
            '<input class="form-control" type="hidden" id="imgInclude'+lineitem+'-'+imgNumber+'" name="imgInclude['+lineitem+']['+imgNumber+']" value="1">' +
            //'<button type="button" class="btn btn-link btn-xs removeImg" id="removeImg['+lineitem+']" onclick="removeImg('+lineitem+','+imgNumber+');" style="color:#000000 !important;">Remove</button>' +
            '<label for="imgInp'+lineitem+'-'+imgNumber+'" class="clone">' +
            '<img src="{{ asset('/image_upload.png') }}" class= "img-rounded img-responsive imgId"' +
            'id="imgId'+lineitem+'-'+imgNumber+'" name ="imgId'+lineitem+'-'+imgNumber+'"' +
            'style="margin-bottom:0;">' +
            '</label>' +
            '<span id="imgName'+lineitem+'-'+imgNumber+'">Choose a File</span>' +
            '<input class="pathPhoto" id="pathPhoto" name="pathPhoto" type="hidden">' +
            '<input id="imgInp'+lineitem+'-'+imgNumber+'" accept="image/x-png, image/jpeg" onchange="submitDisabler();" style="display:none;" name="image_path['+lineitem+']['+imgNumber+']" type="file">' +
            '</div>';
            document.getElementById("imgUploadContainer["+lineitem+"]").appendChild(newdiv);
            if (imgNumber==5){
                document.getElementById("addImageUp"+lineitem).disabled = true;
            }
            callLineItems();
            submitDisabler();

        }
    </script>
@endpush
@endsection