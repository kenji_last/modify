@extends('layouts.admin')
@section('title', 'View Outgoing')
@section('gate', 'active')
@section('in_gateview', 'active')

    @if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_gateview')))
        @section('in_gate_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
        @section('in_gate_co_view', 'active')
    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_closed_view')))
        @section('in_gate_closed_view', 'active')
    @endif

@section('header_title', 'New Request')
@section('header_desc', 'Create new request')
@section('content')
    <!--SELECT DROP DOWN LIST-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}">
    <!--DATE PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') }}">
    <!--TIME PICKER-->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}">

    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <body>
        <div class="row" style="padding-left:2%;padding-right:2%;">
            <div class="col-md-12">
                <div class="panel panel-default" style="max-height:100%;height:90% !important;">
                    <div class="panel-heading">
                        @if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_gateview')))
                            <h4><i class="fa fa-user-secret"></i>&nbsp;Incoming Requests - Check-In</h4>
                        @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
                            <h4><i class="fa fa-user-secret"></i>&nbsp;Incoming Requests - Check-Out</h4>
                        @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_closed_view')))
                            <h4><i class="fa fa-user-secret"></i>&nbsp;Incoming Requests - Closed Requests</h4>
                        @endif
                    </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped" id="example1" style="font-size:12px !important;">
                                <thead>
                                <th>&nbsp;</th>
                                <th>Request No.</th>
                                <th>Status</th>
                                <th>B. Partner Name</th>
                                <th>B. Partner Type</th>
                                <th>Contact</th>
                                <th>Host</th>
                                <th>Date</th>
                                </thead>
                                <tbody>
                                @foreach($myRequests as $mR)
                                    <tr>
                                        <td>
                                            @if ((starts_with(Route::getCurrentRoute()->getPath(), 'in_gateview')))
                                                    <a href="{{url('in_gateview/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_co_view')))
                                                    <?php $latestCheckOutID = App\iLineItems::where('h_ID','=',$mR->id)->max('checkout_ID');?>
                                                        <a href="{{url('in_gate_co_view/'.$mR->id.'/'.$latestCheckOutID)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'in_gate_closed_view')))
                                                <a href="{{url('in_gate_closed_view/'.$mR->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a>
                                            @endif
                                        </td>
                                        <td>{{$mR->RequestNo}}</td>
                                        <td>{{$mR->oStatus->Name}}</td>
                                        <td>{{$mR->BusinessPartnerName}}</td>
                                        <td>{{$mR->oBusinessPartner->Name}}</td>
                                        <td>{{$mR->Contact}}</td>
                                        <td>{{$mR->oUser->FirstName}} {{$mR->oUser->LastName}}</td>
                                        <td>{{date('F d, Y', strtotime($mR->Date))}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </body>

    @push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!--time Picker -->
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#Date').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'MM/DD/YYYY',
                timePickerIncrement: 15,
                locale: {format: 'MM/DD/YYYY'}
            });
        });
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endpush
@endsection