@extends('layouts.master.front')
@section('title', 'Confirmation')

@section('content')
    <div style="margin:20px;padding:0;">
        <div style="
		font-size: 16px;
		line-height: 30px;
		text-align: center;
		font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		background-color: #008d4c;
		color: #fff;
		border-bottom: 0 solid transparent;">
            Vendor Asset Management
        </div>
        <div style="
		margin:10px;font-family: 'Arial','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		font-size:10pt;">
            <p><b>Hello, {{$FirstName}}</b></p>
            @if($state=="approval")
                <p>An outgoing request has been submitted for approval.</p>
            @elseif($state=="checkout")
                <p>A request has been submitted for check-in.</p>
            @endif
            <table style="font-size:10pt;font-family: 'Arial';width:90%;">
                <tr>
                    <td width="180px"><b>Request No.</b></td>
                    <td>{{$singleRequest->RequestNo}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Request Type</b></td>
                    <td>{{$singleRequest->oRequestType->Name}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Date</b></td>
                    <td>{{date('M d, Y',strtotime($singleRequest->Date))}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner</b></td>
                    <td>{{$singleRequest->BusinessPartnerName}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner Type</b></td>
                    <td>{{$singleRequest->oBusinessPartner->Name}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Contact</b></td>
                    <td>{{$singleRequest->Contact}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Destination</b></td>
                    <td>{{$singleRequest->Destination}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Reason</b></td>
                    <td>{{$singleRequest->Reason}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Transporter</b></td>
                    <td>{{$singleRequest->Transporter}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Host</b></td>
                    <td>{{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}</td>
                </tr>

                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                @if(isset($singleRequest->COApprovedBy) || isset($singleRequest->COAccountingApprovedBy) || isset($singleRequest->CORejectedBy) || isset($singleRequest->COReceivedByName))
                    <tr>
                        <td colspan="2"><b>Check-out</b></td>
                    </tr>
                @endif
                @if(isset($singleRequest->COApprovedBy))
                    <tr>
                        <td width="180px"><b>Approved By</b></td>
                        <td>{{$singleRequest->oheadCOApprovedBy_to_user->FirstName}} {{$singleRequest->oheadCOApprovedBy_to_user->LastName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->COAccountingApprovedBy))
                    <tr>
                        <td width="180px"><b>Approved By (Accounting)</b></td>
                        <td>{{$singleRequest->oheadCOAccounting_to_user->FirstName}} {{$singleRequest->oheadCOAccounting_to_user->LastName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->COReceivedByName))
                    <tr>
                        <td width="180px"><b>Received By</b></td>
                        <td>{{$singleRequest->COReceivedByName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->CORejectedBy))
                    <tr>
                        <td width="180px"><b>Reject Reason</b></td>
                        <td>{{$singleRequest->CORejectReason}}</td>
                    </tr>
                    <tr>
                        <td width="180px"><b>Rejected By</b></td>
                        <td>{{$singleRequest->oheadRejectedBy_to_user->FirstName}} {{$singleRequest->oheadRejectedBy_to_user->LastName}}</td>
                    </tr>
                @endif

                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                @if(isset($singleRequest->ApprovedBy) || isset($singleRequest->AccountingApprovedBy) || isset($singleRequest->ReceivedByName) || isset($singleRequest->RejectedBy))
                    <tr>
                        <td colspan="2"><b>Check-in</b></td>
                    </tr>
                @endif
                @if(isset($singleRequest->ApprovedBy))
                    <tr>
                        <td width="180px"><b>Approved By</b></td>
                        <td>{{$singleRequest->oheadApprovedBy_to_user->FirstName}} {{$singleRequest->oheadApprovedBy_to_user->LastName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->AccountingApprovedBy))
                    <tr>
                        <td width="180px"><b>Approved By (Accounting)</b></td>
                        <td>{{$singleRequest->oheadAccounting_to_user->FirstName}} {{$singleRequest->oheadAccounting_to_user->LastName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->ReceivedByName))
                    <tr>
                        <td width="180px"><b>Received By</b></td>
                        <td>{{$singleRequest->ReceivedByName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->RejectedBy))
                    <tr>
                        <td width="180px"><b>Reject Reason</b></td>
                        <td>{{$singleRequest->RejectReason}}</td>
                    </tr>
                    <tr>
                        <td width="180px"><b>Rejected By</b></td>
                        <td>{{$singleRequest->oheadCIRejectedBy_to_user->FirstName}} {{$singleRequest->oheadCIRejectedBy_to_user->LastName}}</td>
                    </tr>
                @endif

            </table>
            <br><br>
            <table style="font-size:10pt;font-family: 'Arial';width:90%;">
                <tr>
                    <td><b>Control Number</b></td>
                    <td><b>Description</b></td>
                    <td><b>Qty</b></td>
                    <td><b>UOM</b></td>
                    <td><b>Returnable?</b></td>
                    <td><b>Expected Return</b></td>
                </tr>
                <tbody>
                @foreach($lineItems as $li)
                    <tr>
                        <td>{{$li->ControlNum}}</td>
                        <td style="word-wrap:break-word;max-width:250px;">{{$li->Description}}</td>
                        {{--<td>{{$li->SerialNum}}</td>--}}
                        <td>{{$li->Quantity}}</td>
                        <td>{{$li->oUOM->Name}}</td>
                        <td>
                            @if($li->Returnable==0)
                                No
                            @else
                                Yes
                            @endif
                        </td>
                        <td>
                            @if($li->Returnable==0)
                                N/A
                            @else
                                {{date('M d, Y',strtotime($li->ExpReturn))}}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if($recepient=="approver")
                @if($state=="approval")
                    <p align="center">
                        <table>
                            <tr>
                                <td>
                                    <form method="post" action="{{url('emailOutApprove')}}" target="_blank">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                        <input type="hidden" name="approverID" value="{{$approverID}}">
                                        <input type="submit" name="subButt" value="Approve" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                    </form>
                                </td>
                                <td>
                                    <form method="post" action="{{url('emailOutReject')}}" target="_blank">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                        <input type="hidden" name="approverID" value="{{$approverID}}">
                                        <input type="submit" name="subButt" value="Reject" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </p>
                @elseif($state=="checkout")
                    <p align="center">
                    <table>
                        <tr>
                            <td>
                                <form method="post" action="{{url('emailOutApproveCheckIn')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="approverID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Approve Check-In" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{{url('emailOutReject')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="approverID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Reject" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                        </tr>
                    </table>
                    </p>
                @endif
            @elseif($recepient=="accounting")
                @if($state=="approval")
                    <p align="center">
                    <table>
                        <tr>
                            <td>
                                <form method="post" action="{{url('emailOutAcctgApprove')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="accountingID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Approve" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{{url('emailOutAcctgReject')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="accountingID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Reject" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                        </tr>
                    </table>
                    </p>
                @elseif($state=="checkout")
                    <p align="center">
                    <table>
                        <tr>
                            <td>
                                <form method="post" action="{{url('emailOutAcctgApproveCheckIn')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="accountingID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Approve Check-In" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{{url('emailOutAcctgReject')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="accountingID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Reject" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                        </tr>
                    </table>
                    </p>
                @endif
            @endif


        </div>
        <div style="
		font-size: 16px;
		line-height: 30px;
		text-align: center;
		font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		background-color: #008d4c;
		color: #fff;
		border-bottom: 0 solid transparent;">
            &nbsp;
        </div>

    </div>
@endsection