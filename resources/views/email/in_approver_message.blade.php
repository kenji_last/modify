@extends('layouts.master.front')
@section('title', 'Confirmation')

@section('content')
    <div class="login-box">
        <div style="
    font-size: 16px;
    line-height: 30px;
    text-align: center;
    font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
    background-color: #008d4c;
    color: #fff;
    border-bottom: 0 solid transparent;">
            Vendor Asset Management
        </div>
        <div style="
        font-family: 'Arial','Helvetica Neue',Helvetica,Arial,sans-serif !important;
        font-size:10pt;">
            <p><b>Good day, {{$FirstName}}</b></p>

                @if($state=="rejected for check-out")
                    <p>Your incoming request has been {{$state}} and has been reverted to Checked In status.</p>
                @else
                    <p>Your incoming request has been {{$state}}.</p>
                @endif

            <table style="font-size:10pt;font-family: 'Arial';width:90%;">
                <tr>
                    <td width="180px"><b>Request No.</b></td>
                    <td>{{$singleRequest->RequestNo}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Date</b></td>
                    <td>{{date('M d, Y',strtotime($singleRequest->Date))}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner</b></td>
                    <td>{{$singleRequest->BusinessPartnerName}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner Type</b></td>
                    <td>{{$singleRequest->oBusinessPartner->Name}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Contact</b></td>
                    <td>{{$singleRequest->Contact}}</td>
                </tr>
                {{--<tr>
                    <td width="180px"><b>Host</b></td>
                    <td>{{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}</td>
                </tr>--}}
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                @if(isset($rejectReason))
                    <tr>
                        <td width="180px"><b>Reject Reason</b></td>
                        <td>{{$rejectReason}}</td>
                    </tr>
                    <tr>
                        <td width="180px"><b>Rejected By</b></td>
                        <td>{{$rejector}}</td>
                    </tr>
                @endif

                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                @if(isset($singleRequest->ApprovedBy) || isset($singleRequest->ReceivedByName) || isset($singleRequest->RejectedBy))
                    <tr>
                        <td colspan="2"><b>Check-in</b></td>
                    </tr>
                @endif
                @if(isset($singleRequest->ApprovedBy))
                    <tr>
                        <td width="180px"><b>Approved By</b></td>
                        <td>{{$singleRequest->iheadApprovedBy_to_user->FirstName}} {{$singleRequest->iheadApprovedBy_to_user->LastName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->ReceivedByName))
                    <tr>
                        <td width="180px"><b>Received By</b></td>
                        <td>{{$singleRequest->ReceivedByName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->RejectedBy))
                    <tr>
                        <td width="180px"><b>Reject Reason</b></td>
                        <td>{{$singleRequest->RejectReason}}</td>
                    </tr>
                    <tr>
                        <td width="180px"><b>Rejected By</b></td>
                        <td>{{$singleRequest->iheadRejectedBy_to_user->FirstName}} {{$singleRequest->iheadRejectedBy_to_user->LastName}}</td>
                    </tr>
                @endif

                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                @if(isset($singleRequest->COApprovedBy) || isset($singleRequest->CORejectedBy) || isset($singleRequest->COReceivedByName))
                    <tr>
                        <td colspan="2"><b>Check-out</b></td>
                    </tr>
                @endif
                @if(isset($singleRequest->COApprovedBy))
                    <tr>
                        <td width="180px"><b>Approved By</b></td>
                        <td>{{$singleRequest->iheadCOApprovedBy_to_user->FirstName}} {{$singleRequest->iheadCOApprovedBy_to_user->LastName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->COReceivedByName))
                    <tr>
                        <td width="180px"><b>Received By</b></td>
                        <td>{{$singleRequest->COReceivedByName}}</td>
                    </tr>
                @endif
                @if(isset($singleRequest->CORejectedBy))
                    <tr>
                        <td width="180px"><b>Reject Reason</b></td>
                        <td>{{$singleRequest->CORejectReason}}</td>
                    </tr>
                    <tr>
                        <td width="180px"><b>Rejected By</b></td>
                        <td>{{$singleRequest->iheadCORejectedBy_to_user->FirstName}} {{$singleRequest->iheadCORejectedBy_to_user->LastName}}</td>
                    </tr>
                @endif

            </table>
            <br><br>
            <table style="font-size:8px;font-family: 'Arial';width:90%;">
                <tr>
                    <td style="font-size:11px;"><b>Control Number</b></td>
                    <td style="font-size:11px;"><b>Description</b></td>
                    {{--<td><b>Serial Number</b></td>--}}
                    <td style="font-size:11px;"><b>Qty</b></td>
                    <td style="font-size:11px;"><b>UOM</b></td>
                    <td style="font-size:11px;"><b>Destination</b></td>
                    <td style="font-size:11px;"><b>Item Type</b></td>
                </tr>
                <tbody>
                @foreach($lineItems as $li)
                    <tr>
                        <td style="font-size:11px;">{{$li->ControlNum}}</td>
                        <td style="font-size:11px;word-wrap:break-word;max-width:250px;">{{$li->Description}}</td>
                        {{--<td>{{$li->SerialNum}}</td>--}}
                        <td style="font-size:11px;">{{$li->Quantity}}</td>
                        <td style="font-size:11px;">{{$li->oUOM->Name}}</td>
                        <td style="font-size:11px;">{{$li->iline_to_destination->Name}}</td>
                        <td style="font-size:11px;">
                            @if($li->Returnable==1 && $li->ItemType==0)
                                Tools
                            @elseif($li->Returnable==0 && $li->ItemType==0)
                                Consumables
                            @elseif($li->Returnable==1 && $li->ItemType==1)
                                Equipment
                            @endif
                            {{--@if($li->Returnable==0)
                                No
                            @else
                                Yes
                            @endif--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <br>
        @if($state=="approved")
        <form method="post" action="{{url('pdf_file')}}" target="_blank">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
            <input type="submit" name="subButt" value="Download PDF" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
        </form>
        @elseif($state=="approved for check-out")
            <form method="post" action="{{url('pdf_co_file')}}" target="_blank">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                <input type="submit" name="subButt" value="Download PDF" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
            </form>
        @endif
        <br>
        <div style="
		font-size: 16px;
		line-height: 30px;
		text-align: center;
		font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		background-color: #008d4c;
		color: #fff;
		border-bottom: 0 solid transparent;">
            &nbsp;
        </div>
    </div>
@endsection