@extends('layouts.master.front')
@section('title', 'Confirmation')

@section('content')
    <div class="login-box">
        <div style="
    font-size: 16px;
    line-height: 30px;
    text-align: center;
    font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
    background-color: #008d4c;
    color: #fff;
    border-bottom: 0 solid transparent;">
            APO-UGEC - Vendor Asset Management
        </div>
        <div style="
        font-family: 'Arial','Helvetica Neue',Helvetica,Arial,sans-serif !important;
        font-size:10pt;">
            <p><b>Good day, {{$FirstName}}</b></p>
            <p>Here are the details for your visit on {{date('M d, Y',strtotime($singleRequest->Date))}}</p>

            <table style="font-size:10pt;font-family: 'Arial';width:90%;">
                <tr>
                    <td width="180px"><b>Request No.</b></td>
                    <td>{{$singleRequest->RequestNo}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Date</b></td>
                    <td>{{date('M d, Y',strtotime($singleRequest->Date))}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner</b></td>
                    <td>{{$singleRequest->BusinessPartnerName}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner Type</b></td>
                    <td>{{$singleRequest->oBusinessPartner->Name}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Project/Purpose</b></td>
                    <td>{{$singleRequest->Project}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Host</b></td>
                    <td>{{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>

            </table>
            <br><br>
            <table style="font-size:10pt;font-family: 'Arial';width:90%;">
                <tr>
                    <td><b>Control Number</b></td>
                    <td><b>Description</b></td>
                    {{--<td><b>Serial Number</b></td>--}}
                    <td><b>Qty</b></td>
                    <td><b>UOM</b></td>
                    <td><b>Destination</b></td>
                    <td><b>Returnable?</b></td>
                </tr>
                <tbody>
                @foreach($lineItems as $li)
                    <tr>
                        <td>{{$li->ControlNum}}</td>
                        <td style="word-wrap:break-word;max-width:250px;">{{$li->Description}}</td>
                        {{--<td>{{$li->SerialNum}}</td>--}}
                        <td>{{$li->Quantity}}</td>
                        <td>{{$li->oUOM->Name}}</td>
                        <td>{{$li->iline_to_destination->Name}}</td>
                        <td>
                            @if($li->Returnable==0)
                                No
                            @else
                                Yes
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <p>Attached is a printable copy of the request document.</p>

        <p>Please print out and present the request document to the guard on duty upon your arrival.</p>
        <br>
        {{--<form method="post" action="{{url('pdf_file')}}" target="_blank">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
            <input type="submit" name="subButt" value="Download PDF" style="
                                        text-decoration:none;
                                        padding: 3px 6px;
                                        margin-bottom: 0;
                                        font-size: 12px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
        </form>--}}
        <br>
        <div style="
		font-size: 16px;
		line-height: 30px;
		text-align: center;
		font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		background-color: #008d4c;
		color: #fff;
		border-bottom: 0 solid transparent;">
            &nbsp;
        </div>
    </div>
@endsection