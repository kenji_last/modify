@extends('layouts.master.front')
@section('title', 'Confirmation')

@section('content')
    <div style="margin:20px;padding:0;">
        <div style="
		font-size: 16px;
		line-height: 30px;
		text-align: center;
		font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		background-color: #008d4c;
		color: #fff;
		border-bottom: 0 solid transparent;">
            Vendor Asset Management
        </div>
        <div style="
		margin:10px;font-family: 'Arial','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		font-size:10pt;">
            <p><b>Hello, {{$FirstName}}</b></p>
            @if($state=="approval")
                <p>A request has been submitted for approval.</p>
            @elseif($state=="checkout")
                <p>A request has been submitted for check-out.</p>
            @endif
            <table style="font-size:10pt;font-family: 'Arial';width:90%;">
                <tr>
                    <td width="180px"><b>Request No.</b></td>
                    <td>{{$singleRequest->RequestNo}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Date</b></td>
                    <td>{{date('M d, Y',strtotime($singleRequest->Date))}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner</b></td>
                    <td>{{$singleRequest->BusinessPartnerName}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Business Partner Type</b></td>
                    <td>{{$singleRequest->oBusinessPartner->Name}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Contact</b></td>
                    <td>{{$singleRequest->Contact}}</td>
                </tr>
                <tr>
                    <td width="180px"><b>Host</b></td>
                    <td>{{$singleRequest->oUser->FirstName}} {{$singleRequest->oUser->LastName}}</td>
                </tr>
            </table>
            <br><br>
            <table style="font-size:8px;font-family: 'Arial';width:90%;">
                <tr>
                    <td style="font-size:11px;"><b>Control Number</b></td>
                    <td style="font-size:11px;"><b>Description</b></td>
                    {{--<td><b>Serial Number</b></td>--}}
                    <td style="font-size:11px;"><b>Qty</b></td>
                    <td style="font-size:11px;"><b>UOM</b></td>
                    <td style="font-size:11px;"><b>Destination</b></td>
                    <td style="font-size:11px;"><b>Item Type</b></td>
                </tr>
                <tbody>
                @foreach($lineItems as $li)
                    <tr>
                        <td style="font-size:11px;">{{$li->ControlNum}}</td>
                        <td style="font-size:11px;word-wrap:break-word;max-width:250px;">{{$li->Description}}</td>
                        {{--<td>{{$li->SerialNum}}</td>--}}
                        <td style="font-size:11px;">{{$li->Quantity}}</td>
                        <td style="font-size:11px;">{{$li->oUOM->Name}}</td>
                        <td style="font-size:11px;">{{$li->iline_to_destination->Name}}</td>
                        <td style="font-size:11px;">
                            @if($li->Returnable==1 && $li->ItemType==0)
                                Tools
                            @elseif($li->Returnable==0 && $li->ItemType==0)
                                Consumables
                            @elseif($li->Returnable==1 && $li->ItemType==1)
                                Equipment
                            @endif
                            {{--@if($li->Returnable==0)
                                No
                            @else
                                Yes
                            @endif--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if($recepient=="approver")
                @if($state=="approval")
                    <p align="center">
                        <table>
                            <tr>
                                <td>
                                    <form method="post" action="{{url('emailApprove')}}" target="_blank">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                        <input type="hidden" name="approverID" value="{{$approverID}}">
                                        <input type="submit" name="subButt" value="Approve" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                    </form>
                                </td>
                                <td>
                                    <form method="post" action="{{url('emailReject')}}" target="_blank">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                        <input type="hidden" name="approverID" value="{{$approverID}}">
                                        <input type="submit" name="subButt" value="Reject" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </p>
                @elseif($state=="checkout")
                    <p align="center">
                    <table>
                        <tr>
                            <td>
                                <form method="post" action="{{url('emailApproveCheckOut')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="approverID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Approve Check-Out" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{{url('emailReject')}}" target="_blank">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="requestID" value="{{$singleRequest->id}}">
                                    <input type="hidden" name="approverID" value="{{$approverID}}">
                                    <input type="submit" name="subButt" value="Reject" style="
                                        text-decoration:none;
                                        padding: 6px 12px;
                                        margin-bottom: 0;
                                        font-size: 14px;
                                        font-weight: 400;
                                        line-height: 1.42857143;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        color: #fff;
                                        border-radius: 3px;
                                        -webkit-box-shadow: none;
                                        -moz-box-shadow: none;
                                        box-shadow: none;
                                        border-width: 1px;
                                        background-color: #3c8dbc;
                                        border-color: #367fa9;
                                        ">
                                </form>
                            </td>
                        </tr>
                    </table>
                    </p>
                @endif
            @endif
        </div>
        <div style="
		font-size: 16px;
		line-height: 30px;
		text-align: center;
		font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
		background-color: #008d4c;
		color: #fff;
		border-bottom: 0 solid transparent;">
            &nbsp;
        </div>

    </div>
@endsection