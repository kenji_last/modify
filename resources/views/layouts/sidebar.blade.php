
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('default_user_a.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->FirstName}} {{Auth::user()->LastName}}</p>
                <a href="{{url('auth/logout')}}"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
        </div>
    </div>
    @if(\App\Http\Controllers\TransactionController::knowYourRole(1)==true)
        <ul class="sidebar-menu">
            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-gear"></i> <span>Administration</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="header" style="color:#d1d3d3;">User</li>
                    <li class="@yield('side_users')"><a href="{{ url('users') }}"><i class="fa fa-users"></i> User Management</a></li>
                    <li class="@yield('division')"><a href="{{ url('division') }}"><i class="fa fa-cloud"></i> Divisions</a></li>
                    <li class="@yield('department')"><a href="{{ url('department') }}"><i class="fa fa-cloud"></i> Department</a></li>
                    <li class="@yield('approvergroup')"><a href="{{ url('approvergroup') }}"><i class="fa fa-user-plus"></i> Approver Groups</a></li>
                    <li class="@yield('templates')"><a href="{{ url('approvaltemplates') }}"><i class="fa fa-user"></i> Approval Templates</a></li>
                    <li class="header" style="color:#d1d3d3;">Configurations</li>
                    <li class="@yield('uom')"><a href="{{ url('uom') }}"><i class="fa fa-cloud"></i> UOM</a></li>
                    <li class="@yield('destinations')"><a href="{{ url('destinations') }}"><i class="fa fa-cloud"></i> Destinations</a></li>
                </ul>
            </li>
        </ul>
    @endif
    @if(\App\Http\Controllers\TransactionController::knowYourRole(2)==true)
          <ul class="sidebar-menu">
              <li class="treeview @yield('requestor')">
                  <a href="#">
                      <i class="fa fa-user"></i> <span>Requestor</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                      <li class="header" style="color:#ffffff;">Incoming Requests</li>
                      <li class="@yield('in_requestor')"><a href="{{url('in_requestor')}}"><i class="fa fa-plus-square"></i> New</a></li>
                      <li class="@yield('in_view')"><a href="{{url('in_myrequests')}}"><i class="fa fa-list"></i> My Incoming Requests</a></li>
                      {{--<li class="header" style="color:#ffffff;">Outgoing Requests</li>
                      <li class="@yield('out_requestor')"><a href="{{url('out_requestor')}}"><i class="fa fa-plus-square"></i> New</a></li>
                      <li class="@yield('out_view')"><a href="{{url('out_myrequests')}}"><i class="fa fa-list"></i> My Outgoing Requests</a></li>--}}
                  </ul>
              </li>
          </ul>
    @endif

      @if(\App\Http\Controllers\TransactionController::knowYourRole(3)==true)
          <ul class="sidebar-menu">
              <li class="treeview @yield('approver')">
                  <a href="#">
                      <i class="fa fa-user-plus"></i> <span>Approver</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                      <li class="header" style="color:#ffffff;">Incoming Requests</li>
                      <li class="@yield('in_approver_ci_for_approval')"><a href="{{url('in_approver/ci_for_approval')}}"><i class="fa fa-arrow-circle-down"></i> For Check-In</a></li>
                      <li class="@yield('in_approver_ci_approved')"><a href="{{url('in_approver/ci_approved')}}"><i class="fa fa-check"></i> Approved Check-In</a></li>
                      <li class="@yield('in_approver_ci_rejected')"><a href="{{url('in_approver/ci_rejected')}}"><i class="fa fa-close"></i> Rejected Check-In</a></li>
                      <li class="@yield('in_approver_co_for_approval')"><a href="{{url('in_approver/co_for_approval')}}"><i class="fa fa-arrow-circle-up"></i> For Check-Out</a></li>
                      <li class="@yield('in_approver_co_approved')"><a href="{{url('in_approver/co_approved')}}"><i class="fa fa-check"></i> Approved Check-Out</a></li>
                      <li class="@yield('in_approver_co_rejected')"><a href="{{url('in_approver/co_rejected')}}"><i class="fa fa-close"></i> Rejected Check-Out</a></li>
                      {{--<li class="header" style="color:#ffffff;">Outgoing Requests</li>
                      <li class="@yield('out_approver_co_for_approval')"><a href="{{url('out_approver/co_for_approval')}}"><i class="fa fa-arrow-circle-up"></i> For Check-Out</a></li>
                      <li class="@yield('out_approver_co_approved')"><a href="{{url('out_approver/co_approved')}}"><i class="fa fa-check"></i> Approved Check-Out</a></li>
                      <li class="@yield('out_approver_co_rejected')"><a href="{{url('out_approver/co_rejected')}}"><i class="fa fa-close"></i> Rejected Check-Out</a></li>
                      <li class="@yield('out_approver_ci_for_approval')"><a href="{{url('out_approver/ci_for_approval')}}"><i class="fa fa-arrow-circle-down"></i> For Check-In</a></li>
                      <li class="@yield('out_approver_ci_approved')"><a href="{{url('out_approver/ci_approved')}}"><i class="fa fa-check"></i> Approved Check-In</a></li>
                      <li class="@yield('out_approver_ci_rejected')"><a href="{{url('out_approver/ci_rejected')}}"><i class="fa fa-close"></i> Rejected Check-In</a></li>--}}
                  </ul>
              </li>
          </ul>
      @endif
      @if(\App\Http\Controllers\TransactionController::knowYourRole(5)==true)
          <ul class="sidebar-menu">
              <li class="treeview @yield('gate')">
                  <a href="#">
                      <i class="fa fa-user-secret"></i> <span>Gate</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                      <li class="header" style="color:#ffffff;">Incoming Requests</li>
                      <li class="@yield('in_gate_view')"><a href="{{url('in_gateview')}}"><i class="fa fa-sign-in"></i> Check-In</a></li>
                      <li class="@yield('in_gate_co_view')"><a href="{{url('in_gate_co_view')}}"><i class="fa fa-sign-out"></i> Check-Out</a></li>
                      <li class="@yield('in_gate_closed_view')"><a href="{{url('in_gate_closed_view')}}"><i class="fa fa-stop-circle"></i> Closed Requests</a></li>
                      {{--<li class="header" style="color:#ffffff;">Outgoing Requests</li>
                      <li class="@yield('out_gate_view')"><a href="{{url('out_gateview')}}"><i class="fa fa-sign-in"></i> Check-Out</a></li>
                      <li class="@yield('out_gate_ci_view')"><a href="{{url('out_gate_ci_view')}}"><i class="fa fa-sign-out"></i> Check-In</a></li>
                      <li class="@yield('out_gate_closed_view')"><a href="{{url('out_gate_closed_view')}}"><i class="fa fa-stop-circle"></i> Closed Requests</a></li>--}}
                  </ul>
              </li>
          </ul>
      @endif

      @if(\App\Http\Controllers\TransactionController::knowYourRole(6)==true)
          <ul class="sidebar-menu">
              <li class="treeview @yield('audit')">
                  <a href="#">
                      <i class="fa fa-bar-chart"></i> <span>Audit</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                      <li class="header" style="color:#ffffff;">Incoming Requests</li>
                      <li class="@yield('in_a_all')"><a href="{{url('in_a_all')}}"><i class="fa fa-sign-in"></i> All</a></li>
                      {{--<li class="header" style="color:#ffffff;">Outgoing Requests</li>
                      <li class="@yield('out_a_all')"><a href="{{url('out_a_all')}}"><i class="fa fa-sign-in"></i> All</a></li>--}}
                  </ul>
              </li>
          </ul>
      @endif
      {{--@if(\App\Http\Controllers\TransactionController::knowYourRole(7)==true)
          <ul class="sidebar-menu">
              <li class="treeview @yield('accounting')">
                  <a href="#">
                      <i class="fa fa-area-chart"></i> <span>Accounting</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                      --}}{{--<li class="header" style="color:#ffffff;">Outgoing Requests</li>
                      <li class="@yield('out_accounting_co_for_approval')"><a href="{{url('out_accounting/co_for_approval')}}"><i class="fa fa-arrow-circle-up"></i> Check-Out</a></li>
                      <li class="@yield('out_accounting_co_approved')"><a href="{{url('out_accounting/co_approved')}}"><i class="fa fa-check"></i> Approved</a></li>
                      <li class="@yield('out_accounting_co_rejected')"><a href="{{url('out_accounting/co_rejected')}}"><i class="fa fa-close"></i> Rejected</a></li>
                      <li class="@yield('out_accounting_ci_for_approval')"><a href="{{url('out_accounting/ci_for_approval')}}"><i class="fa fa-arrow-circle-down"></i> Check-In</a></li>
                      <li class="@yield('out_accounting_ci_approved')"><a href="{{url('out_accounting/ci_approved')}}"><i class="fa fa-check"></i> Approved</a></li>
                      <li class="@yield('out_accounting_ci_rejected')"><a href="{{url('out_accounting/ci_rejected')}}"><i class="fa fa-close"></i> Rejected</a></li>--}}{{--
                  </ul>
              </li>
          </ul>
      @endif--}}

    </section>
</aside>
