<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vendor Asset Management | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../vendor/fortawesome/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{{ asset('../vendor/driftyco/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--Vue js-->
    <script src="{{ asset('/js/vendor.js') }}"></script>
    <!--End Vuejs-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!-- <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <style>
        labels{
            font-family: 'Arial','Helvetica Neue',Helvetica,Arial,sans-serif !important;
            font-size: 18pt;
        }
        .logo{
            font-family: 'Arial Narrow','Helvetica Neue',Helvetica,Arial,sans-serif !important;
            letter-spacing: -1px;
        }
    </style>
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="{{ url('dBoard') }}" class="logo" style="background:#0cb759 !important;font-size:14px;font-family:Arial !important;">
            <span class="logo-mini"><img src="{{asset('img/APO_PLAIN.png')}}" height="30px"/></span>
            <span class="logo-lg"><img src="{{asset('img/APO_PLAIN.png')}}" height="30px"/> Vendor Asset Management</span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </nav>
    </header>

    @include('layouts.sidebar')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('page_title')
                <small>@yield('page_subtitle')</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                @yield('content')
            </div>
        </section><!-- /.content -->
    </div>

</div><!-- ./wrapper -->
<!-- jQuery 2.2.0 -->
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/jQuery/jQuery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset('../vendor/almasaeed2010/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/fastclick/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('../vendor/almasaeed2010/adminlte/dist/js/app.min.js') }}"></script>
@stack('scripts')

</body>

</html>

