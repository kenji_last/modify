@extends('layouts.master.front')
@section('title', 'Register')

@section('content')
    <div class="register-box">
        <div class="register-logo">

        </div>
        <div class="register-box-body">
            <img src="{{ asset('/images/logo.png') }}" style="width:100px;">
            <img src="{{ asset('/images/ugec.png') }}" style="width:100px;">
            <br><br>
            <h4>ROLES</h4><center>
            <a href="<?php echo 'requestor'?>" >Requestor</a></br>
            <a href="">Status</a></center>
            <form action="{{ url('/auth/register') }}" method="post">
                {!! Form::token() !!}

                <div class="row">
                    <div class="col-xs-8">
                            <label>

                            </label>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        {!! Form::submit('OK', ["class"=>"btn btn-primary btn-block btn-flat"]) !!}
                    </div><!-- /.col -->
                </div>
            </form>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->
@endsection
@push('scripts')

@endpush