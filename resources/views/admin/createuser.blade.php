@extends('layouts.admin')
@section('title', 'Add User')
@section('page_title', 'Add User')
@section('page_subtitle', '')
@section('prev_link', 'dashboard')
@section('previous', 'Dashboard')
@section('current', 'Users')
@section('side_users', 'active')
@section('side_user_treeview', 'active')
@section('content')

    <section class="content">

        <div class="box">
            <div class="box-header">
                <a href="{{ url('users') }}" class="btn btn-default">
                    <i class="fa fa-btn fa-backward"></i>&nbsp;&nbsp;Back</a>
                {{--@if ((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                    <a href="{{route('resetPass', $userinfo->id)}}" class="btn btn-danger" title="Reset Password">
                        Reset Password</a>
                @endif--}}
            </div>
            <div class="box-body" style="padding-left:10%;padding-right:45%;">
                @if (Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}<br>
                    </div>
                @endif
                <!--<form method="POST" v-on="submit: onSubmitForm">-->
                @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                    {!! Form::open(array('url' => 'post_createuser')) !!}
                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                    {!! Form::open(array('url' => 'post_edituser/' . $userinfo->id)) !!}
                @endif
                <div class="form-group">
                    <label>Employee Number</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted"> Example: 11693</em></small>
                    <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">
                        @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                            {!! Form::text('username', old('username'), ['id'=> 'username', 'class'=>'form-control','required']) !!}
                        @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                            {!! Form::text('username', $userinfo->username, ['id'=> 'username', 'class'=>'form-control','required']) !!}
                        @endif
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('username')) <p class="help-block" style="color:red;">{{ $errors->first('username') }}</p> @endif
                    </div>
                    <div class="form-group">
                        <label>First Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Brian</em></small>
                        <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                                {!! Form::text('first_name', old('first_name'), ['id'=> 'first_name', 'class'=>'form-control','required']) !!}
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                                {!! Form::text('first_name', $userinfo->FirstName, ['id'=> 'first_name', 'class'=>'form-control','required']) !!}
                            @endif
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('first_name')) <p class="help-block" style="color:red;">{{ $errors->first('first_name') }}</p> @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Middle Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Johnson</em></small>
                        <div class="form-group has-feedback {{ $errors->has('middle_name') ? 'has-error' : '' }}">
                            @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                                {!! Form::text('middle_name', old('middle_name'), ['id'=> 'middle_name', 'class'=>'form-control','required']) !!}
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                                {!! Form::text('middle_name', $userinfo->MiddleName, ['id'=> 'middle_name', 'class'=>'form-control','required']) !!}
                            @endif
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('middle_name')) <p class="help-block" style="color:red;">{{ $errors->first('middle_name') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: Bell</em></small>
                            <div class="form-group has-feedback {{ $errors->has('middle_name') ? 'has-error' : '' }}">
                                @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                                    {!! Form::text('last_name', old('last_name'), ['id'=> 'last_name', 'class'=>'form-control','required']) !!}
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                                    {!! Form::text('last_name', $userinfo->LastName, ['id'=> 'last_name', 'class'=>'form-control','required']) !!}
                                @endif
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('last_name')) <p class="help-block" style="color:red;">{{ $errors->first('last_name') }}</p> @endif
                            </div>

                            <div class="form-group">
                                <label>Division</label>&nbsp;&nbsp;&nbsp;<span id="loading" style="display:none;"><i class="fa fa-cog fa-spin"></i>&nbsp; Loading Departments...</span>
                                <select class="form-control" name="division" id="division" onchange="changeFuncs();" required>
                                    @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                                        <option value="" disabled="true" selected>-- Division --</option>
                                        @foreach($div as $division)
                                                <option value="{{ $division->id }}">{{ $division->Name }}</option>
                                        @endforeach
                                    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                                        {{--<option value="{{$userinfo->Division}}">{{$userinfo->Division}}</option>--}}
                                        @foreach($div as $division)
                                            @if($userinfo->Division == $division->id)
                                                <option value="{{ $division->id }}" Selected>{{ $division->Name }}</option>
                                            @else
                                                <option value="{{ $division->id }}">{{ $division->Name }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                                <div class="form-group" id="blank_department">
                                    <label>Department/Section</label>
                                    <select class="form-control" name="department" id="dep-blank" disabled>
                                        <option value="">-- Department --</option>
                                    </select>
                                </div>
                                @foreach($div as $divi)
                                    <div class="form-group" id="{{$divi->id}}" style="display:none;">
                                        <label>Department/Section</label>
                                        <select class="form-control" name="department" id="dep-{{$divi->id}}">
                                            <option value="" disabled="true" selected>-- Select Department --</option>
                                            @foreach($dep->where('Division', $divi->id) as $dpt)
                                                <option value="{{ $dpt->id }}">{{ $dpt->Name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endforeach
                            @elseif ((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                                <div class="form-group" id="blank_department" style="display:none;">
                                    <label>Department/Section</label>
                                    <select class="form-control" name="department" id="dep-blank" disabled>
                                        <option value="">-- Department --</option>
                                    </select>
                                </div>
                                @foreach($div as $divi)
                                    @if($divi->id==$userinfo->Division)
                                        <div class="form-group" id="{{$divi->id}}">
                                            <label>Department/Section</label>
                                            <select class="form-control" name="department" id="dep-{{$divi->id}}">
                                    @else
                                        <div class="form-group" id="{{$divi->id}}" style="display:none;">
                                            <label>Department/Section</label>
                                            <select class="form-control" name="department" id="dep-{{$divi->id}}" disabled>
                                    @endif
                                            {{--<option value="" disabled="true" selected>-- Select Department --</option>--}}
                                            @foreach($dep->where('Division', $divi->id) as $dpt)
                                                @if($dpt->id==$userinfo->Department)
                                                    <option value="{{ $dpt->id }}" selected>{{ $dpt->Name }}</option>
                                                @else
                                                    <option value="{{ $dpt->id }}">{{ $dpt->Name }}</option>
                                                @endif

                                            @endforeach
                                        </select>
                                    </div>
                                @endforeach
                            @endif

                            <label>Email</label>&nbsp;&nbsp;&nbsp;<small><em class="text-muted">Example: pjbrooks@ugecorp.com</em></small>
                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                                    {!! Form::email('email', old('email'), ['id'=> 'email', 'class'=>'form-control']) !!}
                                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                                    {!! Form::email('email', $userinfo->Email, ['id'=> 'email', 'class'=>'form-control']) !!}
                                @endif
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif

                            </div>


                            @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                                <div class="form-group">
                                    <label>Roles</label>
                                    @foreach($roles as $rl)
                                        <br>
                                        <input type="checkbox" name="roles[{{$rl->id}}]" id="roles[{{$rl->id}}]" class="flat-red">
                                        <strong>{{$rl->Name}}</strong>
                                    @endforeach
                                </div>
                            @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                                <div class="form-group">
                                    <label>Roles</label>
                                    @foreach($roles as $rl)
                                        <br>
                                        <?php $theRole = \App\Userrolesgroup::where('Users_ID','=',$userinfo->id)->where('Active','=',1)->where('UserRoles_ID','=',$rl->id)->get();?>
                                        @if($theRole->count()!=0)
                                            <input type="checkbox" name="roles[{{$rl->id}}]" id="roles[{{$rl->id}}]" checked class="flat-red">
                                        @else
                                            <input type="checkbox" name="roles[{{$rl->id}}]" id="roles[{{$rl->id}}]" class="flat-red">
                                        @endif
                                        <strong>{{$rl->Name}}</strong>
                                    @endforeach
                                </div>
                            @endif
                            <br>
                        </div>
                    </div>
                </div>
                @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                    {!! Form::submit('Register', ["id"=> "submit","class"=>"btn btn-primary btn-block btn-flat"]) !!}
                @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                    {!! Form::submit('Save Changes', ["id"=> "submit","class"=>"btn btn-primary btn-block btn-flat"]) !!}
                @endif
            </div>
        </div>
    </section>
    <!--Aaaaaaaand another Approve Modal Start-->
    <div class="container">
        <div class="modal fade" id="sendrequestModal3" role="dialog">
            <div class="modal-dialog modal-sm" style="width:400px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-exclamation-triangle"></i>&nbsp;Warning</h4>
                    </div>
                    <div class="modal-body">
                        <p>The email address provided is not a UGEC email address (Example: <b>name@ugecorp.com</b>)</p>
                        <p>Proceed with the provided email address anyway?</p>
                    </div>
                    <div class="modal-footer">
                        <!--Delete-->
                        {!! Form::submit('Yes', ["id"=> "submit","class"=>"btn btn-primary"]) !!}
                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Approve Modal End-->

    {{--Roles Modal--}}{{--
    <div class="modal fade" id="modal_roles" role="dialog">
        <div class="modal-dialog" style="width:20%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Roles</h4>
                </div>

                <div class="modal-body" style="height:85%;">
                    --}}{{--@foreach(App\Userroles::where('Active', 1)->get() as $userroles)
                        <input type="checkbox" name="roles[]" id="role{{$userroles->id}}" value= "{{$userroles->id}}" class="flat-red">
                        <strong>{{$userroles->Name}}</strong><br>
                    @endforeach--}}{{--


                    @if ((starts_with(Route::getCurrentRoute()->getPath(), 'createuser')))
                        <input type="checkbox" name="roles1" id="roles1" class="flat-red">
                        <strong>Admin</strong>
                        <br>
                        <input type="checkbox" name="roles2" id="roles2" class="flat-red">
                        <strong>Requestor</strong>
                        <br>
                        <input type="checkbox" name="roles3" id="roles3" class="flat-red">
                        <strong>Approver</strong>
                        <br>
                        <input type="checkbox" name="roles4" id="roles4" class="flat-red">
                        <strong>Guard</strong>
                        <br>
                    @elseif((starts_with(Route::getCurrentRoute()->getPath(), 'edituser')))
                            @if($adminRole==1)
                                <input type="checkbox" name="roles1" id="roles1" class="flat-red" checked>
                            @else
                                <input type="checkbox" name="roles1" id="roles1" class="flat-red">
                            @endif
                            <strong>Admin</strong>
                            <br>

                            @if($requestorRole==1)
                                <input type="checkbox" name="roles2" id="roles2" class="flat-red" checked>
                            @else
                                <input type="checkbox" name="roles2" id="roles2" class="flat-red">
                            @endif
                            <strong>Requestor</strong>
                            <br>

                            @if($approverRole==1)
                                <input type="checkbox" name="roles3" id="roles3" class="flat-red" checked>
                            @else
                                <input type="checkbox" name="roles3" id="roles3" class="flat-red">
                            @endif
                            <strong>Approver</strong>
                            <br>
                            @if($guardRole==1)
                                    <input type="checkbox" name="roles4" id="roles4" class="flat-red" checked>
                            @else
                                    <input type="checkbox" name="roles4" id="roles4" class="flat-red">
                            @endif
                            <strong>Guard</strong>
                            <br>

                    @endif
                    <div class="row">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="rolesFunc()">OK</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" >Cancel</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    --}}{{--Roles Modal--}}
    {!! Form::close() !!}

@endsection
@push('scripts')
<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="{{ asset('/js/department.js') }}"></script>
<script>
    function rolesFunc()
    {
        if(document.getElementById("roles1").checked == true)
            {
                $("#dr1").show();
            }
        else
            {
                $("#dr1").hide();
            }
        if(document.getElementById("roles2").checked == true)
        {
            $("#dr2").show();
        }
        else
        {
            $("#dr2").hide();
        }
        if(document.getElementById("roles3").checked == true)
        {
            $("#dr3").show();
        }
        else
        {
            $("#dr3").hide();
        }

        if(document.getElementById("roles4").checked == true)
        {
            $("#dr4").show();
        }
        else
        {
            $("#dr4").hide();
        }
    }

    function changeFunc()
    {
        $("#loading").show();
        var get_division = document.getElementById("division");
        var division = get_division.options[get_division.selectedIndex].value;

        $("#blank_department").hide();
        $("#not_blank_department").show();

        //loading hide
        setTimeout(function() {
            $("#loading").hide();
        }, 500);
    }
    function changeFuncs() {
        $("#loading").show();
        var selectBox = document.getElementById("division");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        //alert(selectedValue);
        $("#blank_department").hide();
        $("#"+selectedValue).show();
        $("#dep-"+selectedValue).prop('disabled', false);
        @foreach($div as $dept)
            var theSelect = '<?php echo $dept->id;?>';
            if(selectedValue!=theSelect) {
                $("#"+theSelect).hide();
                $("#dep-" + theSelect).prop('disabled', true);
            }
        @endforeach

        //var table = $('#example1').DataTable();
        //table.search(selectedValue).draw();

        setTimeout(function() {
            $("#loading").hide();
        }, 500);

    }

    /*    function changeFunc() {
     $("#loading").show();
     var selectBox = document.getElementById("department");
     var selectedValue = selectBox.options[selectBox.selectedIndex].value;
     //alert(selectedValue);
     $("#blank-division").hide();
     $("#"+selectedValue).show();
     $("#el"+selectedValue).prop('disabled', false);
    @foreach($div as $division)
     var theSelect = '<?php echo $division->id;?>';
     if(selectedValue!=theSelect) {
     $("#"+theSelect).hide();
     $("#el" + theSelect).prop('disabled', true);
     }
    @endforeach

    //var table = $('#example1').DataTable();
     //table.search(selectedValue).draw();

     setTimeout(function() {
     $("#loading").hide();
     }, 500);

     }*/

</script>
@endpush