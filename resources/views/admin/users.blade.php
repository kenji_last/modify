@extends('layouts.admin')
@section('title', 'User Management')
@section('page_title', 'User Management')
@section('side_user_treeview', 'active')
@section('side_users', 'active')
@section('content')
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/all.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style>
        /*#example1 tr {
            cursor: pointer;
        }
        #example1 tbody tr.highlight td {
            background-color: #9DD5F5;
        }
        #example1 tbody tr:hover{
            background-color: #9DD5F5 !important;
        }*/
    </style>

    <section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-xs-12" style="padding-bottom:5px;">
                <a href="{{url('createuser')}}" class="btn btn-primary pull-left" title="Add new User">
                    <i class="fa fa-btn fa-plus"></i>&nbsp;Add User</a>
            </div>
        </div>
            {{--@if (Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}<br>
                </div>
            @endif--}}
        <div class="box-body" style="padding-left:5%;padding-right:5%;">
            {{--<div class="row">
                <div class="col-xs-12" style="padding-bottom:5px;">
                    <a href="{{url('createuser')}}" class="btn btn-primary pull-left" title="Add new User">
                    <i class="fa fa-btn fa-plus"></i>&nbsp;Add User</a>
                </div>
            </div>--}}
            @if (Session::has('flash_message'))
            <div class="row">
                <div class="col-xs-12 alert alert-success">
                    {{ Session::get('flash_message') }}<br>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Division</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Active</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td><a href="<?php echo 'edituser/' .$user->id?>">{{$user->id}}</a></td>
                                <td><a href="<?php echo 'edituser/' .$user->id?>">{{$user->LastName}}, {{$user->FirstName}}</a></td>
                                <td>{{$user->toDivision->Name}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->Email}}</td>
                                <td align="center">
                                    @if($user->Active==0)
                                        <span class="bg-red btn-xs">No</span>
                                    @else
                                        <span class="bg-yellow btn-xs">Yes</span>
                                    @endif
                                </td>
                                <td align="center">
                                    @if(Auth::user()->id!=$user->id)
                                        @if($user->Active==0)
                                            <button id="toggleUser" class="btn btn-xs btn-default"
                                        @else
                                            <button id="toggleUser" class="btn btn-xs btn-warning"
                                        @endif
                                                onclick="toggleUser({{$user->Active}},{{$user->id}},'{{$user->FirstName}} {{$user->LastName}}');">
                                                <i class="fa fa-power-off"></i></button>
                                    @endif

                                        <button id="toggleUser" class="btn btn-xs btn-primary"
                                                onclick="changePassword({{$user->Active}},{{$user->id}},'{{$user->FirstName}} {{$user->LastName}}');">
                                            <i class="fa fa-lock"></i></button>

                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

        </div>
    </div>
    </section>
    {!! Form::open(array('url' => 'toggleUser')) !!}
    {!! Form::token() !!}
    <div id="toggleUserAcc" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:480px;margin-left:35%;margin-right:35%;margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="toggleUserAccTitle"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;">
                        <input type="hidden" name="toggleUserAccID" id="toggleUserAccID">
                        <span id="toggleUserAccMsg"></span>
                    </div>
                </div>
                <div class="modal-footer" id="rejectSets">
                    {!! Form::submit('Continue', array('class'=>'btn btn-primary'), array('data-dismiss'=>'modal')) !!}
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    {!! Form::open(array('url' => 'resetPassword')) !!}
    {!! Form::token() !!}
    <div id="changePass" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:480px;margin-left:35%;margin-right:35%;margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="changePassTitle">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding:0 auto 0 0;margin:0 5% 0 5%;">
                        <input type="hidden" name="changePassID" id="changePassID">
                        <div class="col-xs-12">
                            <span id="changePassErrMsg" style="font-size:12px;color:#ff0000;"></span>
                        </div>
                        <div class="col-xs-12">
                            <label>Password</label>
                            <input type="password" class="form-control" id="changePassNew1" onchange="checkPass();" onkeyup="checkPass();" name="changePassNew1">
                        </div>
                        <div class="col-xs-12">
                            <label>Confirm New Password</label>
                            <input type="password" class="form-control" id="changePassNew2" onchange="checkPass();" onkeyup="checkPass();" name="changePassNew2">
                        </div>
                        <div class="col-xs-12">
                            <br>
                            <button type="button" id="changePassView" onclick="viewPassword();" class="btn btn-default"><i class="fa fa-eye-slash"></i></button>
                            {{--<button type="button" id="changePassView" onclick="generatePassword();" class="btn btn-default" style="display:none;">Generate Password</button>--}}
                        </div>
                    </div>
                </div>
                <div style="display:none;">
                    <?php $plist = \App\PList::all();?>
                    <span id="plcount">{{$plist->count()}}</span>
                    <table id="pls">
                        @foreach($plist as $pl)
                            <tr>
                                <td>
                                    {{$pl->Name}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="modal-footer" id="rejectSets">
                    {!! Form::submit('Continue', array('class'=>'btn btn-primary','id'=>'changePassSubmit','disabled'), array('data-dismiss'=>'modal')) !!}
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection
@push('scripts')
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('../vendor/almasaeed2010/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>
<script>
    function toggleUser(status,id,name) {
        var stat = "";
        if(status==0)
            stat = "Activate";
        else
            stat = "Deactivate";
        document.getElementById("toggleUserAccID").value = id;
        document.getElementById("toggleUserAccTitle").innerHTML = "Confirm Action";
        document.getElementById("toggleUserAccMsg").innerHTML = "This will " + stat + " the user access for " + name + ".";
        $('#toggleUserAcc').modal('show');
    }
    function changePassword(status,id,name) {
        document.getElementById("changePassNew1").value = "";
        document.getElementById("changePassNew2").value = "";
        document.getElementById("changePassID").value = id;
        document.getElementById("changePassTitle").innerHTML = "Change Password";
        $('#changePass').modal('show');
    }
    //CHECK NEW PASSWORDS
    function checkChars(password){
        return !!password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%* #+-=\.\-\(\)\^?&])[A-Za-z\d$@$!%* #+=\.\-\(\)\^?&]{8,}$/);
    }

    function checkPass(){
        var errCount = 0;
        var errMsg = "";
        document.getElementById("changePassErrMsg").innerHTML = errMsg;
        if(document.getElementById("changePassNew1").value != document.getElementById("changePassNew2").value) {
            errCount = errCount + 1;
            errMsg = errMsg + "The Passwords do not match.<br>";
        }
        var password = document.getElementById("changePassNew1").value;
        if(checkChars(password)==false) {
            errCount = errCount + 1;
            errMsg = errMsg + " Password must:<br>&nbsp;be at least 8 characters long<br>&nbsp;contain at least 1 letter<br>&nbsp;contain at least one Number<br>&nbsp;contain at least one special character(!.-)";
        }

        if(errCount>0) {
            document.getElementById("changePassSubmit").disabled = true;
            document.getElementById("changePassErrMsg").innerHTML = errMsg;
        }
        else{
            document.getElementById("changePassSubmit").disabled = false;
        }

    }
    //SHOW PASSWORD
    var viewPass = 0;
    function viewPassword(){
        var tag1 = document.getElementById("changePassNew1");
        var tag2 = document.getElementById("changePassNew2");
        if(viewPass == 0) {
            tag1.setAttribute('type', 'text');
            tag2.setAttribute('type', 'text');
            document.getElementById("changePassView").innerHTML = "<i class='fa fa-eye'></i>";
            viewPass = 1;
        }
        else {
            tag1.setAttribute('type', 'password');
            tag2.setAttribute('type', 'password');
            document.getElementById("changePassView").innerHTML = "<i class='fa fa-eye-slash'></i>";
            viewPass = 0;
        }
    }
    //SHOW PASSWORD - END

    //GENERATING PASSWORD
    function generatePassword(){
        if(viewPass == 0)
            viewPassword();

        var pass = makePassword();
        document.getElementById("changePassNew1").value = pass;
        document.getElementById("changePassNew2").value = pass;
        checkPass();
    }

    function selectSigns(){
        var signs = "!-.";
        var signs_len = 1;
        var randomstring2 = '';
        for (var i=0; i<signs_len; i++) {
            var rnum = Math.floor(Math.random() * signs.length);
            randomstring2 += signs.substring(rnum,rnum+1);
        }
        return randomstring2;
    }
    function selectNumbers(){
        var signs = "1234567890";
        var signs_len = 1;
        var randomstring2 = '';
        for (var i=0; i<signs_len; i++) {
            var rnum = Math.floor(Math.random() * signs.length);
            randomstring2 += signs.substring(rnum,rnum+1);
        }
        return randomstring2;
    }
    function makePassword() {
        if(document.getElementById("plcount").innerHTML>5){
            var plc = document.getElementById("plcount").innerHTML;
            var randomnumber = Math.floor(Math.random() * (plc - 1 + 1)) + 1;

            var oTable = document.getElementById('pls');
            var oCells = oTable.rows.item(randomnumber).cells;
            var cellVal = oCells.item(0).innerHTML;
            cellVal = cellVal.replace(/\s+/g, '');
            //alert(cellVal);
            randomstring = selectSigns() + cellVal + selectNumbers();
        }
        else{
            //var text = Math.random().toString(36).slice(-8);
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var string_length = 8;
            var randomstring = '';
            for (var i=0; i<string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum,rnum+1);
            }
            randomstring = selectSigns() + randomstring + selectNumbers();
        }
        return randomstring;
    }

    //GENERATING PASSWORD

    $(function () {
        $("#example1").DataTable({
            "columnDefs": [
                {
                    "visible": false,
                    "searchable": true
                }
            ],
            "paging": false,
            "scrollY":        "420px",
            "scrollCollapse": true,
            "autoWidth": false
        });
        /*$('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });*/
    });
    /*$('#example1').on('click', 'tbody tr', function(event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
        var trid = $(this).attr('id');
        console.log(trid);
        // $('#myModal').modal('show');
    });*/
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

    });


</script>
@endpush