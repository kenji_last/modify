<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalRoles extends Model
{
    protected $table = 'ApprovalRoles';

    public function aTemplate()
    {
        return $this->belongsTo('App\ApprovalTemplates', 'AT_id');
    }
    public function aStage()
    {
        return $this->belongsTo('App\ApprovalStages', 'AS_id');
    }
}
