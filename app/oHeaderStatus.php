<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class oHeaderStatus extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'oHeaderStatus';
    public function to_oHeaders()
    {
        return $this->hasMany('App\oHeaders', 'Status');
    }

    public function my_oHeaders($id) {
        $instance =$this->hasMany('App\oHeaders', 'Status');
        $instance->getQuery()->where('CreatedBy','=', $id);
        return $instance;
    }
}
