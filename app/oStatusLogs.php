<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class oStatusLogs extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'oStatusLogs';

    public function oUser()
    {
        return $this->belongsTo('App\User', 'Users_id');
    }

    public function oRequest()
    {
        return $this->belongsTo('App\oHeaders', 'oHeaders_id');
    }
    public function oStatus()
    {
        return $this->belongsTo('App\oHeaderStatus', 'Status');
    }
}
