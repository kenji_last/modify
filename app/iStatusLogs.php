<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class iStatusLogs extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'iStatusLogs';

    public function oUser()
    {
        return $this->belongsTo('App\User', 'Users_id');
    }

    public function oRequest()
    {
        return $this->belongsTo('App\iHeaders', 'iHeaders_id');
    }
    public function oStatus()
    {
        return $this->belongsTo('App\iHeaderStatus', 'Status');
    }
}
