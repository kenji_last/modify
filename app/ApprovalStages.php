<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalStages extends Model
{
    protected $table = 'ApprovalStages';

    public function toDivision()
    {
        return $this->belongsTo('App\Division', 'Division_ID');
    }
    public function toApprovers()
    {
        return $this->hasMany('App\ApprovalStageApprovers', 'AS_id');
    }
    public function allUsers()
    {
        return $this->hasMany('App\User', 'Division','Division_ID');
    }
}
