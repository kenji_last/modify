<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class iLineItems extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'iLineItems';

    public function oUOM()
    {
        return $this->belongsTo('App\UOMs', 'UOM');
    }
    public function oStatus()
    {
        return $this->belongsTo('App\LineStatus', 'LineStatus');
    }
    public function iline_to_ihead()
    {
        return $this->belongsTo('App\iHeaders', 'h_ID');
    }
    public function iline_to_destination()
    {
        return $this->belongsTo('App\Destinations', 'Destination');
    }
    public function iline_to_storage()
    {
        //return $this->belongsTo('App\Destinations', 'Destination');
        return $this->hasOne('App\iLineItemStorage', 'l_ID', 'id');
    }
}
