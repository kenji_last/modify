<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalTemplateRequestors extends Model
{
    protected $table = 'ApprovalTemplateRequestors';

    public function toApprovaltemplates()
    {
        return $this->belongsTo('App\ApprovalTemplates', 'AT_id');
    }
    public function toUsers()
    {
        return $this->belongsTo('App\User', 'User_id');
    }
}
