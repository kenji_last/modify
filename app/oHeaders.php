<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class oHeaders extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'oHeaders';


    public function oUser()
    {
        return $this->belongsTo('App\User', 'CreatedBy');
    }
    public function oContact()
    {
        return $this->belongsTo('App\Employees', 'Contact');
    }
    public function oBusinessPartner()
    {
        return $this->belongsTo('App\BusinessPartners', 'BusinessPartner');
    }
    public function oStatus()
    {
        return $this->belongsTo('App\oHeaderStatus', 'Status');
    }
    public function oRequest()
    {
        return $this->belongsTo('App\oRequestType', 'RequestType');
    }
    public function oheadCOApprovedBy_to_user()
    {
        return $this->belongsTo('App\User', 'COApprovedBy');
    }
    public function oheadApprovedBy_to_user()
    {
        return $this->belongsTo('App\User', 'ApprovedBy');
    }
    public function oheadCOAccounting_to_user()
    {
        return $this->belongsTo('App\User', 'COAccountingApprovedBy');
    }
    public function oheadAccounting_to_user()
    {
        return $this->belongsTo('App\User', 'AccountingApprovedBy');
    }
    public function oheadRejectedBy_to_user()
    {
        return $this->belongsTo('App\User', 'CORejectedBy');
    }
    public function oheadCIRejectedBy_to_user()
    {
        return $this->belongsTo('App\User', 'RejectedBy');
    }
    public function oheadReceivedBy_to_user()
    {
        return $this->belongsTo('App\User', 'COReceivedBy');
    }
    public function oRequestType()
    {
        return $this->belongsTo('App\oRequestType', 'RequestType');
    }
}
