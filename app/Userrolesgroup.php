<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userrolesgroup extends Model
{
    protected $table = 'UserRolesGroup';

    public function toUserRoles()
    {
        return $this->belongsTo('App\Userroles', 'UserRoles_ID');
    }

    public function toUsers()
    {
        return $this->hasMany('App\User', 'Users_id');
    }
}
