<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalStageApprovers extends Model
{
    protected $table = 'ApprovalStageApprovers';

    public function toApprovalStages()
    {
        return $this->belongsTo('App\ApprovalStages', 'AS_id');
    }
    public function toUsers()
    {
        return $this->belongsTo('App\User', 'User_id');
    }
}
