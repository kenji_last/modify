<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class iHeaderStatus extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'iHeaderStatus';
    public function to_iHeaders()
    {
        return $this->hasMany('App\iHeaders', 'Status');
    }
    public function my_iHeaders($id) {
        $instance =$this->hasMany('App\iHeaders', 'Status');
        $instance->getQuery()->where('CreatedBy','=', $id);
        return $instance;
    }
}
