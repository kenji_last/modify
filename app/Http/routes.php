<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth/login');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


Route::group(['middleware' => 'auth'], function () {
    //Dashboard
    Route::get('dBoard', 'TransactionController@dboard');

    //Requestor Views
    Route::get('in_requestor', 'TransactionController@inTransactView');
    Route::get('in_requestor/{id}', [
        'as'    => 'in_requestor',
        'uses'  => 'TransactionController@inTransactViewSingle']);
    Route::get('in_checkout/{id}', 'TransactionController@inCheckoutViewSingle');
    Route::get('in_myrequests', 'TransactionController@inView');
    Route::get('in_myrequests/{id}', [
        'as'    => 'in_myrequests',
        'uses'  => 'TransactionController@inMyRequests']);
    Route::get('in_view/{id}', [
        'as'    => 'in_view',
        'uses'  => 'TransactionController@inViewSingle']);
    Route::get('in_co_view/{id}/{ids}', [
        'as'    => 'in_co_view',
        'uses'  => 'TransactionController@inViewCOLogs']);

    Route::get('out_myrequests', 'TransactionController@outView');
    Route::get('out_myrequests/{id}', [
        'as'    => 'out_myrequests',
        'uses'  => 'TransactionController@outMyRequests']);
    Route::get('out_view/{id}', [
        'as'    => 'out_view',
        'uses'  => 'TransactionController@outViewSingle']);

    Route::get('out_requestor', 'TransactionController@outTransactView');
    Route::get('out_requestor/{id}', [
        'as'    => 'out_requestor',
        'uses'  => 'TransactionController@outTransactViewSingle']);

    Route::get('out_checkin/{id}', 'TransactionController@outCheckinViewSingle');

    //Approver Views

    Route::get('in_approver/{id}', [
        'as'    => 'in_approver',
        'uses'  => 'TransactionController@inApproverMyRequests']);
    Route::get('in_approver/{code}/{id}', [
        'as'    => 'in_approver',
        'uses'  => 'TransactionController@inApproverMyRequestsSingle']);

    Route::get('out_approver/{id}', [
        'as'    => 'out_approver',
        'uses'  => 'TransactionController@outApproverMyRequests']);
    Route::get('out_approver/{code}/{id}', [
        'as'    => 'out_approver',
        'uses'  => 'TransactionController@outApproverMyRequestsSingle']);

    //OLD APPROVER VIEWS - DO NOT DELETE
    /*Route::get('in_approverview', 'TransactionController@inApproverView');
    Route::get('in_approverview/{id}', [
        'as'    => 'in_approverview',
        'uses'  => 'TransactionController@inApproverViewSingle']);
    Route::get('in_my_approvals', 'TransactionController@inMyApprovals');
    Route::get('in_my_approvals/{id}', [
        'as'    => 'in_my_approvals',
        'uses'  => 'TransactionController@inMyApprovalsSingle']);
    Route::get('in_my_rejections', 'TransactionController@inMyRejections');
    Route::get('in_my_rejections/{id}', [
        'as'    => 'in_my_rejections',
        'uses'  => 'TransactionController@inMyRejectionsSingle']);
    Route::get('in_checkoutview', 'TransactionController@inApproverCOView');
    Route::get('in_checkoutview/{id}', [
        'as'    => 'in_checkoutview',
        'uses'  => 'TransactionController@inApproverCOViewSingle']);
    Route::get('in_my_co_approvals', 'TransactionController@inMyCOApprovals');
    Route::get('in_my_co_approvals/{id}', [
        'as'    => 'in_my_co_approvals',
        'uses'  => 'TransactionController@inMyCOApprovalsSingle']);*/

    Route::get('out_approverview', 'TransactionController@outApproverView');
    Route::get('out_approverview/{id}', [
        'as'    => 'out_approverview',
        'uses'  => 'TransactionController@outApproverViewSingle']);
    Route::get('out_my_approvals', 'TransactionController@outMyApprovals');
    Route::get('out_my_approvals/{id}', [
        'as'    => 'out_my_approvals',
        'uses'  => 'TransactionController@outMyApprovalsSingle']);
    Route::get('out_my_rejections', 'TransactionController@outMyRejections');
    Route::get('out_my_rejections/{id}', [
        'as'    => 'out_my_rejections',
        'uses'  => 'TransactionController@outMyRejectionsSingle']);

    Route::get('out_my_ci_approvals', 'TransactionController@outMyCIApprovals');
    Route::get('out_checkinview', 'TransactionController@outApproverCIView');
    Route::get('out_checkinview/{id}', [
        'as'    => 'in_checkinview',
        'uses'  => 'TransactionController@outApproverCIViewSingle']);

    //Guard Views
    Route::get('in_securityview', 'TransactionController@iSecurityView');
    Route::get('in_securityview/{id}', [
        'as'    => 'in_securityview',
        'uses'  => 'TransactionController@iSecurityViewSingle']);
    Route::get('in_security_co_view', 'TransactionController@iSecurityCOView');
    Route::get('in_security_co_view/{id}', [
        'as'    => 'in_security_co_view',
        'uses'  => 'TransactionController@iSecurityCOViewSingle']);
    Route::get('in_gateview', 'TransactionController@inGateView');
    Route::get('in_gateview/{id}', [
        'as'    => 'in_gateview',
        'uses'  => 'TransactionController@inGateViewSingle']);

    Route::get('in_gate_co_view', 'TransactionController@inGateCOView');
    Route::get('in_gate_co_view/{id}', [
        'as'    => 'in_gate_co_view',
        'uses'  => 'TransactionController@inGateCOViewOriginal']);
    Route::get('in_gate_co_view/{id}/{ids}', [
        'as'    => 'in_gate_co_view',
        'uses'  => 'TransactionController@inGateCOViewSingle']);

    Route::get('in_gate_closed_view', 'TransactionController@inGateClosed');
    Route::get('in_gate_closed_view/{id}', [
        'as'    => 'in_gate_co_view',
        'uses'  => 'TransactionController@inGateCOViewClosed']);

    Route::get('out_gateview', 'TransactionController@outGateView');
    Route::get('out_gateview/{id}', [
        'as'    => 'out_gateview',
        'uses'  => 'TransactionController@outGateViewSingle']);

    Route::get('out_gate_ci_view', 'TransactionController@outGateCIView');
    Route::get('out_gate_ci_view/{id}', [
        'as'    => 'out_gate_ci_view',
        'uses'  => 'TransactionController@outGateCIViewOriginal']);
    Route::get('out_gate_ci_view/{id}/{ids}', [
        'as'    => 'out_gate_ci_view',
        'uses'  => 'TransactionController@outGateCIViewSingle']);

    Route::get('out_gate_closed_view', 'TransactionController@outGateClosed');
    Route::get('out_gate_closed_view/{id}', [
        'as'    => 'out_gate_co_view',
        'uses'  => 'TransactionController@outGateCOViewClosed']);

    //Audit Views
    Route::get('in_a_all', 'TransactionController@inAuditAll');
    Route::get('out_a_all', 'TransactionController@outAuditAll');

    //Accounting Views
    Route::get('out_accounting/{id}', [
        'as'    => 'out_accounting',
        'uses'  => 'TransactionController@outAccountingMyRequests']);

    Route::get('out_accounting/{code}/{id}', [
        'as'    => 'out_accounting',
        'uses'  => 'TransactionController@outAccountingMyRequestsSingle']);

/*    Route::get('out_accounting_view/{id}', [
        'as'    => 'out_accounting_view',
        'uses'  => 'TransactionController@outAccountingViewSingle']);
    Route::get('out_accounting_my_approvals/{id}', [
        'as'    => 'out_accounting_my_approvals',
        'uses'  => 'TransactionController@outAccountingMyApprovalsSingle']);
    Route::get('out_accounting_my_rejections/{id}', [
        'as'    => 'out_accounting_my_rejections',
        'uses'  => 'TransactionController@outAccountingMyRejectionsSingle']);
    Route::get('out_accounting_ci_view/{id}', [
        'as'    => 'out_accounting_ci_view',
        'uses'  => 'TransactionController@outAccountingCIViewSingle']);*/

    //OLD VIEW ROUTES - DO NOT DELETE THIS YET
    /*Route::get('out_accounting_view', 'TransactionController@outAccountingView');*/
    /*Route::get('out_accounting_my_approvals', 'TransactionController@outAccountingMyApprovals');*/
    /*Route::get('out_accounting_my_rejections', 'TransactionController@outAccountingMyRejections');*/
    /*Route::get('out_accounting_ci_view', 'TransactionController@outAccountingCIView');*/


    //Admin Views
    Route::get('users', 'AdminController@users');
    Route::get('createuser', 'AdminController@createuser');
    Route::get('edituser/{id}', [
        'as'    => 'createuser',
        'uses'  => 'AdminController@edituser']);
    Route::get('userrolegroup','AdminController@userrolegroup');
    //Reset Password
    Route::get('resetPass/{id}', [
        'as'    => 'resetPass',
        'uses'  => 'AdminController@resetPass']);
    //Divisions
    Route::get(
        'division','AdminController@division'
    );
    //Departments
    Route::get(
        'department','AdminController@department'
    );
    Route::get('edit_department/{id}', [
        'as'    => 'edit_department',
        'uses'  => 'AdminController@edit_department']);
    Route::get('edit_division/{id}', [
        'as'    => 'edit_division',
        'uses'  => 'AdminController@edit_division']);
    //UOMs
    Route::get(
        'uom','AdminController@uom'
    );
    Route::get('edit_uom/{id}', [
        'as'    => 'edit_uom',
        'uses'  => 'AdminController@edit_uom']);

    //DESTINATIONS
    Route::get(
        'destinations','AdminController@destinations'
    );
    Route::get('edit_destinations/{id}', [
        'as'    => 'edit_destinations',
        'uses'  => 'AdminController@edit_destinations']);

    //Approval
    Route::get('approvergroup', [
        'as'    => 'approvergroup',
        'uses'  => 'AdminController@approvergroup']);
    Route::get(
        'approvergroup_approvers/{id}/{has}','AdminController@approvergroup_approvers'
    );
    Route::get(
        'approvergroup_approvers_all/{id}/{has}','AdminController@approvergroup_approvers_all'
    );
    Route::get(
        'addapprover/{id}/{user}','AdminController@addapprover'
    );
    Route::get(
        'approverEmailToggle/{id1}/{id2}/{id3}','AdminController@approverEmailToggle'
    );
    Route::get(
        'removeapprover/{id}/{user}','AdminController@removeapprover'
    );
    Route::get(
        'approvaltemplates','AdminController@approvalTemplates'
    );
    Route::get(
        'approvaltemplate_requestors/{id}/{has}','AdminController@approvaltemplate_requestors'
    );
    Route::get(
        'addrequestor/{id}/{user}','AdminController@addrequestor'
    );
    Route::get(
        'removerequestor/{id}/{user}','AdminController@removerequestor'
    );

});

//Requestor Posts
Route::post('in_requestor', 'TransactionController@inTransactSubmit');
Route::post('in_checkout', 'TransactionController@inCheckoutRequest');
Route::post('out_requestor', 'TransactionController@outTransactSubmit');
Route::post('out_checkin', 'TransactionController@outCheckinRequest');

//Approver Posts
Route::post('in_approve', 'TransactionController@inApproveReq');
Route::post('in_reject', 'TransactionController@inRejectReq');
Route::post('in_cancel', 'TransactionController@inCancelReq');
Route::post('in_co_approve', 'TransactionController@inApproveCOReq');
Route::post('in_co_reject', 'TransactionController@inRejectCOReq');

Route::post('out_approve', 'TransactionController@outApproveReq');
Route::post('out_reject', 'TransactionController@outRejectReq');
Route::post('out_cancel', 'TransactionController@outCancelReq');
Route::post('out_ci_approve', 'TransactionController@outApproveCIReq');
Route::post('out_ci_reject', 'TransactionController@outRejectCIReq');

//Guard Posts
Route::post('in_securityview','TransactionController@iSecurityVerify');
Route::post('in_security_co_view','TransactionController@iSecurityCOVerify');
Route::post('in_gateview', 'TransactionController@inGateSubmit');
Route::post('in_gate_co_view', 'TransactionController@inGateCOSubmit');
Route::post('/saveImage',array('as'=>'saveImage','uses'=>'TransactionController@saveImage'));

Route::post('out_gateview', 'TransactionController@outGateSubmit');
Route::post('out_gate_ci_view', 'TransactionController@outGateCISubmit');

//Email Links
Route::get('emailApprove','TransactionController@doNotAccess');
Route::post('emailApprove','TransactionController@emailApprove');
Route::post('emailApproveCheckOut','TransactionController@emailApproveCheckOut');
Route::post('emailReject','TransactionController@emailReject');

Route::post('emailOutApproveCheckIn','TransactionController@emailOutApproveCheckIn');
Route::get('emailOutApprove','TransactionController@doNotAccess');
Route::post('emailOutApprove','TransactionController@emailOutApprove');
Route::post('emailOutReject','TransactionController@emailOutReject');
Route::post('emailOutAcctgApprove','TransactionController@emailOutAcctgApprove');
Route::post('emailOutAcctgReject','TransactionController@emailOutAcctgReject');
Route::post('emailOutAcctgApproveCheckIn','TransactionController@emailOutAcctgApproveCheckIn');

//Accounting Post Routes
Route::post('out_acctg_approve', 'TransactionController@outAccountingApprove');
Route::post('out_acctg_reject', 'TransactionController@outAccountingReject');
Route::post('out_acctg_ci_approve', 'TransactionController@outAccountingCIApprove');
Route::post('out_acctg_ci_reject', 'TransactionController@outAccountingCIReject');

//Admin Post Routes
//Users
Route::post('post_createuser','AdminController@post_createuser');
Route::post('post_edituser/{id}','AdminController@post_edituser');
Route::post(
    'toggleUser','AdminController@toggleUser'
);
Route::post(
    'resetPassword','AdminController@resetPassword'
);
Route::post(
    'firstTimePassword','AdminController@firstTimePassword'
);
//Division
Route::post('save_division','AdminController@save_division');
Route::post('update_division/{id}','AdminController@update_division');
//UOM
Route::post('save_uom','AdminController@save_uom');
Route::post('update_uom/{id}','AdminController@update_uom');
//Destinations
Route::post('save_destination','AdminController@save_destination');
Route::post('update_destination/{id}','AdminController@update_destination');
//Department
Route::post('save_department','AdminController@save_department');
Route::post('update_department/{id}','AdminController@update_department');

//Approval Templates
Route::post('addapproverstages','AdminController@addapproverstages');
Route::post('edit_approvergroup','AdminController@activate_approvalstage');
Route::post('addApprovalTemplates','AdminController@addApprovalTemplates');
Route::post('update_approvaltemplate','AdminController@update_approvaltemplate');

//START PDF
Route::get('pdf_clearance','PdfFileController@pdf_clearance');
Route::get('pdf_approved/{id}','PdfFileController@pdf_approved');
Route::get('pdf_checkout_guard/{id}/{ids}','PdfFileController@pdf_checkout_guard');
Route::get('pdf_co_approved/{id}','PdfFileController@pdf_co_approved');
Route::get('pdf_file','TransactionController@doNotAccess');
Route::post('pdf_file','PdfFileController@pdf_approved_email');
Route::post('pdf_co_file','PdfFileController@pdf_co_approved_email');
Route::get('pdf_gatepass/{id}','PdfFileController@pdf_gatepass');
Route::post('pdf_out_file','PdfFileController@pdf_out_approved_email');
Route::post('pdf_ci_file','PdfFileController@pdf_out_ci_approved_email');
Route::get('pdf_sample','PdfFileController@pdf_sample');


Route::get('pdfiletest','PdfFileController@testdata');

//END PDF

//EXCEL - Start
Route::get('excel','ExcelController@excelsample');
Route::get('excel_req_checkin','ExcelController@requestorCheckedIn');
Route::get('excel_req_checkout','ExcelController@requestorCheckedOut');
Route::get('excel_appr_checkin','ExcelController@approverCheckedIn');
Route::get('excel_appr_checkout','ExcelController@approverCheckedOut');

Route::get('excel_a_checkin','ExcelController@auditCheckedIn');
Route::get('excel_a_checkout','ExcelController@auditCheckedOut');
