<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'emailApprove',
        'emailReject',
        'emailOutApprove',
        'emailOutReject',
        'emailApproveCheckOut',
        'emailOutApproveCheckIn',
        'emailOutAcctgReject',
        'emailOutAcctgApprove',
        'emailOutAcctgApproveCheckIn',
        'pdf_file',
        'pdf_approved_email',
        'pdf_co_approved',
        'pdf_co_approved_email',
        'pdf_co_file',
        'inGateSubmit','in_gateview'
    ];
}
