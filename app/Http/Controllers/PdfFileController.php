<?php

namespace App\Http\Controllers;

use Anouar\Fpdf\Facades;
use App\oHeaders;
use App\oLineItems;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\iHeaders;
use App\iLineItems;
use App\User;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\File;

class PdfFileController extends Controller
{
    public function pdf_clearance(Request $request)
    {
        $id = 1;
        $head = iHeaders::with('iheadApprovedBy_to_user')
            ->where('id', $id)
            ->first();

        $line = iLineItems::with('iline_to_ihead')
            ->where('h_ID', $id)
            ->get();
      //  dd($head);
       // dd($line);
        return view('pdffile.pdf_clearance',compact('head','line'));
    }
    public function pdf_sample(Request $request)
    {
        return view('pdffile.pdf_sample');
    }
    public function pdf_approved($id)
    {
        if(TransactionController::knowYourRole(2)) {
            $theRequest = iHeaders::where('id','=',$id)->first();
            if($theRequest->Status == 4) {
                $head = iHeaders::with('iheadApprovedBy_to_user')
                    ->where('id', $id)
                    ->first();
                $getDept = User::where('id','=',$head->CreatedBy)->first();
                $dept = $getDept->toDivision->Name;

                $line = iLineItems::with('iline_to_ihead')
                    ->where('h_ID', $id)
                    ->get();
                return view('pdffile.pdf_approved', compact('head', 'line','dept'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to print this request.';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }

    public function pdf_checkout_guard($id,$ids)
    {
        if(TransactionController::knowYourRole(5)) {
            $theRequest = iHeaders::where('id','=',$id)->first();
            if($theRequest->Status == 8) {
                $head = iHeaders::with('iheadApprovedBy_to_user')
                    ->where('id', $id)
                    ->first();
                $getDept = User::where('id','=',$head->CreatedBy)->first();
                $dept = $getDept->toDivision->Name;

                $line = iLineItems::with('iline_to_ihead')
                    ->where('h_ID', $id)
                    ->where('checkout_ID', $ids)
                    ->where('CheckOut', '=',True)
                    ->where('Deleted', '=', False)
                    ->get();
                return view('pdffile.pdf_co_guard', compact('head', 'line','dept'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to print this request.';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }

    public function pdf_co_approved($id)
    {
        if(TransactionController::knowYourRole(2)) {
            $theRequest = iHeaders::where('id','=',$id)->first();
            if($theRequest->Status == 8) {
                $head = iHeaders::with('iheadApprovedBy_to_user')
                    ->where('id', $id)
                    ->first();
                $getDept = User::where('id','=',2)->first();
                $dept = $getDept->toDivision->Name;

                /*$line = iLineItems::with('iline_to_ihead')
                    ->where('h_ID', $id)
                    ->get();*/
                $lineItemsCO = iLineItems::where('h_ID','=',$head->id)->orderBy('checkout_ID','DESC')->first();
                $checkOutID = $lineItemsCO->checkout_ID;
                $line = iLineItems::where('h_ID', '=', $head->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();

                return view('pdffile.pdf_co_approved', compact('head', 'line','dept'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to print this request.';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }

    public function pdf_approved_email(Request $request)
    {
            if(!isset(Auth::user()->id)){
                return view('auth.login');
            }
            $theRequest = iHeaders::where('id','=',$request->requestID)->first();
            if($theRequest->Status == 4) {
                $head = iHeaders::with('iheadApprovedBy_to_user')
                    ->where('id', $request->requestID)
                    ->first();
                $getDept = User::where('id','=',2)->first();
                $dept = $getDept->toDivision->Name;

                $line = iLineItems::with('iline_to_ihead')
                    ->where('h_ID', $request->requestID)
                    ->get();
                return view('pdffile.pdf_approved', compact('head', 'line','dept'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to print this request.';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
    }
    public function pdf_co_approved_email(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return view('auth.login');
        }
        $theRequest = iHeaders::where('id','=',$request->requestID)->first();
        if($theRequest->Status == 8) {
            $head = iHeaders::with('iheadApprovedBy_to_user')
                ->where('id', $request->requestID)
                ->first();
            $getDept = User::where('id','=',$head->CreatedBy)->first();
            $dept = $getDept->toDivision->Name;

            /*$line = iLineItems::with('iline_to_ihead')
                ->where('h_ID', $request->requestID)
                ->get();*/
            $lineItemsCO = iLineItems::where('h_ID','=',$head->id)->orderBy('checkout_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkout_ID;
            $line = iLineItems::where('h_ID', '=', $head->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();


            return view('pdffile.pdf_co_approved', compact('head', 'line','dept'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to print this request.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function pdf_out_approved_email(Request $request)
    {
        $theRequest = oHeaders::where('id','=',$request->requestID)->first();
        if($theRequest->Status == 4) {
            $ohead = $theRequest;
            $oline = oLineItems::where('h_ID', $ohead->id)->get();
            return view('pdffile.pdf_gatepass',compact('ohead', 'oline'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to print this request.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function pdf_out_ci_approved_email(Request $request)
    {
        $theRequest = oHeaders::where('id','=',$request->requestID)->first();
        if($theRequest->Status == 4) {
            $ohead = $theRequest;
            $oline = oLineItems::where('h_ID', $ohead->id)->get();
            return view('pdffile.pdf_gatepass',compact('ohead', 'oline'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to print this request.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }

    public function pdf_gatepass($id)
    {
        $ohead = oHeaders::where('id', $id)->first(); //iHead to oHead
        $oline = oLineItems::where('h_ID', $ohead->id)->get();
        return view('pdffile.pdf_gatepass',compact('ohead', 'oline'));
    }

    public static function attachment_contact($id){
        $head = iHeaders::with('iheadApprovedBy_to_user')
            ->where('id', $id)
            ->first();
        $getDept = User::where('id','=',2)->first();
        $dept = $getDept->toDivision->Name;

        $line = iLineItems::with('iline_to_ihead')
            ->where('h_ID', $id)
            ->get();
        Facades\Fpdf::AddPage();
        Facades\Fpdf::SetXY(35,25);
        Facades\Fpdf::SetFont('Arial','B',12);
        Facades\Fpdf::cell(138,8,"TOOLS & CONSUMABLES CHECK IN REQUEST",0,"","C");
        Facades\Fpdf::SetFont('Arial','',8);
        Facades\Fpdf::SetTitle('Tools & Consumables Check In Request');
        Facades\Fpdf::SetMargins('10','10');
        Facades\Fpdf::Image('img/APO_PLAIN.png',93,8,20);
        Facades\Fpdf::SetXY(10,35);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(0,8,"Request No:",0,"","L");
        Facades\Fpdf::SetXY(45,35);
        Facades\Fpdf::SetFont('Arial','',10);
        Facades\Fpdf::Cell(0,8, $head->RequestNo,0,"","L");
        Facades\Fpdf::SetXY(10,40);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(0,8,"Business Partner:",0,"","L");
        Facades\Fpdf::SetXY(45,40);
        Facades\Fpdf::SetFont('Arial','',10);
        Facades\Fpdf::Cell(0,8, $head->BusinessPartnerName,0,"","L");
        Facades\Fpdf::SetXY(10,45);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(0,8,"Name of Contact:",0,"","L");
        Facades\Fpdf::SetXY(45,45);
        Facades\Fpdf::SetFont('Arial','',10);
        Facades\Fpdf::Cell(0,8, $head->Contact,0,"","L");
        Facades\Fpdf::SetXY(10,50);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(0,8,"Contact Email:",0,"","L");
        Facades\Fpdf::SetXY(45,50);
        Facades\Fpdf::SetFont('Arial','',10);
        if(!isset($head->ContactEmail) || $head->ContactEmail=="")
            Facades\Fpdf::Cell(0,8, "N/A",0,"","L");
        else
            Facades\Fpdf::Cell(0,8, $head->ContactEmail,0,"","L");
        Facades\Fpdf::SetXY(10,55);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(0,8,"Division:",0,"","L");
        Facades\Fpdf::SetXY(45,55);
        Facades\Fpdf::SetFont('Arial','',10);
        Facades\Fpdf::Cell(0,8, $dept,0,"",'L');
        Facades\Fpdf::SetXY(10,60);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(0,8,"Request Date:",0,"","L");
        Facades\Fpdf::SetXY(45,60);
        Facades\Fpdf::SetFont('Arial','',10);
        Facades\Fpdf::Cell(0,8, date('F d, Y',strtotime($head->Date)),0,"",'L');
        Facades\Fpdf::SetXY(10,70);
        Facades\Fpdf::Cell("","", '',1,"",'L');
        Facades\Fpdf::SetXY(10,70);
        Facades\Fpdf::Cell("","", '',1,"",'L');
        Facades\Fpdf::SetXY(10,70);
        Facades\Fpdf::SetFont('Arial','B',9);
        Facades\Fpdf::cell(7,10,"#",1,"","C");
        Facades\Fpdf::SetXY(17,70);
        Facades\Fpdf::SetFont('Arial','B',9);
        Facades\Fpdf::cell(65,10,"Description",1,"","C");
        Facades\Fpdf::SetXY(82,70);
        Facades\Fpdf::SetFont('Arial','B',9);
        Facades\Fpdf::cell(25,10,"Control No.",1,"","C");
        Facades\Fpdf::SetXY(107,70);
        Facades\Fpdf::SetFont('Arial','B',9);
        Facades\Fpdf::cell(20,10,"Quantity",1,"","C");
        Facades\Fpdf::SetXY(127,70);
        Facades\Fpdf::SetFont('Arial','B',9);
        Facades\Fpdf::cell(21,10,"UOM",1,"","C");
        Facades\Fpdf::SetXY(148,70);
        Facades\Fpdf::SetFont('Arial','B',9);
        Facades\Fpdf::cell(25,10,"Item Type",1,"","C");
        Facades\Fpdf::SetXY(173,70);
        Facades\Fpdf::SetFont('Arial','B',9);
        Facades\Fpdf::cell(27,10,"Remarks",1,"","C");
        $y = 80;
        $ctr = 1;
        foreach($line as $l)
        {
            Facades\Fpdf::SetXY(10,$y);
            Facades\Fpdf::SetFont('Arial','',8);
            Facades\Fpdf::cell(7,15,$ctr,1,"","C");
            Facades\Fpdf::SetXY(17,$y);
            Facades\Fpdf::SetFont('Arial','',8);
            //MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
            if(strlen($l->Description) <= 40)
                Facades\Fpdf::cell(65,15,$l->Description,1,"","L");
            elseif(strlen($l->Description) > 40 && strlen($l->Description) <= 80)
                Facades\Fpdf::MultiCell(65,5, $l->Description."                    ","BT","L");
            else
                Facades\Fpdf::MultiCell(65,5, $l->Description,"BT","L");
            // Facades\Fpdf::cell(65,10,$l->Description,1,"","C");
            Facades\Fpdf::SetXY(82,$y);
            Facades\Fpdf::SetFont('Arial','',8);
            Facades\Fpdf::cell(25,15,$l->ControlNum,1,"","C");
            Facades\Fpdf::SetXY(107,$y);
            Facades\Fpdf::SetFont('Arial','',8);
            Facades\Fpdf::cell(20,15,$l->Quantity,1,"","C");
            Facades\Fpdf::SetXY(127,$y);
            Facades\Fpdf::SetFont('Arial','',8);
            Facades\Fpdf::cell(21,15,$l->oUOM->Name,1,"","C");
            if($l->Returnable == 1 && $l->ItemType==0)
            {
                $return = "Tools";
            }
            elseif($l->Returnable == 0 && $l->ItemType==0)
            {
                $return = "Consumables";
            }
            elseif($l->Returnable == 1 && $l->ItemType==1)
            {
                $return = "Equipment";
            }
            Facades\Fpdf::SetXY(148,$y);
            Facades\Fpdf::SetFont('Arial','',8);
            Facades\Fpdf::cell(25,15,$return,1,"","C");

            Facades\Fpdf::SetXY(173,$y);
            Facades\Fpdf::SetFont('Arial','',10);
            Facades\Fpdf::cell(27,15,"",1,"","C");
            $ctr = $ctr + 1;
            $y = $y + 15;
            if($ctr == 13)
            {
                $ctr = 1;
                $y = 80;
                Facades\Fpdf::AddPage();
                $y = 20;
            }
        }
        //Footer
        if($head->ApprovedBy != "")
        {
            Facades\Fpdf::SetXY(15,$y+13);
            Facades\Fpdf::SetFont('Arial','',10);
            Facades\Fpdf::cell(40,8,$head->iheadCreatedby_to_user->FirstName . " " . $head->iheadCreatedby_to_user->LastName,"B","","C");
        }
        else
        {
            Facades\Fpdf::SetXY(10,$y+5);
            Facades\Fpdf::SetFont('Arial','',10);
            Facades\Fpdf::cell(40,30,"______________",0,"","C");
        }
        Facades\Fpdf::SetXY(15,$y+10);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(40,30,"Requestor",0,"","C");
        if($head->ApprovedBy != "")
        {
            Facades\Fpdf::SetXY(85,$y+13);
            Facades\Fpdf::SetFont('Arial','',10);
            Facades\Fpdf::cell(40,8,$head->iheadApprovedBy_to_user->FirstName . " " . $head->iheadApprovedBy_to_user->LastName,"B","","C");
        }
        else
        {
            Facades\Fpdf::SetXY(85,$y+5);
            Facades\Fpdf::SetFont('Arial','',10);
            Facades\Fpdf::cell(40,30,"______________",0,"","C");
        }
        Facades\Fpdf::SetXY(85,$y+10);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(40,30,"Approver",0,"","C");

        Facades\Fpdf::SetXY(155,$y+13);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(40,8, "","B","","C");

        Facades\Fpdf::SetXY(155,$y+10);
        Facades\Fpdf::SetFont('Arial','B',10);
        Facades\Fpdf::cell(40,30, "Verified By",0,"","C");

        Facades\Fpdf::SetXY(10,$y);
        Facades\Fpdf::SetFont('Arial','B',15);
        Facades\Fpdf::cell(190,30,"",1,"","C");

        //Saving as Temporary File for attaching to Email.
        $filename=$head->RequestNo.".pdf";
        $dir = "../storage/pdf_attachments/";
        /*if(!file_exists($dir)) {
            File::makeDirectory($dir, $mode = 0777, true, true);
        }*/
        Facades\Fpdf::Output($dir.$filename,'F');
        //exit;
    }

}
