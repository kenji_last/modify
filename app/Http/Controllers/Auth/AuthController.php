<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mail;
use Hash;
use App\UserAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    protected $username = 'username'; //always put this code for credential
    public function redirectPath()
    {
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->first()->toUserRoles->Name;
        //return '/in_requestor';
        return '/dBoard';
    }
    protected $redirectAfterLogout = 'auth/login';
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
        'name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|min:6',
    ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|max:255|unique:users',
            // 'username'  => 'required|alpha_dash|max:20',
            'password'  => 'required|confirmed|min:6|max:99',
            // 'password' => 'required|confirmed|min:8|regex:/^[-(a-zA-Z0-9!@#$%^&*()+_=`~,.]+$/u',
        ]);
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });
        //die("test");
        /*    User::create([
                'email'     => $request->email,
                'username'  => $request->username,
                'password'  => bcrypt($request->password),
                'given_name'  => bcrypt($request->given_name),
                'middle_name'  => bcrypt($request->middle_name),
                'family_name'  => bcrypt($request->family_name),

            ]);*/

        $users = new User();
        $users->FirstName = $request->given_name;
        $users->MiddleName = $request->middle_name;
        $users->LastName = $request->family_name;
        $users->UserName = $request->username;
        $users->Division = $request->division;
        $users->Department = $request->department;
        $users->Email = "N/A";
        $users->Password = bcrypt($request->password);
        $users->Active = 1;
        $users->FirstLogin = 1;
        $users->forgot_confirmation_code = 0;
        $users->forgotpassword = 0;

        $users->save();
        // return redirect()->route('getRegister');
        Session::flash('flash_message', 'Registration Successful');
        return response(view('auth.register'));
    }
    public function getRegister(Request $request)
    {
        return response(view('auth.register'));
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        /* $user_det = User::where('username' , '=', $credentials['username'])->first();
         if($user_det!=null){
             if($user_det->confirmed==0){
                 return redirect($this->loginPath())
                     ->withInput($request->only($this->loginUsername(), 'remember'))
                     ->withErrors([
                         'cred' => 'Your account is not yet active.',
                     ]);
             }
             if($user_det->active==0){
                 return redirect($this->loginPath())
                     ->withInput($request->only($this->loginUsername(), 'remember'))
                     ->withErrors([
                         'cred' => 'Your account is not activated, contact administrator immidiently to activate your account. Thank you.' ,
                     ]);
             }
         }*/

        // This section is the only change - If Inactive, Deny Login
        if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();
            if ($user->Active) {
                Auth::login($user, $request->has('remember'));
                return redirect()->intended($this->redirectPath());
            } else {
                return redirect($this->loginPath()) // Change this to redirect elsewhere
                ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'Active' => 'You must be active to login.'
                    ]);
            }
        }
        //Section End

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);

    }

    public function logout(Request $request)
    {
        Auth::logout();
        Session::flush();
        $request->session()->flush();
    }

}
