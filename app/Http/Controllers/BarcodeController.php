<?php

namespace App\Http\Controllers;

use App\ApprovalRoles;
use App\ApprovalStageApprovers;
use App\ApprovalStages;
use App\ApprovalTemplateRequestors;
use App\HeaderStatus;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\User;
use Illuminate\Http\Request;
use App\BusinessPartners;
use App\Employees;
use App\LineStatus;
use App\UOMs;
use App\iHeaders;
use App\iLineItems;
use Auth;
use App\SeriesControlNo;
use App\SeriesSerialNo;
use Session;
use Mail;

class BarcodeController extends Controller
{

}