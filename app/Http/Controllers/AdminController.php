<?php

namespace App\Http\Controllers;

use App\Destinations;
use App\Division;
use App\UOMs;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Userroles;
use App\Userrolesgroup;
use Auth;
use Validator;
use DB;
use Hash;
use Input;
use App\Department;
use Carbon;
use PhpParser\Node\Expr\AssignOp\Div;
use Session;
use Mail;
use App\ApprovalRoles;
use App\ApprovalStageApprovers;
use App\ApprovalStages;
use App\ApprovalTemplateRequestors;
use App\ApprovalTemplates;
class AdminController extends Controller
{
    public function userrolegroup(Request $request)
    {
        return view('admin.userrolegroup');
    }

    public function users()
    {
      $users = User::all();
      return view('admin.users',compact('users'));
    }
    public function createuser()
    {
        $roles = Userroles::where('Active',1)->get();
        $dep = Department::where('Active',1)->get();
        $div = Division::where('Active',1)->get();
        return view('admin.createuser',compact('div','dep','roles'));
    }
    public function edituser($id,Request $request)
    {
        $roles = Userroles::where('Active',1)->get();
        $dep = Department::where('Active',1)->get();
        $div = Division::where('Active',1)->get();
        $userinfo = User::where('id',$id)->first();

        $adminRole = Userrolesgroup::where('Users_ID',$id)->where('Active','=',1)->where('UserRoles_ID','=',1)->first();
        if($adminRole==null)
            $adminRole = False;
        else
            $adminRole = True;

        $requestorRole = Userrolesgroup::where('Users_ID',$id)->where('Active','=',1)->where('UserRoles_ID','=',2)->first();
        if($requestorRole==null)
            $requestorRole = False;
        else
            $requestorRole = True;

        $approverRole = Userrolesgroup::where('Users_ID',$id)->where('Active','=',1)->where('UserRoles_ID','=',3)->first();
        if($approverRole==null)
            $approverRole = False;
        else
            $approverRole = True;

        $guardRole = Userrolesgroup::where('Users_ID',$id)->where('Active','=',1)->where('UserRoles_ID','=',4)->first();
        if($guardRole==null)
            $guardRole = False;
        else
            $guardRole = True;

        return view('admin.createuser',compact('div','userinfo','adminRole','requestorRole','approverRole','guardRole','dep','roles'));
    }
    public function post_createuser(Request $request)
    {
       //dd($request->department);
        $this->validate($request, [
            'first_name' => 'required|max:50|regex:/^[(a-zA-ZñÑ\s)]+$/u',
            'last_name' => 'required|max:50|regex:/^[(a-zA-ZñÑ\s)]+$/u',
            'middle_name' => 'required|max:50|regex:/^[(a-zA-ZñÑ\s)]+$/u',
            'username' => 'required|max:50|unique:users,username,|regex:/^[(0-9\s)]+$/u',
            'email' => 'email|max:50|unique:users',
        ]);
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });

        $users = new User();
        $users->FirstName = $request->first_name;
        $users->MiddleName = $request->middle_name;
        $users->LastName = $request->last_name;
        $users->UserName = $request->username;
        $users->Division = $request->division;
        $users->Department = $request->department;
        $users->Email = $request->email;
        $users->Password = bcrypt(1234);
        $users->Active = 1;
        $users->FirstLogin = 1;
        $users->forgot_confirmation_code = 0;
        $users->forgotpassword = 0;
        $users->save();
        $users = User::where('Active',1)->get();
        $selected_user = User::where('UserName', $request->username)->first();

        $roles = Userroles::where('Active',1)->get();
        foreach($roles as $rl){
            if(isset($request->roles[$rl->id])){
                $user_roles = new Userrolesgroup();
                $user_roles->UserRoles_ID = $rl->id;
                $user_roles->Users_ID = $selected_user->id;
                $user_roles->Active = 1;
                $user_roles->save();
            }
        }

            /*if($request->roles1 == 'on')
                {
                    $user_roles = new Userrolesgroup();
                    $user_roles->UserRoles_ID = 1;
                    $user_roles->Users_ID = $selected_user->id;
                    $user_roles->Active = 1;
                    $user_roles->save();
                }
            if($request->roles2 == 'on')
                {
                    $user_roles = new Userrolesgroup();
                    $user_roles->UserRoles_ID = 2;
                    $user_roles->Users_ID = $selected_user->id;
                    $user_roles->Active = 1;
                    $user_roles->save();
                }
            if($request->roles3 == 'on')
                {
                    $user_roles = new Userrolesgroup();
                    $user_roles->UserRoles_ID = 3;
                    $user_roles->Users_ID = $selected_user->id;
                    $user_roles->Active = 1;
                    $user_roles->save();
                }
            if($request->roles4 == 'on')
                {
                    $user_roles = new Userrolesgroup();
                    $user_roles->UserRoles_ID = 4;
                    $user_roles->Users_ID = $selected_user->id;
                    $user_roles->Active = 1;
                    $user_roles->save();
                }*/



        Session::flash('flash_message','' . $selected_user->FirstName. ' ' .$selected_user->LastName   .' User has been added');
        return view('admin.users',compact('users'));
    }
    public function post_edituser(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|max:50|regex:/^[(a-zA-ZñÑ\s)]+$/u',
            'last_name' => 'required|max:50|regex:/^[(a-zA-ZñÑ\s)]+$/u',
            'middle_name' => 'required|max:50|regex:/^[(a-zA-ZñÑ\s)]+$/u',
            'username' => 'required|max:50|regex:/^[(0-9\s)]+$/u',
            'email' => 'email|max:50',
        ]);
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });

        $user = User::where('id', $id)->first();
        $user->FirstName = $request->first_name;
        $user->MiddleName = $request->middle_name;
        $user->LastName = $request->last_name;
        $user->UserName = $request->username;
        $user->Division = $request->division;
        $user->Department = $request->department;
        $user->Email = $request->email;
        //$user->Password = bcrypt(1234);
        $user->Active = 1;
        //$user->FirstLogin = 0;
        $user->forgot_confirmation_code = 0;
        $user->forgotpassword = 0;
        $user->save();

        $roles = Userroles::where('Active',1)->get();
        foreach($roles as $rl){
            $existingR = Userrolesgroup::where('Users_ID','=',$user->id)->where('UserRoles_ID','=',$rl->id)->where('Active','=',1)->get();
            if(isset($request->roles[$rl->id])){
                if($existingR->count()==0) {
                    $user_roles = new Userrolesgroup();
                    $user_roles->UserRoles_ID = $rl->id;
                    $user_roles->Users_ID = $user->id;
                    $user_roles->Active = 1;
                    $user_roles->save();
                }
            }
            else{
                if($existingR->count()!=0) {
                    $DeacRole = Userrolesgroup::where('Users_ID','=',$user->id)->where('UserRoles_ID','=',$rl->id)->where('Active','=',1)->first();
                    $DeacRole->Active = 0;
                    $DeacRole->save();
                }
            }
        }

        $users = User::where('Active',1)->get();

        Session::flash('flash_message','Details of user '.$user->FirstName. ' ' . $user->LastName . ' has been updated.');
       // return view('admin.users',compact('users'));
        return redirect('users');
    }
    public function resetPassword(Request $request)
    {
        $pass = User::find($request->changePassID);
        //$pass = User::where('id', $id)->first();
        $pass->password = bcrypt($request->changePassNew1);
        $pass->FirstLogin = 1;
        $pass->PasswordUpdated = DB::raw('CURRENT_TIMESTAMP');
        $pass->update();
        Session::flash('flash_message', 'The password has been reset for '.$pass->username.'.');
        return redirect('users');
    }
    public function firstTimePassword(Request $request)
    {
        $pass = User::find($request->changePassID);
        //$pass = User::where('id', $id)->first();
        $pass->password = bcrypt($request->changePassNew1);
        $pass->FirstLogin = 0;
        $pass->PasswordUpdated = DB::raw('CURRENT_TIMESTAMP');
        $pass->update();
        Session::flash('flash_message', 'The password has been reset for '.$pass->username.'.');
        return redirect('dBoard');
    }
    public function resetPass($id, Request $request)
    {
        $pass = User::find($id);
        //$pass = User::where('id', $id)->first();
        $pass->password = bcrypt('1234');
        $pass->FirstLogin = 1;
        $pass->update();
        Session::flash('flash_message', 'The password has been reset for '.$pass->username.'.');
        return redirect('users');
        //$users = User::all();
       //return redirect('admin.users',compact('users'));
    }
    public function division()
    {
        if(Auth::user()==NULL)
        {
            return redirect('auth/login');
        }
        else
        {
            $division = Division::all();
            return view('setting.division', compact('division'));
        }
    }
    public function edit_division(Request $request, $id)
    {
        $select_division = Division::find($id);
        $division = Division::all();
        return view('setting.division', compact('division','select_division'));
    }
    public function save_division (Request $request)
    {
        $new_division  = new Division();
        $new_division->Name = $request->division;
        $new_division->Active = $request->has('Active')? 1 : 0;
        $new_division->save();
        Session::flash('flash_message', 'New Department added:: '.$request->division.'. Department: '.$new_division->department.'.');
        return redirect('division');
    }
    public function update_division(Request $request, $id)
    {
        $update_division = Division::find($id);
        $update_division->Name = $request->division;
        $update_division->Active = $request->has('Active')? 1 : 0;
        $update_division->save();
        Session::flash('flash_message', 'Vehicle details updated: '.$request->division.', '.$request->division.'.');
        return redirect('division');
    }
    public function destinations()
    {
        if(Auth::user()==NULL)
        {
            return redirect('auth/login');
        }
        else
        {
            $destinations = Destinations::all();
            $parentDestinations = Destinations::where('Active','=',1)->where('SubLocation','=',0)->get();
            return view('setting.destinations', compact('destinations','parentDestinations'));
        }
    }
    public function edit_destinations($id)
    {
        $select_destination = Destinations::find($id);
        $destinations = Destinations::all();
        $parentDestinations = Destinations::where('Active','=',1)->where('SubLocation','=',0)->where('id','<>',$id)->get();
        return view('setting.destinations', compact('destinations','parentDestinations','select_destination'));
    }
    public function save_destination (Request $request)
    {
        //dd($request->parentLocation);
        $new_destination = new Destinations();
        $new_destination->Name = $request->dName;
        $new_destination->SubLocation = $request->has('SubLocation')? 1 : 0;

        if($request->parentLocation!=NULL)
            $new_destination->Parent = $request->parentLocation;

        $new_destination->Active = 1;
        $new_destination->save();

        /*$new_uom  = new UOMs();
        $new_uom->Name = $request->uom;
        $new_uom->Active = $request->has('Active')? 1 : 0;
        $new_uom->save();*/
        Session::flash('flash_message', 'New Destination added: '.$request->dName.'.');
        return redirect('destinations');
    }
    public function update_destination(Request $request, $id)
    {
        //dd($request);
        $update_destination = Destinations::find($id);
        $update_destination->Name = $request->dName;
        $update_destination->SubLocation = $request->has('SubLocation')? 1 : 0;
        if($request->has('SubLocation')) {
            if ($request->parentLocation != NULL)
                $update_destination->Parent = $request->parentLocation;
        }
        else{
            $update_destination->Parent = NULL;
        }
        $update_destination->Active = $request->has('Active')? 1 : 0;
        $update_destination->save();

        /*$update_uom = UOMs::find($id);
        $update_uom->Name = $request->uom;
        $update_uom->Active = $request->has('Active')? 1 : 0;
        $update_uom->save();*/
        Session::flash('flash_message', 'Destination details updated: '.$request->dName.'.');
        return redirect('destinations');
    }

    public function uom()
    {
        if(Auth::user()==NULL)
        {
            return redirect('auth/login');
        }
        else
        {
            $uom = UOMs::all();
            return view('setting.uom', compact('uom'));
        }
    }
    public function edit_uom(Request $request, $id)
    {
        $select_uom = UOMs::find($id);
        $uom = UOMs::all();
        return view('setting.uom', compact('uom','select_uom'));
    }
    public function save_uom (Request $request)
    {
        $new_uom  = new UOMs();
        $new_uom->Name = $request->uom;
        $new_uom->Active = $request->has('Active')? 1 : 0;
        $new_uom->save();
        Session::flash('flash_message', 'New Department added:: '.$request->uom.'.');
        return redirect('uom');
    }
    public function update_uom(Request $request, $id)
    {
        $update_uom = UOMs::find($id);
        $update_uom->Name = $request->uom;
        $update_uom->Active = $request->has('Active')? 1 : 0;
        $update_uom->save();
        Session::flash('flash_message', 'Unit of Measure details updated: '.$request->uom.'.');
        return redirect('uom');
    }

    public function department()
    {
        $department = Department::all();
        $division = Division::where('Active', 1)->get();
        return view('setting.department', compact('department','division'));
    }
    public function edit_department($id,Request $request)
    {
        $select_department = Department::find($id);
        $department = Department::all();
        $division = Division::where('Active', 1)->get();
        return view('setting.department', compact('department','division','select_department'));
    }
    public function save_department (Request $request)
    {
       // dd($request);
        $new_department  = new Department();
        $new_department->Name = $request->department;
        $new_department->Division = $request->department_series;
        $new_department->Active = $request->has('Active')? 1 : 0;
        $new_department->save();
        Session::flash('flash_message', 'New Department added:: '.$request->department.'.');
        return redirect('department');
    }
    public function update_department(Request $request, $id)
    {
        $update_department = Department::find($id);
        $update_department->Name = $request->department;
        $update_department->Active = $request->has('Active')? 1 : 0;
        $update_department->save();
        Session::flash('flash_message', 'Unit of Measure details updated: '.$request->department.'.');
        return redirect('department');
    }

    public function approvergroup()
    {
        $test2 = DB::table('ApprovalRoles')
            ->leftJoin('ApprovalTemplateRequestors', 'dbo.ApprovalRoles.at_id','=','dbo.ApprovalTemplateRequestors.at_id')
            ->leftJoin('users', 'dbo.ApprovalTemplateRequestors.user_id','=','dbo.users.id')
            ->select('dbo.ApprovalRoles.id','dbo.ApprovalRoles.at_id','dbo.ApprovalRoles.as_id')
            ->where('ApprovalTemplateRequestors.active',1)
            ->groupBy('dbo.ApprovalRoles.id','dbo.ApprovalRoles.at_id','dbo.ApprovalRoles.as_id')
            ->get();

        $div = Division::where('active',1)->get();
        $supp = ApprovalStages::orderBy('created_at')->get();

        return view('setting.approvergroup', compact('div','supp','test2'));

    }
    public function addapproverstages(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
            'division' => 'required',
        ]);
        $superapprover = new Approvalstages();
        $superapprover->Name = $request->description;
        $superapprover->active = 1;
        $superapprover->Division_ID = $request->division;
        $superapprover->save();
        Session::flash('flash_message', 'Your password has been updated.');
        return redirect('approvergroup');
    }
    public function activate_approvalstage(Request $request)
    {
        $editApprovalGroup = Approvalstages::find($request->editID);
        $editApprovalGroup->Name = $request->editDescription;
        $editApprovalGroup->Division_ID = $request->editDivision;
        if(isset($request->editActive))
            $editApprovalGroup->Active = 1;
        else
            $editApprovalGroup->Active = 0;
        $editApprovalGroup->update();
        return redirect('approvergroup');
    }
    public function approvergroup_approvers($hasPending,$id)
    {
            $currentRequestors = DB::table('ApprovalTemplateRequestors')
                ->join('ApprovalRoles', 'dbo.ApprovalTemplateRequestors.AT_id','=','dbo.ApprovalRoles.AT_id')
                ->select('dbo.ApprovalTemplateRequestors.User_id')
                ->where('ApprovalRoles.active',1)
                ->where('ApprovalTemplateRequestors.active',1)
                ->where('dbo.ApprovalRoles.as_id',$id)
                ->groupBy('dbo.ApprovalTemplateRequestors.User_id')
                ->get();

            $all=0;
            $appSt = ApprovalStages::find($id);
            $apprvrs = ApprovalStageApprovers::where('AS_id',$id)->where('Active', 1)->get();

            $result = array();
            foreach($apprvrs as $ob)
            {
                array_push($result, $ob->User_id);
            }

            $allapprovers = Userrolesgroup::where('UserRoles_ID','=',3)->get();
            $result2 = array();
            foreach($allapprovers as $aa)
            {
                array_push($result2, $aa->Users_ID);
            }

            $theUsers = $appSt->allUsers()->whereNotIn('id',$result)->whereIn('id',$result2)->orderBy('LastName')->get();

            //dd($theUsers);

            return view ('setting.addapprovers', compact('appSt','apprvrs','theUsers','id','all','hasPending','currentRequestors'));
    }
    public function approvergroup_approvers_all($hasPending,$id)
    {
        $currentRequestors = DB::table('ApprovalTemplateRequestors')
            ->join('ApprovalRoles', 'dbo.ApprovalTemplateRequestors.AT_id','=','dbo.ApprovalRoles.AT_id')
            ->select('dbo.ApprovalTemplateRequestors.User_id')
            ->where('ApprovalRoles.active',1)
            ->where('ApprovalTemplateRequestors.active',1)
            ->where('dbo.ApprovalRoles.as_id',$id)
            ->groupBy('dbo.ApprovalTemplateRequestors.User_id')
            ->get();

        $all=1;
        $appSt = ApprovalStages::find($id);
        $apprvrs = ApprovalStageApprovers::where('AS_id',$id)->where('Active', 1)->get();

        $result = array();
        foreach($apprvrs as $ob)
        {
            array_push($result, $ob->User_id);
        }


        $allapprovers = Userrolesgroup::where('UserRoles_ID','=',3)->where('Active', 1)->get();

        $result2 = array();
        foreach($allapprovers as $aa)
        {
            array_push($result2, $aa->Users_ID);
        }
        $theApprovers = Userrolesgroup::select('Users_ID')->where('Active',1)->where('UserRoles_ID',3)->get();

        //$theUsers = $appSt->allUsers()->whereNotIn('id',$result)->whereIn('id',$result2)->orderBy('LastName')->get();
        $theUsers = User::whereIn('id',$theApprovers)->whereNotIn('id',$result)->orderBy('LastName')->get();
        //dd($theUsers);

        return view ('setting.addapprovers', compact('appSt','apprvrs','theUsers','id','all','hasPending','currentRequestors'));
    }
    public function addapprover($id,$user)
    {
        $addApprover = new ApprovalStageApprovers();
        $addApprover->AS_id = $id;
        $addApprover->User_id = $user;
        $addApprover->Active = 1;
        $addApprover->save();
        Session::flash('flash_message', 'Approver added.');
        return redirect('approvergroup_approvers/6e6f/'.$id);

    }
    public function removeapprover($id,$user)
{
$removeApprover = ApprovalStageApprovers::find($user);
$removeApprover->Active = 0;
$removeApprover->save();
Session::flash('flash_message', 'Approver removed.');
return redirect('approvergroup_approvers/6e6f/'.$id);
}
    public function approverEmailToggle($mayPending,$id1,$id){
        $toUpdate = ApprovalStageApprovers::find($id);
        if($toUpdate->Email==1)
            $toUpdate->Email = 0;
        else
            $toUpdate->Email = 1;
        $toUpdate->save();
        return redirect('approvergroup_approvers/'.$mayPending."/".$id1);
    }

    public function approvalTemplates(){
        $test2 = DB::table('ApprovalRoles')
            ->leftJoin('ApprovalTemplateRequestors', 'dbo.ApprovalRoles.AT_id','=','dbo.ApprovalTemplateRequestors.AT_id')
            ->leftJoin('Users', 'dbo.ApprovalTemplateRequestors.User_id','=','dbo.Users.id')
            ->leftJoin('iHeaders', 'dbo.Users.id','=','dbo.iHeaders.CreatedBy')
            ->select('dbo.ApprovalRoles.id','dbo.ApprovalRoles.AT_id','dbo.ApprovalRoles.AS_id')
            ->whereIn('iHeaders.Status',[2])
            ->where('ApprovalTemplateRequestors.Active',1)
            ->groupBy('dbo.ApprovalRoles.id','dbo.ApprovalRoles.AT_id','dbo.ApprovalRoles.AS_id','iHeaders.Status')
            ->get();

        $t3st = ApprovalStages::where('Active',1)->get();

        $t4st = ApprovalTemplates::
            leftJoin('dbo.ApprovalRoles','dbo.ApprovalTemplates.id','=','dbo.ApprovalRoles.AT_id')
            ->leftJoin('dbo.Division','dbo.ApprovalTemplates.Division_ID','=','dbo.Division.id')
            ->select(
                'dbo.ApprovalTemplates.id',
                'dbo.ApprovalTemplates.Name AS atname',
                'dbo.ApprovalTemplates.Active',
                'dbo.ApprovalRoles.id as ar_id',
                'dbo.ApprovalRoles.AT_id as at_id',
                'dbo.ApprovalRoles.AS_id as as_id',
                'dbo.Division.id as divid',
                'dbo.Division.Name as diviname'
                )
            ->where('dbo.ApprovalRoles.Active',1)
            ->orderBy('dbo.ApprovalTemplates.created_at')
            ->get();
            //dd($t4st);

            $users = Division::where('Active',1)
                ->get();
            $supp = ApprovalStageApprovers::where('Active',1)
                ->get();
            $obmainlog = ApprovalTemplateRequestors::where('Active', 1)->get();

            $result = array();
            foreach($obmainlog as $ob)
            {
                array_push($result, $ob->User_id);
            }

            $allrequestors = Userrolesgroup::where('UserRoles_ID','=',2)->get();
            $result1 = array();
            foreach($allrequestors as $rr)
            {
                array_push($result1, $rr->Users_ID);
            }
            $allapprovers = Userrolesgroup::where('UserRoles_ID','=',3)->get();
            $result2 = array();
            foreach($allapprovers as $aa)
            {
                array_push($result2, $aa->Users_ID);
            }
            $theUsers = User::where('Active',1)->whereIn('id',$result1)->whereNotIn('id',$result)->get();
            $theApprovers = User::where('Active',1)->whereIn('id',$result2)->orderBy('LastName')->get();

            return view('setting.approvaltemplates', compact('users','test2','t3st','t4st','theUsers','supp','theApprovers'));
    }
    public function addApprovalTemplates(Request $request)
    {
        //$tt = Approvalstages::orderBy('as_id','desc')->first();
        // dd($tt->as_id);
        //dd("Save Continues Here.");
        //dd($request->approvalstages);
        //dd($request->approvalstages);
        if($request->approvalstages==null) {
            $this->validate($request, [
                'description' => 'required',
                'department' => 'required',
                'description3' => 'required',
                'emp' => 'required',
                'department3' => 'required',
                'appr' => 'required'
            ]);
        }
        else{

            $this->validate($request, [
                'description' => 'required',
                'department' => 'required',
                'emp' => 'required',
                'approvalstages' => 'required',
            ]);

            $theApprovers = Approvalstages::where('id', $request->approvalstages)->first()->toApprovers()->where('active', 1)->get();
            $match=0;
            $list = '';
            for ($i = 0; $i < count($request->emp); $i++) {
                foreach($theApprovers as $theAppz) {
                    if ($request->emp[$i] == $theAppz->User_id) {
                        $match = $match + 1;
                        $list = $list . $theAppz->toUsers->FirstName . ' ' . $theAppz->toUsers->Lastname . ', ';
                    }
                }
            }
            if ($match!=0) {
                Session::flash('fail_apps', 'The following requestors are assigned as approvers in the Approver Group: '.$list);
                Session::flash('theDesc', $request->description);
                Session::flash('theEmp', $request->emp);
                Session::flash('theDept', $request->department);
                Session::flash('theAppz', $request->approvalstages);
                return redirect('approvaltemplates');
            }
        }

        $requestorg = new Approvaltemplates();
        //$requestorg->name = $request->name;
        $requestorg->Name = $request->description;
        $requestorg->Active =1;
        $requestorg->Division_ID = $request->department;
        $requestorg->save();

        if($request->approvalstages==null) {
            $dbcount = Approvalstages::where('Division_ID', $request->department3)->count();
            $dbcount = $dbcount + 1;

            $approvalg = new Approvalstages();
            $approvalg->Name = $request->description3;
            $approvalg->Active =1;
            $approvalg->Division_ID = $request->department3;
            //$approvalg->sequence = $dbcount;
            $approvalg->save();

            $thisIsRole = new Approvalroles();
            $thisIsRole->AS_id = Approvalstages::orderBy('id', 'desc')->first()->id;
            $thisIsRole->AT_id = Approvaltemplates::orderBy('id', 'desc')->first()->id;
            //$thisIsRole->description = 'Not Here';
            $thisIsRole->Active =1;

            $thisIsLatest = Approvalstages::orderBy('id','desc')->first();
            for ($i = 0; $i < count($request->appr); $i++) {
                $approvrz = new Approvalstageapprovers();
                $approvrz->AS_id = $thisIsLatest->id;
                $approvrz->User_id = $request->appr[$i];
                $approvrz->Active = 1;
                //$approvrz->email_sending = 1;
                $approvrz->save();
            }

        }
        else{
            $thisIsRole = new Approvalroles();
            $thisIsRole->AS_id = $request->approvalstages;
            $thisIsRole->AT_id = Approvaltemplates::orderBy('id', 'desc')->first()->id;
            //$thisIsRole->description = 'Not Here';
            $thisIsRole->Active =1;
        }

        if(count($request->emp)!=0) {
            for ($i = 0; $i < count($request->emp); $i++) {
                $rqstrnw = new Approvaltemplaterequestors();
                $rqstrnw->AT_id = Approvaltemplates::orderBy('id', 'desc')->first()->id;
                $rqstrnw->User_id = $request->emp[$i];
                $rqstrnw->Active = 1;
                $rqstrnw->save();
            }
        }

        $namenew = Approvaltemplates::orderBy('id', 'desc')->first()->Name;
        Session::flash('succ_message', 'New requestor group "'.$namenew.'" Created.');
        $thisIsRole->save();

        //dd(Approvaltemplates::orderBy('at_id', 'desc')->first());
        //Session::flash('flash_message', 'Requestor Group created.');

        return redirect('approvaltemplates');
    }
    public function approvaltemplate_requestors($hasPending,$id)
    {

        $currentApprovers = DB::table('ApprovalStageApprovers')
            ->join('ApprovalRoles', 'dbo.ApprovalStageApprovers.AS_id','=','dbo.ApprovalRoles.AS_id')
            ->select('dbo.ApprovalStageApprovers.User_id')
            ->where('ApprovalRoles.Active',1)
            ->where('ApprovalStageApprovers.Active',1)
            ->where('dbo.ApprovalRoles.AT_id',$id)
            ->groupBy('dbo.ApprovalStageApprovers.user_id')
            ->get();

        $all=0;

        $appTe = ApprovalTemplates::find($id);
        $requestrs = ApprovalTemplateRequestors::where('AT_id',$id)->where('Active', 1)->get();

        $sortUsers = ApprovalTemplateRequestors::where('Active', 1)->get();
        $result = array();

        foreach($sortUsers as $ob)
        {
            array_push($result, $ob->User_id);
        }

        $allRequestors = Userrolesgroup::where('UserRoles_ID','=',2)->get();
        $result2 = array();
        foreach($allRequestors as $aa)
        {
            array_push($result2, $aa->Users_ID);
        }

        //$theUsers = $appTe->allUsers()->whereNotIn('id',$result)->where('requestor',1)->orderBy('LastName')->get();
        $theUsers = $appTe->allUsers()->whereNotIn('id',$result)->whereIn('id',$result2)->orderBy('LastName')->get();
        //dd($theUsers);
        return view ('setting.addrequestors', compact('appTe','requestrs','theUsers','id','all','hasPending','currentApprovers'));
    }
    public function update_approvaltemplate(Request $request){

        $updateTemplate = ApprovalTemplates::where('id','=',$request->editID)->first();
        $updateTemplate->Name = $request->editDescription;
        $updateTemplate->Division_ID = $request->editDivision;
        if(isset($request->editActive))
            $updateTemplate->Active = 1;
        else
            $updateTemplate->Active = 0;
        $updateTemplate->save();
        return redirect('approvaltemplates');
    }
    public function addrequestor($id,$user)
    {
        $addApprover = new ApprovalTemplateRequestors();
        $addApprover->AT_id = $id;
        $addApprover->User_id = $user;
        $addApprover->Active = 1;
        $addApprover->save();
        Session::flash('flash_message', 'Requestor added.');
        return redirect('approvaltemplate_requestors/6e6f/'.$id);

    }
    public function removerequestor($id,$user)
    {
        $removeApprover = ApprovalTemplateRequestors::find($user);
        $removeApprover->Active = 0;
        $removeApprover->save();
        Session::flash('flash_message', 'Requestor removed.');
        return redirect('approvaltemplate_requestors/6e6f/'.$id);
    }
    public function toggleUser(Request $request)
    {
        $users = User::find($request->toggleUserAccID);
        $state = "";
        if($users->Active == 0) {
            $users->Active = 1;
            $state = "activated";
        }
        else {
            $users->Active = 0;
            $state = "deactivated";
        }
        $users->save();
        Session::flash('flash_message',$users->FirstName.' '.$users->LastName.'\'s user access has been '.$state.'.');
        return redirect('users');
    }
}

