<?php

namespace App\Http\Controllers;

use Anouar\Fpdf\Fpdf;
use App\ApprovalRoles;
use App\ApprovalStageApprovers;
use App\ApprovalStages;
use App\ApprovalTemplateRequestors;
use App\Destinations;
use App\Division;
use App\iHeaderStatus;
use App\iLineItemImages;
use App\iLineItemStorage;
use App\iRequestType;
use App\iStatusLogs;
use App\oHeaders;
use App\oHeaderStatus;
use App\oLineItems;
use App\oRequestType;
use App\oStatusLogs;
use App\Userrolesgroup;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\User;
use Illuminate\Http\Request;
use App\BusinessPartners;
use App\Employees;
use App\LineStatus;
use App\UOMs;
use App\iHeaders;
use App\iLineItems;
use Auth;
use App\SeriesControlNo;
use App\SeriesSerialNo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Milon\Barcode\DNS1D;
use Session;
use Mail;
use App\Photo;

class TransactionController extends Controller
{
    //Series - Control Number - 09/25/2017
    public function getControlNumberSeries($rq){
        $series = SeriesControlNo::where('Request', '=', $rq)->where('Year', '=', date("y"))->where('Active', '=',1)->first();
        if($series==NULL) {
            $controlNo = NULL;
        }
        else {
            $padding = "00000";
            $controlNo = $series->Request.$series->Year.substr($padding, 0, strlen($padding) - strlen($series->LastSeries)).$series->LastSeries;
        }
        return $controlNo;
    }
    public function createControlNumberSeries($rq){
        SeriesControlNo::where('Request', '=', $rq)->where('Active', '=',1)->update(['Active' => 0]);

        $controlNumberSeries = new SeriesControlNo();
        $controlNumberSeries->Request = $rq;
        $controlNumberSeries->Year = date("y");
        $controlNumberSeries->LastSeries = 0;
        $controlNumberSeries->Active = 1;
        $controlNumberSeries->save();
    }
    public function updateControlNumberSeries($rq){
        $next = SeriesControlNo::where('Request', '=', $rq)->where('Year', '=', date("y"))->where('Active', '=',1)->first()->LastSeries + 1;
        SeriesControlNo::where('Request', '=', $rq)->where('Year', '=', date("y"))->where('Active', '=',1)->update(['LastSeries' => $next]);
    }
    //Series - Control Number - END - 09/25/2017

    //Series - Serial Number - 09/25/2017
    public function getSerialNumberSeries(){
        //$series = SeriesSerialNo::where('Year', '=', date("Y"))->where('WorkWeek', '=',date('W'))->where('Active', '=',1)->first();
        $series = SeriesSerialNo::where('Year', '=', date("Y"))->where('Month', '=',date('m'))->where('Active', '=',1)->first();
        if($series==NULL) {
            $serialNo = NULL;
        }
        else {
            $wwPadding = "00";
            $padding = "00000";
            $serialNo = $series->Year.substr($padding, 0, strlen($wwPadding) - strlen($series->Month)).$series->Month.substr($padding, 0, strlen($padding) - strlen($series->LastSeries)).$series->LastSeries;
        }
        return $serialNo;
    }
    public function createSerialNumberSeries(){
        SeriesSerialNo::where('Active', '=',1)->update(['Active' => 0]);
        $controlNumberSeries = new SeriesSerialNo();
        $controlNumberSeries->Year = date("Y");
        $controlNumberSeries->Month = date('m');
        $controlNumberSeries->LastSeries = 0;
        $controlNumberSeries->Active = 1;
        $controlNumberSeries->save();
    }
    public function updateSerialNumberSeries(){
        $next = SeriesSerialNo::where('Year', '=', date("Y"))->where('Month', '=',date('m'))->where('Active', '=',1)->first()->LastSeries + 1;
        SeriesSerialNo::where('Year', '=', date("Y"))->where('Month', '=',date('m'))->where('Active', '=',1)->update(['LastSeries' => $next]);
    }
    //Series - Serial Number - END - 09/25/2017

    //This Function will be called when checking your User Roles 09/27/2017.
    //USE THE ID FROM UserRoles AS YOUR PARAMETERS.
    public static function knowYourRole($role){
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $yourRole = False;
        foreach($UserRole as $uR){
            if($uR->UserRoles_ID == $role)
                $yourRole = True;
        }
        return $yourRole;
    }
    // END

    //This is a Dashboard thing for counting the requests.
    public static function countTheReqs($status){
        $myRequests = iHeaders::where('CreatedBy','=',Auth::user()->id)->where('Status','=',$status)->count();
        return $myRequests;
    }
    public static function countAllReqs($status){
        $myRequests = iHeaders::where('Status','=',$status)->count();
        return $myRequests;
    }
    //END

    public function dboard(){
        $userCount = 0;
        if(self::knowYourRole(1)) {
            $userCount = User::where('Active', '=', True)->count();
        }
        //Requestor View
        if(self::knowYourRole(2)) {
            $requestor = self::knowYourRole(2);
            $myRequests = iHeaders::where('CreatedBy', '=', Auth::user()->id)->get();
            $myRequests1 = array();
            foreach ($myRequests as $lR) {
                array_push($myRequests1, $lR->id);
            }
            $myCILineItems = iLineItems::whereNotNull('InDate')
                ->where('Deleted', '=', 0)
                ->whereIn('h_ID', $myRequests1)
                ->whereNull('OutDate')
                ->where('Returnable', '=', 1)
                ->orderBy('h_ID', 'asc')
                ->orderBy('LineNum', 'asc')
                ->get();
            $myCOLineItems = iLineItems::whereNotNull('OutDate')->where('Deleted', '=', 0)->whereIn('h_ID', $myRequests1)->where('Returnable', '=', 1)->get();
            $rStatus = iHeaderStatus::all();
            $rOStatus = oHeaderStatus::all();
        }

        //Approver View
        $ICIReqsForApproval = 0;
        $ICIReqsApproved = 0;
        $ICIReqsRejected = 0;
        $ICOReqsForApproval = 0;
        $ICOReqsApproved = 0;

        $OCOReqsForApproval = 0;
        $OCOReqsApproved = 0;
        $OCOReqsRejected = 0;
        $OCIReqsForApproval = 0;
        $OCIReqsApproved = 0;
        if(self::knowYourRole(3)) {
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }

                    $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->get();
                    $myRequests1 = array();
                    foreach ($myRequests as $lR) {
                        array_push($myRequests1, $lR->id);
                    }

                    $myCILineItems = iLineItems::whereNotNull('InDate')
                        ->where('Deleted', '=', 0)
                        ->whereIn('h_ID', $myRequests1)
                        ->whereNull('OutDate')
                        ->where('Returnable', '=', 1)
                        ->orderBy('h_ID', 'asc')
                        ->orderBy('LineNum', 'asc')
                        ->get();
                    $myCOLineItems = iLineItems::whereNotNull('OutDate')->where('Deleted', '=', 0)->whereIn('h_ID', $myRequests1)->where('Returnable', '=', 1)->get();

                    $ICIReqsForApproval = iHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',2)->count();
                    $ICIReqsApproved = iHeaders::where('ApprovedBy','=',Auth::user()->id)->count();
                    $ICIReqsRejected = iHeaders::where('RejectedBy','=',Auth::user()->id)->count();
                    $ICOReqsForApproval = iHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',6)->count();
                    $ICOReqsApproved = iHeaders::where('COApprovedBy','=',Auth::user()->id)->count();

                    $OCOReqsForApproval = oHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',2)->count();
                    $OCOReqsApproved = oHeaders::where('COApprovedBy','=',Auth::user()->id)->count();
                    $OCOReqsRejected = oHeaders::where('CORejectedBy','=',Auth::user()->id)->count();
                    $OCIReqsForApproval = oHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',6)->count();
                    $OCIReqsApproved = oHeaders::where('ApprovedBy','=',Auth::user()->id)->count();

                }
            }
        }
        if(self::knowYourRole(5)) {
            $ICIReqsForApproval = iHeaders::where('Status','=',4)->count();
            $ICOReqsForApproval = iHeaders::where('Status','=',8)->count();
            $OCOReqsForApproval = oHeaders::where('Status','=',4)->count();
            $OCIReqsForApproval = oHeaders::where('Status','=',8)->count();
        }
        if(self::knowYourRole(7)) {
            $OCOReqsForApproval = oHeaders::where('Status','=',3)->count();
            $OCOReqsApproved = oHeaders::where('COAccountingApprovedBy','=',Auth::user()->id)->count();
            $OCOReqsRejected = oHeaders::where('CORejectedBy','=',Auth::user()->id)->whereNotNull('COApprovedBy')->count();
            $OCIReqsForApproval = oHeaders::where('Status','=',7)->count();
            $OCIReqsApproved = oHeaders::where('AccountingApprovedBy','=',Auth::user()->id)->count();

            $myCILineItems = iLineItems::whereNotNull('InDate')->where('Deleted', '=', 0)->whereNull('OutDate')->where('Returnable', '=', 1)->get();
            $myCOLineItems = iLineItems::whereNotNull('OutDate')->where('Deleted', '=', 0)->where('Returnable', '=', 1)->get();
        }

        return view('transactions.dashboard',
            compact('userCount',
                'ICIReqsForApproval','ICIReqsApproved','ICOReqsForApproval','ICOReqsApproved','ICIReqsRejected',
                'OCOReqsForApproval','OCOReqsApproved','OCIReqsForApproval','OCIReqsApproved','OCOReqsRejected',
                'rStatus','rOStatus','myCILineItems','myCOLineItems'));
    }

    // Requestor Views
    public function inTransactView()
    {
        if(self::knowYourRole(2)){
            $checkApprovalTemplate = ApprovalTemplateRequestors::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            //Check if Approval Role is Applicable for Incoming Requests
            /*//dd(ApprovalRoles::where('AT_id','=',$checkApprovalTemplate->toApprovaltemplates->id)
                ->where('Incoming','=',True)
                ->where('Active','=',True)
                ->get());*/

            if($checkApprovalTemplate!=null) {
                /*if($checkApprovalTemplate->toApprovaltemplates->Incoming==0){
                    $title = 'Access Denied';
                    $message = 'You are not added to an Approval Template for Incoming Requests. Please contact an Administrator';
                    $restriction = 1;
                    return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
                }else {*/
                    $descAutoComp = iLineItems::distinct()->get(['Description']);

                    $user_det = User::where('username', '=', Auth::user()->id)->first();
                    $bus_part = BusinessPartners::all();
                    $linestat = LineStatus::all();
                    $contacts = iHeaders::select('Contact')->distinct()->get();
                    $uoms = UOMs::all();
                    $reqtype = iRequestType::all();
                    $generaldestinations = Destinations::where('Active','=',TRUE)->where('SubLocation','=',FALSE)->get();
                    $specificdestinations = Destinations::where('Active','=',TRUE)->where('SubLocation','=',TRUE)->get();
                    //dd($reqtype);
                    return view('transactions_requestor.in_new_request', compact('user_det', 'bus_part', 'contacts', 'linestat', 'uoms', 'descAutoComp', 'reqtype','generaldestinations','specificdestinations'));
                //}
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not added to an Approval Template for Incoming Requests. Please contact an Administrator';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function inTransactSubmit(Request $request){
        //dd($request);
        if(isset($request->saveRequest)){
            $status = 1;
        }
        elseif(isset($request->submitRequest)){
            $status = 2;
        }

        if(isset($request->id)){
            //EDITING EXISTING REQUEST
            $iHeaderUpdate = iHeaders::find($request->id);
            $iHeaderUpdate->BusinessPartner = $request->BusinessPartner;
            if($request->BusinessPartner == 3)
                $iHeaderUpdate->BusinessPartnerName = "UGEC";
            elseif($request->BusinessPartner == 4)
                $iHeaderUpdate->BusinessPartnerName = "APO";
            else
                $iHeaderUpdate->BusinessPartnerName = $request->BusinessPartnerName;
            $iHeaderUpdate->Contact = $request->Contact;
            $iHeaderUpdate->ContactEmail = $request->ContactEmail;
            $iHeaderUpdate->RequestType = $request->RequestType;
            $iHeaderUpdate->Project = $request->Project;
            $iHeaderUpdate->Date = $request->Date;
            $iHeaderUpdate->Status = $status;
            if ($status == 2) {
                $iHeaderUpdate->Notify = 1;
            }
            $iHeaderUpdate->save();

            $AssetNumber = $iHeaderUpdate->RequestNo;

            $log = new iStatusLogs();
            $log->iHeaders_id = $request->id;
            $log->RequestNo = $AssetNumber;
            $log->Users_id = Auth::user()->id;
            $log->Status = $status;
            $log->save();

            //Save changes for existing Line Items
            for ($i = 1; $i <= $request->savedAssetCount; $i++) {
                if (isset($request->savedID[$i])) {
                    $iLineItemUpdate = iLineItems::find($request->savedID[$i]);
                    $iLineItemUpdate->Description = $request->savedDescription[$i];
                    //$iLineItemUpdate->SerialNum = $request->savedSerialNum[$i];
                    $iLineItemUpdate->Quantity = $request->savedQuantity[$i];
                    $iLineItemUpdate->Destination = $request->savedDestination[$i];
                    $iLineItemUpdate->UOM = $request->savedUOM[$i];
                    if (isset($request->savedReturnable[$i])) {
                        if ($request->savedReturnable[$i] == "on")
                            $iLineItemUpdate->Returnable = 1;
/*                        else
                            $iLineItemUpdate->Returnable = 0;*/
                    }
                    else
                        $iLineItemUpdate->Returnable = 0;

                    $iLineItemUpdate->Deleted = $request->savedDeleted[$i];
                    $iLineItemUpdate->save();

                    $storageID = iLineItemStorage::where('l_ID','=',$request->savedID[$i])->first()->id;
                    $iLineItemStorageUpdate = iLineItemStorage::find($storageID);
                    $iLineItemStorageUpdate->Storage = $request->savedStorage[$i];
                    $iLineItemStorageUpdate->save();
                }
            }
            // END - Save changes for existing Line Items
            if ($request->assetCount!=0) {
                //$lineCounter = $request->realAssetCount;
                $lineCounter = iLineItems::where('h_ID','=',$request->id)->orderBy('LineNum','DESC')->first()->LineNum + 1;
                for ($i = 0; $i < $request->assetCount; $i++) {
                    if (isset($request->Description[$i])) {

                        $iline = new iLineItems();
                        $iline->h_ID = $request->id;
                        $iline->LineNum = $lineCounter;
                        $iline->LineStatus = 1;

                        $iline->Description = $request->Description[$i];
                        $iline->Destination = $request->Destinations[$i];
                        if(self::getSerialNumberSeries()==null) {
                            self::createSerialNumberSeries();
                        }
                        $TempSerialNumber = self::getSerialNumberSeries();
                        self::updateSerialNumberSeries();

                        //$iline->SerialNum = $request->SerialNum[$i];
                        $iline->ControlNum = $TempSerialNumber;
                        $iline->Quantity = $request->Quantity[$i];
                        $iline->UOM = $request->UOM[$i];
                        if (isset($request->Returnable[$i])) {
                            if ($request->Returnable[$i] == "on")
                                $iline->Returnable = 1;
                            else
                                $iline->Returnable = 0;
                        } else {
                            $iline->Returnable = 0;
                        }
                        $iline->ItemType = $request->ItemType[$i];
                        $iline->save();

                        //Saving Storage
                            //$lastLineItem = iLineItems::orderBy('created_at', 'desc')->first()->id;
                        //$lastLineItem = iLineItems::orderBy('created_at', 'desc')->first()->id;
                        $lastLineItem = iLineItems::where('ControlNum', '=',$TempSerialNumber)->first()->id;
                            $istore = new iLineItemStorage();
                            $istore->l_ID = $lastLineItem;
                            $istore->Storage = $request->Storage[$i];
                            $istore->save();
                        //Saving Storage - End

                        $lineCounter = $lineCounter + 1;
                    }
                }
            }
            $title = 'Confirmation';
            $message = 'Changes to Request Number ' . $iHeaderUpdate->RequestNo . ' has been saved.';
        }
        else {
            //NEW REQUEST
            if(self::getControlNumberSeries("I")==null) {
                self::createControlNumberSeries("I");
            }
            $AssetNumber = self::getControlNumberSeries("I");
            self::updateControlNumberSeries("I");

            $iheader = new iHeaders();
            $iheader->RequestNo = $AssetNumber;
            $iheader->BusinessPartner = $request->BusinessPartner;
            $iheader->Project = $request->Project;
            if($request->BusinessPartner == 3)
                $iheader->BusinessPartnerName = "United Graphic Expression Corporation";
            elseif($request->BusinessPartner == 4)
                $iheader->BusinessPartnerName = "APO";
            else
                $iheader->BusinessPartnerName = $request->BusinessPartnerName;
            //$iheader->BusinessPartnerName = $request->BusinessPartnerName;
            $iheader->Contact = $request->Contact;
            $iheader->ContactEmail = $request->ContactEmail;
            $iheader->Date = $request->Date;
            $iheader->RequestType = $request->RequestType;
            $iheader->CreatedBy = Auth::user()->id;
            $iheader->Status = $status;
            if ($status == 2) {
                $iheader->Notify = 1;
            }
            $iheader->save();

            $lastSave = iHeaders::orderBy('created_at', 'desc')->first()->id;

            $log = new iStatusLogs();
            $log->iHeaders_id = $lastSave;
            $log->RequestNo = $AssetNumber;
            $log->Users_id = Auth::user()->id;
            $log->Status = $status;
            $log->save();

            $lineCounter = 1;

            for ($i = 0; $i < $request->assetCount; $i++) {
                if (isset($request->Description[$i])) {

                    $iline = new iLineItems();
                    $iline->h_ID = $lastSave;
                    $iline->LineNum = $lineCounter;
                    $iline->LineStatus = 1;
                    $iline->Quantity = $request->Quantity[$i];
                    $iline->Destination = $request->Destinations[$i];
                    $iline->Description = $request->Description[$i];
                    //$iline->Destination = $request->Destination[$i];

                    if(self::getSerialNumberSeries()==null) {
                        self::createSerialNumberSeries();
                    }
                    $TempSerialNumber = self::getSerialNumberSeries();
                    self::updateSerialNumberSeries();

                    //$iline->SerialNum = $request->SerialNum[$i];
                    $iline->ControlNum = $TempSerialNumber;
                    $iline->UOM = $request->UOM[$i];
                    if(isset($request->Returnable[$i])) {
                        if ($request->Returnable[$i] == "on")
                            $iline->Returnable = 1;
                        else
                            $iline->Returnable = 0;
                    }
                    else{
                        $iline->Returnable = 0;
                    }
                    $iline->ItemType = $request->ItemType[$i];
                    $iline->save();

                    //Saving Storage
                        //$lastLineItem = iLineItems::orderBy('created_at', 'desc')->first()->id;
                        $lastLineItem = iLineItems::where('ControlNum', '=',$TempSerialNumber)->first()->id;
                        $istore = new iLineItemStorage();
                        $istore->l_ID = $lastLineItem;
                        $istore->Storage = $request->Storage[$i];
                        $istore->save();
                    //Saving Storage - End

                    $lineCounter = $lineCounter + 1;
                }
            }
            $title = 'Confirmation';
            $message = 'Request Number ' . $AssetNumber . ' has been created.';
        }

        if ($status == 2) {
            //Mail confirmation to Approver/s - START
            $getAT = ApprovalTemplateRequestors::where('User_id', '=', Auth::user()->id)->where('Active','=',1)->first();
            $getAR = ApprovalRoles::where('AT_id', '=', $getAT->AT_id)->first();
            $getApprovers = ApprovalStageApprovers::where('AS_id', '=', $getAR->AS_id)->where('Email','=',1)->get();

            $approvers = array();
            foreach ($getApprovers as $gA)
                array_push($approvers, $gA->User_id);

            $getApproverEmails = User::whereIn('id', $approvers)
                ->distinct()
                ->get();

            $singleRequest = iHeaders::where('RequestNo', '=', $AssetNumber)->first();
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach ($getApproverEmails as $email) {
                    $user_email = $email['Email'];
                    if ($user_email != null) {
                        Mail::send('email.in_requestor_message',
                            [
                                'recepient' => 'approver',
                                'singleRequest' => $singleRequest,
                                'lineItems' => $lineItems,
                                'email' => $email['Email'],
                                'FirstName' => $email['FirstName'],
                                'approverID' => $email['id'],
                                'state' => 'approval'
                            ],
                            function ($message) use ($email) {
                                //attachment example
                                //$message->attachData('AAAA', 'invoice.txt');
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("New Incoming Request for Approval");
                                $message->to($email['Email']);
                            });
                    }
                }
            //Mail confirmation to Approver/s - END
            $title = 'Confirmation';
            $message = 'Request Number ' . $AssetNumber . ' has been submitted for approval.';
        }
        return view('transactions_requestor.in_m_requestor', compact('title', 'message'));
    }
    public function inView(Request $request){
        if(self::knowYourRole(2)) {
            $myRequests = iHeaders::where('CreatedBy', '=', Auth::user()->id)->get();
            $status = iHeaderStatus::all();
            return view('transactions_requestor.in_v_all', compact('myRequests','statusName','status'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }

    public function inViewSingle($id)
    {
        if(self::knowYourRole(2)) {
            $singleRequest = iHeaders::where('id', '=', $id)->first();
            $coVersion = iLineItems::where('h_ID', '=', $singleRequest->id)->select('checkout_ID')->where('Deleted', '=', False)->distinct()->get();
            $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkout_ID;
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();

            $retCount = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('Returnable','=',True)->where('ItemType','=',0)->count();
            $nonRetCount = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('Returnable','=',False)->count();
            $equipCount = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('Returnable','=',True)->where('ItemType','=',1)->count();

            //$lineItems = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->get();
            $alreadyCheckedOut = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('OutDate','<>','')->count();

            if (Auth::user()->id == $singleRequest->CreatedBy)
                return view('transactions_requestor.in_v_single', compact('singleRequest', 'lineItems','alreadyCheckedOut','coVersion','retCount','nonRetCount','equipCount'));
            else {
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function inViewCOLogs($id,$co)
    {
        if(self::knowYourRole(2)) {
            $singleRequest = iHeaders::where('id', '=', $id)->first();
            //$lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
            $coVersion = iLineItems::where('h_ID', '=', $singleRequest->id)->select('checkout_ID')->where('Deleted', '=', False)->distinct()->get();
            $checkOutID = $co;
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
            $alreadyCheckedOut = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('OutDate','<>','')->count();

            if (Auth::user()->id == $singleRequest->CreatedBy) {
                if($lineItems->count()!=0)
                    return view('transactions_requestor.in_v_single', compact('singleRequest', 'lineItems', 'alreadyCheckedOut','coVersion'));
                else{
                    $title = 'Access Denied';
                    $message = 'This version does not exist';
                    $restriction = 1;
                    return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
                }
            }
            else {
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }

    public function inTransactViewSingle($id)
    {
        $singleRequest = iHeaders::where('id','=',$id)->first();
        $lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        //$lineItemsCount = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->count();
        $lineItemsCount = iLineItems::where('h_ID','=',$id)->max('LineNum');
        if(Auth::user()->id == $singleRequest->CreatedBy && $singleRequest->oStatus->Name == "Saved") {
            $descAutoComp = iLineItems::distinct()->get(['Description']);
            $user_det = User::where('username' , '=', Auth::user()->id)->first();
            $bus_part =BusinessPartners::all();
            $contacts =Employees::all();
            $linestat =LineStatus::all();
            $reqtype = iRequestType::all();
            $uoms =UOMs::all();
            $generaldestinations = Destinations::where('Active','=',TRUE)->where('SubLocation','=',FALSE)->get();
            $specificdestinations = Destinations::where('Active','=',TRUE)->where('SubLocation','=',TRUE)->get();
            return view('transactions_requestor.in_new_request',compact('user_det','bus_part','contacts','linestat','uoms','singleRequest', 'lineItems','lineItemsCount','descAutoComp','reqtype','generaldestinations','specificdestinations'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            return view('transactions.in_m_general',compact('title','message'));
        }
    }
    public function inMyRequests($id){
        if(self::knowYourRole(2) && $id<=11 && $id>=1) {
            $statusName = iHeaderStatus::where('id','=',$id)->first();
            $myRequests = iHeaders::where('CreatedBy', '=', Auth::user()->id)->where('Status','=',$id)->get();
            $status = iHeaderStatus::all();
            return view('transactions_requestor.in_v_all', compact('myRequests','statusName','status'));
        }
        elseif($id>=12 || $id<=0){
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            return view('transactions_requestor.in_m_requestor',compact('title','message'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            return view('transactions.in_m_general',compact('title','message'));
        }
    }
    public function inCheckoutRequest(Request $request){

        $iHeaderUpdate = iHeaders::find($request->reqID);
        //Check again if Request is still in pending
        if ($iHeaderUpdate->Status == 5 ||$iHeaderUpdate->Status == 7) {
            $iHeaderUpdate->Status = 6;
            $iHeaderUpdate->CORejectedBy = null;
            $iHeaderUpdate->CORejectReason = null;
            //$iHeaderUpdate->ApprovedBy = Auth::user()->id;
            $iHeaderUpdate->save();

            $lineItemsToInclude = iLineItems::where('h_ID','=',$request->reqID)->get();

            $lineItemsCO = iLineItems::where('h_ID','=',$request->reqID)->orderBy('checkout_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkout_ID + 1;

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->checkout_ID = $checkOutID;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            foreach($lineItemsToInclude as $li){
                if (isset($request->Include[$li->LineNum])){
                    $lineItemUpdate = iLineItems::where('id','=',$li->id)->first();
                    $lineItemUpdate->checkout_ID = $checkOutID;
                    $lineItemUpdate->CheckOut = 1;
                    $lineItemUpdate->update();
                }
            }

            $title = 'Confirmation';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' has been submitted for check-out.';


            //Mail confirmation to Approver/s - START
            $getAT = ApprovalTemplateRequestors::where('User_id','=',Auth::user()->id)->first();
            $getAR = ApprovalRoles::where('AT_id','=',$getAT->AT_id)->first();
            $getApprovers = ApprovalStageApprovers::where('AS_id','=',$getAR->AS_id)->where('Email','=',1)->get();

            $approvers = array();
            foreach($getApprovers as $gA)
                array_push($approvers, $gA->User_id);

            $getApproverEmails = User::whereIn('id',$approvers)
                ->distinct()
                ->get();

            $singleRequest = iHeaders::where('id', '=', $request->reqID)->first();
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.in_requestor_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'state'=>'checkout'
                        ],
                        function ($message) use ($email) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("A request has been submitted for Check-Out");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to submit request for check out.';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_requestor.in_m_requestor', compact('title', 'message'));
    }
    public function inCheckoutViewSingle($id)
    {
        $singleRequest = iHeaders::where('id','=',$id)->first();
        $lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $lineItemsCount = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->count();
        if(Auth::user()->id == $singleRequest->CreatedBy && ($singleRequest->Status == 5 || $singleRequest->Status == 7)) {
            $descAutoComp = iLineItems::distinct()->get(['Description']);
            $user_det = User::where('username' , '=', Auth::user()->id)->first();
            $bus_part =BusinessPartners::all();
            $contacts =Employees::all();
            $linestat =LineStatus::all();
            $uoms =UOMs::all();
            return view('transactions_requestor.in_co_request',compact('user_det','bus_part','contacts','linestat','uoms','singleRequest', 'lineItems','lineItemsCount','descAutoComp'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            return view('transactions.in_m_general',compact('title','message'));
        }
    }

    public function outTransactView()
    {
        if(self::knowYourRole(0)){
            $checkApprovalTemplate = ApprovalTemplateRequestors::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();

            if($checkApprovalTemplate!=null) {
                $descAutoComp = oLineItems::distinct()->get(['Description']);
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $linestat = LineStatus::all();
                $contacts = oHeaders::select('Contact')->distinct()->get();
                $uoms = UOMs::all();
                $reqtype = oRequestType::all();
                return view('transactions_requestor.out_new_request', compact('user_det', 'bus_part', 'contacts', 'linestat', 'uoms', 'descAutoComp', 'reqtype'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not added to an Approval Template for Incoming Requests. Please contact an Administrator';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function outTransactSubmit(Request $request){
        if(isset($request->saveRequest)){
            $status = 1;
        }
        elseif(isset($request->submitRequest)){
            $status = 2;
        }

        if(isset($request->id)){
            //EDITING EXISTING REQUEST
            $oHeaderUpdate = oHeaders::find($request->id);
            //dd($oHeaderUpdate);
            $oHeaderUpdate->BusinessPartner = $request->BusinessPartner;
            if($request->BusinessPartner == 3)
                $oHeaderUpdate->BusinessPartnerName = "UGEC";
            elseif($request->BusinessPartner == 4)
                $oHeaderUpdate->BusinessPartnerName = "APO";
            else
                $oHeaderUpdate->BusinessPartnerName = $request->BusinessPartnerName;
            $oHeaderUpdate->Contact = $request->Contact;
            $oHeaderUpdate->ContactEmail = $request->ContactEmail;
            $oHeaderUpdate->RequestType = $request->RequestType;
            $oHeaderUpdate->Destination = $request->Destination;
            $oHeaderUpdate->Reason = $request->Reason;
            $oHeaderUpdate->Transporter = $request->Transporter;
            $oHeaderUpdate->Date = $request->Date;
            $oHeaderUpdate->Status = $status;
            if ($status == 2) {
                $oHeaderUpdate->Notify = 1;
            }
            $oHeaderUpdate->save();

            $AssetNumber = $oHeaderUpdate->RequestNo;

            $lastSave = oHeaders::orderBy('created_at', 'desc')->first()->id;

            $log = new oStatusLogs();
            $log->oHeaders_id = $lastSave;
            $log->RequestNo = $AssetNumber;
            $log->Users_id = Auth::user()->id;
            $log->Status = $status;
            $log->save();

            //Save changes for existing Line Items
            for ($i = 1; $i <= $request->savedAssetCount; $i++) {
                if (isset($request->savedID[$i])) {
                    $oLineItemUpdate = oLineItems::find($request->savedID[$i]);
                    $oLineItemUpdate->Description = $request->savedDescription[$i];
                    $oLineItemUpdate->Purpose = $request->savedPurpose[$i];
                    //$iLineItemUpdate->SerialNum = $request->savedSerialNum[$i];
                    $oLineItemUpdate->Quantity = $request->savedQuantity[$i];
                    $oLineItemUpdate->UOM = $request->savedUOM[$i];
                    if (isset($request->savedReturnable[$i])) {
                        if ($request->savedReturnable[$i] == "on") {
                            $oLineItemUpdate->Returnable = 1;
                            $oLineItemUpdate->ExpReturn = $request->savedExpectedReturn[$i];
                        }
                        /*                        else
                                                    $iLineItemUpdate->Returnable = 0;*/
                    }
                    else
                        $oLineItemUpdate->Returnable = 0;

                    $oLineItemUpdate->Deleted = $request->savedDeleted[$i];
                    $oLineItemUpdate->save();
                }
            }
            // END - Save changes for existing Line Items
            if ($request->assetCount!=0) {
                $lineCounter = $request->realAssetCount;
                for ($i = 0; $i < $request->assetCount; $i++) {
                    if (isset($request->Description[$i])) {

                        $oline = new oLineItems();
                        $oline->h_ID = $request->id;
                        $oline->LineNum = $lineCounter;
                        $oline->LineStatus = 1;

                        $oline->Description = $request->Description[$i];
                        $oline->Purpose = $request->Purpose[$i];

                        if(self::getSerialNumberSeries()==null) {
                            self::createSerialNumberSeries();
                        }
                        $TempSerialNumber = self::getSerialNumberSeries();
                        self::updateSerialNumberSeries();

                        //$iline->SerialNum = $request->SerialNum[$i];
                        $oline->ControlNum = $TempSerialNumber;
                        $oline->Quantity = $request->Quantity[$i];
                        $oline->UOM = $request->UOM[$i];
                        if (isset($request->Returnable[$i])) {
                            if ($request->Returnable[$i] == "on") {
                                $oline->Returnable = 1;
                                $oline->ExpReturn = $request->ExpectedReturn[$i];
                            }
                        } else {
                            $oline->Returnable = 0;
                        }
                        $oline->save();
                        $lineCounter = $lineCounter + 1;
                    }
                }
            }
            $title = 'Confirmation';
            $message = 'Changes to Request Number ' . $oHeaderUpdate->RequestNo . ' has been saved.';
        }
        else{
            //NEW REQUEST
            if(self::getControlNumberSeries("O")==null) {
                self::createControlNumberSeries("O");
            }
            $AssetNumber = self::getControlNumberSeries("O");
            self::updateControlNumberSeries("O");

            $oheader = new oHeaders();
            $oheader->RequestNo = $AssetNumber;
            $oheader->BusinessPartner = $request->BusinessPartner;
            if($request->BusinessPartner == 3)
                $oheader->BusinessPartnerName = "United Graphic Expression Corporation";
            elseif($request->BusinessPartner == 4)
                $oheader->BusinessPartnerName = "APO";
            else
                $oheader->BusinessPartnerName = $request->BusinessPartnerName;
            //$iheader->BusinessPartnerName = $request->BusinessPartnerName;
            $oheader->Contact = $request->Contact;
            $oheader->ContactEmail = $request->ContactEmail;
            $oheader->Destination = $request->Destination;
            $oheader->Reason = $request->Reason;
            $oheader->Transporter = $request->Transporter;
            $oheader->Date = $request->Date;
            $oheader->RequestType = $request->RequestType;
            $oheader->CreatedBy = Auth::user()->id;
            $oheader->Status = $status;
            if ($status == 2) {
                $oheader->Notify = 1;
            }
            $oheader->save();

            $lastSave = oHeaders::orderBy('created_at', 'desc')->first()->id;

            $log = new oStatusLogs();
            $log->oHeaders_id = $lastSave;
            $log->RequestNo = $AssetNumber;
            $log->Users_id = Auth::user()->id;
            $log->Status = $status;
            $log->save();

            $lineCounter = 1;

            for ($i = 0; $i < $request->assetCount; $i++) {
                if (isset($request->Description[$i])) {

                    $oline = new oLineItems();
                    $oline->h_ID = $lastSave;
                    $oline->LineNum = $lineCounter;
                    $oline->LineStatus = 1;
                    $oline->Description = $request->Description[$i];
                    $oline->Purpose = $request->Purpose[$i];
                    $oline->Quantity = $request->Quantity[$i];
                    if(self::getSerialNumberSeries()==null) {
                        self::createSerialNumberSeries();
                    }
                    $TempSerialNumber = self::getSerialNumberSeries();
                    self::updateSerialNumberSeries();

                    //$iline->SerialNum = $request->SerialNum[$i];
                    $oline->ControlNum = $TempSerialNumber;
                    $oline->UOM = $request->UOM[$i];
                    if(isset($request->Returnable[$i])) {
                        if ($request->Returnable[$i] == "on") {
                            $oline->Returnable = 1;
                            $oline->ExpReturn = $request->ExpectedReturn[$i];
                        }
                        else
                            $oline->Returnable = 0;
                    }
                    else{
                        $oline->Returnable = 0;
                    }


                    $oline->save();
                    $lineCounter = $lineCounter + 1;
                }
            }
            $title = 'Confirmation';
            $message = 'Outgoing Request Number ' . $AssetNumber . ' has been created.';
        }
        //Email Notification
        if ($status == 2) {
            //Mail confirmation to Approver/s - START
            $getAT = ApprovalTemplateRequestors::where('User_id', '=', Auth::user()->id)->where('Active','=',1)->first();
            $getAR = ApprovalRoles::where('AT_id', '=', $getAT->AT_id)->first();
            $getApprovers = ApprovalStageApprovers::where('AS_id', '=', $getAR->AS_id)->where('Email','=',1)->get();

            $approvers = array();
            foreach ($getApprovers as $gA)
                array_push($approvers, $gA->User_id);

            $getApproverEmails = User::whereIn('id', $approvers)
                ->distinct()
                ->get();

            $singleRequest = oHeaders::where('RequestNo', '=', $AssetNumber)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            foreach ($getApproverEmails as $email) {
                $user_email = $email['Email'];
                if ($user_email != null) {
                    Mail::send('email.out_requestor_message',
                        [
                            'recepient' => 'approver',
                            'singleRequest' => $singleRequest,
                            'lineItems' => $lineItems,
                            'email' => $email['Email'],
                            'FirstName' => $email['FirstName'],
                            'approverID' => $email['id'],
                            'state' => 'approval'
                        ],
                        function ($message) use ($email) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("New Outgoing Request for Approval");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END
            $title = 'Confirmation';
            $message = 'Request Number ' . $AssetNumber . ' has been submitted for approval.';
        }

        return view('transactions_requestor.in_m_requestor', compact('title', 'message'));
    }
    public function outView(Request $request){
        if(self::knowYourRole(2)) {
            $myRequests = oHeaders::where('CreatedBy', '=', Auth::user()->id)->get();
            $status = oHeaderStatus::all();
            return view('transactions_requestor.out_v_all', compact('myRequests','statusName','status'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function outViewSingle($id)
    {
        if(self::knowYourRole(2)) {
            $singleRequest = oHeaders::where('id', '=', $id)->first();
            $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();

            //$lineItems = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->get();
            $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('InDate','<>','')->count();

            if (Auth::user()->id == $singleRequest->CreatedBy)
                return view('transactions_requestor.out_v_single', compact('singleRequest', 'lineItems','alreadyCheckedOut','coVersion'));
            else {
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function outTransactViewSingle($id)
    {
        $singleRequest = oHeaders::where('id','=',$id)->first();
        $lineItems = oLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $lineItemsCount = oLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->count();
        if(Auth::user()->id == $singleRequest->CreatedBy && $singleRequest->oStatus->Name == "Saved") {
            $descAutoComp = oLineItems::distinct()->get(['Description']);
            $user_det = User::where('username' , '=', Auth::user()->id)->first();
            $bus_part =BusinessPartners::all();
            $contacts =Employees::all();
            $linestat =LineStatus::all();
            $reqtype = oRequestType::all();
            $uoms =UOMs::all();
            return view('transactions_requestor.out_new_request',compact('user_det','bus_part','contacts','linestat','uoms','singleRequest', 'lineItems','lineItemsCount','descAutoComp','reqtype'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            return view('transactions.in_m_general',compact('title','message'));
        }
    }
    public function outMyRequests($id){
        if(self::knowYourRole(2) && $id<=11 && $id>=1) {
            $statusName = oHeaderStatus::where('id','=',$id)->first();
            $myRequests = oHeaders::where('CreatedBy', '=', Auth::user()->id)->where('Status','=',$id)->get();
            $status = oHeaderStatus::all();
            return view('transactions_requestor.out_v_all', compact('myRequests','statusName','status'));
        }
        elseif($id>=12 || $id<=0){
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            return view('transactions_requestor.in_m_requestor',compact('title','message'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            return view('transactions.in_m_general',compact('title','message'));
        }
    }
    public function outCheckinRequest(Request $request){

        $iHeaderUpdate = oHeaders::find($request->reqID);
        //Check again if Request is still in pending
        if ($iHeaderUpdate->Status == 5 ||$iHeaderUpdate->Status == 7) {
            $iHeaderUpdate->Status = 6;
            $iHeaderUpdate->RejectedBy = null;
            $iHeaderUpdate->RejectReason = null;
            //$iHeaderUpdate->ApprovedBy = Auth::user()->id;
            $iHeaderUpdate->save();

            $lineItemsToInclude = oLineItems::where('h_ID','=',$request->reqID)->get();

            $lineItemsCO = oLineItems::where('h_ID','=',$request->reqID)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID + 1;

            $log = new oStatusLogs();
            $log->oHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->checkin_ID = $checkOutID;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            foreach($lineItemsToInclude as $li){
                if (isset($request->Include[$li->LineNum])){
                    $lineItemUpdate = oLineItems::where('id','=',$li->id)->first();
                    $lineItemUpdate->checkin_ID = $checkOutID;
                    $lineItemUpdate->CheckIn = 1;
                    $lineItemUpdate->update();
                }
            }

            $title = 'Confirmation';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' has been submitted for check-in.';


            //Mail confirmation to Approver/s - START
            $getAT = ApprovalTemplateRequestors::where('User_id','=',Auth::user()->id)->first();
            $getAR = ApprovalRoles::where('AT_id','=',$getAT->AT_id)->first();
            $getApprovers = ApprovalStageApprovers::where('AS_id','=',$getAR->AS_id)->where('Email','=',1)->get();

            $approvers = array();
            foreach($getApprovers as $gA)
                array_push($approvers, $gA->User_id);

            $getApproverEmails = User::whereIn('id',$approvers)
                ->distinct()
                ->get();

            $singleRequest = oHeaders::where('id', '=', $request->reqID)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_requestor_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'state'=>'checkout'
                        ],
                        function ($message) use ($email) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("A request has been submitted for Check-In");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to submit request for check-in.';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_requestor.in_m_requestor', compact('title', 'message'));
    }
    public function outCheckinViewSingle($id)
    {
        $singleRequest = oHeaders::where('id','=',$id)->first();
        $lineItems = oLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $lineItemsCount = oLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->count();
        if(Auth::user()->id == $singleRequest->CreatedBy && ($singleRequest->Status == 5 || $singleRequest->Status == 7)) {
            $descAutoComp = oLineItems::distinct()->get(['Description']);
            $user_det = User::where('username' , '=', Auth::user()->id)->first();
            $bus_part =BusinessPartners::all();
            $contacts =Employees::all();
            $linestat =LineStatus::all();
            $uoms =UOMs::all();
            return view('transactions_requestor.out_ci_request',compact('user_det','bus_part','contacts','linestat','uoms','singleRequest', 'lineItems','lineItemsCount','descAutoComp'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            return view('transactions.out_m_general',compact('title','message'));
        }
    }

    //Approver Views

    public function inApproverMyRequests($code)
    {
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }

                    //Sa Code na ito nagkakatalo
                    if($code == "ci_for_approval")
                        $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',2)->get();
                    elseif($code == "ci_approved")
                        $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('ApprovedBy','=',Auth::user()->id)->get();
                    elseif($code == "ci_rejected")
                        $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('RejectedBy','=',Auth::user()->id)->get();
                    elseif($code == "co_for_approval")
                        $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',6)->get();
                    elseif($code == "co_approved")
                        $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('COApprovedBy','=',Auth::user()->id)->get();
                    elseif($code == "co_rejected")
                        $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('CORejectedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.in_v_for_approval', compact('myRequests','code'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }
    public function inApproverMyRequestsSingle($code,$id){
        $singleRequest = iHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = iLineItems::where('h_ID', '=', $singleRequest->id)->select('checkout_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkout_ID;
        $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        $retCount = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('Returnable','=',True)->where('ItemType','=',0)->count();
        $nonRetCount = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('Returnable','=',False)->count();
        $equipCount = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('Returnable','=',True)->where('ItemType','=',1)->count();


        if($code == "ci_for_approval"){
            if($singleRequest->Status == 2)
                return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code','retCount','nonRetCount','equipCount'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "ci_approved"){
            if($singleRequest->ApprovedBy == Auth::user()->id)
                return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code','retCount','nonRetCount','equipCount'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "ci_rejected"){
            if($singleRequest->RejectedBy == Auth::user()->id)
                return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code','retCount','nonRetCount','equipCount'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "co_for_approval"){
            if($singleRequest->Status == 6)
                return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code','retCount','nonRetCount','equipCount'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "co_approved"){
            if($singleRequest->COApprovedBy == Auth::user()->id)
                return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code','retCount','nonRetCount','equipCount'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "co_rejected"){
            if($singleRequest->CORejectedBy == Auth::user()->id)
                return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code','retCount','nonRetCount','equipCount'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }

    }

    public function outApproverMyRequests($code)
    {
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }

                    //Sa Code na ito nagkakatalo
                    if($code == "co_for_approval")
                        $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',2)->get();
                    elseif($code == "co_approved")
                        $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('COApprovedBy','=',Auth::user()->id)->get();
                    elseif($code == "co_rejected")
                        $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('CORejectedBy','=',Auth::user()->id)->get();
                    elseif($code == "ci_for_approval")
                        $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',6)->get();
                    elseif($code == "ci_approved")
                        $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('ApprovedBy','=',Auth::user()->id)->get();
                    elseif($code == "ci_rejected")
                        $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('RejectedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.out_v_for_approval', compact('myRequests','code'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }
    public function outApproverMyRequestsSingle($code,$id){
        $singleRequest = oHeaders::where('id','=',$id)->first();
        //$lineItems = oLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkin_ID;
        $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        if($code == "co_for_approval"){
            if($singleRequest->Status == 2)
                return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "co_approved"){
            if($singleRequest->COApprovedBy == Auth::user()->id)
                return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "co_rejected"){
            if($singleRequest->CORejectedBy == Auth::user()->id)
                return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "ci_for_approval"){
            if($singleRequest->Status == 6)
                return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "ci_approved"){
            if($singleRequest->ApprovedBy == Auth::user()->id)
                return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        elseif($code == "ci_rejected"){
            if($singleRequest->RejectedBy == Auth::user()->id)
                return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }

    }

    //OLD APPROVER VIEWS DO NOT DELETE
    /*public function inApproverView(Request $request){
        //Check if User is an Approver
        $UserRole = Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();

        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',2)->get();
                    return view('transactions_approver.in_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inMyApprovals(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('ApprovedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.in_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inMyRejections(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('RejectedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.in_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inApproverCOView(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',6)->get();
                    return view('transactions_approver.in_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inMyCOApprovals(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = iHeaders::whereIn('CreatedBy',$requestors)->where('COApprovedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.in_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/

    //OLD APPROVER SINGLE REQUEST VIEWS DO NOT DELETE
    /*public function inApproverViewSingle($id){
        $singleRequest = iHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = iLineItems::where('h_ID', '=', $singleRequest->id)->select('checkout_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkout_ID;
        $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        if($singleRequest->oStatus->Name == "Pending")
            return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inMyApprovalsSingle($id){
        $singleRequest = iHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = iLineItems::where('h_ID', '=', $singleRequest->id)->select('checkout_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkout_ID;
        $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        if($singleRequest->ApprovedBy == Auth::user()->id)
            return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inMyRejectionsSingle($id){
        $singleRequest = iHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = iLineItems::where('h_ID', '=', $singleRequest->id)->select('checkout_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkout_ID;
        $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        if($singleRequest->RejectedBy == Auth::user()->id)
            return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inApproverCOViewSingle($id){
        $singleRequest = iHeaders::where('id','=',$id)->first();
        $latestCheckOutID = iLineItems::where('h_ID','=',$singleRequest->id)->max('checkout_ID');
        $lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->where('checkout_ID','=',$latestCheckOutID)->get();
        if($singleRequest->Status == 6)
            return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    /*public function inMyCOApprovalsSingle($id){
        $singleRequest = iHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = iLineItems::where('h_ID', '=', $singleRequest->id)->select('checkout_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkout_ID;
        $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        if($singleRequest->COApprovedBy == Auth::user()->id)
            return view('transactions_approver.in_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/

    public function inApproveReq(Request $request){
        $iHeaderUpdate = iHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($iHeaderUpdate->Status == 2) {
            $iHeaderUpdate->Status = 4;
            $iHeaderUpdate->ApprovedBy = Auth::user()->id;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' has been approved.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email','FirstName')
                ->get();

            $singleRequest = iHeaders::where('RequestNo', '=', $iHeaderUpdate->RequestNo)->first();
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            $approver = Auth::user();
            PdfFileController::attachment_contact($singleRequest->id);
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.in_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => "approved",
                            'requestID'=>$iHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been approved");
                            $message->to($email['Email']);
                        });
                }
            }
            if($singleRequest->ContactEmail!="") {
                Mail::send('email.contactMessage',
                    [
                        'recepient' => 'approver',
                        'singleRequest' => $singleRequest,
                        'lineItems' => $lineItems,
                        'email' => $singleRequest->ContactEmail,
                        'FirstName' => $singleRequest->Contact,
                        'approverID' => "",
                        'state' => "approved",
                        'requestID' => $iHeaderUpdate->RequestNo
                    ],
                    function ($message) use ($singleRequest) {
                        $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                        $message->attach(storage_path("../storage/pdf_attachments/".$singleRequest->RequestNo.".pdf"));
                        $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                        $message->to($singleRequest->ContactEmail);
                    });
            }
            //Mail confirmation to Approver/s - END

        }
        else{
            $title = 'Unable to approve request';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.in_m_approver',compact('title','message'));
    }
    public function inRejectReq(Request $request){
        $iHeaderUpdate = iHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($iHeaderUpdate->Status == 2) {
            $iHeaderUpdate->Status = 10;
            $iHeaderUpdate->RejectedBy = Auth::user()->id;
            $iHeaderUpdate->RejectReason = $request->RejectReason;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Rejected = 1;
            $log->RejectReason = $request->RejectReason;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' has been rejected.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $rejector = Auth::user();
            $singleRequest = iHeaders::where('RequestNo', '=', $iHeaderUpdate->RequestNo)->first();
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.in_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'rejectReason' => $request->RejectReason,
                            'rejector' => $rejector->FirstName." ".$rejector->LastName,
                            'state' => "rejected",
                            'requestID'=>$iHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been rejected");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to reject request';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.in_m_approver',compact('title','message'));
    }
    public function inCancelReq(Request $request){
        $iHeaderUpdate = iHeaders::find($request->id);
        //Check again if Request is Approved
        if ($iHeaderUpdate->Status == 3 || $iHeaderUpdate->Status == 4) {
            $iHeaderUpdate->Status = 11;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' has been canceled.';
        }
        elseif ($iHeaderUpdate->Status == 8) {
            $latestCheckOutID = iLineItems::where('h_ID','=',$iHeaderUpdate->id)->max('checkout_ID');
            $revertItems = iLineItems::where('h_ID','=',$iHeaderUpdate->id)->where('checkout_ID','=',$latestCheckOutID)->get();
            foreach($revertItems as $ri){
                $revert = iLineItems::where('id','=',$ri->id)->first();
                $revert->checkout_ID = 0;
                $revert->CheckOut = 0;
                $revert->save();
            }

            //dd($revertItems);
            $iHeaderUpdate->Status = 7;
            $iHeaderUpdate->save();


            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' has been canceled.';
        }
        else{
            $title = 'Unable to cancel request';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_requestor.in_m_requestor',compact('title','message'));
    }
    public function inApproveCOReq(Request $request){
        $iHeaderUpdate = iHeaders::find($request->id);

        //Check again if Request is still in pending
        if ($iHeaderUpdate->Status == 6) {
            $iHeaderUpdate->Status = 8;
            $iHeaderUpdate->COApprovedBy = Auth::user()->id;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';
            $message = 'Request ' . $iHeaderUpdate->RequestNo . ' has been approved for Check-out.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();

            $singleRequest = iHeaders::where('RequestNo', '=', $iHeaderUpdate->RequestNo)->first();
            $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkout_ID;
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();

            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.in_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'state' => "approved for check-out",
                            'requestID'=>$iHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been approved for check-out");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END

        }
        else{
            $title = 'Unable to approve request';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.in_m_approver',compact('title','message'));
    }
    public function inRejectCOReq(Request $request){
        $iHeaderUpdate = iHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($iHeaderUpdate->Status == 6) {
            //BRINGS THE REQUEST BACK TO CHECKED IN
            $iHeaderUpdate->Status = 7;
            $iHeaderUpdate->CORejectedBy = Auth::user()->id;
            $iHeaderUpdate->CORejectReason = $request->RejectReason;
            $iHeaderUpdate->save();



            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $lineItemsCO = iLineItems::where('h_ID','=',$iHeaderUpdate->id)->orderBy('checkout_ID','DESC')->first();
            $log->checkout_ID = $lineItemsCO->checkout_ID;
            $log->Rejected = 1;
            $log->RejectReason = $request->RejectReason;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' has been rejected for Check-out.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $rejector = Auth::user();
            $singleRequest = iHeaders::where('RequestNo', '=', $iHeaderUpdate->RequestNo)->first();
            $singleRequest = iHeaders::where('id', '=', $iHeaderUpdate->id)->first();
            $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkout_ID;
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
            //$lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.in_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'rejectReason' => $request->RejectReason,
                            'rejector' => $rejector->FirstName." ".$rejector->LastName,
                            'state' => "rejected for check-out",
                            'requestID'=>$iHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been rejected for check-out");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END
            $latestCheckOutID = iLineItems::where('h_ID','=',$iHeaderUpdate->id)->max('checkout_ID');
            $revertItems = iLineItems::where('h_ID','=',$iHeaderUpdate->id)->where('checkout_ID','=',$latestCheckOutID)->get();
            foreach($revertItems as $ri){
                $revert = iLineItems::where('id','=',$ri->id)->first();
                $revert->checkout_ID = 0;
                $revert->CheckOut = 0;
                $revert->save();
            }
        }
        else{
            $title = 'Unable to reject request';
            $message = 'Request '.$iHeaderUpdate->RequestNo.' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.in_m_approver',compact('title','message'));
    }


    public function outApproverView(Request $request){
        //Check if User is an Approver
        $UserRole = Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();

        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',2)->get();
                    return view('transactions_approver.out_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }
    public function outApproverViewSingle($id){
        $singleRequest = oHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkin_ID;
        $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('InDate','<>','')->count();
        if($singleRequest->oStatus->Name == "Pending")
            return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }

    public function outApproveReq(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 2) {

            //NOTE: 11/17/2017 - Change Status to 3 if Request Type is Fixed Asset.
            //KEYWORD: Accounting Approval
            if ($oHeaderUpdate->RequestType == 2) {
                $oHeaderUpdate->Status = 3;
                $reqState = "forwarded to Accounting for approval";
            } else {
                $oHeaderUpdate->Status = 4;
                $reqState = "approved";
            }
            $oHeaderUpdate->COApprovedBy = Auth::user()->id;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            if ($oHeaderUpdate->RequestType == 2)
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been forwarded to Accounting for approval.';
            else
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been approved.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id', '=', $oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email', 'FirstName')
                ->get();

            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            $approver = Auth::user();
            foreach ($getApproverEmails as $email) {
                $user_email = $email['Email'];
                if ($user_email != null) {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient' => 'approver',
                            'singleRequest' => $singleRequest,
                            'lineItems' => $lineItems,
                            'email' => $email['Email'],
                            'FirstName' => $email['FirstName'],
                            'approverID' => $email['id'],
                            'approver' => $approver->FirstName . " " . $approver->LastName,
                            'state' => $reqState,
                            'requestID' => $oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email, $singleRequest, $reqState) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Outgoing Request " . $singleRequest->RequestNo . " has been " . $reqState);
                            $message->to($email['Email']);
                        });
                }
            }
            //SEND EMAIL TO ACCOUNTING
            if ($oHeaderUpdate->RequestType == 2) {
                //Mail confirmation to Accounting - START
                $getApprovers = Userrolesgroup::where('UserRoles_ID', '=', 7)->where('Active', '=', 1)->get();
                $aapprovers = array();

                foreach ($getApprovers as $gA)
                    array_push($aapprovers, $gA->Users_ID);
                $getApproverEmails = User::whereIn('id', $aapprovers)
                    ->distinct()
                    ->get();

                $singleRequest = oHeaders::where('id', '=', $request->id)->first();
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach ($getApproverEmails as $email) {
                    $user_email = $email['Email'];
                    if ($user_email != null) {
                        Mail::send('email.out_requestor_message',
                            [
                                'recepient' => 'accounting',
                                'singleRequest' => $singleRequest,
                                'lineItems' => $lineItems,
                                'email' => $email['Email'],
                                'FirstName' => $email['FirstName'],
                                'approverID' => $email['id'],
                                'state' => 'approval'
                            ],
                            function ($message) use ($email) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("New Outgoing Request for Approval");
                                $message->to($email['Email']);
                            });
                    }
                }
                //Mail confirmation to Accounting - END
            } //SEND EMAIL TO ACCOUNTING - END
            else {
                if ($singleRequest->ContactEmail != "") {
                    Mail::send('email.contactOutMessage',
                        [
                            'recepient' => 'approver',
                            'singleRequest' => $singleRequest,
                            'lineItems' => $lineItems,
                            'email' => $singleRequest->ContactEmail,
                            'FirstName' => $singleRequest->Contact,
                            'approverID' => "",
                            'state' => "approved",
                            'requestID' => $oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                            $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                            $message->to($singleRequest->ContactEmail);
                        });
                }
            }
            //Mail confirmation to Approver/s - END

        }
        else{
            $title = 'Unable to approve request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.out_m_approver',compact('title','message'));
    }
    public function outRejectReq(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 2) {
            $oHeaderUpdate->Status = 10;
            $oHeaderUpdate->CORejectedBy = Auth::user()->id;
            $oHeaderUpdate->CORejectReason = $request->RejectReason;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->Rejected = 1;
            $log->RejectReason = $request->RejectReason;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' has been rejected.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $rejector = Auth::user();
            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'rejectReason' => $request->RejectReason,
                            'rejector' => $rejector->FirstName." ".$rejector->LastName,
                            'state' => "rejected",
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been rejected");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to reject request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.out_m_approver',compact('title','message'));
    }
    public function outCancelReq(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is Approved
        if ($oHeaderUpdate->Status == 3 || $oHeaderUpdate->Status == 4) {
            $oHeaderUpdate->Status = 11;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' has been canceled.';
        }
        elseif ($oHeaderUpdate->Status == 8) {
            $latestCheckOutID = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->max('checkin_ID');
            $revertItems = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->where('checkin_ID','=',$latestCheckOutID)->get();
            foreach($revertItems as $ri){
                $revert = oLineItems::where('id','=',$ri->id)->first();
                $revert->checkin_ID = 0;
                $revert->CheckIn = 0;
                $revert->save();
            }

            //dd($revertItems);
            $oHeaderUpdate->Status = 5;
            $oHeaderUpdate->save();


            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' has been canceled.';
        }
        else{
            $title = 'Unable to cancel request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_requestor.in_m_requestor',compact('title','message'));
    }

    public function outMyApprovals(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('COApprovedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.out_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }
    public function outMyRejections(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('CORejectedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.out_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }
    public function outMyApprovalsSingle($id){
        $singleRequest = oHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkin_ID;
        $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        if($singleRequest->COApprovedBy == Auth::user()->id)
            return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }
    public function outMyRejectionsSingle($id){
        $singleRequest = oHeaders::where('id','=',$id)->first();
        //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
        $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
        $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
        $checkOutID = $lineItemsCO->checkin_ID;
        $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
        $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

        if($singleRequest->CORejectedBy == Auth::user()->id)
            return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }

    public function outApproverCIView(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('Status','=',6)->get();
                    return view('transactions_approver.out_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }
    public function outApproverCIViewSingle($id){
        $singleRequest = oHeaders::where('id','=',$id)->first();
        $latestCheckOutID = oLineItems::where('h_ID','=',$singleRequest->id)->max('checkin_ID');
        $lineItems = oLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->where('checkin_ID','=',$latestCheckOutID)->get();
        if($singleRequest->Status == 6)
            return view('transactions_approver.out_v_for_approval_single',compact('singleRequest','lineItems'));
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this request.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }

    public function outApproveCIReq(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);

        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 6) {

            if($oHeaderUpdate->RequestType == 2) {
                $oHeaderUpdate->Status = 7;
                $reqState = "forwarded to Accounting for approval";
            }
            else {
                $oHeaderUpdate->Status = 8;
                $reqState = "approved for check-out";
            }

            $oHeaderUpdate->ApprovedBy = Auth::user()->id;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';
            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been approved for Check-in.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();

            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();

            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => Auth::user()->FirstName." ".Auth::user()->LastName,
                            'state' => $reqState,
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest,$reqState) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been ".$reqState);
                            $message->to($email['Email']);
                        });
                }
            }
            //SEND EMAIL TO ACCOUNTING
            if($oHeaderUpdate->RequestType == 2){
                //Mail confirmation to Accounting - START

                $getApprovers = Userrolesgroup::where('UserRoles_ID','=',7)->where('Active','=',1)->get();
                $aapprovers = array();

                foreach ($getApprovers as $gA)
                    array_push($aapprovers, $gA->Users_ID);
                $getApproverEmails = User::whereIn('id', $aapprovers)
                    ->distinct()
                    ->get();

                $singleRequest = oHeaders::where('id', '=', $request->id)->first();
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach ($getApproverEmails as $email) {
                    $user_email = $email['Email'];
                    if ($user_email != null) {
                        Mail::send('email.out_requestor_message',
                            [
                                'recepient' => 'accounting',
                                'singleRequest' => $singleRequest,
                                'lineItems' => $lineItems,
                                'email' => $email['Email'],
                                'FirstName' => $email['FirstName'],
                                'approverID' => $email['id'],
                                'state' => 'checkout'
                            ],
                            function ($message) use ($email) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("New Outgoing Request for Approval");
                                $message->to($email['Email']);
                            });
                    }
                }
                //Mail confirmation to Accounting - END
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been forwarded to Accounting for approval.';
            }
            //SEND EMAIL TO ACCOUNTING - END
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to approve request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.out_m_approver',compact('title','message'));
    }
    public function outRejectCIReq(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 6) {
            $oHeaderUpdate->Status = 5;
            $oHeaderUpdate->ApprovedBy = null;
            $oHeaderUpdate->RejectedBy = Auth::user()->id;
            $oHeaderUpdate->RejectReason = $request->RejectReason;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $lineItemsCO = oLineItems::where('h_ID','=',$request->id)->orderBy('checkin_ID','DESC')->first();
            $log->checkin_ID = $lineItemsCO->checkin_ID;
            $log->Rejected = 1;
            $log->RejectReason = $request->RejectReason;
            $log->save();

            $title = 'Confirmation';

            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been rejected.';
            $state = "rejected";

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $rejector = Auth::user()->id;

            $singleRequest = oHeaders::where('id', '=', $oHeaderUpdate->id)->first();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            //dd($singleRequest);
            //$lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'rejectReason' => $request->RejectReason,
                            'rejector' => Auth::user()->FirstName." ".Auth::user()->LastName,
                            'state' => "rejected for check-in",
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been rejected for check-in");
                            $message->to($email['Email']);
                        });
                }
            }

            $latestCheckOutID = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->max('checkin_ID');
            $revertItems = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->where('checkin_ID','=',$latestCheckOutID)->get();
            foreach($revertItems as $ri){
                $revert = oLineItems::where('id','=',$ri->id)->first();
                $revert->checkin_ID = 0;
                $revert->CheckIn = 0;
                $revert->save();
            }

            //Mail confirmation to Approver/s - END

        }
        else{
            $title = 'Unable to reject request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.out_m_approver',compact('title','message'));
    }
    public function outMyCIApprovals(Request $request){
        //Check if User is an Approver
        $UserRole = \App\Userrolesgroup::where('Users_ID','=',Auth::user()->id)->get();
        $areYouAnApprover = 0;
        foreach($UserRole as $uR){
            if($uR->toUserRoles->Name == "Approver")
                $areYouAnApprover = 1;
        }
        if($areYouAnApprover==1) {
            //Check requests by users under Approval template
            $ApprovalStageID = ApprovalStageApprovers::where('User_id','=',Auth::user()->id)->where('Active','=',True)->first();
            if(isset($ApprovalStageID)){
                //If Approver belongs to an Approval Stage
                $ApprovalStageID = $ApprovalStageID->AS_id;
                $ApprovalTemplateID = ApprovalRoles::where('AS_id','=',$ApprovalStageID)->where('Active','=',True)->first();
                if(isset($ApprovalTemplateID)){
                    $ApprovalTemplateID = $ApprovalTemplateID->AT_id;
                    $listRequestors = ApprovalTemplateRequestors::where('AT_id','=',$ApprovalTemplateID)->where('Active','=',True)->get();
                    $requestors=array();
                    foreach($listRequestors as $lR){
                        array_push($requestors,$lR->User_id);
                    }
                    $myRequests = oHeaders::whereIn('CreatedBy',$requestors)->where('ApprovedBy','=',Auth::user()->id)->get();
                    return view('transactions_approver.out_v_for_approval', compact('myRequests'));
                }
                else{
                    $title = 'Access Denied';
                    $message = 'You have no Approval Role. Contact an Administrator.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                //If Approver DOES NOT belong to an Approval Stage
                $title = 'Access Denied';
                $message = 'You are not included in an Approval Stage. Contact an Administrator.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }

    // Security Views
    public function iSecurityView(){
        if(self::knowYourRole(4)){
            $myRequests = iHeaders::where('Status','=',3)->get();
            return view('transactions_security.in_v_security',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_security.in_m_security',compact('title','message','restriction'));
        }

    }
    public function iSecurityViewSingle($id){
        if(self::knowYourRole(4)) {
            $singleRequest = iHeaders::where('id', '=', $id)->first();
            $lineItems = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->get();
                return view('transactions_security.in_v_security_single', compact('singleRequest', 'lineItems'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_security.in_m_security',compact('title','message','restriction'));
        }
    }
    public function iSecurityVerify(Request $request){
        //For now, this is for skipping the printing process. 10/04/2017
        $singleRequest = iHeaders::where('id','=',$request->id)->first();
        $singleRequest->Status = 4;
        $singleRequest->SecurityControl = Auth::user()->id;
        $singleRequest->BCodePrinted = 1;
        $singleRequest->save();

        $log = new iStatusLogs();
        $log->iHeaders_id = $singleRequest->id;
        $log->RequestNo = $singleRequest->RequestNo;
        $log->Users_id = Auth::user()->id;
        $log->Status = $singleRequest->Status;
        $log->save();

        //$myRequests = iHeaders::where('Status','=',3)->get();
        $title = 'Confirmation';
        $message = 'Request '.$singleRequest->RequestNo.' is now ready for check-in.';
        $buttonLink = "in_securityview";
        $buttonText = "Check-in Requests";
        return view('transactions_security.in_m_security',compact('title','message','restriction','buttonLink','buttonText'));
        //End

    }

    public function iSecurityCOView(){
        if(self::knowYourRole(4)){
            $myRequests = iHeaders::where('Status','=',7)->get();
            return view('transactions_security.in_v_security',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_security.in_m_security',compact('title','message','restriction'));
        }

    }
    public function iSecurityCOViewSingle($id){
        if(self::knowYourRole(4)) {
            $singleRequest = iHeaders::where('id', '=', $id)->first();
            $lineItems = iLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->get();
            return view('transactions_security.in_v_security_single', compact('singleRequest', 'lineItems'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_security.in_m_security',compact('title','message','restriction'));
        }
    }
    public function iSecurityCOVerify(Request $request){
        //For now, this is for skipping the printing process. 10/04/2017
        $singleRequest = iHeaders::where('id','=',$request->id)->first();
        $singleRequest->Status = 8;
        $singleRequest->COSecurityControl = Auth::user()->id;
        $singleRequest->save();

        $log = new iStatusLogs();
        $log->iHeaders_id = $singleRequest->id;
        $log->RequestNo = $singleRequest->RequestNo;
        $log->Users_id = Auth::user()->id;
        $log->Status = $singleRequest->Status;
        $log->save();

        //$myRequests = iHeaders::where('Status','=',3)->get();
        $title = 'Confirmation';
        $message = 'Request '.$singleRequest->RequestNo.' is now ready for check-out.';
        $buttonLink = "in_security_co_view";
        $buttonText = "Check-out Requests";
        return view('transactions_security.in_m_security',compact('title','message','restriction','buttonLink','buttonText'));
        //End

    }

    // Gate Views
    public function inGateView(){
        if(self::knowYourRole(5)){
            $myRequests = iHeaders::where('Status','=',4)->get();
            return view('transactions_gate.in_v_guard',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function inGateViewSingle($id)
    {
        if(self::knowYourRole(5)){
            $singleRequest = iHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 4) {
                $lineItems = iLineItems::where('h_ID', '=', $id)->where('LineStatus', '=', 1)->where('Deleted', '=', False)->get();
                $lineItemsCount = iLineItems::where('h_ID', '=', $id)->where('LineStatus', '=', 1)->where('Deleted', '=', False)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.in_v_guard_ci_single', compact('id', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function inGateSubmit(Request $request){
        /*$data=array('image_path'=>Photo::insertPhoto((string)$request->image_path[1]->getClientOriginalName(),'../public/image_upload/','no image'));
        DB::table('tblimage')->insert($data);*/
        //Working Upload Code , Non-Dependent to Photo.php`
        /*if($request->image_path[1]!=null){
            $imageName = time().'.'.$request->image_path[1]->getClientOriginalExtension();
            $request->image_path[1]->move(public_path('/image_upload'), $imageName);
        }*/
        for ($i = 1;$i<=$request->assetCount;$i++) {
            if ($request->Included[$i] == 1) {
                $iLineUpdate = iLineItems::find($request->LineID[$i]);
                $iLineUpdate->InDate = $request->InDate[$i];
                if(isset($request->SerialNum[$i])) {
                    $iLineUpdate->SerialNum = $request->SerialNum[$i];
                }
                $iLineUpdate->Quantity = $request->Quantity[$i];
                $iLineUpdate->ImageCount = $request->imgCounter[$i];

                for ($j = 0;$j<=$request->imgCounter[$i];$j++){
                    //if(isset($request->image_path[$i][$j]) && $request->image_path[$i][$j]!=null){
                    if($request->imgInclude[$i][$j]==1){

                        //Saving Image Details to Table
                        $iLineItemImages = new iLineItemImages();
                        $iLineItemImages->l_ID = $iLineUpdate->id;
                        $iLineItemImages->ControlNum = $iLineUpdate->ControlNum;
                        $iLineItemImages->ImageNum = $j;
                        $iLineItemImages->ImageFormat = $request->image_path[$i][$j]->getClientOriginalExtension();
                        $iLineItemImages->save();

                        $imageName = $iLineUpdate->ControlNum.'-'.$j.'.'.$request->image_path[$i][$j]->getClientOriginalExtension();
                        Storage::disk('upload')->put($imageName, file_get_contents($request->image_path[$i][$j]));
                        $iLineUpdate->ImageFormat = $request->image_path[$i][$j]->getClientOriginalExtension();
                    }
                }
                //Previous Single Image Upload Code. Do Not Delete
                //if(isset($request->image_path[$i]) && $request->image_path[$i]!=null){
                    //Working Upload Code. Cannot Be Viewed
                    /*$imageName = $iLineUpdate->ControlNum.'.'.$request->image_path[$i]->getClientOriginalExtension();*/
                    //$request->image_path[$i]->move(storage_path('uploads'), $imageName);
                    ///$imageName = $iLineUpdate->ControlNum.'.'.$request->image_path[$i]->getClientOriginalExtension();
                    ///Storage::disk('upload')->put($imageName, file_get_contents($request->image_path[$i]));
                    ///$iLineUpdate->ImageFormat = $request->image_path[$i]->getClientOriginalExtension();
                //}
                if ($iLineUpdate->Returnable == 0)
                    $iLineUpdate->LineStatus = 2;
                $iLineUpdate->save();
            }
            elseif ($request->Included[$i] == 0) {
                    $iLineUpdate = iLineItems::find($request->LineID[$i]);
                    $iLineUpdate->Deleted = 1;
                    $iLineUpdate->save();
            }
        }
            $iHeaderUpdate = iHeaders::find($request->id);
            $iHeaderUpdate->Status = 5;
            $iHeaderUpdate->ReceivedBy = Auth::user()->id;
            $iHeaderUpdate->ReceivedByName = $request->ReceivedByName;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

        //If all Line Items for the current request is Closed, The Request status will be set to Closed.
        /*$CheckOpenLines = iLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('Deleted','=',False)->count();
        if($CheckOpenLines==0){
            $iHeaderUpdate = iHeaders::find($request->id);
            $iHeaderUpdate->Status = 4;
            $iHeaderUpdate->save();
        }*/
        //END
        $title = 'Confirmation';
        $message = 'Request updated!';

        //If all Line Items for the current request is Closed, The Request status will be set to Closed.
        $CheckOpenLines = iLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('Deleted','=',False)->count();
        if($CheckOpenLines==0){
            $iHeaderUpdate = iHeaders::find($request->id);
            $iHeaderUpdate->Status = 9;
            //$iHeaderUpdate->COReceivedBy = Auth::user()->id;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            $message = 'The request '.$iHeaderUpdate->RequestNo.' has been closed.';
        }
        //END



        $buttonLink = "in_gateview";
        $buttonText = "Check-in Requests";
        return view('transactions_gate.in_m_gate',compact('title','message','restriction','buttonLink','buttonText'));
    }

    public function inGateCOView(){
        if(self::knowYourRole(5)){
            $myRequests = iHeaders::where('Status','=',8)->get();
            return view('transactions_gate.in_v_guard',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function inGateCOViewOriginal($id)
    {
        if(self::knowYourRole(5)){
            $singleRequest = iHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 8) {
                $lineItems = iLineItems::where('h_ID', '=', $id)->get();
                $lineItemsCount = iLineItems::where('h_ID', '=', $id)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.in_v_guard_co_single', compact('id', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function inGateCOViewSingle($id,$ids)
    {
        if(self::knowYourRole(5)){
            $singleRequest = iHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 8) {
                $lineItems = iLineItems::where('h_ID', '=', $id)->get();
                $lineItemsCount = iLineItems::where('h_ID', '=', $id)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.in_v_guard_co_single', compact('id','ids', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function inGateCOSubmit(Request $request){
        for ($i = 1;$i<=$request->assetCount;$i++) {
            if ($request->Included[$i] == 1) {
                $iLineUpdate = iLineItems::find($request->LineID[$i]);
                $iLineUpdate->OutDate = $request->OutDate[$i];
                //$iLineUpdate->InRemarks = $request->InRemarks[$i];
                $iLineUpdate->LineStatus = 2;
                $iLineUpdate->save();
            }
            /*elseif ($request->Included[$i] == 0) {
                $iLineUpdate = iLineItems::find($request->LineID[$i]);
                $iLineUpdate->Deleted = 1;
                $iLineUpdate->save();
            }*/
        }

        /*$iHeaderUpdate = iHeaders::find($request->id);
        $iHeaderUpdate->Status = 5;
        $iHeaderUpdate->ReceivedBy = Auth::user()->id;
        $iHeaderUpdate->save();*/

        $title = 'Confirmation';
        $message = 'Request updated!';

        $CheckCurrentLines = iLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('checkout_ID','=',$request->checkout_ID)->where('Deleted','=',False)->count();
        if($CheckCurrentLines==0){
            $iHeaderUpdate = iHeaders::find($request->id);
            $iHeaderUpdate->Status = 7;
            //$iHeaderUpdate->COApprovedBy = null;
            $iHeaderUpdate->COReceivedBy = Auth::user()->id;
            $iHeaderUpdate->COReceivedByName = $request->COReceivedByName;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            $message = 'The request '.$iHeaderUpdate->RequestNo.' has been updated.';
        }

        //If all Line Items for the current request is Closed, The Request status will be set to Closed.
        $CheckOpenLines = iLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('Deleted','=',False)->count();
        if($CheckOpenLines==0){
            $iHeaderUpdate = iHeaders::find($request->id);
            $iHeaderUpdate->Status = 9;
            $iHeaderUpdate->COReceivedBy = Auth::user()->id;
            $iHeaderUpdate->COReceivedByName = $request->COReceivedByName;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            $message = 'The request '.$iHeaderUpdate->RequestNo.' has been closed.';
        }
        //END

        $buttonLink = "in_gate_co_view";
        $buttonText = "Check-out Requests";
        return view('transactions_gate.in_m_gate',compact('title','message','restriction','buttonLink','buttonText'));
    }

    public function inGateClosed(){
        if(self::knowYourRole(5)){
            $myRequests = iHeaders::where('Status','=',9)->get();
            return view('transactions_gate.in_v_guard',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function inGateCOViewClosed($id)
    {
        if(self::knowYourRole(5)){
            $singleRequest = iHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 9) {
                $lineItems = iLineItems::where('h_ID', '=', $id)->get();
                $lineItemsCount = iLineItems::where('h_ID', '=', $id)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.in_v_guard_co_single', compact('id', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }

    public function outGateView(){
        if(self::knowYourRole(5)){
            $myRequests = oHeaders::where('Status','=',4)->get();
            return view('transactions_gate.out_v_guard',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.out_m_gate',compact('title','message','restriction'));
        }
    }
    public function outGateViewSingle($id)
    {
        if(self::knowYourRole(5)){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 4) {
                $lineItems = oLineItems::where('h_ID', '=', $id)->where('LineStatus', '=', 1)->where('Deleted', '=', False)->get();
                $lineItemsCount = oLineItems::where('h_ID', '=', $id)->where('LineStatus', '=', 1)->where('Deleted', '=', False)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.out_v_guard_co_single', compact('id', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.out_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function outGateSubmit(Request $request){
        /*$data=array('image_path'=>Photo::insertPhoto((string)$request->image_path[1]->getClientOriginalName(),'../public/image_upload/','no image'));
        DB::table('tblimage')->insert($data);*/

        //Working Upload Code , Non-Dependent to Photo.php`
        /*if($request->image_path[1]!=null){
            $imageName = time().'.'.$request->image_path[1]->getClientOriginalExtension();
            $request->image_path[1]->move(public_path('/image_upload'), $imageName);
        }*/

        for ($i = 1;$i<=$request->assetCount;$i++) {
            if ($request->Included[$i] == 1) {
                $oLineUpdate = oLineItems::find($request->LineID[$i]);
                $oLineUpdate->OutDate = $request->OutDate[$i];
                if(isset($request->image_path[$i]) && $request->image_path[$i]!=null){
                    //Working Upload Code. Cannot Be Viewed
                    /*$imageName = $oLineUpdate->ControlNum.'.'.$request->image_path[$i]->getClientOriginalExtension();*/
                    //$request->image_path[$i]->move(storage_path('uploads'), $imageName);
                    $imageName = $oLineUpdate->ControlNum.'.'.$request->image_path[$i]->getClientOriginalExtension();
                    Storage::disk('upload')->put($imageName, file_get_contents($request->image_path[$i]));
                    $oLineUpdate->ImageFormat = $request->image_path[$i]->getClientOriginalExtension();
                }
                if ($oLineUpdate->Returnable == 0)
                    $oLineUpdate->LineStatus = 2;
                $oLineUpdate->save();
            }
            elseif ($request->Included[$i] == 0) {
                $oLineUpdate = oLineItems::find($request->LineID[$i]);
                $oLineUpdate->Deleted = 1;
                $oLineUpdate->save();
            }
        }

        $oHeaderUpdate = oHeaders::find($request->id);
        $oHeaderUpdate->Status = 5;
        $oHeaderUpdate->COReceivedBy = Auth::user()->id;
        $oHeaderUpdate->COReceivedByName = $request->ReceivedByName;
        $oHeaderUpdate->save();

        $log = new oStatusLogs();
        $log->oHeaders_id = $oHeaderUpdate->id;
        $log->RequestNo = $oHeaderUpdate->RequestNo;
        $log->Users_id = Auth::user()->id;
        $log->Status = $oHeaderUpdate->Status;
        $log->save();

        //If all Line Items for the current request is Closed, The Request status will be set to Closed.
        /*$CheckOpenLines = oLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('Deleted','=',False)->count();
        if($CheckOpenLines==0){
            $oHeaderUpdate = oHeaders::find($request->id);
            $oHeaderUpdate->Status = 4;
            $oHeaderUpdate->save();
        }*/
        //END
        $title = 'Confirmation';
        $message = 'Request updated!';

        //If all Line Items for the current request is Closed, The Request status will be set to Closed.
        $CheckOpenLines = oLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('Deleted','=',False)->count();
        if($CheckOpenLines==0){
            $oHeaderUpdate = oHeaders::find($request->id);
            $oHeaderUpdate->Status = 9;
            //$oHeaderUpdate->COReceivedBy = Auth::user()->id;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $message = 'The request '.$oHeaderUpdate->RequestNo.' has been closed.';
        }
        //END



        $buttonLink = "in_gateview";
        $buttonText = "Check-in Requests";
        return view('transactions_gate.in_m_gate',compact('title','message','restriction','buttonLink','buttonText'));
    }

    public function outGateCIView(){
        if(self::knowYourRole(5)){
            $myRequests = oHeaders::where('Status','=',8)->get();
            return view('transactions_gate.out_v_guard',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.out_m_gate',compact('title','message','restriction'));
        }
    }
    public function outGateClosed(){
        if(self::knowYourRole(5)){
            $myRequests = oHeaders::where('Status','=',9)->get();
            return view('transactions_gate.out_v_guard',compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.out_m_gate',compact('title','message','restriction'));
        }
    }
    public function outGateCOViewClosed($id)
    {
        if(self::knowYourRole(5)){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 9) {
                $lineItems = oLineItems::where('h_ID', '=', $id)->get();
                $lineItemsCount = oLineItems::where('h_ID', '=', $id)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.out_v_guard_ci_single', compact('id', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.out_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.out_m_gate',compact('title','message','restriction'));
        }
    }

    public function outGateCIViewOriginal($id)
    {
        if(self::knowYourRole(5)){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 8) {
                $lineItems = oLineItems::where('h_ID', '=', $id)->get();
                $lineItemsCount = oLineItems::where('h_ID', '=', $id)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.out_v_guard_ci_single', compact('id', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function outGateCIViewSingle($id,$ids)
    {
        if(self::knowYourRole(5)){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            if($singleRequest->Status == 8) {
                $lineItems = oLineItems::where('h_ID', '=', $id)->get();
                $lineItemsCount = oLineItems::where('h_ID', '=', $id)->count();
                $user_det = User::where('username', '=', Auth::user()->id)->first();
                $bus_part = BusinessPartners::all();
                $uoms = UOMs::all();
                return view('transactions_gate.out_v_guard_ci_single', compact('id','ids', 'user_det', 'bus_part', 'uoms', 'singleRequest', 'lineItems', 'lineItemsCount'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_gate.in_m_gate',compact('title','message','restriction'));
        }
    }
    public function outGateCISubmit(Request $request){

        for ($i = 1;$i<=$request->assetCount;$i++) {
            if ($request->Included[$i] == 1) {
                $oLineUpdate = oLineItems::find($request->LineID[$i]);
                $oLineUpdate->InDate = $request->InDate[$i];
                //$oLineUpdate->InRemarks = $request->InRemarks[$i];
                $oLineUpdate->LineStatus = 2;
                $oLineUpdate->save();
            }
            /*elseif ($request->Included[$i] == 0) {
                $oLineUpdate = oLineItems::find($request->LineID[$i]);
                $oLineUpdate->Deleted = 1;
                $oLineUpdate->save();
            }*/
        }

        /*$oHeaderUpdate = oHeaders::find($request->id);
        $oHeaderUpdate->Status = 5;
        $oHeaderUpdate->ReceivedBy = Auth::user()->id;
        $oHeaderUpdate->save();*/

        $title = 'Confirmation';
        $message = 'Request updated!';

        $CheckCurrentLines = oLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('checkin_ID','=',$request->checkin_ID)->where('Deleted','=',False)->count();
        if($CheckCurrentLines==0){
            $oHeaderUpdate = oHeaders::find($request->id);
            $oHeaderUpdate->Status = 5;
            //$oHeaderUpdate->COApprovedBy = null;
            $oHeaderUpdate->ReceivedBy = Auth::user()->id;
            $oHeaderUpdate->ReceivedByName = $request->COReceivedByName;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $message = 'The request '.$oHeaderUpdate->RequestNo.' has been updated.';
        }

        //If all Line Items for the current request is Closed, The Request status will be set to Closed.
        $CheckOpenLines = oLineItems::where('h_ID','=',$request->id)->where('LineStatus','=',1)->where('Deleted','=',False)->count();
        if($CheckOpenLines==0){
            $oHeaderUpdate = oHeaders::find($request->id);
            $oHeaderUpdate->Status = 9;
            $oHeaderUpdate->COReceivedBy = Auth::user()->id;
            $oHeaderUpdate->COReceivedByName = $request->COReceivedByName;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $message = 'The request '.$oHeaderUpdate->RequestNo.' has been closed.';
        }
        //END

        $buttonLink = "out_gate_ci_view";
        $buttonText = "Check-out Requests";
        return view('transactions_gate.in_m_gate',compact('title','message','restriction','buttonLink','buttonText'));

    }

    public function emailReject(Request $request){
        $iHeaderUpdate = iHeaders::find($request->requestID);
        if(isset($request->RejectReason)){
            if ($iHeaderUpdate->Status == 2) {
                $iHeaderUpdate->Status = 10;
                $iHeaderUpdate->RejectedBy = $request->approverID;
                $iHeaderUpdate->RejectReason = $request->RejectReason;
                $iHeaderUpdate->save();

                $log = new iStatusLogs();
                $log->iHeaders_id = $iHeaderUpdate->id;
                $log->RequestNo = $iHeaderUpdate->RequestNo;
                $log->Users_id = $request->approverID;
                $log->Rejected = 1;
                $log->RejectReason = $request->RejectReason;
                $log->Status = $iHeaderUpdate->Status;
                $log->save();

                $title = 'Confirmation';

                $message = 'Request ' . $iHeaderUpdate->RequestNo . ' has been rejected.';
                $state = "rejected";

                //Mail confirmation to Approver/s - START
                $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                    ->distinct()
                    ->select('Email')
                    ->get();
                $rejector = User::where('id','=',$request->approverID)->first();
                $singleRequest = iHeaders::where('RequestNo', '=', $iHeaderUpdate->RequestNo)->first();
                $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach($getApproverEmails as $email)
                {
                    $user_email = $email['Email'];
                    if ($user_email != null)
                    {
                        Mail::send('email.in_approver_message',
                            [
                                'recepient'=>'approver',
                                'singleRequest'=>$singleRequest,
                                'lineItems'=>$lineItems,
                                'email' => $email['Email'],
                                'FirstName'=>$email['FirstName'],
                                'approverID'=>$email['id'],
                                'rejectReason' => $request->RejectReason,
                                'rejector' => $rejector->FirstName." ".$rejector->LastName,
                                'state' => "rejected",
                                'requestID'=>$iHeaderUpdate->RequestNo
                            ],
                            function ($message) use ($email,$singleRequest) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("Request ".$singleRequest->RequestNo." has been rejected");
                                $message->to($email['Email']);
                            });
                    }
                }
                //Mail confirmation to Approver/s - END

            }elseif ($iHeaderUpdate->Status == 6) {
                $iHeaderUpdate->Status = 7;
                $iHeaderUpdate->COApprovedBy = null;
                $iHeaderUpdate->CORejectedBy = $request->approverID;
                $iHeaderUpdate->CORejectReason = $request->RejectReason;
                $iHeaderUpdate->save();

                $log = new iStatusLogs();
                $log->iHeaders_id = $iHeaderUpdate->id;
                $log->RequestNo = $iHeaderUpdate->RequestNo;
                $log->Users_id = $request->approverID;
                $log->Rejected = 1;
                $log->RejectReason = $request->RejectReason;
                $lineItemsCO = iLineItems::where('h_ID','=',$request->requestID)->orderBy('checkout_ID','DESC')->first();
                $log->checkout_ID = $lineItemsCO->checkout_ID;
                $log->Status = $iHeaderUpdate->Status;
                $log->save();

                $title = 'Confirmation';

                $message = 'Request ' . $iHeaderUpdate->RequestNo . ' has been rejected.';
                $state = "rejected";

                //Mail confirmation to Approver/s - START
                $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                    ->distinct()
                    ->select('Email')
                    ->get();
                $rejector = User::where('id','=',$request->approverID)->first();

                $singleRequest = iHeaders::where('id', '=', $iHeaderUpdate->id)->first();
                $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
                $checkOutID = $lineItemsCO->checkout_ID;
                $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();
                //dd($singleRequest);
                //$lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach($getApproverEmails as $email)
                {
                    $user_email = $email['Email'];
                    if ($user_email != null)
                    {
                        Mail::send('email.in_approver_message',
                            [
                                'recepient'=>'approver',
                                'singleRequest'=>$singleRequest,
                                'lineItems'=>$lineItems,
                                'email' => $email['Email'],
                                'FirstName'=>$email['FirstName'],
                                'approverID'=>$email['id'],
                                'rejectReason' => $request->RejectReason,
                                'rejector' => $rejector->FirstName." ".$rejector->LastName,
                                'state' => "rejected for check-out",
                                'requestID'=>$iHeaderUpdate->RequestNo
                            ],
                            function ($message) use ($email,$singleRequest) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("Request ".$singleRequest->RequestNo." has been rejected for check-out");
                                $message->to($email['Email']);
                            });
                    }
                }

                $latestCheckOutID = iLineItems::where('h_ID','=',$iHeaderUpdate->id)->max('checkout_ID');
                $revertItems = iLineItems::where('h_ID','=',$iHeaderUpdate->id)->where('checkout_ID','=',$latestCheckOutID)->get();
                foreach($revertItems as $ri){
                    $revert = iLineItems::where('id','=',$ri->id)->first();
                    $revert->checkout_ID = 0;
                    $revert->CheckOut = 0;
                    $revert->save();
                }

                //Mail confirmation to Approver/s - END

            } else {
                $title = 'Unable to approve request';
                $message = 'Request ' . $iHeaderUpdate->RequestNo . ' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
            }
            return view('transactions.in_m_general', compact('title', 'message'));
        }
        else {
            $approver = $request->approverID;
            if ($iHeaderUpdate->Status == 2 || $iHeaderUpdate->Status == 6) {
                $title = $iHeaderUpdate->RequestNo;
                $message = '';
                return view('transactions.in_m_reject', compact('title', 'message', 'iHeaderUpdate', 'approver'));
            }else {
                $title = 'Unable to approve request';
                $message = 'Request ' . $iHeaderUpdate->RequestNo . ' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
                return view('transactions.in_m_general', compact('title', 'message'));
            }
        }
    }
    public function emailApprove(Request $request){
        $iHeaderUpdate = iHeaders::find($request->requestID);
        //Check again if Request is still in pending
            if ($iHeaderUpdate->Status == 2) {
                $iHeaderUpdate->Status = 4;
                $iHeaderUpdate->ApprovedBy = $request->approverID;
                $iHeaderUpdate->save();

                $log = new iStatusLogs();
                $log->iHeaders_id = $iHeaderUpdate->id;
                $log->RequestNo = $iHeaderUpdate->RequestNo;
                $log->Users_id = $request->approverID;
                $log->Status = $iHeaderUpdate->Status;
                $log->save();

                $title = 'Confirmation';

                $message = 'Request ' . $iHeaderUpdate->RequestNo . ' has been approved.';
                $state = "approved";

                //Mail confirmation to Approver/s - START
                $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                    ->distinct()
                    ->select('Email')
                    ->get();
                $approver = User::where('id','=',$request->approverID)->first();
                $singleRequest = iHeaders::where('RequestNo', '=', $iHeaderUpdate->RequestNo)->first();
                $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                PdfFileController::attachment_contact($singleRequest->id);
                foreach($getApproverEmails as $email)
                {
                    $user_email = $email['Email'];
                    if ($user_email != null)
                    {
                        Mail::send('email.in_approver_message',
                            [
                                'recepient'=>'approver',
                                'singleRequest'=>$singleRequest,
                                'lineItems'=>$lineItems,
                                'email' => $email['Email'],
                                'FirstName'=>$email['FirstName'],
                                'approverID'=>$email['id'],
                                'approver' => $approver->FirstName." ".$approver->LastName,
                                'state' => "approved",
                                'requestID'=>$iHeaderUpdate->RequestNo
                            ],
                            function ($message) use ($email,$singleRequest) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("Request ".$singleRequest->RequestNo." has been approved");
                                $message->to($email['Email']);
                            });
                    }
                }
                if($singleRequest->ContactEmail!="") {
                    Mail::send('email.contactMessage',
                        [
                            'recepient' => 'approver',
                            'singleRequest' => $singleRequest,
                            'lineItems' => $lineItems,
                            'email' => $singleRequest->ContactEmail,
                            'FirstName' => $singleRequest->Contact,
                            'approverID' => $email['id'],
                            'state' => "approved",
                            'requestID' => $iHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                            $message->attach(storage_path("../storage/pdf_attachments/".$singleRequest->RequestNo.".pdf"));
                            $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                            $message->to($singleRequest->ContactEmail);
                        });
                }
                //Mail confirmation to Approver/s - END

            } else {
                $title = 'Unable to approve request';
                $message = 'Request ' . $iHeaderUpdate->RequestNo . ' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
            }
            return view('transactions.in_m_general', compact('title', 'message'));
    }
    public function emailApproveCheckOut(Request $request){
        $iHeaderUpdate = iHeaders::find($request->requestID);
        //Check again if Request is still in pending
        if ($iHeaderUpdate->Status == 6) {
            $iHeaderUpdate->Status = 8;
            $iHeaderUpdate->COApprovedBy = $request->approverID;
            $iHeaderUpdate->save();

            $log = new iStatusLogs();
            $log->iHeaders_id = $iHeaderUpdate->id;
            $log->RequestNo = $iHeaderUpdate->RequestNo;
            $log->Users_id = $request->approverID;
            $log->Status = $iHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';
            $approver = User::where('id','=',$request->approverID)->first();
            $message = 'Request ' . $iHeaderUpdate->RequestNo . ' has been approved for Check-out.';
            $state = "checkout";

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$iHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();

            $singleRequest = iHeaders::where('RequestNo', '=', $iHeaderUpdate->RequestNo)->first();
            $lineItemsCO = iLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkout_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkout_ID;
            $lineItems = iLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkout_ID','=',$checkOutID)->get();

            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.in_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => "approved for check-out",
                            'requestID'=>$iHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been approved for check-out");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END

        } else {
            $title = 'Unable to approve request';
            $message = 'Request ' . $iHeaderUpdate->RequestNo . ' is already in '.$iHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions.in_m_general', compact('title', 'message'));
    }

    public function emailOutApprove(Request $request){
        $oHeaderUpdate = oHeaders::find($request->requestID);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 2) {
            //NOTE: 11/17/2017 - Change Status to 3 if Request Type is Fixed Asset.
            //KEYWORD: Accounting Approval
            if($oHeaderUpdate->RequestType == 2) {
                $oHeaderUpdate->Status = 3;
                $reqState = "forwarded to Accounting for approval";
            }
            else {
                $oHeaderUpdate->Status = 4;
                $reqState = "approved";
            }
            $oHeaderUpdate->COApprovedBy = $request->approverID;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = $request->approverID;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';

            if($oHeaderUpdate->RequestType == 2)
                $message = 'Request '.$oHeaderUpdate->RequestNo.' has been forwarded to Accounting for approval.';
            else
                $message = 'Request '.$oHeaderUpdate->RequestNo.' has been approved.';
            //$message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been approved.';
            $state = "approved";

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $approver = User::where('id','=',$request->approverID)->first();
            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();

            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => $reqState,
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest,$reqState) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Outgoing Request ".$singleRequest->RequestNo." has been ".$reqState);
                            $message->to($email['Email']);
                        });
                }
            }
            //SEND EMAIL TO ACCOUNTING
            if($oHeaderUpdate->RequestType == 2){
                //Mail confirmation to Accounting - START
                //$getAT = ApprovalTemplateRequestors::where('User_id', '=', Auth::user()->id)->where('Active','=',1)->first();
                //$getAR = ApprovalRoles::where('AT_id', '=', $getAT->AT_id)->first();
                //$getApprovers = ApprovalStageApprovers::where('AS_id', '=', $getAR->AS_id)->where('Email','=',1)->get();
                $getApprovers = Userrolesgroup::where('UserRoles_ID','=',7)->where('Active','=',1)->get();

                $aapprovers = array();

                foreach ($getApprovers as $gA)
                    array_push($aapprovers, $gA->Users_ID);
                $getApproverEmails = User::whereIn('id', $aapprovers)
                    ->distinct()
                    ->get();



                $singleRequest = oHeaders::where('id', '=', $request->requestID)->first();
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach ($getApproverEmails as $email) {
                    $user_email = $email['Email'];
                    if ($user_email != null) {
                        Mail::send('email.out_requestor_message',
                            [
                                'recepient' => 'accounting',
                                'singleRequest' => $singleRequest,
                                'lineItems' => $lineItems,
                                'email' => $email['Email'],
                                'FirstName' => $email['FirstName'],
                                'approverID' => $email['id'],
                                'state' => 'approval'
                            ],
                            function ($message) use ($email) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("New Outgoing Request for Approval");
                                $message->to($email['Email']);
                            });
                    }
                }
                //Mail confirmation to Accounting - END
            }
            //SEND EMAIL TO ACCOUNTING - END
            else {
                if ($singleRequest->ContactEmail != "") {
                    Mail::send('email.contactOutMessage',
                        [
                            'recepient' => 'approver',
                            'singleRequest' => $singleRequest,
                            'lineItems' => $lineItems,
                            'email' => $singleRequest->ContactEmail,
                            'FirstName' => $singleRequest->Contact,
                            'approverID' => "",
                            'state' => "approved",
                            'requestID' => $oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                            $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                            $message->to($singleRequest->ContactEmail);
                        });
                }
            }

            /*if($singleRequest->ContactEmail!="") {
                Mail::send('email.contactMessage',
                    [
                        'recepient' => 'approver',
                        'singleRequest' => $singleRequest,
                        'lineItems' => $lineItems,
                        'email' => $singleRequest->ContactEmail,
                        'FirstName' => $singleRequest->Contact,
                        'approverID' => "",
                        'state' => "approved",
                        'requestID' => $iHeaderUpdate->RequestNo
                    ],
                    function ($message) use ($singleRequest) {
                        $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                        $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                        $message->to($singleRequest->ContactEmail);
                    });
            }*/
            //Mail confirmation to Approver/s - END

        } else {
            $title = 'Unable to approve request';
            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions.in_m_general', compact('title', 'message'));
    }
    public function emailOutReject(Request $request){
        $oHeaderUpdate = oHeaders::find($request->requestID);
        if(isset($request->RejectReason)){
            if ($oHeaderUpdate->Status == 2) {
                $oHeaderUpdate->Status = 10;
                $oHeaderUpdate->CORejectedBy = $request->approverID;
                $oHeaderUpdate->CORejectReason = $request->RejectReason;
                $oHeaderUpdate->save();

                $log = new oStatusLogs();
                $log->oHeaders_id = $oHeaderUpdate->id;
                $log->RequestNo = $oHeaderUpdate->RequestNo;
                $log->Users_id = $request->approverID;
                $log->Rejected = 1;
                $log->RejectReason = $request->RejectReason;
                $log->Status = $oHeaderUpdate->Status;
                $log->save();

                $title = 'Confirmation';

                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been rejected.';
                $state = "rejected";

                //Mail confirmation to Approver/s - START
                $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                    ->distinct()
                    ->select('Email')
                    ->get();
                $rejector = User::where('id','=',$request->approverID)->first();
                $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach($getApproverEmails as $email)
                {
                    $user_email = $email['Email'];
                    if ($user_email != null)
                    {
                        Mail::send('email.out_approver_message',
                            [
                                'recepient'=>'approver',
                                'singleRequest'=>$singleRequest,
                                'lineItems'=>$lineItems,
                                'email' => $email['Email'],
                                'FirstName'=>$email['FirstName'],
                                'approverID'=>$email['id'],
                                'rejectReason' => $request->RejectReason,
                                'rejector' => $rejector->FirstName." ".$rejector->LastName,
                                'state' => "rejected",
                                'requestID'=>$oHeaderUpdate->RequestNo
                            ],
                            function ($message) use ($email,$singleRequest) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("Request ".$singleRequest->RequestNo." has been rejected");
                                $message->to($email['Email']);
                            });
                    }
                }
                //Mail confirmation to Approver/s - END

            }
            elseif ($oHeaderUpdate->Status == 6) {
                $oHeaderUpdate->Status = 5;
                $oHeaderUpdate->ApprovedBy = null;
                $oHeaderUpdate->RejectedBy = $request->approverID;
                $oHeaderUpdate->RejectReason = $request->RejectReason;
                $oHeaderUpdate->save();

                $log = new oStatusLogs();
                $log->oHeaders_id = $oHeaderUpdate->id;
                $log->RequestNo = $oHeaderUpdate->RequestNo;
                $log->Users_id = $request->approverID;
                $log->Status = $oHeaderUpdate->Status;
                $lineItemsCO = oLineItems::where('h_ID','=',$request->requestID)->orderBy('checkin_ID','DESC')->first();
                $log->checkin_ID = $lineItemsCO->checkin_ID;
                $log->Rejected = 1;
                $log->RejectReason = $request->RejectReason;
                $log->save();

                $title = 'Confirmation';

                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been rejected.';
                $state = "rejected";

                //Mail confirmation to Approver/s - START
                $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                    ->distinct()
                    ->select('Email')
                    ->get();
                $rejector = User::where('id','=',$request->approverID)->first();

                $singleRequest = oHeaders::where('id', '=', $oHeaderUpdate->id)->first();
                $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
                $checkOutID = $lineItemsCO->checkin_ID;
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
                //dd($singleRequest);
                //$lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach($getApproverEmails as $email)
                {
                    $user_email = $email['Email'];
                    if ($user_email != null)
                    {
                        Mail::send('email.out_approver_message',
                            [
                                'recepient'=>'approver',
                                'singleRequest'=>$singleRequest,
                                'lineItems'=>$lineItems,
                                'email' => $email['Email'],
                                'FirstName'=>$email['FirstName'],
                                'approverID'=>$email['id'],
                                'rejectReason' => $request->RejectReason,
                                'rejector' => $rejector->FirstName." ".$rejector->LastName,
                                'state' => "rejected for check-in",
                                'requestID'=>$oHeaderUpdate->RequestNo
                            ],
                            function ($message) use ($email,$singleRequest) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("Request ".$singleRequest->RequestNo." has been rejected for check-in");
                                $message->to($email['Email']);
                            });
                    }
                }

                $latestCheckOutID = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->max('checkin_ID');
                $revertItems = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->where('checkin_ID','=',$latestCheckOutID)->get();
                foreach($revertItems as $ri){
                    $revert = oLineItems::where('id','=',$ri->id)->first();
                    $revert->checkin_ID = 0;
                    $revert->CheckIn = 0;
                    $revert->save();
                }

                //Mail confirmation to Approver/s - END

            }
            else {
                $title = 'Unable to approve request';
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
            }
            return view('transactions.in_m_general', compact('title', 'message'));
        }
        else {
            $approver = $request->approverID;
            if ($oHeaderUpdate->Status == 2 || $oHeaderUpdate->Status == 6) {
                $title = $oHeaderUpdate->RequestNo;
                $message = '';
                return view('transactions.out_m_reject', compact('title', 'message', 'oHeaderUpdate', 'approver'));
            }else {
                $title = 'Unable to approve request';
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
                return view('transactions.in_m_general', compact('title', 'message'));
            }
        }
    }

    public function emailOutAcctgApprove(Request $request){
        $oHeaderUpdate = oHeaders::find($request->requestID);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 3) {
            //NOTE: 11/17/2017 - Change Status to 3 if Request Type is Fixed Asset.
            //KEYWORD: Accounting Approval
            /*if($oHeaderUpdate->RequestType == 2) {
                $oHeaderUpdate->Status = 3;
                $reqState = "forwarded to Accounting for approval";
            }
            else {*/
                $oHeaderUpdate->Status = 4;
                $reqState = "approved";
            //}
            $oHeaderUpdate->COAccountingApprovedBy = $request->accountingID;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = $request->accountingID;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';

            /*if($oHeaderUpdate->RequestType == 2)
                $message = 'Request '.$oHeaderUpdate->RequestNo.' has been forwarded to Accounting for approval.';
            else*/
                $message = 'Request '.$oHeaderUpdate->RequestNo.' has been approved.';
            //$message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been approved.';
            $state = "approved";

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $approver = User::where('id','=',$request->accountingID)->first();
            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();

            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => $reqState,
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest,$reqState) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Outgoing Request ".$singleRequest->RequestNo." has been ".$reqState);
                            $message->to($email['Email']);
                        });
                }
            }
            /*if($singleRequest->ContactEmail!="") {
                Mail::send('email.contactMessage',
                    [
                        'recepient' => 'approver',
                        'singleRequest' => $singleRequest,
                        'lineItems' => $lineItems,
                        'email' => $singleRequest->ContactEmail,
                        'FirstName' => $singleRequest->Contact,
                        'approverID' => "",
                        'state' => "approved",
                        'requestID' => $iHeaderUpdate->RequestNo
                    ],
                    function ($message) use ($singleRequest) {
                        $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                        $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                        $message->to($singleRequest->ContactEmail);
                    });
            }*/
            //Mail confirmation to Approver/s - END

        } else {
            $title = 'Unable to approve request';
            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions.in_m_general', compact('title', 'message'));
    }
    public function emailOutAcctgReject(Request $request){
        $oHeaderUpdate = oHeaders::find($request->requestID);
        if(isset($request->RejectReason)){
            if ($oHeaderUpdate->Status == 3) {
                $oHeaderUpdate->Status = 10;
                $oHeaderUpdate->CORejectedBy = $request->accountingID;
                $oHeaderUpdate->CORejectReason = $request->RejectReason;
                $oHeaderUpdate->save();

                $log = new oStatusLogs();
                $log->oHeaders_id = $oHeaderUpdate->id;
                $log->RequestNo = $oHeaderUpdate->RequestNo;
                $log->Users_id = $request->accountingID;
                $log->Status = $oHeaderUpdate->Status;
                $log->Rejected = 1;
                $log->RejectReason = $request->RejectReason;
                $log->save();

                $title = 'Confirmation';

                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been rejected.';
                $state = "rejected";

                //Mail confirmation to Approver/s - START
                $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                    ->distinct()
                    ->select('Email')
                    ->get();
                $rejector = User::where('id','=',$request->accountingID)->first();
                $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach($getApproverEmails as $email)
                {
                    $user_email = $email['Email'];
                    if ($user_email != null)
                    {
                        Mail::send('email.out_approver_message',
                            [
                                'recepient'=>'approver',
                                'singleRequest'=>$singleRequest,
                                'lineItems'=>$lineItems,
                                'email' => $email['Email'],
                                'FirstName'=>$email['FirstName'],
                                'approverID'=>$email['id'],
                                'rejectReason' => $request->RejectReason,
                                'rejector' => $rejector->FirstName." ".$rejector->LastName,
                                'state' => "rejected",
                                'requestID'=>$oHeaderUpdate->RequestNo
                            ],
                            function ($message) use ($email,$singleRequest) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("Request ".$singleRequest->RequestNo." has been rejected");
                                $message->to($email['Email']);
                            });
                    }
                }
                //Mail confirmation to Approver/s - END

            }
            elseif ($oHeaderUpdate->Status == 7) {
                $oHeaderUpdate->Status = 5;
                $oHeaderUpdate->ApprovedBy = null;
                $oHeaderUpdate->RejectedBy = $request->accountingID;
                $oHeaderUpdate->RejectReason = $request->RejectReason;
                $oHeaderUpdate->save();

                $log = new oStatusLogs();
                $log->oHeaders_id = $oHeaderUpdate->id;
                $log->RequestNo = $oHeaderUpdate->RequestNo;
                $log->Users_id = $request->accountingID;
                $log->Status = $oHeaderUpdate->Status;
                $lineItemsCO = oLineItems::where('h_ID','=',$request->requestID)->orderBy('checkin_ID','DESC')->first();
                $log->checkin_ID = $lineItemsCO->checkin_ID;
                $log->Rejected = 1;
                $log->RejectReason = $request->RejectReason;
                $log->save();

                $title = 'Confirmation';

                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been rejected.';
                $state = "rejected";

                //Mail confirmation to Approver/s - START
                $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                    ->distinct()
                    ->select('Email')
                    ->get();
                $rejector = User::where('id','=',$request->accountingID)->first();

                $singleRequest = oHeaders::where('id', '=', $oHeaderUpdate->id)->first();
                $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
                $checkOutID = $lineItemsCO->checkin_ID;
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
                //dd($singleRequest);
                //$lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach($getApproverEmails as $email)
                {
                    $user_email = $email['Email'];
                    if ($user_email != null)
                    {
                        Mail::send('email.out_approver_message',
                            [
                                'recepient'=>'approver',
                                'singleRequest'=>$singleRequest,
                                'lineItems'=>$lineItems,
                                'email' => $email['Email'],
                                'FirstName'=>$email['FirstName'],
                                'approverID'=>$email['id'],
                                'rejectReason' => $request->RejectReason,
                                'rejector' => $rejector->FirstName." ".$rejector->LastName,
                                'state' => "rejected for check-in",
                                'requestID'=>$oHeaderUpdate->RequestNo
                            ],
                            function ($message) use ($email,$singleRequest) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("Request ".$singleRequest->RequestNo." has been rejected for check-in");
                                $message->to($email['Email']);
                            });
                    }
                }

                $latestCheckOutID = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->max('checkin_ID');
                $revertItems = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->where('checkin_ID','=',$latestCheckOutID)->get();
                foreach($revertItems as $ri){
                    $revert = oLineItems::where('id','=',$ri->id)->first();
                    $revert->checkin_ID = 0;
                    $revert->CheckIn = 0;
                    $revert->save();
                }

                //Mail confirmation to Approver/s - END

            }
            else {
                $title = 'Unable to approve request';
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
            }
            return view('transactions.in_m_general', compact('title', 'message'));
        }
        else {
            $accounting = $request->accountingID;
            if ($oHeaderUpdate->Status == 3 || $oHeaderUpdate->Status == 7) {
                $title = $oHeaderUpdate->RequestNo;
                $message = '';
                return view('transactions.out_m_reject', compact('title', 'message', 'oHeaderUpdate', 'accounting'));
            }else {
                $title = 'Unable to approve request';
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
                return view('transactions.in_m_general', compact('title', 'message'));
            }
        }
    }

    public function emailOutApproveCheckIn(Request $request){
        $oHeaderUpdate = oHeaders::find($request->requestID);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 6) {

            /*if($oHeaderUpdate->RequestType == 2)
                $oHeaderUpdate->Status = 7;
            else
                $oHeaderUpdate->Status = 8;*/

            if($oHeaderUpdate->RequestType == 2) {
                $oHeaderUpdate->Status = 7;
                $reqState = "forwarded to Accounting for approval";
            }
            else {
                $oHeaderUpdate->Status = 8;
                $reqState = "approved for check-in";
            }

            $oHeaderUpdate->ApprovedBy = $request->approverID;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = $request->approverID;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';
            $approver = User::where('id','=',$request->approverID)->first();

            if($oHeaderUpdate->RequestType == 2)
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been forwarded to Accounting for approval.';
            else
                $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been approved for Check-in.';
            $state = "checkout";

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();

            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();

            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => $reqState,
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest,$reqState) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been ".$reqState);
                            $message->to($email['Email']);
                        });
                }
            }
            //SEND EMAIL TO ACCOUNTING
            if($oHeaderUpdate->RequestType == 2){
                //Mail confirmation to Accounting - START
                //$getAT = ApprovalTemplateRequestors::where('User_id', '=', Auth::user()->id)->where('Active','=',1)->first();
                //$getAR = ApprovalRoles::where('AT_id', '=', $getAT->AT_id)->first();
                //$getApprovers = ApprovalStageApprovers::where('AS_id', '=', $getAR->AS_id)->where('Email','=',1)->get();
                $getApprovers = Userrolesgroup::where('UserRoles_ID','=',7)->where('Active','=',1)->get();

                $aapprovers = array();

                foreach ($getApprovers as $gA)
                    array_push($aapprovers, $gA->Users_ID);
                $getApproverEmails = User::whereIn('id', $aapprovers)
                    ->distinct()
                    ->get();



                $singleRequest = oHeaders::where('id', '=', $request->requestID)->first();
                $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
                foreach ($getApproverEmails as $email) {
                    $user_email = $email['Email'];
                    if ($user_email != null) {
                        Mail::send('email.out_requestor_message',
                            [
                                'recepient' => 'accounting',
                                'singleRequest' => $singleRequest,
                                'lineItems' => $lineItems,
                                'email' => $email['Email'],
                                'FirstName' => $email['FirstName'],
                                'approverID' => $email['id'],
                                'state' => 'checkout'
                            ],
                            function ($message) use ($email) {
                                $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                                $message->subject("New Outgoing Request for Approval");
                                $message->to($email['Email']);
                            });
                    }
                }
                //Mail confirmation to Accounting - END
            }
            //SEND EMAIL TO ACCOUNTING - END
            //Mail confirmation to Approver/s - END

        } else {
            $title = 'Unable to approve request';
            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions.in_m_general', compact('title', 'message'));
    }
    public function emailOutAcctgApproveCheckIn(Request $request){
        $oHeaderUpdate = oHeaders::find($request->requestID);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 7) {
                $oHeaderUpdate->Status = 8;
                $reqState = "approved for check-in";

            $oHeaderUpdate->AccountingApprovedBy = $request->accountingID;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = $request->accountingID;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();

            $title = 'Confirmation';
            $approver = User::where('id','=',$request->accountingID)->first();

            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been approved for Check-in.';
            $state = "checkout";

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();

            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();

            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => $reqState,
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest,$reqState) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been ".$reqState);
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END

        } else {
            $title = 'Unable to approve request';
            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions.in_m_general', compact('title', 'message'));
    }

    //Accounting Views
    public function outAccountingMyRequests($code){
        if(self::knowYourRole(7)) {
            //I DID NOT BASE THIS FROM THE APPROVER VIEW
            //ACCOUNTING APPROVERS ARE NOT INCLUDED IN APPROVAL TEMPLATES AT THE MOMENT.
            if($code == "co_for_approval")
                $myRequests = oHeaders::where('Status','=',3)->get();
            elseif($code == "co_approved")
                $myRequests = oHeaders::where('COAccountingApprovedBy','=',Auth::user()->id)->get();
            elseif($code == "co_rejected")
                $myRequests = oHeaders::where('CORejectedBy','=',Auth::user()->id)->whereNotNull('COApprovedBy')->get();
            elseif($code == "ci_for_approval")
                $myRequests = oHeaders::where('Status','=',7)->get();
            elseif($code == "ci_approved")
                $myRequests = oHeaders::where('AccountingApprovedBy','=',Auth::user()->id)->get();
            elseif($code == "ci_rejected")
                $myRequests = oHeaders::where('RejectedBy','=',Auth::user()->id)->whereNotNull('ApprovedBy')->get();
            return view('transactions_accounting.out_v_for_acctg_approval', compact('myRequests','code'));

        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }
    public function outAccountingMyRequestsSingle($code,$id){
        if(self::knowYourRole(7)) {
            //I DID NOT BASE THIS FROM THE APPROVER VIEW
            //ACCOUNTING APPROVERS ARE NOT INCLUDED IN APPROVAL TEMPLATES AT THE MOMENT.

            $singleRequest = oHeaders::where('id','=',$id)->first();
            //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
            $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

            if($code == "co_for_approval"){
                if($singleRequest->Status == 3)
                    return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
                else{
                    $title = 'Access Denied';
                    $message = 'You are not allowed to access this request.';
                    $restriction = 1;
                    return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
                }
            }
            elseif($code == "co_approved"){
                if($singleRequest->COAccountingApprovedBy == Auth::user()->id)
                    return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
                else{
                    $title = 'Access Denied';
                    $message = 'You are not allowed to access this request.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            elseif($code == "co_rejected"){
                if(isset($singleRequest->COApprovedBy) && $singleRequest->CORejectedBy == Auth::user()->id)
                    return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
                else{
                    $title = 'Access Denied';
                    $message = 'You are not allowed to access this request.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            elseif($code == "ci_for_approval"){
                if($singleRequest->Status == 7)
                    return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
                else{
                    $title = 'Access Denied';
                    $message = 'You are not allowed to access this request.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            elseif($code == "ci_approved"){
                if($singleRequest->AccountingApprovedBy == Auth::user()->id)
                    return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
                else{
                    $title = 'Access Denied';
                    $message = 'You are not allowed to access this request.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            elseif($code == "ci_rejected"){
                if(isset($singleRequest->ApprovedBy) && $singleRequest->RejectedBy == Auth::user()->id)
                    return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut','code'));
                else{
                    $title = 'Access Denied';
                    $message = 'You are not allowed to access this request.';
                    $restriction = 1;
                    return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
                }
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this page.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
        }
    }

    public function outAccountingApprove(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is still in For Accounting Approval
        if ($oHeaderUpdate->Status == 3) {
            $oHeaderUpdate->Status = 4;
            $oHeaderUpdate->COAccountingApprovedBy = Auth::user()->id;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' has been approved.';

            //Mail confirmation to Requestor - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email','FirstName')
                ->get();

            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            $approver = Auth::user();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => "approved",
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Outgoing Request ".$singleRequest->RequestNo." has been approved");
                            $message->to($email['Email']);
                        });
                }
            }
            /*if($singleRequest->ContactEmail!="") {
                Mail::send('email.contactMessage',
                    [
                        'recepient' => 'approver',
                        'singleRequest' => $singleRequest,
                        'lineItems' => $lineItems,
                        'email' => $singleRequest->ContactEmail,
                        'FirstName' => $singleRequest->Contact,
                        'approverID' => "",
                        'state' => "approved",
                        'requestID' => $iHeaderUpdate->RequestNo
                    ],
                    function ($message) use ($singleRequest) {
                        $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                        $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                        $message->to($singleRequest->ContactEmail);
                    });
            }*/
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to approve request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_accounting.out_m_acctg',compact('title','message'));
    }
    public function outAccountingReject(Request $request){

        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 3) {
            $oHeaderUpdate->Status = 10;
            $oHeaderUpdate->CORejectedBy = Auth::user()->id;
            $oHeaderUpdate->CORejectReason = $request->RejectReason;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->Rejected = 1;
            $log->RejectReason = $request->RejectReason;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' has been rejected.';

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $rejector = Auth::user();
            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'rejectReason' => $request->RejectReason,
                            'rejector' => $rejector->FirstName." ".$rejector->LastName,
                            'state' => "rejected",
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been rejected");
                            $message->to($email['Email']);
                        });
                }
            }
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to reject request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_accounting.out_m_acctg',compact('title','message'));
    }

    public function outAccountingCIApprove(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is still in For Accounting Approval
        if ($oHeaderUpdate->Status == 7) {
            $oHeaderUpdate->Status = 8;
            $oHeaderUpdate->AccountingApprovedBy = Auth::user()->id;
            $oHeaderUpdate->RejectedBy = null;
            $oHeaderUpdate->RejectReason = null;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $log->save();


            $title = 'Confirmation';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' has been approved for check-in.';

            //Mail confirmation to Requestor - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email','FirstName')
                ->get();

            $singleRequest = oHeaders::where('RequestNo', '=', $oHeaderUpdate->RequestNo)->first();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            //$lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            $approver = Auth::user();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'approver' => $approver->FirstName." ".$approver->LastName,
                            'state' => "approved for check-in",
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Outgoing Request ".$singleRequest->RequestNo." has been approved for check-in");
                            $message->to($email['Email']);
                        });
                }
            }
            /*if($singleRequest->ContactEmail!="") {
                Mail::send('email.contactMessage',
                    [
                        'recepient' => 'approver',
                        'singleRequest' => $singleRequest,
                        'lineItems' => $lineItems,
                        'email' => $singleRequest->ContactEmail,
                        'FirstName' => $singleRequest->Contact,
                        'approverID' => "",
                        'state' => "approved",
                        'requestID' => $iHeaderUpdate->RequestNo
                    ],
                    function ($message) use ($singleRequest) {
                        $message->from(env('MAIL_USERNAME'), "APO-UGEC - Vendor Asset Management");
                        $message->subject("Your Visit on " . date('M d, Y', strtotime($singleRequest->Date)));
                        $message->to($singleRequest->ContactEmail);
                    });
            }*/
            //Mail confirmation to Approver/s - END
        }
        else{
            $title = 'Unable to approve request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_accounting.out_m_acctg',compact('title','message'));
    }
    public function outAccountingCIReject(Request $request){
        $oHeaderUpdate = oHeaders::find($request->id);
        //Check again if Request is still in pending
        if ($oHeaderUpdate->Status == 7) {
            $oHeaderUpdate->Status = 5;
            $oHeaderUpdate->ApprovedBy = null;
            $oHeaderUpdate->RejectedBy = Auth::user()->id;
            $oHeaderUpdate->RejectReason = $request->RejectReason;
            $oHeaderUpdate->save();

            $log = new oStatusLogs();
            $log->oHeaders_id = $oHeaderUpdate->id;
            $log->RequestNo = $oHeaderUpdate->RequestNo;
            $log->Users_id = Auth::user()->id;
            $log->Status = $oHeaderUpdate->Status;
            $singleRequest = oHeaders::where('id', '=', $oHeaderUpdate->id)->first();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $log->checkin_ID = $lineItemsCO->checkin_ID;
            $log->Rejected = 1;
            $log->RejectReason = $request->RejectReason;
            $log->save();

            $title = 'Confirmation';

            $message = 'Request ' . $oHeaderUpdate->RequestNo . ' has been rejected.';
            $state = "rejected";

            //Mail confirmation to Approver/s - START
            $getApproverEmails = User::where('id','=',$oHeaderUpdate->CreatedBy)
                ->distinct()
                ->select('Email')
                ->get();
            $rejector = Auth::user()->id;

            $singleRequest = oHeaders::where('id', '=', $oHeaderUpdate->id)->first();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            //dd($singleRequest);
            //$lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->get();
            foreach($getApproverEmails as $email)
            {
                $user_email = $email['Email'];
                if ($user_email != null)
                {
                    Mail::send('email.out_approver_message',
                        [
                            'recepient'=>'approver',
                            'singleRequest'=>$singleRequest,
                            'lineItems'=>$lineItems,
                            'email' => $email['Email'],
                            'FirstName'=>$email['FirstName'],
                            'approverID'=>$email['id'],
                            'rejectReason' => $request->RejectReason,
                            'rejector' => Auth::user()->FirstName." ".Auth::user()->LastName,
                            'state' => "rejected for check-in",
                            'requestID'=>$oHeaderUpdate->RequestNo
                        ],
                        function ($message) use ($email,$singleRequest) {
                            $message->from(env('MAIL_USERNAME'), "Vendor Asset Management");
                            $message->subject("Request ".$singleRequest->RequestNo." has been rejected for check-in");
                            $message->to($email['Email']);
                        });
                }
            }

            $latestCheckOutID = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->max('checkin_ID');
            $revertItems = oLineItems::where('h_ID','=',$oHeaderUpdate->id)->where('checkin_ID','=',$latestCheckOutID)->get();
            foreach($revertItems as $ri){
                $revert = oLineItems::where('id','=',$ri->id)->first();
                $revert->checkin_ID = 0;
                $revert->CheckIn = 0;
                $revert->save();
            }

            //Mail confirmation to Approver/s - END

        }
        else{
            $title = 'Unable to reject request';
            $message = 'Request '.$oHeaderUpdate->RequestNo.' is already in '.$oHeaderUpdate->oStatus->Name.' status.';
        }
        return view('transactions_approver.out_m_approver',compact('title','message'));
    }

    //OLD VIEW CODES - DO NOT DELETE THIS YET
    /*    public function outAccountingCIView(Request $request){

            if(self::knowYourRole(7)) {
                //I DID NOT BASE THIS FROM THE APPROVER VIEW
                //ACCOUNTING APPROVERS ARE NOT INCLUDED IN APPROVAL TEMPLATES AT THE MOMENT.
                $myRequests = oHeaders::where('Status','=',7)->get();
                return view('transactions_accounting.out_v_for_acctg_approval', compact('myRequests'));
            }
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this page.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
    }
    public function outAccountingView(){
        if(self::knowYourRole(7)) {
            //I DID NOT BASE THIS FROM THE APPROVER VIEW
            //ACCOUNTING APPROVERS ARE NOT INCLUDED IN APPROVAL TEMPLATES AT THE MOMENT.
            $myRequests = oHeaders::where('Status','=',3)->get();
            return view('transactions_accounting.out_v_for_acctg_approval', compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }
    public function outAccountingMyApprovals(){
        if(self::knowYourRole(7)) {
            //I DID NOT BASE THIS FROM THE APPROVER VIEW
            //ACCOUNTING APPROVERS ARE NOT INCLUDED IN APPROVAL TEMPLATES AT THE MOMENT.
            $myRequests = oHeaders::where('COAccountingApprovedBy','=',Auth::user()->id)->get();
            return view('transactions_accounting.out_v_for_acctg_approval', compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }
    public function outAccountingMyRejections(){
        if(self::knowYourRole(7)) {
            //I DID NOT BASE THIS FROM THE APPROVER VIEW
            //ACCOUNTING APPROVERS ARE NOT INCLUDED IN APPROVAL TEMPLATES AT THE MOMENT.
            $myRequests = oHeaders::where('CORejectedBy','=',Auth::user()->id)->whereNotNull('COApprovedBy')->get();
            return view('transactions_accounting.out_v_for_acctg_approval', compact('myRequests'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
        }
    }*/
    //OLD VIEW CODES - DO NOT DELETE THIS YET - END

    //OLD SINGLE VIEW CODES - DO NOT DELETE THIS YET
    /*
        public function outAccountingViewSingle($id){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
            $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

            if($singleRequest->Status == 3)
                return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }
        public function outAccountingMyApprovalsSingle($id){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
            $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

            if($singleRequest->COAccountingApprovedBy == Auth::user()->id)
                return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        public function outAccountingMyRejectionsSingle($id){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
            $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

            if(isset($singleRequest->COApprovedBy) && $singleRequest->CORejectedBy == Auth::user()->id)
                return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.out_m_approver',compact('title','message','restriction'));
            }
        }
        public function outAccountingCIViewSingle($id){
            $singleRequest = oHeaders::where('id','=',$id)->first();
            //$lineItems = iLineItems::where('h_ID','=',$id)->where('Deleted','=',False)->get();
            $coVersion = oLineItems::where('h_ID', '=', $singleRequest->id)->select('checkin_ID')->where('Deleted', '=', False)->distinct()->get();
            $lineItemsCO = oLineItems::where('h_ID','=',$singleRequest->id)->orderBy('checkin_ID','DESC')->first();
            $checkOutID = $lineItemsCO->checkin_ID;
            $lineItems = oLineItems::where('h_ID', '=', $singleRequest->id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->get();
            $alreadyCheckedOut = oLineItems::where('h_ID', '=', $id)->where('Deleted', '=', False)->where('checkin_ID','=',$checkOutID)->where('OutDate','<>','')->count();

            if($singleRequest->Status == 7)
                return view('transactions_accounting.out_v_for_acctg_approval_single',compact('singleRequest','lineItems','coVersion','alreadyCheckedOut'));
            else{
                $title = 'Access Denied';
                $message = 'You are not allowed to access this request.';
                $restriction = 1;
                return view('transactions_approver.in_m_approver',compact('title','message','restriction'));
            }
        }*/
    //OLD SINGLE VIEW CODES - DO NOT DELETE THIS YET - END

    //Audit Views
    public function inAuditAll(){
        if(self::knowYourRole(6)) {
            $myCILineItems = iLineItems::whereNotNull('InDate')->where('Deleted','=',0)->whereNull('OutDate')->where('Returnable','=',1)->get();

            $myRequests = iHeaders::all();
            $status = iHeaderStatus::all();
            return view('transactions_audit.in_a_all', compact('myRequests','status','myCILineItems'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function outAuditAll(){
        if(self::knowYourRole(6)) {
            $myCILineItems = oLineItems::whereNotNull('OutDate')->where('Deleted','=',0)->whereNull('InDate')->where('Returnable','=',1)->get();

            $myRequests = oHeaders::all();
            $status = oHeaderStatus::all();
            return view('transactions_audit.in_a_all', compact('myRequests','status','myCILineItems'));
        }
        else{
            $title = 'Access Denied';
            $message = 'You are not allowed to access this page.';
            $restriction = 1;
            return view('transactions_requestor.in_m_requestor',compact('title','message','restriction'));
        }
    }
    public function doNotAccess(){
        $title = 'Error';
        $message = "You are not authorized to access this page.";
        return view('transactions.in_m_general',compact('title','message'));
    }
    public function saveImage()
    {
        $data=array('image_path'=>Photo::insertPhoto('image_path','../public/image_upload/','no image'));
        DB::table('tblimage')->insert($data);

    }

}