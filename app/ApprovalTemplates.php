<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalTemplates extends Model
{
    protected $table = 'ApprovalTemplates';

    public function toDivision()
    {
        return $this->belongsTo('App\Division', 'Division_ID');
    }
    public function toRequestors()
    {
        return $this->hasMany('App\ApprovalTemplateRequestors', 'AT_id');
    }
    public function allUsers()
    {
        return $this->hasMany('App\User', 'Division','Division_ID');
    }
}
