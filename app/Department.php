<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'Department';
    public function department_to_division()
    {
        return $this->belongsTo('App\Division', 'Division');
    }
}
