<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class iHeaders extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'iHeaders';


    public function oUser()
    {
        return $this->belongsTo('App\User', 'CreatedBy');
    }
    public function oContact()
    {
        return $this->belongsTo('App\Employees', 'Contact');
    }
    public function oBusinessPartner()
    {
        return $this->belongsTo('App\BusinessPartners', 'BusinessPartner');
    }
    public function oStatus()
    {
        return $this->belongsTo('App\iHeaderStatus', 'Status');
    }
    public function iheadCreatedby_to_user()
    {
        return $this->belongsTo('App\User', 'CreatedBy');
    }
    public function iheadApprovedBy_to_user()
    {
        return $this->belongsTo('App\User', 'ApprovedBy');
    }
    public function iheadCOApprovedBy_to_user()
    {
        return $this->belongsTo('App\User', 'COApprovedBy');
    }
    public function iheadCORejectedBy_to_user()
    {
        return $this->belongsTo('App\User', 'CORejectedBy');
    }
    public function iheadRejectedBy_to_user()
    {
        return $this->belongsTo('App\User', 'RejectedBy');
    }
    public function iheadSecurityControl_to_user()
    {
        return $this->belongsTo('App\User', 'SecurityControl');
    }
    public function iheadReceivedBy_to_user()
    {
        return $this->belongsTo('App\User', 'ReceivedBy');
    }
    public function iheadCOReceivedBy_to_user()
    {
        return $this->belongsTo('App\User', 'COReceivedBy');
    }
    public function checkOutID(){
        return $this->hasMany('App\iStatusLogs', 'iHeaders_id', 'id');
    }
    public function ihead_request_type()
    {
        return $this->belongsTo('App\iRequestType', 'RequestType');
    }
}
