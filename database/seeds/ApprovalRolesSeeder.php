<?php

use Illuminate\Database\Seeder;

class ApprovalRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'AT_id' => 1,
                'AS_id' => 1,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'

            ]
        ];
        DB::table('ApprovalRoles')->insert($records);
    }
}
