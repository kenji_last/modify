<?php

use Illuminate\Database\Seeder;

class oHeaderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'Name' => 'Saved',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Pending',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'For Accounting Approval',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Ready for Check-out',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Checked Out',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Pending for Check-in',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'For Accounting Approval',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Approved for Check-in',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Closed',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Rejected',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Name' => 'Canceled',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ]
        ];
        DB::table('oHeaderStatus')->insert($records);
    }
}