<?php

use Illuminate\Database\Seeder;

class SeriesSerialNoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'Year' => "17",
                'Month' => 9,
                'LastSeries' => 1,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ]
        ];
        DB::table('SeriesSerialNo')->insert($records);
    }
}
