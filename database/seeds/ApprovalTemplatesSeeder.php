<?php

use Illuminate\Database\Seeder;

class ApprovalTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'Name' => 'IT Requestors',
                'Division_ID' => 1,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ]
        ];
        DB::table('ApprovalTemplates')->insert($records);
    }
}
