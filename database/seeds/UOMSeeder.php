<?php

use Illuminate\Database\Seeder;

class UOMSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
                [
                    'Name' => 'PCS',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'BOXES',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'CASES',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'BOTTLES',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'CANS',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'SHEETS',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'METERS',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'MG',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'G',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'KG',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'L',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ],
                [
                    'Name' => 'ML',
                    'Active' => 1,
                    'created_at'     => '2017-08-15 10:23:00',
                    'updated_at'     => '2017-08-15 10:23:00'
                ]
            ];
        DB::table('UOMs')->insert($records);
    }
}
