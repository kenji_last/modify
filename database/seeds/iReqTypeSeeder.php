<?php

use Illuminate\Database\Seeder;

class iReqTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'Name' => 'Tools and Equipment',
                'Active' => 1,
                'created_at'     => '2017-10-25 10:23:00',
                'updated_at'     => '2017-10-25 10:23:00'
            ]
        ];
        DB::table('iRequestType')->insert($records);
    }
}
