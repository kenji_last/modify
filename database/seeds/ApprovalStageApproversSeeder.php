<?php

use Illuminate\Database\Seeder;

class ApprovalStageApproversSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'AS_id' => 1,
                'User_id' => 4,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'AS_id' => 1,
                'User_id' => 8,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ]
        ];
        DB::table('ApprovalStageApprovers')->insert($records);
    }
}