<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'EmpCode' => '10487',
                'FirstName' => 'Allan Jay',
                'MiddleName' => 'M',
                'LastName' => 'Becina',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'EmpCode' => '09399',
                'FirstName' => 'Mark Allain',
                'MiddleName' => 'C',
                'LastName' => 'Dionisio',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'EmpCode' => '11694',
                'FirstName' => 'Gerard Albert',
                'MiddleName' => 'R',
                'LastName' => 'Young',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'EmpCode' => '11160',
                'FirstName' => 'Mathew',
                'MiddleName' => 'G',
                'LastName' => 'Magante',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'EmpCode' => '12321',
                'FirstName' => 'Baby Jean',
                'MiddleName' => 'D',
                'LastName' => 'Greno',
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
        ];
        //DB::table('Employees')->insert($records);
    }
}
