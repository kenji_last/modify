<?php

use Illuminate\Database\Seeder;

class SeriesControlNoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'Request' => 'I',
                'Year' => "17",
                'LastSeries' => 1,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'

            ],
            [
                'Request' => 'O',
                'Year' => "17",
                'LastSeries' => 1,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'

            ]
        ];
        DB::table('SeriesControlNo')->insert($records);
    }
}
