<?php

use Illuminate\Database\Seeder;

class UserRolesGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'Users_ID' => 3,
                'UserRoles_ID' => 1,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 3,
                'UserRoles_ID' => 2,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 1,
                'UserRoles_ID' => 1,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 2,
                'UserRoles_ID' => 2,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 4,
                'UserRoles_ID' => 3,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 8,
                'UserRoles_ID' => 3,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 11,
                'UserRoles_ID' => 2,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 10,
                'UserRoles_ID' => 2,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 5,
                'UserRoles_ID' => 4,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 5,
                'UserRoles_ID' => 5,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 6,
                'UserRoles_ID' => 4,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 6,
                'UserRoles_ID' => 5,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 7,
                'UserRoles_ID' => 5,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 13,
                'UserRoles_ID' => 6,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ],
            [
                'Users_ID' => 14,
                'UserRoles_ID' => 7,
                'Active' => 1,
                'created_at'     => '2017-08-15 10:23:00',
                'updated_at'     => '2017-08-15 10:23:00'
            ]
        ];
        DB::table('UserRolesGroup')->insert($records);
    }
}
