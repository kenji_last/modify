<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ApprovalRolesSeeder::class);
        $this->call(ApprovalStagesSeeder::class);
        $this->call(ApprovalTemplatesSeeder::class);
        $this->call(ApprovalStageApproversSeeder::class);
        $this->call(ApprovalTemplateRequestorsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UOMSeeder::class);
        $this->call(BusinessPartnerSeeder::class);
        //$this->call(EmployeeSeeder::class);
        $this->call(LineStatusSeeder::class);
        $this->call(iHeaderStatusSeeder::class);
        $this->call(oHeaderStatusSeeder::class);
        $this->call(DivisionSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(UserRolesSeeder::class);
        $this->call(UserRolesGroupSeeder::class);
        $this->call(SeriesSerialNoSeeder::class);
        $this->call(SeriesControlNoSeeder::class);
        $this->call(iReqTypeSeeder::class);
        $this->call(oReqTypeSeeder::class);

        Model::reguard();
    }
}
