<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oHeaders', function (Blueprint $table) {
                /*$table->increments('id');
                $table->string('RequestNo');
                $table->integer('BusinessPartner');
                $table->string('BusinessPartnerName');
                $table->dateTime('Date');
                $table->string('Contact');
                $table->integer('CreatedBy');
                $table->integer('Status')->default(1);
                $table->timestamps();*/

            $table->increments('id');
            $table->string('RequestNo');
            $table->integer('RequestType')->nullable();
            $table->integer('BusinessPartner');
            $table->string('BusinessPartnerName');
            $table->string('Contact');
            $table->string('ContactEmail')->nullable();
            $table->string('Destination');
            $table->string('Reason');
            $table->string('Transporter');

            $table->dateTime('Date');
            $table->integer('CreatedBy');

            $table->integer('ApprovedBy')->nullable();
            $table->integer('RejectedBy')->nullable();
            $table->string('RejectReason')->nullable();
            $table->integer('SecurityControl')->nullable();
            $table->integer('ReceivedBy')->nullable();
            $table->string('ReceivedByName')->nullable();
            $table->integer('AccountingApprovedBy')->nullable();

            $table->integer('COApprovedBy')->nullable();
            $table->integer('CORejectedBy')->nullable();
            $table->string('CORejectReason')->nullable();
            $table->integer('COSecurityControl')->nullable();
            $table->integer('COReceivedBy')->nullable();
            $table->string('COReceivedByName')->nullable();
            $table->integer('COAccountingApprovedBy')->nullable();

            $table->integer('Status')->default(1);
            $table->boolean('BCodePrinted')->default(0);
            $table->boolean('Notify')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('oHeaders');
    }
}
