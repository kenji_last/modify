<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ApprovalRoles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('AT_id');
            $table->integer('AS_id');
            $table->boolean('Incoming')->default(1);
            $table->boolean('Outgoing')->default(1);
            $table->boolean('Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ApprovalRoles');
    }
}
