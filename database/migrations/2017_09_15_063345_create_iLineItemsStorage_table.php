<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateILineItemsStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iLineItemsStorage', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('l_ID');
            $table->integer('Storage');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('iLineItemsStorage');
    }
}
