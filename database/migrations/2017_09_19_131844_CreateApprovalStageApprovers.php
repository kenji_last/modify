<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalStageApprovers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ApprovalStageApprovers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('AS_id');
            $table->integer('User_id');
            $table->boolean('Email')->default(1);
            $table->boolean('Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ApprovalStageApprovers');
    }
}
