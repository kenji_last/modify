<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    public function up()
    {
        Schema::create('iStatusLogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iHeaders_id');
            $table->string('RequestNo');
            $table->integer('Users_id');
            $table->integer('Status');
            $table->string('RejectReason')->nullable();
            $table->integer('checkout_ID')->nullable();
            $table->boolean('Rejected')->default(0);
            $table->timestamps();
        });
        Schema::create('oStatusLogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('oHeaders_id');
            $table->string('RequestNo');
            $table->integer('Users_id');
            $table->integer('Status');
            $table->string('RejectReason')->nullable();
            $table->integer('checkin_ID')->nullable();
            $table->boolean('Rejected')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('iStatusLogs');
        Schema::drop('oStatusLogs');
    }
}
