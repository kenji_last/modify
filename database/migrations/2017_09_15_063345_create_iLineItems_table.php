<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateILineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iLineItems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('h_ID');
            $table->integer('LineNum');
            $table->integer('LineStatus');
            $table->string('Description');
            $table->integer('Destination');
            $table->string('SerialNum')->default("-")->nullable();;
            $table->string('ControlNum');
            $table->integer('Quantity');
            $table->integer('UOM');
            $table->boolean('Returnable')->default(0);
            $table->integer('ItemType')->default(0);
            $table->boolean('Deleted')->default(0);
            $table->integer('checkout_ID')->default(0);
            $table->boolean('CheckOut')->default(0);
            $table->dateTime('InDate')->nullable();
            //$table->string('InRemarks')->nullable();
            $table->dateTime('OutDate')->nullable();
            $table->string('ImageFormat')->nullable();
            $table->integer('ImageCount')->nullable();
            //$table->string('OutRemarks')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('iLineItems');
    }
}
