<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalTemplateRequestors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ApprovalTemplateRequestors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('AT_id');
            $table->integer('User_id');
            $table->boolean('Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ApprovalTemplateRequestors');
    }
}
