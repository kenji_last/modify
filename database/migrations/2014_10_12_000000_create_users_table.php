<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('FirstName');
            $table->string('MiddleName');
            $table->string('LastName');
            $table->string('username');
            $table->string('password', 60);
            $table->dateTime('PasswordUpdated')->default(DB::raw('CURRENT_TIMESTAMP'));;
            $table->integer('Division');
            $table->integer('Department');
            $table->string('Email')             ->nullable();
            $table->boolean('Active')           ->default(1);
            $table->boolean('FirstLogin')      ->default(1);
            $table->boolean('forgot_confirmation_code')     ->nullable();
            $table->boolean('forgotpassword')   ->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Users');
    }
}
