<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalStages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ApprovalStages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name')->default(NULL);
            $table->integer('Division_ID');
            /*$table->boolean('Incoming')->default(1);
            $table->boolean('Outgoing')->default(1);*/
            $table->boolean('Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ApprovalStages');
    }
}
