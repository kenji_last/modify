<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iHeaders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RequestNo');
            $table->integer('RequestType')->nullable();
            $table->integer('BusinessPartner');
            $table->string('BusinessPartnerName')->nullable();
            $table->string('Contact');
            $table->string('ContactEmail')->nullable();
            $table->string('Project')->nullable();
            $table->dateTime('Date');
            $table->integer('CreatedBy');
            $table->integer('ApprovedBy')->nullable();
            $table->integer('RejectedBy')->nullable();
            $table->string('RejectReason')->nullable();
            $table->integer('SecurityControl')->nullable();
            $table->integer('ReceivedBy')->nullable();
            $table->string('ReceivedByName')->nullable();
            $table->integer('COApprovedBy')->nullable();
            $table->integer('CORejectedBy')->nullable();
            $table->string('CORejectReason')->nullable();
            $table->integer('COSecurityControl')->nullable();
            $table->integer('COReceivedBy')->nullable();
            $table->string('COReceivedByName')->nullable();
            $table->integer('Status')->default(1);
            $table->boolean('BCodePrinted')->default(0);
            $table->boolean('Notify')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('iHeaders');
    }
}
