<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateILineItemImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iLineItemImages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('l_ID');
            $table->string('ControlNum');
            $table->integer('ImageNum');
            $table->string('ImageFormat');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('iLineItemImages');
    }
}
