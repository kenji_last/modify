<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oLineItems', function (Blueprint $table) {
            /*$table->increments('id');
            $table->integer('h_ID');
            $table->integer('LineNum');
            $table->integer('LineStatus');
            $table->string('Description');
            $table->string('SerialNum');
            $table->integer('UOM');
            $table->boolean('Returnable')->default(0);
            $table->date('InDate')->nullable();
            $table->string('InRemarks')->nullable();
            $table->date('OutDate')->nullable();
            $table->string('OutRemarks')->nullable();
            $table->timestamps();*/

            $table->increments('id');
            $table->integer('h_ID');
            $table->integer('LineNum');
            $table->integer('LineStatus');
            $table->string('Description');
            $table->string('Purpose');
            $table->string('SerialNum')->default("N/A");
            $table->string('ControlNum');
            $table->integer('Quantity');
            $table->integer('UOM');
            $table->boolean('Returnable')->default(0);
            $table->dateTime('ExpReturn')->nullable();
            $table->boolean('Deleted')->default(0);
            $table->integer('checkin_ID')->default(0);
            $table->boolean('CheckIn')->default(0);
            $table->dateTime('InDate')->nullable();
            $table->dateTime('OutDate')->nullable();
            $table->string('ImageFormat')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('oLineItems');
    }
}
