<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIReqTypes extends Migration
{
    public function up()
    {
        Schema::create('iRequestType', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name');
            $table->boolean('Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iRequestType');
    }
}
