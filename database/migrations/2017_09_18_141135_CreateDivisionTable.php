<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Division', function (Blueprint $table) {
            $table->increments('id');
            //$table->string('prefix')->default(NULL);
            $table->string('Name')->default(NULL);
            //$table->string('series_no')->default(NULL);
            $table->boolean('Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Division');
    }
}
