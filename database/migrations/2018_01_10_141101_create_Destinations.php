<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinations extends Migration
{
    public function up()
    {
        Schema::create('Destinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name');
            $table->boolean('SubLocation');
            $table->integer('Parent')->nullable();
            $table->boolean('Active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Destinations');
    }
}
